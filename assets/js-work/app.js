/*! INVASORIA KIT 2021 JS - IVÁN SORIA 2021.*/
(function() {
    APP = {
        settings: {
            ajax_enabled: true,
            from_home: true,
            scroll_flag: false,
            path: '',
            path_last: '',
            local_copy: false, //Is this on a local server (true) or a production/live server (false)? This will automatically change.
            debug: true, //Console messages
            tracking_code: {
                //google: 'G-H8FPWZNQJB', //Leave empty to deactivate
                google: '', //Leave empty to deactivate
                matomo: false, //Use false to deactivate
            },
            multilanguage: true, //For a Kirby multilanguage site, Note: an automatic language detection logic should be working soon
            current_language: 'es',
            site_title: document.getElementById('main').dataset.title,
            site_root: document.getElementById('main').dataset.root,
            site_language: document.getElementById('main').dataset.language,
            local_hostname: 'cis-mexico-website.test',
            local_path: '',
            local_pathlast: '',
            local_is_not_localhost: true,
            live_hostname: 'cismexico.com',
            live_path: '/',
            live_pathlast: '',
            prepros_port: '8848',
            prepros_path: '/',
            prepros_pathlast: '',
            local_levels: 3,
            live_levels: 3,
            prepros_levels: 0,
            uri_last: 'cover',
        },
        init: function() {
            console.log('Welcome to CIS México');
            UI.init();
            HELPERS.isThisLocal();
            if (as.tracking_code.google != '' && as.local_copy == false) {
                GOOGLEANALYTICS.init(as.tracking_code.google);
            }
            if (as.tracking_code.matomo == true && as.local_copy == false) {
                MATOMO.init();
            }
            if (as.ajax_enabled) {
                HISTORY_ENGINE.init();
            }
        }
    };
    window.APP = APP;
    window.as = APP.settings;
    window.onload = function() {
        app = new APP.init();
    };
}());
//@prepros-append modules/01.ui.js
//@prepros-append modules/02.history.engine.js
//@prepros-append modules/03.pages.js
//@prepros-append modules/04.helpers.js
//@prepros-append modules/99.google.analytics.js
//@prepros-append modules/99.matomo.analytics.js