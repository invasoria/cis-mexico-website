var UI = (function(ui) {
    ui = {
        settings: {
            main: document.getElementById('main'),
            container: document.getElementById('container'),
            fetch_container: document.getElementById('fetch-container'),
            cover_trigger: document.getElementById('cover-trigger'),
            cover_title: document.getElementById('cover-title'),
            cover_programs: document.getElementById('cover-programs'),
            //SCROLLING STUFF
            s: '',
            locoScroll: '',
            scroll_triggers: '',
            is_scrolling: false,
            scroll_y_position: '',
            scroll_container: '',
            scroll_thumb: '',
            section_uri: '',
            section_title: '',
            main_header: document.getElementById('main-header'),
            sticky_header: document.getElementById('sticky-header'),
            is_scrolling_with_action: false,
            nav_is_hidden_on_scroll: false,
            //HISTORY STUFF
            section_uri: '',
            section_title: '',
            section_description: '',
            nav_observer: '',
            //NAVIGATION
            main_nav: document.getElementById('main-nav'),
            main_nav_list: document.getElementById('main-nav-list'),
            main_fullscreen_nav: document.getElementById('nav-header'),
            footer_nav_list: document.getElementById('footer-nav-list'),
            time_progress: document.getElementById('time-progress'),
            menu_timeline: '',
            menu_is_open: false,
            scroll_b: '',
            //SERVICES
            carousel_trigger: document.getElementById('carousel-trigger'),
            carousel_container: document.getElementById('carousel-container'),
            carousel_button: document.getElementById('services-button'),
            flkty: '',
            services_data: [],
            service_to_remove: '',
            service_image__to_remove: '',
            category_split: '',
            title_split: '',
            new_titles_container: document.getElementById('js-new-titles'),
            new_image_container: document.getElementById('js-new-image'),
            carousel_intro_flag: false,
            carousel_is_open_flag: false,
            current_index: 1,
            previous_index: 0,
            direction: 'forward',
            services_timeline: '',
        },
        init: function() {
            UI.initLocomotiveScroll();
            UI.initLinks();
            UI.initInterface();
            //UI.initHoverListener();
            //UI.addResizeEvent();
            //UI.resizeHandler();
            UI.initCarousel();
        },
        /**
         *----------------------------------------------------------------------
         *
         * ::STICKY NAV
         *
         *----------------------------------------------------------------------
         */
        initInterface: function() {
            UI.settings.menu_timeline = gsap.timeline();
            let tl = UI.settings.menu_timeline;
            tl.set('#nav-header', {
                y: 0,
                autoAlpha: 1,
            });
            tl.set('.js-appear-on-menu-open', {
                y: '100',
                autoAlpha: 0,
            });
            tl.to('.js-nav-header-column', {
                duration: .75,
                ease: "Quint.easeInOut",
                //delay: .5,
                stagger: 0.1,
                y: '0%',
            });
            /*
            tl.to('.js-hide-on-menu-open', {
                duration: 1,
                y: '-100',
                ease: "Quint.easeInOut",
                autoAlpha: 0,
                stagger: 0.15,
            }, "<");
            //*/
            tl.to('.cis-logo__st0', {
                duration: .75,
                ease: "Quint.easeInOut",
                //fill: 'red',
                fill: '#F2F0F2',
            }, "<");
            tl.to('.cis__hamburger__fill', {
                duration: 1.5,
                ease: "Quint.easeInOut",
                fill: '#3A86FF',
            }, "<");
            tl.to('.js-appear-on-menu-open', {
                duration: .75,
                ease: "Quint.easeOut",
                autoAlpha: 1,
                y: 0,
                delay: .4,
                stagger: 0.1,
            }, "<");
            tl.to(('#menu-toggle-svg'), {
                duration: 1,
                ease: "Quint.easeInOut",
                rotation: 180,
                //delay: .5,
                //autoAlpha: 1,
            }, "<");
            tl.to(('#menu-toggle-bar-1'), {
                duration: 1,
                ease: "Quint.easeInOut",
                x: 4,
                y: 0,
                rotation: 45,
                //delay: .5,
                //autoAlpha: 1,
            }, .5);
            tl.to(('#menu-toggle-bar-2'), {
                duration: 1,
                ease: "Quint.easeInOut",
                x: 2,
                y: 8,
                rotation: -45,
                //delay: .5,
                //autoAlpha: 1,
            }, .5);
            tl.to(('#menu-toggle-bar-3'), {
                duration: 1,
                ease: "Quint.easeInOut",
                y: -6,
                //delay: .5,
                autoAlpha: 0,
            }, .5);
            tl.pause();
            //HELPERS.startHoverEffect(document.querySelectorAll('.js-hover-effect-ui-item'));
            HELPERS.initHoverAnimation(document.querySelectorAll('.js-hover-effect-ui-item'));
            HELPERS.initHoverAnimation(document.querySelectorAll('.prev-service'));
            HELPERS.initHoverAnimation(document.querySelectorAll('.next-service'), 'inverted');
            HELPERS.initCustomCursor();
        },
        openMenu: function() {
            UI.settings.menu_is_open = true;
            UI.closeCarousel();
            let tl = UI.settings.menu_timeline;
            tl.timeScale(1);
            tl.play();
            document.getElementById('menu-toggle').classList.remove('open-menu');
            document.getElementById('menu-toggle').classList.add('close-menu');
            UI.settings.main_nav_list.classList.add('cis__header__nav__list--hide');
            //document.getElementById('mobile-menu').classList.add('mobile-menu--open');
        },
        closeMenu: function() {
            let tl = UI.settings.menu_timeline;
            tl.timeScale(2.75);
            tl.reverse();
            document.getElementById('menu-toggle').classList.add('open-menu');
            document.getElementById('menu-toggle').classList.remove('close-menu');
            UI.settings.main_nav_list.classList.remove('cis__header__nav__list--hide');
            UI.settings.menu_is_open = false;
            //document.getElementById('mobile-menu').classList.remove('mobile-menu--open');
        },
        showMenu: function() {
            UI.settings.main_nav.classList.remove('cis__header--hidden');
            let new_section = document.getElementById('new-section');
            /*
            if (new_section.dataset.template !== 'servicios') {
                UI.settings.carousel_container.classList.remove('cis__services__carousel--section-active');
                UI.settings.carousel_trigger.classList.remove('cis__services__trigger--section-active');
                UI.settings.carousel_button.classList.remove('cis__services__button--section-active');
            }
            //*/
        },
        hideMenu: function() {
            UI.settings.main_nav.classList.add('cis__header--hidden');
            let new_section = document.getElementById('new-section');
            /*
            if (new_section.dataset.template && new_section.dataset.template !== 'servicios') {
                UI.settings.carousel_container.classList.add('cis__services__carousel--section-active');
                UI.settings.carousel_trigger.classList.add('cis__services__trigger--section-active');
                UI.settings.carousel_button.classList.add('cis__services__button--section-active');
            }
            //*/
        },
        /**
         *----------------------------------------------------------------------
         *
         * ::SCROLL SMOOTHNESS, PARALLAX AND LAZY LOADING
         *
         *----------------------------------------------------------------------
         */
        initLocomotiveScroll: function() {
            gsap.registerPlugin(ScrollTrigger);
            UI.settings.locoScroll = new LocomotiveScroll({
                el: document.querySelector('[data-scroll-container]'),
                smooth: true,
                smoothMobile: true,
                //lerp: 0.07,
                //multiplier: 1.5,
                lerp:.1,
                reloadOnContextChange: !1,
                //repeat: true,
                getDirection: true,
                getSpeed: true,
                tablet: {
                    smooth: true,
                    //direction: "vertical",
                    //gestureDirection: "vertical",
                    breakpoint: 1024
                },
                smartphone: {
                    smooth: true,
                    //direction: "vertical",
                    //gestureDirection: "vertical"
                }
            });
            /**
             *----------------------------------------------------------------------
             *
             * ::SCROLLTRIGGER DATA
             *
             *----------------------------------------------------------------------
             */
            UI.settings.locoScroll.on("scroll", ScrollTrigger.update);
            ScrollTrigger.scrollerProxy(document.getElementById('scroll-container'), {
                scrollTop(value) {
                    return arguments.length ? UI.settings.locoScroll.scrollTo(value, 0, 0) : UI.settings.locoScroll.scroll.instance.scroll.y;
                }, // we don't have to define a scrollLeft because we're only scrolling vertically.
                getBoundingClientRect() {
                    return {
                        top: 0,
                        left: 0,
                        width: window.innerWidth,
                        height: window.innerHeight
                    };
                }
            });
            UI.settings.locoScroll.on('scroll', (obj) => {
                UI.settings.scroll_y_position = obj["scroll"]["y"];
                //console.log(obj["scroll"]["y"]);
                //console.log(obj["direction"]);
                //console.log(obj);
                //console.log(UI.settings.scroll_y_position);
                let services_button = UI.settings.carousel_button;
                if (obj["scroll"]["y"] > 500) {
                    services_button.classList.add('cis__services__button--park-below');
                    //UI.settings.main_nav_list.classList.add('cis__header__nav__list--hide');
                } else {
                    services_button.classList.remove('cis__services__button--park-below');
                    //UI.settings.main_nav_list.classList.remove('cis__header__nav__list--hide');
                }
            });
            UI.settings.scroll_container = document.getElementById('scroll-container');
            UI.settings.scroll_thumb = document.querySelector('.c-scrollbar_thumb');
            //*
            const el = document.querySelectorAll('.js-lazy-image');
            const services = document.querySelectorAll('.js-lazy-service-image');
            if (HELPERS.canUseWebP()) {
                const el_to_switch = document.querySelectorAll('.js-can-use-webp');
                for (var i = el_to_switch.length - 1; i >= 0; i--) {
                    if (el_to_switch[i].dataset.webp) {
                        let parentElement = el_to_switch[i];
                        let theFirstChild = parentElement.firstChild;
                        let newElement = document.createElement("source");
                        newElement.setAttribute("srcset", el_to_switch[i].dataset.webp);
                        // Insert the new element before the first child
                        parentElement.insertBefore(newElement, theFirstChild)
                    }
                }
            }
            /*=INIT COVER/HOME TEXT AND CARDS FX
            ----------------------------------------------------------------------*/
            UI.settings.appear_scroll_triggers = [];
            gsap.utils.toArray("#scroll-container .js-observe").forEach((item) => {
                HELPERS.setView(item);
                const trigger = gsap.to(item, {
                    scrollTrigger: {
                        trigger: item,
                        scroller: UI.settings.scroll_container,
                        invalidateOnRefresh: true,
                        //markers: true,
                        once: true,
                        onEnter: function() {
                            HELPERS.inScrollview(item);
                        },
                        onEnterBack: function() {
                            //HELPERS.inScrollview(item);
                        },
                        onLeave: function() {
                            //HELPERS.setView(item);
                        }
                    },
                });
                UI.settings.appear_scroll_triggers.push(trigger);
            });
            /*=INIT COVER/HOME LAZY LOAD FOR IMAGES
            ----------------------------------------------------------------------*/
            HELPERS.initLazyImages(UI.settings.scroll_container.querySelectorAll('.js-lazy-image'));
            HELPERS.initLazyImages(UI.settings.scroll_container.querySelectorAll('.js-lazy-service-image'));
        },
        addResizeEvent: function() {
            window.addEventListener('resize', UI.resizeHandler, {
                passive: true
            });
        },
        removeResizeEvent: function() {
            window.removeEventListener('resize', UI.resizeHandler, {
                passive: true
            });
            console.log('removed resize');
        },
        resizeHandler: function() {
            /*
            let w = window;
            let x = w.innerWidth || e.clientWidth || g.clientWidth;
            if (x < 768) {
                //INACTIVE STUFF FOR SMALL RESOLUTIONS
            } else {
                //REACTIVATE DESKTOP OR LARGE RESOLUTION STUFF
            }
            */
            if (document.getElementById('section-nav')) {
                document.getElementById('section-nav').dataset.scrollOffset = document.getElementById('cis-concept-hero').offsetHeight;
            }
            /*
            document.getElementById('header-dummy').style.height = document.getElementById('section-nav').offsetHeight + 'px';
            document.getElementById('header-dummy').style.marginTop = '-' + document.getElementById('section-nav').offsetHeight + 'px';
            //*/
            if (UI.settings.locoScroll) {
                UI.settings.locoScroll.update();
            }
            //console.log('TESTING!');
        }.debounce(150),
        resetLocomotiveScroll: function() {
            UI.settings.locoScroll.update();
            //UI.settings.locoScroll.scrollTo('top', 0, 0, [0.25, 0.00, 0.35, 1.00], 0);
        },
        /**
         *----------------------------------------------------------------------
         *
         * ::ALL LINKS AND BUTTONS STUFF
         *
         *----------------------------------------------------------------------
         */
        initLinks: function() {
            document.addEventListener('click', UI.callback, false);
        },
        initHoverListener: function() {
            document.getElementById('hover-effect').addEventListener('mouseover', UI.mouseOver, false);
            document.getElementById('hover-effect').addEventListener('mouseout', UI.mouseOut, false);
        },
        mouseOver: function() {
            document.getElementById('cis-hero').classList.add('cis__hero--active');
            UI.settings.main_nav.classList.add('cis__hero--active');
        },
        mouseOut: function() {
            document.getElementById('cis-hero').classList.remove('cis__hero--active');
            UI.settings.main_nav.classList.remove('cis__hero--active');
        },
        findPurpose: function(el) {
            if (el.tagName == 'A' && el.href) {
                return el;
            } else if (el.tagName == 'BUTTON' || el.tagName == 'BUTTON' && el.dataset.action) {
                return el;
            } else if (el.parentElement) {
                return UI.findPurpose(el.parentElement);
            } else {
                return null;
            }
        },
        callback: function(e) {
            const element = UI.findPurpose(e.target);
            if (element == null) {
                return;
            }
            e.preventDefault();
            // Do something here
            let clickedItem = element;
            if ((clickedItem.classList.contains('section-link') || !clickedItem.hasAttribute("class")) && clickedItem.getAttribute("target") != '_blank' && PAGES.settings.section_is_loading == false) {
                UI.settings.section_uri = clickedItem.dataset.uri;
                UI.settings.section_title = clickedItem.dataset.title || clickedItem.getAttribute('title');
                UI.settings.section_description = clickedItem.dataset.description;
                History.pushState({
                    url: UI.settings.section_uri,
                }, (UI.settings.section_title ? UI.settings.section_title + " - " : '') + as.site_title, clickedItem.href);
                if (clickedItem.dataset.description) {
                    document.querySelector('meta[name="description"]').setAttribute("content", UI.settings.section_description);
                }
                //UI.closeMenu();
            } else if (clickedItem.classList.contains('scroll-link')) {
                if (UI.settings.locoScroll) {
                    UI.settings.locoScroll.scrollTo(document.getElementById(clickedItem.dataset.id), {
                        offset: -160
                    });
                }
            } else if (clickedItem.classList.contains('external-link') || clickedItem.classList.contains('download-target') || (!clickedItem.hasAttribute("class") && clickedItem.getAttribute("target") == '_blank')) {
                if (clickedItem.href == 'mailto:' && as.tracking_code.matomo == true && as.local_copy == false) {
                    //Category, Name, Action, Value
                    _paq.push(['trackEvent', 'Contact', 'Clicked mailto link or CTA', 'email goes here']);
                } else if (as.tracking_code.matomo == true && as.local_copy == false) {
                    MATOMO.matTrackExternalLink(clickedItem.href);
                }
                window.open(clickedItem.href, '_blank');
            } else if (clickedItem.classList.contains('same-window')) {
                window.open(clickedItem.href, "_self");
            } else if (clickedItem.classList.contains('open-menu')) {
                UI.openMenu();
            } else if (clickedItem.classList.contains('close-menu')) {
                UI.closeMenu();
            } else if (clickedItem.classList.contains('open-carousel')) {
                UI.openCarousel();
            } else if (clickedItem.classList.contains('close-carousel')) {
                UI.closeCarousel();
            } else if (clickedItem.classList.contains('prev-service')) {
                UI.prevService();
            } else if (clickedItem.classList.contains('next-service')) {
                UI.nextService();
            } else if (clickedItem.classList.contains('close-privacy-banner')) {
                HELPERS.closePrivacyBanner();
            } else if (clickedItem.classList.contains('go-to-service')) {
                UI.customPageDot(clickedItem);
            } else if (clickedItem.classList.contains('copy-link')) {
                navigator.clipboard.writeText(clickedItem.href);
                let item = clickedItem.querySelector('.copy-link-tooltip');
                gsap.set(item, {
                    autoAlpha: 1,
                });
                gsap.to(item, {
                    duration: 1,
                    autoAlpha: 0,
                    delay: .75,
                    ease: "Quint.easeInOut",
                });
            } else {
                if (as.debug) {
                    console.log('TARGET IS NOT AN ANCHOR NOR A BUTTON OR IT HAS NO ACTION DEFINED.');
                }
            }
            e.stopPropagation();
        },
        /**
         *----------------------------------------------------------------------
         *
         * ::COVER STUFF
         *
         *----------------------------------------------------------------------
         */
        //ALL GOES HERE
        /**
         *----------------------------------------------------------------------
         *
         * ::SERVICES CAROUSEL
         *
         *----------------------------------------------------------------------
         */
        initCarousel: function() {
            let services = document.querySelectorAll('.carousel-cell');
            let elem = document.getElementById('services-carousel');
            let data = UI.settings.services_data;
            for (var i = 0; i < services.length; i++) {
                data[i] = {
                    category: services[i].dataset.category,
                    title: services[i].dataset.title,
                    customtitle: services[i].dataset.customtitle,
                    cover: services[i].dataset.cover,
                    object: services[i].dataset.object,
                    webp: services[i].dataset.webp,
                    ratio: services[i].dataset.ratio,
                    color: services[i].dataset.color,
                    //rectangle: document.getElementById('rectangle-' + i),
                    //imageContainer: document.getElementById('image-' + i),
                }
            }
            UI.settings.flkty = new Flickity(elem, {
                // options
                //adaptiveHeight: true,
                setGallerySize: false,
                selectedAttraction: 0.05,
                friction: 0.5,
                prevNextButtons: false,
                pageDots: false,
                wrapAround: true,
                on: {
                    ready: function() {
                        //UI.customPageDot();
                    },
                    dragStart: function() {
                        UI.settings.flkty.slider.querySelectorAll('.js-carousel-link').forEach(slide => slide.style.pointerEvents = "none");
                        /*
                        gsap.to('#servicios-marquee', {
                            duration: .75,
                            ease: "Quint.easeOut",
                            autoAlpha: 0,
                        });
                        //*/
                    },
                    dragEnd: function() {
                        UI.settings.flkty.slider.querySelectorAll('.js-carousel-link').forEach(slide => slide.style.pointerEvents = "all");
                        /*
                        gsap.to('#servicios-marquee', {
                            duration: 1,
                            ease: "Quint.easeOut",
                            autoAlpha: 1.5,
                        });
                        //*/
                    },
                    settle: function() {
                        if (UI.settings.locoScroll) {
                            UI.settings.locoScroll.update();
                        }
                    },
                    change: function(progress) {
                        /*=STORE NEW INDEX
                        ----------------------------------------------------------------------*/
                        UI.settings.current_index = UI.settings.flkty.selectedIndex + 1;
                        /*=GET CURRENT COLORS
                        ----------------------------------------------------------------------*/
                        let expr = UI.settings.services_data[UI.settings.flkty.selectedIndex]['color'];
                        let new_color_1 = '#FFB2D3';
                        //let new_color_2 = '#F9C7DD';
                        let new_color_2 = '#F9C7DD';
                        switch (expr) {
                            case 'pink':
                                new_color_1 = '#FFB2D3';
                                //new_color_2 = '#F9C7DD';
                                new_color_2 = '#f2f0f2';
                                break;
                            case 'orange':
                                new_color_1 = '#FAA14A';
                                //new_color_2 = '#F9E0C7';
                                new_color_2 = '#f2f0f2';
                                break;
                            case 'red':
                                new_color_1 = '#FF5267';
                                //new_color_2 = '#FFBFC8';
                                new_color_2 = '#f2f0f2';
                                break;
                            case 'coral':
                                new_color_1 = '#FF7757';
                                //new_color_2 = '#FFAC98';
                                new_color_2 = '#f2f0f2';
                                break;
                            case 'yellow':
                                new_color_1 = '#F7B700';
                                //new_color_2 = '#FFF3D3';
                                new_color_2 = '#f2f0f2';
                                break;
                            case 'purple':
                                new_color_1 = '#8338EC';
                                //new_color_2 = '#E9D9FF';
                                new_color_2 = '#f2f0f2';
                                break;
                            case 'blue':
                                new_color_1 = '#3A86FF';
                                //new_color_2 = '#E0EBFF';
                                new_color_2 = '#f2f0f2';
                                break;
                            case 'green':
                                new_color_1 = '#2FC38F';
                                //new_color_2 = '#ADDACA';
                                new_color_2 = '#f2f0f2';
                            default:
                        }
                        /*=DETECT DIRECTION
                        ----------------------------------------------------------------------*/
                        let delta = UI.settings.flkty.selectedIndex - UI.settings.previous_index;
                        let lastIndex = UI.settings.flkty.slides.length - 1;
                        if (UI.settings.previous_index == lastIndex && UI.settings.flkty.selectedIndex === 0) {
                            console.log('wrapped forward');
                        } else if (UI.settings.previous_index === 0 && UI.settings.flkty.selectedIndex == lastIndex) {
                            console.log('wrapped back')
                        } else if (delta > 0) {
                            //console.log('moved forward', delta)
                            UI.settings.direction = 'forward';
                        } else {
                            UI.settings.direction = 'back';
                            //console.log('moved back', delta)
                        }
                        UI.settings.previous_index = UI.settings.flkty.selectedIndex;
                        /*=PLACE NEW ELEMENTS AND ANIMATE
                        ----------------------------------------------------------------------*/
                        let data = UI.settings.services_data;
                        let new_id = 'new-titles-' + UI.settings.flkty.selectedIndex + HELPERS.uniqueID();
                        let new_image_id = 'new-image-' + 0 + HELPERS.uniqueID();
                        let new_category_ID = 'new-category-' + UI.settings.flkty.selectedIndex + HELPERS.uniqueID();
                        let new_title_ID = 'new-title-' + UI.settings.flkty.selectedIndex + HELPERS.uniqueID();
                        UI.settings.new_titles_container.insertAdjacentHTML('beforeend', '<div class="services-carousel__new-titles-container" id="' + new_id + '"> <h5 class="services-carousel__category" id="' + new_category_ID + '"> ' + data[UI.settings.flkty.selectedIndex]['category'] + '</h5> <h3 class="services-carousel__heading"> <span class="link--underline" id="' + new_title_ID + '"> ' + data[UI.settings.flkty.selectedIndex]['customtitle'] + '</span> </h3> </div>');
                        UI.settings.new_image_container.insertAdjacentHTML('beforeend', '<div id="' + new_image_id + '" class="cis__services__carousel__image__container" style="padding-top:' + data[UI.settings.flkty.selectedIndex]['ratio'] + '%;"><figure class="cis__services__carousel__image__figure" style="padding-top:' + data[UI.settings.flkty.selectedIndex]['ratio'] + '%;"><picture class="" data-alt="' + new_title_ID + '" data-webp="' + data[UI.settings.flkty.selectedIndex]['webp'] + '" style="padding-top:' + data[UI.settings.flkty.selectedIndex]['ratio'] + '%;" ><source srcset="' + data[UI.settings.flkty.selectedIndex]['webp'] + '"><source srcset="' + data[UI.settings.flkty.selectedIndex]['object'] + '"><img alt="' + data[UI.settings.flkty.selectedIndex]['title'] + '" /></picture></figure></div>');
                        let service_category = document.getElementById(new_category_ID);
                        let service_title = document.getElementById(new_title_ID);
                        let container_to_remove = document.getElementById(new_id);
                        let image_to_remove = document.getElementById(new_image_id);
                        UI.removeCurrentTitles(UI.settings.service_to_remove, UI.settings.service_image__to_remove);
                        //document.getElementById('servicios-marquee').textContent = data[UI.settings.flkty.selectedIndex]['title'];
                        //document.getElementById('servicios-marquee').dataset.text = data[UI.settings.flkty.selectedIndex]['title'];
                        //console.log(data[UI.settings.flkty.selectedIndex]['customtitle']);
                        //IMAGE SETTING
                        if (UI.settings.direction === 'forward') {
                            gsap.set(image_to_remove, {
                                perspective: 400,
                                //autoAlpha: 0,
                                force3D: true,
                                y: '100vh',
                                rotation: 10,
                                x: -160,
                                transformOrigin: "center center",
                                //transformOrigin: "top center -150",
                                //rotationX: -40,
                                //rotationY: -60,
                            });
                            let prev_element = document.getElementById('second-image-' + UI.settings.current_index - 1);
                            let next_element = document.getElementById('second-image-' + UI.settings.current_index + 1);
                            let element = document.getElementById('second-image-' + UI.settings.current_index);
                            if (element) {
                                gsap.set(element, {
                                    x: '120%',
                                });
                            }
                            if (next_element) {
                                gsap.set(next_element, {
                                    x: '-120%',
                                });
                            }
                            gsap.to(element, {
                                duration: 1,
                                delay: .25,
                                ease: "Quint.easeOut",
                                x: '0%',
                            });
                        } else {
                            gsap.set(image_to_remove, {
                                perspective: 400,
                                //autoAlpha: 0,
                                force3D: true,
                                y: '-100vh',
                                rotation: -10,
                                x: 160,
                                transformOrigin: "center center",
                                //transformOrigin: "top center -150",
                                //rotationX: -40,
                                //rotationY: -60,
                            });
                            let prev_element = document.getElementById('second-image-' + UI.settings.current_index - 1);
                            let next_element = document.getElementById('second-image-' + UI.settings.current_index + 1);
                            let element = document.getElementById('second-image-' + UI.settings.current_index);
                            if (element) {
                                gsap.set(element, {
                                    x: '-120%',
                                });
                            }
                            if (next_element) {
                                gsap.set(next_element, {
                                    x: '120%',
                                });
                            }
                            gsap.to(element, {
                                duration: 1,
                                delay: .25,
                                ease: "Quint.easeOut",
                                x: '0%',
                            });
                        }
                        gsap.to(image_to_remove, {
                            duration: 1.5,
                            //autoAlpha: 1,
                            y: 0,
                            x: 0,
                            rotation: 0,
                            ease: Quint.easeInOut,
                        });
                        //SPLIT CATEGORY
                        let category_split = new SplitText(service_category, {
                            type: "words,chars",
                            reduceWhiteSpace: false,
                        });
                        let category_chars = category_split.chars;
                        gsap.set(category_chars, {
                            perspective: 400,
                            opacity: 0,
                            scale: 0,
                            y: 80,
                            force3D: true,
                            rotationX: 180,
                            transformOrigin: "0% 50% -50",
                        });
                        gsap.to(category_chars, {
                            duration: 1.5,
                            delay: .35,
                            opacity: 1,
                            scale: 1,
                            y: 0,
                            rotationX: 0,
                            transformOrigin: "0% 50% -50",
                            ease: Back.easeOut,
                            stagger: 0.01,
                        });
                        //SPLIT TITLE
                        UI.settings.title_split = new SplitText(service_title, {
                            //type: "lines",
                            type: "words,lines",
                            reduceWhiteSpace: false,
                        });
                        let title_lines = UI.settings.title_split.lines;
                        gsap.set(title_lines, {
                            perspective: 400,
                            autoAlpha: 0,
                            force3D: true,
                            y: 130,
                            transformOrigin: "top center -150",
                            //rotationX: -120,
                            rotationY: 60,
                        });
                        gsap.to(title_lines, {
                            duration: 1.75,
                            delay: .5,
                            autoAlpha: 1,
                            rotationX: 0,
                            rotationY: 0,
                            y: 0,
                            ease: Quint.easeOut,
                            stagger: 0.01,
                            onStart: function() {
                                service_title.classList.add('link--underline-full');
                            },
                            onComplete: function() {
                                //UI.settings.title_split.revert();
                            },
                        });
                        service_title.style.backgroundImage = HELPERS.getCssValuePrefix() + 'linear-gradient(0deg,' + new_color_1 + ' 0%, ' + new_color_1 + ' 100%)';
                        UI.settings.service_to_remove = container_to_remove;
                        UI.settings.service_image__to_remove = image_to_remove;
                        if (UI.settings.locoScroll) {
                            UI.settings.locoScroll.update();
                        }
                        let element = document.getElementById('page-dot-button-' + UI.settings.flkty.selectedIndex);
                        UI.customPageDot(element);
                        gsap.to('.carousel-change-bg-1', {
                            duration: .5,
                            background: new_color_1,
                            ease: "Quint.easeInOut",
                        });
                        gsap.to('.carousel-change-bg-2', {
                            duration: .5,
                            background: new_color_2,
                            ease: "Quint.easeInOut",
                        });
                    },
                    scroll: function(progress) {
                        /*
                        let element = ;
                        gsap.to("#second-image-" + 1, {
                            duration: .15
                            //ease: "Quint.easeInOut",
                            x: '0%',
                        });
                        //*/
                        /*
                        let element = 'second-image-' + UI.settings.current_index;
                        let ratio = 0.6 * 0.8;
                        let parallax = ((0.05 - (0.5 + progress) * ratio) * 100);
                        console.log(parallax);
                        //console.log(UI.settings.direction);
                        document.getElementById(element).style.transform = 'translateX(' + parallax + '%)';
                        //*/
                    },
                }
            });
            UI.initCarouselTL();
        },
        carouselIntroFunction: function() {
            /*=GET CURRENT COLORS
            ----------------------------------------------------------------------*/
            let expr = UI.settings.services_data[0]['color'];
            let new_color_1 = '#FFB2D3';
            //let new_color_2 = '#F9C7DD';
            let new_color_2 = '#F9C7DD';
            switch (expr) {
                case 'pink':
                    new_color_1 = '#FFB2D3';
                    //new_color_2 = '#F9C7DD';
                    new_color_2 = '#f2f0f2';
                    break;
                case 'orange':
                    new_color_1 = '#FAA14A';
                    //new_color_2 = '#F9E0C7';
                    new_color_2 = '#f2f0f2';
                    break;
                case 'red':
                    new_color_1 = '#FF5267';
                    //new_color_2 = '#FFBFC8';
                    new_color_2 = '#f2f0f2';
                    break;
                case 'coral':
                    new_color_1 = '#FF7757';
                    //new_color_2 = '#FFAC98';
                    new_color_2 = '#f2f0f2';
                    break;
                case 'yellow':
                    new_color_1 = '#F7B700';
                    //new_color_2 = '#FFF3D3';
                    new_color_2 = '#f2f0f2';
                    break;
                case 'purple':
                    new_color_1 = '#8338EC';
                    //new_color_2 = '#E9D9FF';
                    new_color_2 = '#f2f0f2';
                    break;
                case 'blue':
                    new_color_1 = '#3A86FF';
                    //new_color_2 = '#E0EBFF';
                    new_color_2 = '#f2f0f2';
                    break;
                case 'green':
                    new_color_1 = '#2FC38F';
                    //new_color_2 = '#ADDACA';
                    new_color_2 = '#f2f0f2';
                default:
            }
            /*=PLACE FIRST ELEMENTS AND ANIMATE
            ----------------------------------------------------------------------*/
            let data = UI.settings.services_data;
            let new_id = 'new-titles-' + 0 + HELPERS.uniqueID();
            let new_image_id = 'new-image-' + 0 + HELPERS.uniqueID();
            let new_category_ID = 'new-category-' + 0 + HELPERS.uniqueID();
            let new_title_ID = 'new-title-' + 0 + HELPERS.uniqueID();
            UI.settings.new_titles_container.insertAdjacentHTML('beforeend', '<div class="services-carousel__new-titles-container" id="' + new_id + '"> <h5 class="services-carousel__category" id="' + new_category_ID + '"> ' + data[0]['category'] + '</h5> <h3 class="services-carousel__heading"> <span class="link--underline" id="' + new_title_ID + '"> ' + data[0]['customtitle'] + '</span> </h3> </div>');
            UI.settings.new_image_container.insertAdjacentHTML('beforeend', '<div id="' + new_image_id + '" class="cis__services__carousel__image__container" style="padding-top:' + data[0]['ratio'] + '%;"><figure class="cis__services__carousel__image__figure" style="padding-top:' + data[0]['ratio'] + '%;"><picture class="" data-alt="' + new_title_ID + '" data-webp="' + data[0]['webp'] + '" style="padding-top:' + data[0]['ratio'] + '%;" ><source srcset="' + data[0]['webp'] + '"><source srcset="' + data[0]['object'] + '"><img alt="' + data[0]['title'] + '" /></picture></figure></div>');
            let service_category = document.getElementById(new_category_ID);
            let service_title = document.getElementById(new_title_ID);
            let container_to_remove = document.getElementById(new_id);
            let image_to_remove = document.getElementById(new_image_id);
            //IMAGE SETTING
            gsap.set(image_to_remove, {
                perspective: 400,
                autoAlpha: 0,
                force3D: true,
                //y: 800,
                transformOrigin: "top center -150",
                //rotationX: -40,
                //rotationY: -60,
            });
            gsap.to(image_to_remove, {
                duration: 2.25,
                delay: .75,
                autoAlpha: 1,
                //y: 0,
                //rotationY: 0,
                ease: Quint.easeOut,
            });
            //SPLIT CATEGORY
            let category_split = new SplitText(service_category, {
                type: "words,chars",
                reduceWhiteSpace: false,
            });
            let category_chars = category_split.chars;
            gsap.set(category_chars, {
                perspective: 400,
                autoAlpha: 0,
                scale: 0,
                y: 80,
                force3D: true,
                rotationX: 180,
                transformOrigin: "0% 50% -50",
            });
            gsap.to(category_chars, {
                delay: .5,
                duration: 1.5,
                autoAlpha: 1,
                scale: 1,
                y: 0,
                rotationX: 0,
                ease: Back.easeOut,
                transformOrigin: "0% 50% -50",
                stagger: 0.01,
            });
            //SPLIT TITLE
            UI.settings.title_split = new SplitText(service_title, {
                type: "lines",
                reduceWhiteSpace: false,
            });
            let title_lines = UI.settings.title_split.lines;
            gsap.set(title_lines, {
                perspective: 400,
                autoAlpha: 0,
                force3D: true,
                y: 130,
                transformOrigin: "top center -150",
                //rotationX: -40,
                rotationY: 60,
            });
            gsap.to(title_lines, {
                duration: 1.75,
                delay: .75,
                autoAlpha: 1,
                rotationX: 0,
                rotationY: 0,
                y: 0,
                ease: Quint.easeOut,
                stagger: 0.01,
                onStart: function() {
                    service_title.classList.add('link--underline-full');
                }
            });
            gsap.to('#services-carousel-dots', {
                duration: 1.5,
                delay: .5,
                autoAlpha: 1,
                y: 0,
                ease: Quint.easeOut,
            });
            gsap.to('.services-carousel__white-box', {
                duration: 1.75,
                delay: .75,
                autoAlpha: 1,
                ease: Quint.easeOut,
            });
            gsap.to('.cis__services__carousel__image', {
                duration: 1.75,
                delay: 1,
                autoAlpha: 1,
                ease: Quint.easeOut,
            });
            service_title.style.backgroundImage = HELPERS.getCssValuePrefix() + 'linear-gradient(0deg,' + data[0]['color'] + ' 0%, ' + data[0]['color'] + ' 100%)';
            UI.settings.service_to_remove = container_to_remove;
            UI.settings.service_image__to_remove = image_to_remove;
            UI.settings.carousel_intro_flag = true;
            if (UI.settings.locoScroll) {
                UI.settings.locoScroll.update();
            }
            let element = document.getElementById('page-dot-button-0');
            UI.customPageDot(element);
            gsap.to('.carousel-change-bg-1', {
                duration: .5,
                background: new_color_1,
                ease: "Quint.easeInOut",
            });
            gsap.to('.carousel-change-bg-2', {
                duration: .5,
                background: new_color_2,
                ease: "Quint.easeInOut",
            });
        },
        removeCurrentTitles: function(recycle, recycleImage) {
            var toRecycle = recycle;
            if (toRecycle !== undefined) {
                /*
                UI.settings.category_split.clear();
                UI.settings.category_split.kill();
                UI.settings.category_split = null;

                UI.settings.title_split.clear();
                UI.settings.title_split.kill();
                UI.settings.title_split = null;
                //*/
                //UI.settings.category_split = null;
                //UI.settings.title_split = null;
                gsap.to(toRecycle, {
                    duration: 1,
                    ease: Quint.easeInOut,
                    autoAlpha: 0,
                    y: '-15vh',
                    onComplete: function() {
                        //alert(recycle);
                        //document.getElementById(ui.settings.id_check).style.backgroundImage = ui.getCssValuePrefix() + 'linear-gradient(0deg,' + ui.settings.dataArray[ui.settings.flkty.selectedIndex]['color'] + ' 0%, ' + ui.settings.dataArray[ui.settings.flkty.selectedIndex]['color'] + ' 100%)';
                        toRecycle.remove();
                    }
                });
                if (UI.settings.direction === 'forward') {
                    gsap.set(recycleImage, {
                        //perspective: 400,
                        force3D: true,
                        transformOrigin: "center center",
                        //transformOrigin: "top center",
                    });
                    gsap.to(recycleImage, {
                        duration: 1.25,
                        //autoAlpha: 0,
                        y: '-100vh',
                        rotation: -25,
                        x: 160,
                        //scaleY: 5,
                        ease: Quint.easeInOut,
                        onComplete: function() {
                            recycleImage.remove();
                        }
                    });
                } else {
                    gsap.set(recycleImage, {
                        //perspective: 400,
                        force3D: true,
                        transformOrigin: "center center",
                        //transformOrigin: "top center",
                    });
                    gsap.to(recycleImage, {
                        duration: 1.25,
                        //autoAlpha: 0,
                        y: '100vh',
                        rotation: 25,
                        x: -160,
                        //scaleY: 5,
                        ease: Quint.easeInOut,
                        onComplete: function() {
                            recycleImage.remove();
                        }
                    });
                }
            }
        },
        removeCurrentElements: function(recycle, stays) {
            if (recycle && recycle !== undefined) {
                recycle.remove();
            }
        },
        initCarouselTL: function() {
            UI.settings.services_timeline = gsap.timeline();
            let tl = UI.settings.services_timeline;
            tl.to('.js-hide-on-menu-open', {
                duration: 1,
                y: '-100',
                ease: "Quint.easeInOut",
                autoAlpha: 0,
                stagger: 0.15,
                onReverseComplete: function() {
                    UI.settings.main_nav.style.zIndex = '110';
                }
            }, "<");
            tl.pause();
        },
        openCarousel: function() {
            UI.closeMenu();
            let tl = UI.settings.services_timeline;
            tl.timeScale(1);
            tl.play();
            document.getElementById('carousel-container').classList.remove('cis__services__carousel--inactive');
            gsap.delayedCall(1, delayedHandler);
            if (!UI.settings.carousel_intro_flag) {
                UI.carouselIntroFunction();
            }
            //HIDE MAIN NAV LIST
            UI.settings.main_nav_list.classList.add('cis__header__nav__list--hide');
            //INIT AND STYLE OPEN BUTTON
            document.querySelector('.cis__services__trigger').classList.add('cis__services__trigger--initial');

            function delayedHandler() {
                UI.settings.fetch_container.style.visibility = "hidden";
                UI.settings.fetch_container.style.opacity = "0";
            }
            let open_services = UI.settings.carousel_button;
            if (open_services.classList.contains('cis__services__button--park-below')) {
                open_services.classList.add('cis__services__button--park-below--open');
            } else {
                open_services.classList.add('cis__services__button--open');
            }
            //open_services.style.removeProperty('top');
            /*
            open_services.style.removeProperty('transform');
            gsap.to(open_services, {
                duration: 1,
                ease: "Quint.easeInOut",
                x: '5vw',
                y: 'calc(50vh - 60px)',
                onComplete: function() {
                    //open_services.style.removeProperty('top');
                    open_services.style.removeProperty('transform');
                },
            });
            //*/
            //TOGGLE OPEN AND CLOSE FUNCTION
            open_services.classList.remove('open-carousel');
            open_services.classList.add('close-carousel');
            //BUTTON STLYES
            let service_button = UI.settings.carousel_button;
            service_button.classList.add('cis__services__button--active');
            UI.settings.main_nav.style.zIndex = '120';
            //CAROUSEL FLAG
            UI.settings.carousel_is_open_flag = true;
        },
        closeCarousel: function() {
            let tl = UI.settings.services_timeline;
            tl.timeScale(2.75);
            tl.reverse();
            UI.settings.fetch_container.style.visibility = "visible";
            UI.settings.fetch_container.style.opacity = "1";
            document.querySelector('.cis__services__trigger').classList.remove('cis__services__trigger--initial');
            document.getElementById('carousel-container').classList.add('cis__services__carousel--inactive');
            let open_services = UI.settings.carousel_button;
            if (open_services.classList.contains('cis__services__button--park-below')) {
                open_services.classList.remove('cis__services__button--park-below--open');
            } else {
                open_services.classList.remove('cis__services__button--open');
            }
            //SHOW MAIN NAV LIST
            UI.settings.main_nav_list.classList.remove('cis__header__nav__list--hide');
            //TOGGLE OPEN AND CLOSE FUNCTION
            open_services.classList.remove('close-carousel');
            open_services.classList.add('open-carousel');
            //BUTTON STLYES
            let service_button = UI.settings.carousel_button;
            service_button.classList.remove('cis__services__button--active');
            //CAROUSEL FLAG
            UI.settings.carousel_is_open_flag = false;
        },
        nextService: function() {
            let services = UI.settings.flkty;
            services.next();
            let element = document.getElementById('page-dot-button-' + UI.settings.flkty.selectedIndex);
            UI.customPageDot(element);
        },
        prevService: function() {
            let services = UI.settings.flkty;
            services.previous();
            let element = document.getElementById('page-dot-button-' + UI.settings.flkty.selectedIndex);
            UI.customPageDot(element);
        },
        customPageDot: function(element) {
            if (element.dataset.id) {
                //console.log(element.dataset.id);
                UI.settings.flkty.select(element.dataset.id);
                let cells = document.querySelectorAll('.services-carousel__page-dots__list__item');
                for (var i = 0; i < cells.length; i++) {
                    cells[i].classList.remove('services-carousel__page-dots__list__item--active');
                }
                document.getElementById('page-dot-' + element.dataset.id).classList.add('services-carousel__page-dots__list__item--active');
            }
        },
    }
    return ui;
}(UI || {}));