var HISTORY_ENGINE = (function(historyEngine) {
    historyEngine = {
        settings: {
            current_action: '',
            first_history_check: 0,
        },
        init: function() {
            /**
             *----------------------------------------------------------------------
             *
             * ::AJAX URL HANDLING: LOCAL VS PREPROS VS PRODUCTION
             *
             *----------------------------------------------------------------------
             */
            HELPERS.isThisLocal();
            var URIfull = window.location.pathname;
            var URIlast = URIfull.substr(URIfull.lastIndexOf("/") + 1);
            var URIsplit = URIfull.split("/");
            var URIlevels = URIsplit.length;
            if (as.debug) {
                console.log("---------------------");
                console.log("FIRST STATE REPLACE");
                console.log("Full URL: " + window.location.pathname);
                console.log("Split URL: protocol+domain" + URIsplit);
                console.log("Last URI: " + URIlast);
                console.log("Split URL Levels: " + URIlevels);
            }
            if (URIlast == "" || URIlast == as.uri_last || URIlast == as.uri_last) {
                History.pushState({
                    url: as.uri_last
                }, as.site_title, as.uri_last);
                PAGES.settings.it_started_from_a_section = false;
                HISTORY_ENGINE.weveGotHistory();
            } else {
                History.pushState({
                    url: URIlast
                }, as.site_title, URIlast);
                PAGES.settings.it_started_from_a_section = true;
                HISTORY_ENGINE.weveGotHistory();
            }
            //settings = historyEngine.settings;
            //*
            History.Adapter.bind(window, "statechange", function(event) {
                historyEngine.weveGotHistory();
                //var State = History.getState(); // Note: We are using History.getState() instead of event.state
            });
            //*/
            //window.addEventListener('popstate', function(){alert("location: " + document.location + ", state: " + JSON.stringify(event.state));historyEngine.weveGotHistory()});
            //History.Adapter.onDomLoad(historyEngine.weveGotHistory);
            //if (URIlevels > 2) {
            HISTORY_ENGINE.settings.first_history_check = 1;
            //}
        },
        weveGotHistory: function() {
            if (as.local_copy == true) {
                if (as.local_is_not_localhost) {
                    /**
                     *----------------------------------------------------------------------
                     *
                     * ::SIMULATED DOMAIN LOCAL LOGIC
                     *
                     *----------------------------------------------------------------------
                     */
                    var URIfull = window.location.pathname;
                    var URIlast = URIfull.substr(URIfull.lastIndexOf("/") + 1);
                    var URIsplit = URIfull.split("/");
                    var URIlevels = URIsplit.length;
                    var URIaction = URIsplit[0];
                    var URIfirst = URIsplit[0];
                    if (URIlevels == as.local_levels) {
                        if (URIlast == as.uri_last) {
                            URIaction = as.uri_last;
                        } else {
                            URIaction = 'page';
                        }
                    }
                    if (URIlevels > as.local_levels) {
                        URIaction = 'subpage';
                    }
                } else {
                    /**
                     *----------------------------------------------------------------------
                     *
                     * ::LOCALHOST LOGIC
                     *
                     *----------------------------------------------------------------------
                     */
                    var URIfull = window.location.pathname.substr(window.location.pathname.lastIndexOf(as.path) + as.path.length);
                    //var URIfull = window.location.pathname;
                    var URIlast = URIfull.substr(URIfull.lastIndexOf("/") + 1);
                    var URIsplit = URIfull.split("/");
                    var URIlevels = URIsplit.length;
                    var URIaction = URIsplit[0];
                    var URIfirst = URIsplit[0];
                    if (URIlevels == as.local_levels) {
                        if (URIlast == as.uri_last) {
                            URIaction = as.uri_last;
                        } else {
                            URIaction = 'page';
                        }
                    }
                    if (URIlevels > as.local_levels) {
                        URIaction = 'subpage';
                    }
                }
            } else if (as.local_copy == 'prepros') {
                /**
                 *----------------------------------------------------------------------
                 *
                 * ::PREPROS LOGIC
                 *
                 *----------------------------------------------------------------------
                 */
                var URIfull = window.location.pathname;
                var URIlast = URIfull.substr(URIfull.lastIndexOf("/") + 1);
                var URIsplit = URIfull.split("/");
                var URIlevels = URIsplit.length;
                var URIaction = URIsplit[0];
                var URIfirst = URIsplit[0];
                if (URIlevels == as.local_levels) {
                    if (URIlast == as.uri_last) {
                        URIaction = as.uri_last;
                    } else {
                        URIaction = 'page';
                    }
                }
                if (URIlevels > as.local_levels) {
                    URIaction = 'subpage';
                }
            } else if (as.local_copy == false) {
                /**
                 *----------------------------------------------------------------------
                 *
                 * ::PRODUCTION LOGIC
                 *
                 *----------------------------------------------------------------------
                 */
                var URIfull = URIfull = window.location.pathname;
                var URIlast = URIfull.substr(URIfull.lastIndexOf("/") + 1);
                var URIsplit = URIfull.split("/");
                var URIlevels = URIsplit.length;
                var URIaction = URIsplit[0];
                var URIfirst = URIsplit[0];
                // This URIlevels operation is needed to check true and return to Home on Click
                if (URIlevels == as.live_levels) {
                    URIaction = as.uri_last;
                }
                if (URIlevels == as.live_levels) {
                    if (URIlast == as.uri_last) {
                        URIaction = as.uri_last;
                    } else {
                        URIaction = 'page';
                    }
                }
                if (URIlevels > as.live_levels) {
                    URIaction = 'subpage';
                }
            }
            if (as.debug) {
                console.log("----------------------");
                console.log("HISTORY ENGINE PUSH STATE");
                console.log("Full URL: " + URIfull);
                console.log("Split URL: protocol+domain" + URIsplit);
                console.log("Last URI: " + URIlast);
                console.log("Split URL Levels: " + URIlevels);
                //console.log("First Level for URI: " + URIsplit[1]);
                //console.log("First saved Level for URI: " + URIfirst);
                console.log("Action for URI: " + URIaction);
                //console.log("It comes from scrollTrigger action? " + PAGES.settings.it_came_from_scrolltrigger);
                console.log("----------------------");
            }
            historyEngine.settings.current_action = URIaction;
            switch (URIaction) {
                case as.uri_last:
                    if (URIlast == "" || URIlast == "/" || URIlast == as.uri_last) {
                        URIlast = as.uri_last;
                    }
                    if (as.debug) {
                        console.log("We should return to: " + URIlast);
                    }
                    //
                    PAGES.init(URIlast, null, "#fetch-container", null);
                    break;
                case 'page':
                    if (URIlast == "" || URIlast == "/") {
                        URIlast = as.uri_last;
                    }
                    if (as.local_copy == true) {
                        if (as.local_is_not_localhost) {
                            //
                            // Simulated domain Local Request Init
                            //
                            if (as.debug) {
                                console.log("We should locally load (on a test domain) the following page: " + window.location.protocol + "//" + window.location.host + as.path + URIfull);
                            }
                            PAGES.init('page', window.location.protocol + "//" + window.location.host + as.path + URIfull, "#fetch-container", URIfull, URIlast);
                        } else {
                            //
                            // Localhost Request Init
                            //
                            if (as.debug) {
                                console.log("We should locally load the following page: " + window.location.protocol + "//" + window.location.host + "/" + as.path + URIfull);
                            }
                            PAGES.init('page', window.location.protocol + "//" + window.location.host + "/" + as.path + URIfull, "#fetch-container", URIfull, URIlast);
                        }
                    } else if (as.local_copy == 'prepros') {
                        //
                        // Prepros port Request Init
                        //
                        if (as.debug) {
                            console.log("We should locally load (on a Prepros port) the following page: " + window.location.protocol + "//" + window.location.host + URIfull);
                        }
                        PAGES.init('page', window.location.protocol + "//" + window.location.host + URIfull, "#fetch-container", URIfull, URIlast);
                    } else if (as.local_copy == false) {
                        //
                        // Live Request init
                        //
                        if (as.debug) {
                            console.log("We should remotely load the following page: " + window.location.protocol + "//" + window.location.host + URIfull);
                        }
                        PAGES.init('page', window.location.protocol + "//" + window.location.host + URIfull, "#fetch-container", URIfull, URIlast);
                    }
                    break;
                case 'subpage':
                    if (URIlast == "" || URIlast == "/") {
                        URIlast = as.uri_last;
                    }
                    if (as.local_copy == true) {
                        if (as.local_is_not_localhost) {
                            //
                            // Simulated domain Local Request Init
                            //
                            if (as.debug) {
                                console.log("We should locally load (on a test domain) the following subpage: " + window.location.protocol + "//" + window.location.host + as.path + URIfull);
                            }
                            PAGES.init('subpage', window.location.protocol + "//" + window.location.host + as.path + URIfull, "#fetch-container", URIfull, URIlast);
                        } else {
                            //
                            // Localhost Request Init
                            //
                            if (as.debug) {
                                console.log("We should locally load the following subpage: " + window.location.protocol + "//" + window.location.host + "/" + as.path + URIfull);
                            }
                            PAGES.init('subpage', window.location.protocol + "//" + window.location.host + "/" + as.path + URIfull, "#fetch-container", URIfull, URIlast);
                        }
                    } else if (as.local_copy == 'prepros') {
                        //
                        // Prepros port Request Init
                        //
                        if (as.debug) {
                            console.log("We should locally load (on a Prepros port) the following subpage: " + window.location.protocol + "//" + window.location.host + URIfull);
                        }
                        PAGES.init('subpage', window.location.protocol + "//" + window.location.host + URIfull, "#fetch-container", URIfull, URIlast);
                    } else if (as.local_copy == false) {
                        //
                        // Live Request init
                        //
                        if (as.debug) {
                            console.log("We should remotely load the following subpage: " + window.location.protocol + "//" + window.location.host + URIfull);
                        }
                        PAGES.init('subpage', window.location.protocol + "//" + window.location.host + URIfull, "#fetch-container", URIfull, URIlast);
                    }
                    break;
                default:
                    // statements_def
                    break;
            }
        },
    }
    return historyEngine;
}(HISTORY_ENGINE || {}));