var PAGES = (function(pages) {
    pages = {
        settings: {
            use_transition: true,
            subsection_is_loading: false,
            subsection_is_open: false,
            section_is_loading: false,
            section_is_open: false,
            it_came_from_home: false,
            it_started_from_a_section: false,
            it_just_started_with_a_bang: true,
            request: '',
            s: '',
            list: '',
            project_image_container: '',
            project_image_to_remove: '',
            color_block: '',
            color_block_context: '',
            color: '#ffffff',
            color_context: 'transparent',
            over_color: '',
            fit_watch: '',
        },
        init: function(type, url, target, trackingurl, uri) {
            switch (type) {
                case 'cover':
                    if (!PAGES.settings.it_just_started_with_a_bang) {
                        //RETURN TO COVER MODULE LOGIC
                    } else {
                        //COVER.init();
                        PAGES.settings.it_just_started_with_a_bang = false;
                    }
                    if (PAGES.settings.use_transition) {
                        PAGES.createSweepShape(window.location.protocol + "//" + window.location.host + as.path + "/cover", "#fetch-container", "/cover", "cover");
                    } else {
                        PAGES.loadSection(window.location.protocol + "//" + window.location.host + as.path + "/cover", "#fetch-container", "/cover", "cover");
                    }
                    break;
                case 'page':
                    if (PAGES.settings.use_transition) {
                        PAGES.createSweepShape(url, target, trackingurl);
                    } else {
                        PAGES.loadSection(url, target, trackingurl);
                    }
                    break;
                case 'subpage':
                    if (PAGES.settings.use_transition) {
                        PAGES.createSweepShape(url, target, trackingurl);
                    } else {
                        PAGES.loadSection(url, target, trackingurl);
                    }
                    break;
                default:
                    //pages.loadCover();
            }
        },
        /**
         *----------------------------------------------------------------------
         *
         * ::TRANSITIONS SYSTEM
         *
         *----------------------------------------------------------------------
         */
        createSweepShape: function(url, target, trackingurl) {
            let newPreloader = document.createElementNS("http://www.w3.org/2000/svg", "svg");
            let newPath = document.createElementNS(newPreloader.namespaceURI, 'rect');
            newPath.setAttribute('width', '100%');
            newPath.setAttribute('height', '100%');
            newPath.setAttribute('id', 'new-path-preloader');
            newPreloader.setAttribute('width', '100');
            newPreloader.setAttribute('height', '100');
            newPreloader.setAttribute('id', 'section-preloader');
            newPreloader.setAttribute("class", "section-preloader");
            newPreloader.appendChild(newPath);
            document.body.appendChild(newPreloader);
            PAGES.settings.section_is_loading = true;
            PAGES.settings.section_is_open = false;
            let sectionPreloader = newPreloader;
            let sectionPreloaderPath = document.getElementById('new-path-preloader');
            TweenMax.set(sectionPreloader, {
                autoAlpha: 1,
            });
            if (UI.settings.menu_is_open === true) {
                TweenMax.to(sectionPreloader, .75, {
                    transform: 'translateY(0vh)',
                    ease: Quint.easeIn,
                    delay: .15,
                    onComplete: function() {
                        PAGES.removeSectionContent();
                        PAGES.loadSection(url, target, trackingurl, sectionPreloader);
                    }
                });
            } else {
                TweenMax.to(sectionPreloader, .75, {
                    transform: 'translateY(0vh)',
                    ease: Quint.easeIn,
                    delay: .05,
                    onComplete: function() {
                        PAGES.removeSectionContent();
                        PAGES.loadSection(url, target, trackingurl, sectionPreloader);
                    }
                });
            }
            UI.closeMenu();
            UI.closeCarousel();
        },
        createSlideShape: function(url, target, trackingurl) {
            let newPreloader = document.createElementNS("http://www.w3.org/2000/svg", "svg");
            let newRect = document.createElementNS(newPreloader.namespaceURI, 'rect');
            newRect.setAttribute('width', '100%');
            newRect.setAttribute('height', '100%');
            newPreloader.setAttribute('width', '100');
            newPreloader.setAttribute('height', '100');
            newPreloader.setAttribute('id', 'section-preloader' + UI.uniqueID());
            newPreloader.setAttribute("class", "section-preloader--slide");
            newPreloader.appendChild(newRect);
            document.body.appendChild(newPreloader);
            sections.settings.section_is_loading = true;
            sections.settings.section_is_open = false;
            let sectionPreloader = newPreloader;
            TweenMax.set(sectionPreloader, {
                autoAlpha: 1,
            });
            TweenMax.to(sectionPreloader, 1, {
                transform: 'translateX(0vw)',
                ease: Expo.easeIn,
                onComplete: function() {
                    //sections.removeSectionContent();
                    //sections.loadSection(url, target, trackingurl, sectionPreloader);
                    sections.showCoverQuickly(sectionPreloader);
                    sections.removeSectionContent();
                    //sections.fadeAndRemoveSectionContent();
                }
            });
        },
        removeSweepShape: function(sectionPreloader) {
            TweenMax.to(sectionPreloader, 1, {
                transform: 'translateY(-100vh)',
                ease: Quint.easeOut,
                overwrite: 'all',
                delay: 0.15,
                onComplete: function() {
                    sectionPreloader.remove();
                }
            });
            /*
            var sectionPreloaderPath = document.getElementById('new-path-preloader');
            TweenMax.to(sectionPreloaderPath, 1.25, {
                attr: {
                    d: "M1920,1080c0,0-225.16,0-959.08,0S0,1080,0,1080V0c0,0,256.66,0,960.92,0S1920,0,1920,0V1080z",
                },
                ease: Expo.easeOut,
            });
            //*/
        },
        removeSlideShape: function(sectionPreloader) {
            TweenMax.to(sectionPreloader, 1.5, {
                transform: 'translateX(100vw)',
                ease: Expo.easeOut,
                overwrite: 'all',
                delay: 0.15,
                onComplete: function() {
                    sectionPreloader.remove();
                }
            });
            /*
            var sectionPreloaderPath = document.getElementById('new-path-preloader');
            TweenMax.to(sectionPreloaderPath, 1.25, {
                attr: {
                    d: "M1920,1080H960.92H0V0c0,0,256.66,0,960.92,0S1920,0,1920,0V1080z",
                },
                ease: Expo.easeOut,
            });
            //*/
        },
        removeSectionContent: function() {
            let new_section = document.getElementById('new-section');
            if (new_section) {
                PAGES.houseCleaning(new_section);
                //new_section.remove();
            }
        },
        fadeAndRemoveSectionContent: function() {
            let new_section = document.getElementById('new-section');
            if (new_section) {
                TweenMax.to(new_section, .75, {
                    autoAlpha: 0,
                    ease: Expo.easeInOut,
                    onComplete: function() {
                        sections.houseCleaning(new_section);
                    }
                });
            }
        },
        /**
         *----------------------------------------------------------------------
         *
         * ::LOAD NEW SECTIONS IN
         *
         *----------------------------------------------------------------------
         */
        loadSection: function(url, target, trackingurl, sectionPreloader) {
            PAGES.removeSectionContent();
            fetch(url, {
                headers: new Headers({
                    'x-kirby-fetch': 'yes'
                })
            }).then(function(response) {
                return response.text();
            }).then(function(body) {
                let container = document.querySelector(target);
                container.innerHTML = body;
                let new_section = document.getElementById('new-section');
                let new_title = new_section.dataset.title;
                if (pages.settings.it_started_from_a_section) {
                    if (as.debug) {
                        //console.log("THIS SHOULD CHANGE JUST ONCE!");
                    }
                    document.title = new_title + ' - ' + as.site_title;
                    pages.settings.it_started_from_a_section = false;
                }
                pages.settings.section_is_loading = false;
                pages.settings.section_is_open = true;
                if (UI.settings.locoScroll) {
                    UI.settings.locoScroll.destroy();
                    UI.initLocomotiveScroll();
                }
                let coverCall = function() {
                    UI.settings.locoScroll.update()
                }
                HELPERS.setActiveNav(UI.settings.main_nav, new_section.dataset.id);
                HELPERS.setActiveNav(UI.settings.main_fullscreen_nav, new_section.dataset.id);
                HELPERS.setActiveNav(UI.settings.footer_nav_list, new_section.dataset.id);
                HELPERS.cleanHoverAnimation(document.querySelectorAll('.js-hover-effect-item'));
                if (document.querySelectorAll('.js-hover-effect-item').length) {
                    HELPERS.startHoverEffect(document.querySelectorAll('.js-hover-effect-item'));
                    HELPERS.initHoverAnimation(document.querySelectorAll('.js-hover-effect-item'));
                }
                document.getElementById('carousel-container').classList.remove('cis__services__carousel--section-active');
                switch (new_section.dataset.template) {
                    case 'cover':
                        if (PAGES.settings.use_transition) {
                            PAGES.removeSweepShape(sectionPreloader);
                        }
                        //TweenMax.delayedCall(1.5, coverCall);
                        //UI.initCarousel();
                        //UI.initHoverListener();
                        HELPERS.initIntro();
                        //UI.resizeHandler();
                        HELPERS.servicesModuleImage();
                        break;
                    case 'servicios':
                        if (PAGES.settings.use_transition) {
                            PAGES.removeSweepShape(sectionPreloader);
                        }
                        //document.getElementById('carousel-container').classList.add('cis__services__carousel--section-active');
                        //TweenMax.delayedCall(1.5, coverCall);
                        HELPERS.servicesModuleImage();
                        break;
                    case 'legal':
                        if (PAGES.settings.use_transition) {
                            PAGES.removeSweepShape(sectionPreloader);
                        }
                        //TweenMax.delayedCall(1.5, coverCall);
                        break;
                    default:
                        if (as.debug) {
                            //console.log('HOWDY');
                        }
                        if (PAGES.settings.use_transition) {
                            PAGES.removeSweepShape(sectionPreloader);
                        }
                        //TweenMax.delayedCall(1.5, coverCall);
                }
                HELPERS.fitImages('.fit-into-parent');

                /**
                 *----------------------------------------------------------------------
                 *
                 * ::TESTING
                 *
                 *----------------------------------------------------------------------
                 */
                 
                

                //HELPERS.checkImagesLoadedAndRefreshLocomotiveScroll();
                /**
                 *----------------------------------------------------------------------
                 *
                 * ::CLEAN OBSERVERS
                 *
                 *----------------------------------------------------------------------
                 */
                if (HELPERS.settings.observer_array) {
                    HELPERS.unobserveAll('.js-observe');
                }
                if (HELPERS.settings.nav_observer) {
                    HELPERS.unobserveNav('.js-nav-observe');
                }
                //let observeEffects = new HELPERS.initObserver('.js-observe');
                let observeNav = new HELPERS.initNavObserver('.js-nav-observe');
                //*
                if (as.tracking_code.google != '' && !as.local_copy) {
                    GOOGLEANALYTICS.gaTrack(trackingurl);
                }
                if (as.tracking_code.matomo == true && as.local_copy == false) {
                    MATOMO.matTrack(trackingurl, new_title + ' - ' + as.site_title);
                }
                //*/
            });
        },
        /**
         *----------------------------------------------------------------------
         *
         * ::CLEAN EVERYTHING UP
         *
         *----------------------------------------------------------------------
         */
        houseCleaning: function(new_section) {
            if (new_section) {
                switch (new_section.dataset.template) {
                    case 'cover':
                        /*
                        if (UI.settings.flkty) {
                            UI.settings.flkty.destroy();
                        }
                        UI.removeResizeEvent();
                        //*/
                        //new_section.remove();
                        //alert("COVER CLEANING!");
                        break;
                    case 'programs':
                        new_section.remove();
                        //alert("PROGRAMS CLEANING!");
                        break;
                    default:
                        new_section.remove();
                }
            }
        },
    }
    return pages;
}(PAGES || {}));