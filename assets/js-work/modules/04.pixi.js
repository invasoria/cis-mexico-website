var PIXIJS = (function(pixijs) {
    pixijs = {
        settings: {
            pixi_init: false,
            pixi_is_running: '',
            playground: '',
            canvas: '',
            app: '',
            graphics: '',
            graphics_tween: '',
            ratio: 1920 / 1080,
            count: 0,
            raf: '',
            renderer: '',
            tp: '',
            preview: '',
            displacement_sprite: '',
            displacementFilter: '',
            stage: '',
            fit_watch: '',
            container_width: '',
            container_height: '',
            pixi_top_container: document.getElementById('pixi-top-container'),
            pixi_bottom_container: document.getElementById('pixi-bottom-container'),
            fit_watch_bottom_texture: '',
            bottom: '',
            texture_bottom_1: '',
            texture_bottom_2: '',
            to_hide: document.getElementById('no-js-image'),
            bg_app: '',
            bg_app_fit_watch: '',
            bg_app_container: document.getElementById('canvas-draw'),
        },
        setPixi: function() {
            pixijs.settings.container_width = pixijs.settings.pixi_top_container.clientWidth + 12;
            pixijs.settings.container_height = pixijs.settings.pixi_top_container.clientHeight + 10;
            pixijs.settings.playground = pixijs.settings.pixi_top_container;
            pixijs.settings.renderer = PIXI.autoDetectRenderer(pixijs.settings.container_width, pixijs.settings.container_height, {
                transparent: true,
            });
            pixijs.settings.renderer.autoResize = true;
            pixijs.settings.bg_app = new PIXI.Application({
                autoResize: true,
                resolution: devicePixelRatio,
                transparent: true,
            });
            const app = pixijs.settings.bg_app;
            pixijs.settings.bg_app_container.appendChild(app.view);
            // Lets create a red square, this isn't 
            // necessary only to show something that can be position
            // to the bottom-right corner
            const rect = new PIXI.Graphics().beginFill(0xff0000).drawRect(-100, -100, 100, 100);
            // Add it to the stage
            app.stage.addChild(rect);
            rect.position.set(app.screen.width, app.screen.height);
            // Listen for window resize events
            /*
            window.addEventListener('resize', resize);
            // Resize function window
            function resize() {
                // Resize the renderer
                app.renderer.resize(document.getElementById('main').offsetWidth, document.getElementById('main').offsetHeight);
                // You can use the 'screen' property as the renderer visible
                // area, this is more useful than view.width/height because
                // it handles resolution
                rect.position.set(app.screen.width, app.screen.height);
            }
            resize();
            */
            pixijs.settings.bg_app_fit_watch = fit(app.view, pixijs.settings.bg_app_container, {
                hAlign: fit.CENTER,
                vAlign: fit.CENTER,
                cover: true,
                watch: true,
                apply: true
            });
        },
        setPixiScene: function(url) {
            pixijs.settings.count = 0;
            pixijs.settings.playground.appendChild(pixijs.settings.renderer.view);
            pixijs.settings.stage = new PIXI.Container();
            pixijs.settings.tp = PIXI.Texture.fromImage(url);
            pixijs.settings.tp.crossOrigin = typeof crossorigin === 'string' ? crossorigin : 'anonymous';
            pixijs.settings.preview = new PIXI.Sprite(pixijs.settings.tp);
            pixijs.settings.preview.anchor.x = 0;
            pixijs.settings.preview.width = pixijs.settings.container_width;
            pixijs.settings.preview.height = pixijs.settings.container_height;
            if (as.local_copy != false) {
                pixijs.settings.displacement_sprite = PIXI.Sprite.fromImage('http://localhost:8848/assets/images/map_original.png');
            } else {
                pixijs.settings.displacement_sprite = PIXI.Sprite.fromImage(as.site_root + '/assets/images/map_original.png');
            }
            pixijs.settings.displacement_sprite.texture.baseTexture.wrapMode = PIXI.WRAP_MODES.REPEAT;
            pixijs.settings.displacementFilter = new PIXI.filters.DisplacementFilter(pixijs.settings.displacement_sprite);
            pixijs.settings.displacement_sprite.scale.y = 1.5;
            pixijs.settings.displacement_sprite.scale.x = 1.5;
            pixijs.settings.stage.addChild(pixijs.settings.displacement_sprite);
            pixijs.settings.stage.addChild(pixijs.settings.preview);
            pixijs.settings.fit_watch = fit(pixijs.settings.renderer.view, pixijs.settings.pixi_top_container, {
                hAlign: fit.CENTER,
                vAlign: fit.CENTER,
                cover: true,
                watch: true,
                apply: true
            });
            var app = new PIXI.Application(pixijs.settings.container_width, pixijs.settings.container_height, {
                transparent: true,
            });
            pixijs.settings.pixi_bottom_container.appendChild(app.view);
            var cachebuster = Math.round(new Date().getTime() / 1000);
            pixijs.settings.texture_bottom_1 = PIXI.Texture.fromImage(as.site_root + '/assets/images/alexia-01.jpg' + '?v=' + cachebuster);
            pixijs.settings.texture_bottom_2 = PIXI.Texture.fromImage(as.site_root + '/assets/images/alexia-02.jpg' + '?v=' + cachebuster);
            // create a new Sprite using the texture
            pixijs.settings.bottom = new PIXI.Sprite(pixijs.settings.texture_bottom_1);
            //pixijs.settings.bottom.anchor.x = 0;
            pixijs.settings.bottom.anchor.set(0);
            pixijs.settings.bottom.width = pixijs.settings.container_width;
            pixijs.settings.bottom.height = pixijs.settings.container_height;
            //app.width = pixijs.settings.container_width;
            //app.height = pixijs.settings.container_height;
            app.stage.addChild(pixijs.settings.bottom);
            pixijs.settings.fit_watch_bottom_texture = fit(app.view, pixijs.settings.pixi_bottom_container, {
                hAlign: fit.CENTER,
                vAlign: fit.CENTER,
                cover: true,
                watch: true,
                apply: true
            });
            PIXIJS.settings.to_hide.style.visibility = "hidden";
            // Resize to Parent Node and Fullscreen
            // https://github.com/pixijs/pixi.js/issues/4731 
            /*
            pixijs.settings.app = new PIXI.Application();
            pixijs.settings.graphics = new PIXI.Graphics();
            //pixijs.settings.graphics.lineStyle(1, '0x' + pixijs.settings.dataArray[pixijs.settings.flkty.selectedIndex]['color'], 1);
            pixijs.settings.graphics.lineStyle(2, '0x000000', 1);
            pixijs.settings.graphics.drawCircle(0, 0, 200);
            pixijs.settings.graphics.x = 960;
            pixijs.settings.graphics.y = 540;
            pixijs.settings.stage.addChild(pixijs.settings.graphics);
            var getMousePosition = function getMousePosition() {
                return pixijs.settings.renderer.plugins.interaction.mouse.global;
            };
            var xVelocity = 0.1;
            var yVelocity = 0.1;
            var mousePosition = getMousePosition();
            var speed = 10;
            var angle = 45;
            pixijs.settings.app.ticker.add(function() {
                mousePosition = getMousePosition();
                var dx = mousePosition.x - pixijs.settings.graphics.x;
                var dy = mousePosition.y - pixijs.settings.graphics.y;
                //angle = Math.atan2(dy, dx);
                //xVelocity = Math.cos(angle) * speed;
                //yVelocity = Math.sin(angle) * speed;
                console.log(dy);
                //pixijs.settings.graphics.x += xVelocity;
                //pixijs.settings.graphics.y += yVelocity;
                
                pixijs.settings.graphics_tween = new TweenMax.to(pixijs.settings.graphics, 2, {
                    pixi: {
                        x: dx,
                        y: dy,
                    }
                });
                
            });
            */
            pixijs.animatePixi();
            /*
            TweenMax.set(COVER.settings.pixi_container, {
                autoAlpha: 0,
                //scale: .5,
            });
            */
            /*
            TweenMax.to(pixijs.settings.playground, 3, {
                //opacity: 1,
                scale: 1,
                ease: Expo.easeOut,
                delay: .2,
            });
            */
            /*
            TweenMax.to(COVER.settings.pixi_container, 2, {
                autoAlpha: 1,
                //ease: Circ.easeOut,
                //x:100,
                delay: .1,
            });
            */
        },
        setNewPixiImage: function(url) {
            pixijs.removePixiTexture();
            pixijs.settings.count = 0;
            //pixijs.settings.playground.appendChild(pixijs.settings.renderer.view);
            //pixijs.settings.stage = new PIXI.Container();
            pixijs.settings.tp = PIXI.Texture.fromImage(url);
            pixijs.settings.tp.crossOrigin = typeof crossorigin === 'string' ? crossorigin : 'anonymous';
            pixijs.settings.preview = new PIXI.Sprite(pixijs.settings.tp);
            pixijs.settings.preview.anchor.x = 0;
            //pixijs.settings.stage.addChild(pixijs.settings.displacement_sprite);
            pixijs.settings.stage.addChild(pixijs.settings.preview);
            pixijs.settings.fit_watch = fit(pixijs.settings.renderer.view, COVER.settings.pixi_container, {
                hAlign: fit.CENTER,
                vAlign: fit.CENTER,
                cover: true,
                watch: true,
                apply: true
            });
            pixijs.animatePixi();
            TweenMax.set(COVER.settings.pixi_container, {
                autoAlpha: 0,
                //scale: .5,
            });
            TweenMax.to(COVER.settings.pixi_container, 2, {
                autoAlpha: 1,
                //ease: Circ.easeOut,
                delay: .25,
            });
        },
        removePixiTexture: function() {
            cancelAnimationFrame(PIXIJS.settings.raf);
            PIXIJS.settings.raf = null;
            PIXIJS.settings.tp.destroy(true);
            PIXIJS.settings.preview.destroy(true);
            PIXIJS.settings.fit_watch = null;
        },
        pausePixiScene: function() {
            cancelAnimationFrame(PIXIJS.settings.raf);
            PIXIJS.settings.pixi_is_running = false;
        },
        resumePixiScene: function() {
            if (!PIXIJS.settings.pixi_is_running) {
                cancelAnimationFrame(PIXIJS.settings.raf);
            }
            PIXIJS.settings.raf = requestAnimationFrame(PIXIJS.animatePixi);
        },
        removePixiScene: function() {
            //if (pixijs.settings.stage != '' && pixijs.settings.stage != null && pixijs.settings.playground != null) {
            cancelAnimationFrame(pixijs.settings.raf);
            //pixijs.settings.graphics.destroy(true);
            //pixijs.settings.graphics = null;
            pixijs.settings.raf = null;
            pixijs.settings.tp.destroy(true);
            pixijs.settings.preview.destroy(true);
            pixijs.settings.displacement_sprite.destroy(true);
            pixijs.settings.stage.destroy(true);
            if (pixijs.settings.stage) {
                pixijs.settings.stage.removeChildren();
                pixijs.settings.stage = null;
            }
            pixijs.settings.renderer.destroy(true);
            pixijs.settings.renderer = null;
            //pixijs.settings.app.ticker.update();
            //pixijs.settings.graphics_tween.kill();
            //pixijs.settings.app.destroy(true);
            //pixijs.settings.app = null;
            //if (pixijs.settings.playground.hasChildNodes()) {
            //pixijs.settings.playground.removeChild(pixijs.settings.canvas);
            //}
            //pixijs.settings.playground = null;
            pixijs.settings.fit_watch = null;
            //}
            PIXIJS.settings.pixi_is_running = false;
        },
        animatePixi: function() {
            pixijs.settings.raf = requestAnimationFrame(pixijs.animatePixi);
            var filter_scale = 18;
            if (!UI.settings.open_project_hover) {
                TweenMax.to(pixijs.settings.displacementFilter, .75, {
                    pixi: {
                        scaleX: filter_scale,
                        scaleY: filter_scale,
                    },
                    //ease: Power2.easeInOut
                })
                /*
                if (pixijs.settings.displacementFilter.scale.x < 500 && pixijs.settings.displacementFilter.scale.y < 500) {
                    pixijs.settings.displacementFilter.scale.x += 5;
                    pixijs.settings.displacementFilter.scale.y += 5;
                } else {
                    pixijs.settings.displacementFilter.scale.x = 500;
                    pixijs.settings.displacementFilter.scale.y = 500;
                }
                */
            } else {
                TweenMax.to(pixijs.settings.displacementFilter, .5, {
                    pixi: {
                        scaleX: 20,
                        scaleY: 20,
                    },
                })
                /*
                if (pixijs.settings.displacementFilter.scale.x > 10) {
                    pixijs.settings.displacementFilter.scale.x -= 5;
                    pixijs.settings.displacementFilter.scale.y -= 10;
                }
                //pixijs.settings.displacementFilter.scale.x -= 10;
                //pixijs.settings.displacementFilter.scale.y -= 10;
                */
            }
            pixijs.settings.displacement_sprite.x = pixijs.settings.count * 20;
            pixijs.settings.displacement_sprite.y = pixijs.settings.count * 20;
            pixijs.settings.count += 0.05;
            pixijs.settings.stage.filters = [pixijs.settings.displacementFilter];
            pixijs.settings.renderer.render(pixijs.settings.stage);
            pixijs.settings.canvas = pixijs.settings.playground.querySelector('canvas');
        },
        placePixiImageOnStage: function() {
            

            function onClick(event) {
                sprite.scale.x = event.data.global.x / (app.screen.width / 2);
                sprite.scale.y = event.data.global.y / (app.screen.height / 2);
            }
        },
    }
    return pixijs;
}(PIXIJS || {}));