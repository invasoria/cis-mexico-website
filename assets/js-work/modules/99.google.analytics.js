var GOOGLEANALYTICS = (function(gas) {
    gas = {
        init: function(code) {
            //*
            if (code != '') {
                if (!as.local_copy) {
                    gas.gaTracker(code);
                    if (as.debug) {
                        console.log("We are using Google Analytics. Tracking the following code: " + code);
                    }
                }
            } else {
                if (as.debug) {
                    console.log("We are not using Google Analytics.");
                }
            }
            //*/
        },
        gaTracker: function(id) {
            // usage:
            HELPERS.getScript('https://www.googletagmanager.com/gtag/js?id=' + id, function() {
                window.dataLayer = window.dataLayer || [];

                function gtag() {
                    //window.dataLayer.push(arguments);
                    dataLayer.push(arguments);
                }
                gtag('js', new Date());
                gtag('config', id);
                console.log("We are using Google Analytics. Tracking the following code: " + id);
            });
        },
        gaTrack: function(path, title) {
            /*
            ga('set', {
                page: path,
                title: title
            });
            ga('send', 'pageview');
            //*/
            /*
            ga('set', 'page', '/new-page.html');
            ga('send', 'pageview');
            //*/
            window.dataLayer.push({
                event: 'pageview',
                page: {
                    path: path,
                    title: title,
                }
            });
            if (as.debug) {
                console.log('Tracking with Google Analytics: ' + path);
            }
        },
    }
    return gas;
}(GOOGLEANALYTICS || {}));