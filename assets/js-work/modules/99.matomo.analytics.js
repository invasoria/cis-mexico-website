var MATOMO = (function(mat) {
    mat = {
        init: function() {
            //*
            if (!as.local_copy) {
                MATOMO.matTracker();
                if (as.debug) {
                    console.log("We are using Matomo Analytics.");
                }
            } else {
                if (as.debug) {
                    console.log("We are not using Google Analytics.");
                }
            }
            //*/
        },
        matTracker: function(id) {
            var _paq = window._paq = window._paq || [];
            /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
            //_paq.push(['trackPageView']);
            //_paq.push(['enableLinkTracking']);
            var u = "//estonoesradio.mx/matomo/";
            _paq.push(['setTrackerUrl', u + 'matomo.php']);
            _paq.push(['setSiteId', '1']);
            var d = document,
                g = d.createElement('script'),
                s = d.getElementsByTagName('script')[0];
            g.type = 'text/javascript';
            g.async = true;
            g.src = u + 'matomo.js';
            s.parentNode.insertBefore(g, s);
        },
        matTrack: function(path, title) {
            //var currentUrl = location.href;
            //_paq.push(['setReferrerUrl', currentUrl]);
            //currentUrl = '/' + window.location.hash.substr(1);
            //currentUrl = path;
            _paq.push(['setCustomUrl', path]);
            _paq.push(['setDocumentTitle', title]);
            // remove all previously assigned custom variables, requires Matomo (formerly Piwik) 3.0.2
            _paq.push(['deleteCustomVariables', 'page']);
            _paq.push(['trackPageView']);
            // make Matomo aware of newly added content
            /*
            var content = document.getElementById('content');
            _paq.push(['MediaAnalytics::scanForMedia', content]);
            _paq.push(['FormAnalytics::scanForForms', content]);
            _paq.push(['trackContentImpressionsWithinNode', content]);
            //*/
            _paq.push(['enableLinkTracking']);
            if (as.debug) {
                console.log("---------------------");
                console.log("Tracking with Matomo: " + path);
                console.log("New Title: " + title);
            }
        },
        matTrackExternalLink: function(link) {
            _paq.push(['trackLink', link, 'link']);
            console.log("User clicked on a link and will visit externally: " + link);
        },
        /**
         *----------------------------------------------------------------------
         *
         * ::OPT OUT LOGIC
         *
         *----------------------------------------------------------------------
         */
        initOptOut: function() {
            var optOut = document.getElementById("optout");
            if (optOut) {
                if (as.tracking_code.matomo == true && as.local_copy == false) {
                    optOut.addEventListener("click", function() {
                        if (this.checked) {
                            _paq.push(['forgetUserOptOut']);
                        } else {
                            _paq.push(['optUserOut']);
                        }
                        MATOMO.setOptOut(optOut);
                    });
                    MATOMO.setOptOut(optOut);
                    //alert("DONE")
                }
            }
        },
        setOptOut: function(element) {
            _paq.push([function() {
                element.checked = !this.isUserOptedOut();
                document.querySelector('label[for=optout] strong').innerText = this.isUserOptedOut() ? 'En este momento no procesamos ningún dato tuyo. Haz clic aquí para participar.' : 'En este momento sí procesamos tu información. Haz click aquí para no participar.';
            }]);
        },
    }
    return mat;
}(MATOMO || {}));