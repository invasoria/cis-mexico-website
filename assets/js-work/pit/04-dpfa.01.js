var DPFA = (function(dpfa) {
    dpfa = {
        settings: {
            document: document,
            items: document.getElementById('pancha-data'),
            currentValue: document.getElementById('artist'),
            videoOnTop: document.getElementById('video-top'),
            queue: {},
            videoOnBottom: '',
            currentValueIndex: 0,
            valuesArray: [],
            valuesArrayPosition: 0,
            contentArray: [],
            contentArrayPosition: 0,
            currentProject: '',
            homeSpace: {},
            currentSeek: 0,
            seek: 0,
        },
        init: function() {
            var settings = dpfa.settings;
            var s = settings;
            DPFA.preLoadAssets();
            //DPFA.getItems(s);
            //DPFA.initTimebar();
            //DPFA.initInteractiveItems();
            //alert("DPFA!");
            /*
            var app = new PIXI.Application(1920, 1080, {
                forceCanvas: true,
            });
            document.getElementById("canvas").appendChild(app.view);
            var container = new PIXI.Container();
            app.stage.addChild(container);
            var video = document.createElement('video');
            video.src = 'http://localhost:7887/assets/video/lisiada.mp4';
            video.crossOrigin = 'Anonymous';
            video.muted = true;
            var texture = PIXI.Texture.fromVideo(video);
            var sprite = new PIXI.Sprite(texture);
            container.addChild(sprite);
            var videoSource = sprite.texture.baseTexture.source;
            sprite.interactive = true;
            */
            /*
            sprite.on("pointermove", function(event) {
                var mousePosition = event.data.global;
                videoSource.currentTime = videoSource.duration * (mousePosition.x / window.innerWidth);
            })
            */
        },
        getItems: function(s) {
            s.valuesArray = s.items.getAttribute('data-artists').split(",");
            for (var i = 0; i < s.valuesArray.length; i++) {
                s.valuesArray
            }
            console.log("-");
            console.log("Our Artists:");
            //console.log('Current Site: ' + ' ' + $('#items-container').data('site'));
            for (var i = 0; i < s.valuesArray.length; i++) {
                var currentItem = document.getElementById('artist-info-' + i);
                s.contentArray[i] = {
                    artist: currentItem.dataset.artist,
                    in : currentItem.dataset.in,
                    out: currentItem.dataset.out,
                    video: currentItem.dataset.video,
                }
                console.log("[" + i + "] - " + s.valuesArray[i]);
            }
            console.log("-");
            console.log("HOME");
            //console.log(s.contentArray);
            //console.log(s.siteURL + s.siteLang + s.contentArray[0]['path']);
        },
        /*****************************************
           Preload assets
          ******************************************/
        preLoadAssets: function() {
            // create queue
            DPFA.settings.queue = new createjs.LoadQueue();
            var videosTarget = null;
            DPFA.settings.queue.on("complete", DPFA.handleComplete, this);
            // create manifest for files to load
            DPFA.settings.queue.loadManifest([
              {id: 'lisiada',src: 'assets/video/test/boucle1.mp4',type: createjs.Types.BINARY},
              {id: 'artist_1',src: 'assets/video/test/color.mp4',type: createjs.Types.BINARY},
              ]);
            // handle  & show progress
            DPFA.settings.queue.on("progress", function(evt) {
                var p = DPFA.settings.queue.progress * 100;
                document.querySelector('.preload-counter').textContent = " " + Math.round(p) + "%";
            });
        },
        handleComplete: function() {
            var videosTarget = DPFA.settings.queue.getResult("lisiada");
            var video = DPFA.settings.document.createElement('video');
            var elem = document.getElementById('video-bg');
            elem.innerHTML = '';
            var src = videosTarget;
            var blob = new Blob([src], {
                type: "video/mp4"
            });
            var urlCreator = window.URL || window.webkitURL;
            var objUrl = urlCreator.createObjectURL(blob);
            video.id = 'video-bottom';
            video.src = objUrl;
            video.autoplay = true;
            video.loop = true;
            //video.muted = true;
            //video.poster = '';
            video.setAttribute('webkit-playsinline', 'webkit-playsinline');
            video.setAttribute('playsinline', 'playsinline');
            elem.appendChild(video);
            DPFA.settings.videoOnBottom = document.getElementById('video-bottom');
            var settings = dpfa.settings;
            var s = settings;
            DPFA.getItems(s);
            DPFA.initTimebar();
            DPFA.initInteractiveItems();
            /*
        
            <video class="none" id="video-bottom" webkit-playsinline="webkit-playsinline" playsinline="playsinline" autoplay="autoplay" loop="loop" muted="muted"<?php if($page->videoposter()!=''): ?> poster="<?php echo $page->file($page->videoposter())->url(); ?>"<?php endif ?>>
                <source src="<?php echo $site->url(); ?>/assets/video/lisiada.mp4" type="video/mp4">
            </video>

            // Insert Image
            var image = queue.getResult("myImage");
            $('.img-holder').append(image);

            // Insert Video
            var videosTarget = queue.getResult("myVideo");
            var $video = $('<video controls muted autoplay />');
            var $source = $('<source type="video/mp4"/>');
            var src = videosTarget;
            var blob = new Blob( [ src ], { type: "video/mp4" } );
            var urlCreator = window.URL || window.webkitURL;
            var objUrl = urlCreator.createObjectURL( blob );
            $source.attr('src', objUrl);
            $video.append($source);
            $('.video-holder').append($video);
            */
        },
        changeVideos: function(){
          
        },
        initTimebar: function() {
            /*
            TweenMax.set(DPFA.settings.position, {
                width: "0%",
            });
            TweenMax.set(DPFA.settings.background, {
                width: "0%",
            });
            TweenMax.to(DPFA.settings.background, .75, {
                width: "100%",
                ease: Expo.easeInOut,
                delay: .15
            });
            TweenMax.to(DPFA.settings.position, .75, {
                width: "0%",
                ease: Expo.easeInOut,
                delay: .15
            });
            */
            var v = document.getElementsByTagName("video")[0];
            var duration = v.duration;
            v.addEventListener('timeupdate', function(event) {
                DPFA.settings.homeSpace.round = function(number, precision) {
                    var factor = Math.pow(10, precision);
                    var tempNumber = number * factor;
                    var roundedTempNumber = Math.round(tempNumber);
                    return roundedTempNumber / factor;
                };
                //console.log(DPFA.settings.homeSpace.round(v.currentTime, 1));
                if (DPFA.settings.homeSpace.round(v.currentTime, 1) >= DPFA.settings.contentArray[DPFA.settings.currentValueIndex]['in'] && DPFA.settings.homeSpace.round(v.currentTime, 1) < DPFA.settings.contentArray[DPFA.settings.currentValueIndex]['out']) {
                    //DPFA.settings.title.href = DPFA.settings.siteURL + DPFA.settings.siteLang + DPFA.settings.contentArray[i]['path'];
                    //console.log(DPFA.settings.currentValueIndex);
                    //console.log(DPFA.settings.valuesArray.length);
                    DPFA.changeValueText(DPFA.settings.currentValueIndex);
                } else {
                    DPFA.changeValueIndex();
                }
                DPFA.settings.currentSeek = DPFA.settings.homeSpace.round(v.currentTime, 1);
                DPFA.settings.seek = (DPFA.settings.homeSpace.round(v.currentTime, 1) * 100) / duration;
                //console.log(DPFA.settings.seek);
            }, true);
            v.addEventListener('ended', function(event) {
                console.log("FINISHED!");
                v.play();
            }, true);
        },
        changeValueText: function(i) {
            DPFA.settings.currentValue.innerHTML = DPFA.settings.contentArray[i]['artist'];
            DPFA.settings.currentValue.dataset.video = DPFA.settings.contentArray[i]['video'];
        },
        changeValueIndex: function() {
            if (DPFA.settings.currentValueIndex < DPFA.settings.valuesArray.length - 1) {
                DPFA.settings.currentValueIndex++;
            } else {
                DPFA.settings.currentValueIndex = 0;
            }
        },
        initInteractiveItems: function() {
            DPFA.settings.currentValue.addEventListener('mousedown', DPFA.artistMouseDownHandler);
            DPFA.settings.currentValue.addEventListener('mouseup', DPFA.artistMouseUpOutHandler);
            DPFA.settings.currentValue.addEventListener('mouseout', DPFA.artistMouseUpOutHandler);
            DPFA.settings.currentValue.addEventListener("touchstart", DPFA.artistMouseDownHandler, false);
            DPFA.settings.currentValue.addEventListener("touchend", DPFA.artistMouseUpOutHandler, false);
            DPFA.settings.currentValue.addEventListener("touchcancel", DPFA.artistMouseUpOutHandler, false);
        },
        artistMouseDownHandler: function(e) {
            var clickedItem = e.target;
            //alert(clickedItem.dataset.video);
            var video = DPFA.settings.document.createElement('video');
            var elem = DPFA.settings.videoOnTop;
            var videoBottom = DPFA.settings.videoOnBottom;
            elem.innerHTML = '';
            //video.className += ' insert-video--done';
            video.src = clickedItem.dataset.video;
            video.autoplay = true;
            video.loop = true;
            //video.muted = true;
            //video.poster = '';
            video.setAttribute('webkit-playsinline', 'webkit-playsinline');
            video.setAttribute('playsinline', 'playsinline');
            elem.appendChild(video);
            videoBottom.pause();
            e.stopPropagation();
            e.preventDefault();
        },
        artistMouseUpOutHandler: function(e) {
            var elem = DPFA.settings.videoOnTop;
            var videoBottom = DPFA.settings.videoOnBottom;
            videoBottom.play();
            elem.innerHTML = '';
            e.stopPropagation();
            e.preventDefault();
        },
    }
    return dpfa;
}(DPFA || {}));
/*

let ww = window.innerWidth;
let wh = window.innerHeight;
const options = {
  bg: {
    src: "https://res.cloudinary.com/dheeu8pj9/image/upload/v1540179278/HvSDhSJ.jpg",
    // src: "https://res.cloudinary.com/dheeu8pj9/image/upload/v1540179383/frida.jpg",
    ratio: 1080 / 1661
  },
  displacementMap: {
    intensity: 40,
    mouseDelay: 0.05,
    speed: 1,
    size: {
      height: 300,
      width: 300,
    },
    src: "https://res.cloudinary.com/dheeu8pj9/image/upload/v1540179223/filter_NRM.jpg",
    wrapMode: PIXI.WRAP_MODES.REPEAT
  }
};

const renderer = new PIXI.autoDetectRenderer({
  width: window.innerWidth,
  height: window.innerHeight
});

document.querySelector("#root").appendChild(renderer.view);

const stage = new PIXI.Container();

let bgResource = null;
let displacementMapResource = null;

const pixiLoader = new PIXI.loaders.Loader();
pixiLoader.add("bg", options.bg.src);
pixiLoader.add("displacementMap", options.displacementMap.src);
pixiLoader.load((loader, { bg, displacementMap }) => {
  displacementMapResource = new PIXI.Sprite(displacementMap.texture);
  displacementMapResource.width = options.displacementMap.size.width;
  displacementMapResource.height = options.displacementMap.size.height;
  displacementMapResource.texture.baseTexture.wrapMode =
    options.displacementMap.wrapMode;
  
  bgResource = new PIXI.Sprite(bg.texture);
  bgResource.anchor.x = 0.5;
  bgResource.anchor.y = 0.5;
  bgResource.x = (ww - 50) / 2
  bgResource.y = (wh - 50) / 2;
  bgResource.width = ww + 100;
  bgResource.height = bgResource.width * options.bg.ratio;
  bgResource.interactive = true;
  bgResource.filters = [
    new PIXI.filters.DisplacementFilter(
      displacementMapResource,
      options.displacementMap.intensity
    )
  ];
  stage.addChild(bgResource, displacementMapResource);
});

let oldX = 0;
let oldY = 0;
let currentY = 0;
let currentX = 0;

window.addEventListener("mousemove", e => {
  currentY = e.pageX;
  currentX = e.pageY;
});

const ticker = new PIXI.ticker.Ticker();
ticker.stop();
ticker.add(deltaTime => {
  const diffX = currentY - oldX;
  const diffY = currentX - oldY;

  if (displacementMapResource) {
    displacementMapResource.x =
      displacementMapResource.x +
      options.displacementMap.speed -
      diffX * options.displacementMap.mouseDelay;
    displacementMapResource.y =
      displacementMapResource.y -
      options.displacementMap.speed +
      diffY * options.displacementMap.mouseDelay;
  }

  oldX = oldX + diffX * options.displacementMap.mouseDelay;
  oldY = oldY + diffY * options.displacementMap.mouseDelay;
  renderer.render(stage);
});
ticker.start();

window.addEventListener("resize", e => {
  ww = window.innerWidth;
  wh = window.innerHeight;
  bgResource.width = ww + 100;
  bgResource.height = bgResource.width * options.bg.ratio;
  bgResource.x = (ww - 50) / 2
  bgResource.y = (wh - 50) / 2;
  renderer.resize(ww, wh);
});
*/