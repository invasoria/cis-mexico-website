var DPFA = (function(dpfa) {
    dpfa = {
        settings: {
            document: document,
            items: document.getElementById('pancha-data'),
            currentValue: document.getElementById('artist'),
            eye: document.getElementById('eye'),
            sign: document.getElementById('sign'),
            videoOnTop: document.getElementById('video-top'),
            section_uri: '',
            section_title: '',
            queue: {},
            videoOnBottom: '',
            currentValueIndex: 0,
            valuesArray: [],
            valuesArrayPosition: 0,
            contentArray: [],
            contentArrayPosition: 0,
            currentProject: '',
            homeSpace: {},
            sound: '',
            isActive: false,
        },
        init: function() {
            var settings = dpfa.settings;
            var s = settings;
            DPFA.getItems(s);
            DPFA.initIntro();
        },
        initIntro: function() {
            TweenMax.to(document.getElementById('intro'), 1.75, {
                autoAlpha: 1,
                ease: Expo.easeInOut,
                onComplete: function() {
                    DPFA.preloadFirst();
                }
            });
            TweenMax.to(DPFA.settings.sign, 20, {
                rotation: "360",
                ease: Linear.easeNone,
                repeat: -1,
            });
        },
        killIntro: function(e) {
            TweenMax.to(document.getElementById('intro'), 1.75, {
                autoAlpha: 0,
                ease: Expo.easeInOut,
                onComplete: function() {
                    document.getElementById('intro').remove();
                }
            });
            TweenMax.to(document.getElementById('dpfa-top-content'), 1.75, {
                autoAlpha: 1,
                ease: Expo.easeInOut,
                //delay: .25,
            });
            DPFA.playSound();
            e.preventDefault();
        },
        initIntroInteractiveItems: function() {
            document.getElementById('intro-ok').classList.remove('dpfa__is-loader');
            document.getElementById('intro-ok').addEventListener('click', DPFA.killIntro);
            document.getElementById('about-button').addEventListener('click', DPFA.makeLink);
        },
        getItems: function(s) {
            s.valuesArray = s.items.getAttribute('data-artists').split(",");
            for (var i = 0; i < s.valuesArray.length; i++) {
                s.valuesArray
            }
            //DPFA.shuffleArray(s.valuesArray);
            console.log("-");
            console.log("Our Artists:");
            //console.log('Current Site: ' + ' ' + $('#items-container').data('site'));
            for (var i = 0; i < s.valuesArray.length; i++) {
                var currentItem = document.getElementById('artist-info-' + i);
                s.contentArray[i] = {
                    artist: currentItem.dataset.artist,
                    in : currentItem.dataset.in,
                    out: currentItem.dataset.out,
                    video: currentItem.dataset.video,
                    video: currentItem.dataset.video,
                }
                console.log("[" + i + "] - " + s.valuesArray[i]);
            }
            console.log("-");
            console.log("HOME");
            //console.log(s.contentArray);
            //console.log(s.siteURL + s.siteLang + s.contentArray[0]['path']);
        },
        shuffleArray: function(array) {
            for (var i = array.length - 1; i > 0; i--) {
                var j = Math.floor(Math.random() * (i + 1));
                var temp = array[i];
                array[i] = array[j];
                array[j] = temp;
            }
        },
        preloadFirst: function() {
            // create queue
            DPFA.settings.queue = new createjs.LoadQueue();
            var videosTarget = null;
            DPFA.settings.queue.on("complete", DPFA.firstPlaceVideos, this);
            // create manifest for files to load
            DPFA.settings.queue.loadManifest([{
                id: 'lisiada_' + DPFA.settings.currentValueIndex,
                src: 'assets/video/lisiada_' + DPFA.settings.currentValueIndex + '.mp4',
                type: createjs.Types.BINARY
            }, {
                id: 'artist_' + DPFA.settings.currentValueIndex,
                src: DPFA.settings.contentArray[DPFA.settings.currentValueIndex]['video'],
                type: createjs.Types.BINARY
            }, ]);
            DPFA.settings.sound = new Howl({
                src: ['assets/video/audio/lisiada_' + DPFA.settings.currentValueIndex + '.mp3']
            });
            // handle  & show progress
            DPFA.settings.queue.on("progress", function(evt) {
                var p = DPFA.settings.queue.progress * 100;
                document.getElementById('intro-ok').textContent = " " + Math.round(p) + "%";
            });
        },
        firstPlaceVideos: function() {
            TweenMax.to(document.getElementById('video-bg'), 2.5, {
                autoAlpha: 1,
                ease: Expo.easeInOut,
                delay: .25,
            });
            //CHANGE OK BUTTON TEXT
            document.getElementById('intro-ok').textContent = "ENTENDIDO";
            DPFA.initIntroInteractiveItems();
            var videosTarget = DPFA.settings.queue.getResult('lisiada_' + DPFA.settings.currentValueIndex);
            var video = DPFA.settings.document.createElement('video');
            var elem = document.getElementById('video-bg');
            elem.innerHTML = '';
            var src = videosTarget;
            var blob = new Blob([src], {
                type: "video/mp4"
            });
            var urlCreator = window.URL || window.webkitURL;
            var objUrl = urlCreator.createObjectURL(blob);
            video.id = 'video-bottom';
            video.src = objUrl;
            //video.autoplay = true;
            video.loop = true;
            video.muted = true;
            video.setAttribute('webkit-playsinline', 'webkit-playsinline');
            video.setAttribute('playsinline', 'playsinline');
            elem.appendChild(video);
            DPFA.settings.videoOnBottom = document.getElementById('video-bottom');
            var settings = dpfa.settings;
            var s = settings;
            //
            // PUSH NEW ARTIST DATA INTO INTERACTIVE OBJECT
            //
            //DPFA.settings.currentValue.innerHTML = DPFA.settings.contentArray[DPFA.settings.currentValueIndex]['artist'];
            DPFA.initInteractiveItems();
            var videoBottom = DPFA.settings.videoOnBottom;
            videoBottom.play();
            DPFA.interactiveVideoUpdate();
        },
        playSound: function() {
            DPFA.settings.sound.play();
        },
        preloadNextVideos: function() {
            TweenMax.to(document.getElementById('preload-spin'), 1, {
                autoAlpha: 1,
                ease: Expo.easeInOut,
            });
            if (DPFA.settings.currentValueIndex < DPFA.settings.valuesArray.length - 1) {
                DPFA.settings.currentValueIndex++;
            } else {
                DPFA.settings.currentValueIndex = 0;
            }
            // create queue
            DPFA.settings.queue = new createjs.LoadQueue();
            var videosTarget = null;
            DPFA.settings.queue.on("complete", DPFA.placeNextVideos, this);
            // create manifest for files to load
            DPFA.settings.queue.loadManifest([{
                id: 'lisiada_' + DPFA.settings.currentValueIndex,
                src: 'assets/video/lisiada_' + DPFA.settings.currentValueIndex + '.mp4',
                type: createjs.Types.BINARY
            }, {
                id: 'artist_' + DPFA.settings.currentValueIndex,
                src: DPFA.settings.contentArray[DPFA.settings.currentValueIndex]['video'],
                type: createjs.Types.BINARY
            }, ]);
            DPFA.settings.sound = new Howl({
                src: ['assets/video/audio/lisiada_' + DPFA.settings.currentValueIndex + '.mp3']
            });
        },
        placeNextVideos: function() {
            TweenMax.to(document.getElementById('preload-spin'), 1, {
                autoAlpha: 0,
                ease: Expo.easeInOut,
                delay: 1,
            });
            var videosTarget = DPFA.settings.queue.getResult("lisiada_" + DPFA.settings.currentValueIndex);
            var video = DPFA.settings.document.createElement('video');
            var elem = document.getElementById('video-bg');
            elem.innerHTML = '';
            var src = videosTarget;
            var blob = new Blob([src], {
                type: "video/mp4"
            });
            var urlCreator = window.URL || window.webkitURL;
            var objUrl = urlCreator.createObjectURL(blob);
            video.id = 'video-bottom';
            video.src = objUrl;
            //video.autoplay = true;
            video.loop = true;
            video.muted = true;
            video.setAttribute('webkit-playsinline', 'webkit-playsinline');
            video.setAttribute('playsinline', 'playsinline');
            elem.appendChild(video);
            DPFA.settings.videoOnBottom = document.getElementById('video-bottom');
            var settings = dpfa.settings;
            var s = settings;
            var videoBottom = DPFA.settings.videoOnBottom;
            videoBottom.play();
            DPFA.interactiveVideoUpdate();
        },
        interactiveVideoUpdate: function() {
            /*
              DPFA.settings.videoOnBottom = document.getElementById('video-bottom');
              document.onmousemove = handleMouseMove.throttle(100);

              function handleMouseMove(event) {
                  var eventDoc, doc, body;
                  event = event || window.event; // IE-ism
                  // If pageX/Y aren't available and clientX/Y are,
                  // calculate pageX/Y - logic taken from jQuery.
                  // (This is to support old IE)
                  if (event.pageX == null && event.clientX != null) {
                      eventDoc = (event.target && event.target.ownerDocument) || document;
                      doc = eventDoc.documentElement;
                      body = eventDoc.body;
                      event.pageX = event.clientX + (doc && doc.scrollLeft || body && body.scrollLeft || 0) - (doc && doc.clientLeft || body && body.clientLeft || 0);
                      event.pageY = event.clientY + (doc && doc.scrollTop || body && body.scrollTop || 0) - (doc && doc.clientTop || body && body.clientTop || 0);
                  }
                  // Use event.pageX / event.pageY here
                  console.log(event.pageX);
                  DPFA.settings.videoOnBottom.pause();
                  DPFA.settings.videoOnBottom.currentTime = DPFA.settings.videoOnBottom.duration * (event.pageX / window.innerWidth);
                  TweenMax.to(DPFA.settings.videoOnBottom, .1, {
                      currentTime: DPFA.settings.videoOnBottom.duration * (event.pageX / window.innerWidth),
                      ease: Power0.easeNone,
                      //delay: .25
                  });
              };
              */
        },
        initInteractiveItems: function() {
            DPFA.settings.eye.addEventListener('mousedown', DPFA.artistMouseDownHandler);
            DPFA.settings.eye.addEventListener('mouseup', DPFA.artistMouseUpHandler);
            DPFA.settings.eye.addEventListener('mouseout', DPFA.artistMouseOutHandler);
            DPFA.settings.eye.addEventListener("touchstart", DPFA.artistMouseDownHandler, false);
            DPFA.settings.eye.addEventListener("touchend", DPFA.artistMouseUpHandler, false);
            DPFA.settings.eye.addEventListener("touchcancel", DPFA.artistMouseOutHandler, false);
        },
        artistMouseDownHandler: function(e) {
            var clickedItem = e.target;
            //alert(clickedItem.dataset.video);
            DPFA.settings.currentValue.innerHTML = DPFA.settings.contentArray[DPFA.settings.currentValueIndex]['artist'];
            var videosTarget = DPFA.settings.queue.getResult("artist_" + DPFA.settings.currentValueIndex);
            var video = DPFA.settings.document.createElement('video');
            var elem = DPFA.settings.videoOnTop;
            elem.innerHTML = '';
            var src = videosTarget;
            var blob = new Blob([src], {
                type: "video/mp4"
            });
            var urlCreator = window.URL || window.webkitURL;
            var objUrl = urlCreator.createObjectURL(blob);
            //alert(objUrl);
            video.src = objUrl;
            video.autoplay = true;
            video.loop = true;
            //video.muted = true;
            video.setAttribute('webkit-playsinline', 'webkit-playsinline');
            video.setAttribute('playsinline', 'playsinline');
            elem.appendChild(video);
            DPFA.settings.videoOnBottom = document.getElementById('video-bottom');
            var settings = dpfa.settings;
            var s = settings;
            var videoBottom = DPFA.settings.videoOnBottom;
            videoBottom.pause();
            DPFA.preloadNextVideos();
            DPFA.settings.isActive = true;
            e.stopPropagation();
            e.preventDefault();
        },
        artistMouseOutHandler: function(e) {
            var elem = DPFA.settings.videoOnTop;
            var videoBottom = DPFA.settings.videoOnBottom;
            videoBottom.play();
            if (DPFA.settings.isActive) {
              DPFA.playSound();
            }
            DPFA.settings.isActive = false;
            elem.innerHTML = '';
            DPFA.settings.currentValue.innerHTML = '';
            e.stopPropagation();
            e.preventDefault();
        },
        artistMouseUpHandler: function(e) {
            var elem = DPFA.settings.videoOnTop;
            var videoBottom = DPFA.settings.videoOnBottom;
            videoBottom.play();
            DPFA.playSound();
            DPFA.settings.isActive = false;
            elem.innerHTML = '';
            DPFA.settings.currentValue.innerHTML = '';
            e.stopPropagation();
            e.preventDefault();
        },
        makeLink: function(e) {
            var clickedItem = e.target;
            if (clickedItem.classList.contains('section-link') && PAGES.settings.section_is_loading == false) {
                //alert("Hello " + clickedItem);
                DPFA.settings.section_uri = clickedItem.dataset.uri;
                DPFA.settings.section_title = clickedItem.dataset.title;
                History.pushState({
                    url: DPFA.settings.section_uri,
                }, DPFA.settings.section_title + " - " + as.site_title, clickedItem.href);
            }
            e.preventDefault();
        }
    }
    return dpfa;
}(DPFA || {}));