var HELPERS = (function(helpers) {
    helpers = {
        settings: {
            //SETTINGS GO HERE
            //setting: option,
            split_heading_timeline: '',
            observer_array: '',
            nav_observer: '',
            services_images_triggers: '',
            services_images: '',
            references: {},
        },
        init: function() {
            var settings = helpers.settings;
            var s = settings;
        },
        isThisLocal: function() {
            if (document.location.hostname == as.local_hostname) {
                as.path = as.local_path;
                as.pathlast = as.local_pathlast;
                as.local_copy = true;
            } else if (document.location.hostname == as.live_hostname) {
                as.path = as.live_path;
                as.pathlast = as.live_pathlast;
                as.local_copy = false;
            }
            if (document.location.port == "8888" || document.location.port == as.prepros_port) {
                as.path = as.prepros_path;
                as.pathlast = as.prepros_pathlast;
                as.local_copy = 'prepros';
            }
        },
        /**
         *----------------------------------------------------------------------
         *
         * ::INIT GENERAL INTROS
         *
         *----------------------------------------------------------------------
         */
        initIntro: function() {
            //intro-element-animation
            let items = document.querySelectorAll('.js-intro-animation');
            let items_with_delay = document.querySelectorAll('.js-intro-animation-with-delay');
            if (items.length) {
                gsap.to(items, {
                    duration: .1,
                    ease: "Quint.easeInOut",
                    delay: .5,
                    autoAlpha: 1,
                    stagger: 0.1,
                    onComplete: function() {
                        items.forEach(function(element) {
                            if (element.classList.contains('js-disabled-anchor')) {
                                element.classList.remove('js-disabled-anchor');
                            }
                        });
                    }
                });
            }
            if (items_with_delay.length) {
                gsap.to(items_with_delay, {
                    duration: 2.5,
                    ease: "Quint.easeInOut",
                    delay: .5,
                    autoAlpha: 1,
                });
            }
        },
        /**
         *----------------------------------------------------------------------
         *
         * ::LAZY LOAD FOR IMAGES (LOZAD DEPENDENT)
         *
         *----------------------------------------------------------------------
         */
        initLazyImages: function(elements) {
            const el = elements;
            if (HELPERS.canUseWebP()) {
                const el_to_switch = document.querySelectorAll('.js-can-use-webp');
                for (var i = el_to_switch.length - 1; i >= 0; i--) {
                    if (el_to_switch[i].dataset.webp) {
                        let parentElement = el_to_switch[i];
                        let theFirstChild = parentElement.firstChild;
                        let newElement = document.createElement("source");
                        newElement.setAttribute("srcset", el_to_switch[i].dataset.webp);
                        // Insert the new element before the first child——that is, insert webp src before anything else so it gets used by the browser.
                        parentElement.insertBefore(newElement, theFirstChild)
                    }
                }
            }
            const observer = lozad(el, {
                rootMargin: "0px",
                threshold: 0,
                enableAutoReload: false,
                loaded: function(el) {
                    // Custom implementation on a loaded element
                    el.classList.add('loaded');
                    el.classList.add('--fade-in-and-transform');
                },
            });
            observer.observe();
        },
        /**
         *----------------------------------------------------------------------
         *
         * ::CHECK FOR WEBP CAPABILITY
         *
         *----------------------------------------------------------------------
         */
        canUseWebP: function() {
            var elem = document.createElement('canvas');
            if (!!(elem.getContext && elem.getContext('2d'))) {
                // was able or not to get WebP representation
                return elem.toDataURL('image/webp').indexOf('data:image/webp') == 0;
            }
            // very old browser like IE 8, canvas not supported
            return false;
        },
        /**
         * Format the time from seconds to M:SS.
         * @param  {Number} secs Seconds to format.
         * @return {String}      Formatted time.
         */
        formatTime: function(secs) {
            let minutes = Math.floor(secs / 60) || 0;
            let seconds = (secs - minutes * 60) || 0;
            return minutes + ':' + (seconds < 10 ? '0' : '') + seconds;
        },
        getScript(url, success) {
            var head = document.getElementsByTagName("head")[0],
                done = false;
            var script = document.createElement("script");
            script.src = url;
            // Attach handlers for all browsers
            script.onload = script.onreadystatechange = function() {
                if (!done && (!this.readyState || this.readyState == "loaded" || this.readyState == "complete")) {
                    done = true;
                    success();
                }
            };
            head.appendChild(script);
        },
        /**
         *----------------------------------------------------------------------
         *
         * ::PRIVACY BANNER
         *
         *----------------------------------------------------------------------
         */
        closePrivacyBanner: function() {
            let path = '';
            if (as.local_copy == true) {
                if (as.local_is_not_localhost) {
                    path = '/rest/privacy:true';
                }
            } else {
                path = '/rest/privacy:true';
            }
            fetch(path, {
                method: "GET",
                headers: new Headers({
                    'x-kirby-fetch': 'yes',
                })
                //*/
            }).then((response) => response.text()).then((text) => {
                //console.log(text);
                /*
                TweenMax.to(document.getElementById('privacy-banner'), 1, {
                    autoAlpha: 0,
                    ease: Expo.easeInOut,
                })
                */
            })
        },
        /**
         *----------------------------------------------------------------------
         *
         * ::SET ACTIVE SECTION IN NAVIGATION
         *
         *----------------------------------------------------------------------
         */
        setActiveNav: function(nav, template) {
            let active_nav_item = '.main-nav-' + template;
            nav.querySelectorAll('.main-nav-item').forEach(element => element.classList.remove('active-nav-item'));
            if (document.querySelectorAll(active_nav_item)) {
                let all_items = document.querySelectorAll(active_nav_item);
                for (var i = 0; i < all_items.length; i++) {
                    all_items[i].classList.add('active-nav-item');
                }
            }
        },
        /**
         *----------------------------------------------------------------------
         *
         * ::OBSERVER TOOLS
         *
         *----------------------------------------------------------------------
         */
        initObserver: function(elements, type) {
            const items = document.querySelectorAll(elements);
            const config = {
                //rootMargin: '0px 0px -50% 0px',
                rootMargin: '10px',
                threshold: 0.25,
            };
            HELPERS.settings.observer_array = new IntersectionObserver(items => {
                if (items[0].intersectionRatio !== 0) {
                    if (!items[0].target.classList.contains('hide-and-show-nav')) {
                        HELPERS.settings.observer_array.unobserve(items[0].target);
                        HELPERS.inView(items[0].target);
                    }
                } else {
                    HELPERS.outView(items[0].target);
                }
            }, config);
            //
            //
            //Assign effect type
            //
            //
            items.forEach(item => {
                if (item.classList.contains('reveal-text-animation')) {
                    item.childSplit = new SplitText(item, {
                        type: "words,chars,lines",
                        linesClass: "split-child",
                        //reduceWhiteSpace: false,
                    });
                    item.parentSplit = new SplitText(item, {
                        linesClass: "split-parent",
                        //reduceWhiteSpace: false,
                    });
                    gsap.set(item.childSplit.lines, {
                        yPercent: 108,
                        //autoAlpha: 0,
                    });
                } else if (item.classList.contains('reveal-block-animation')) {
                    item.classList.add('reveal-block--set');
                } else if (item.classList.contains('fade-animation')) {
                    item.classList.add('fade--set');
                } else if (item.classList.contains('fade-and-slide-animation')) {
                    item.classList.add('fade-in-and-slide--set');
                }
                HELPERS.settings.observer_array.observe(item);
            });
        },
        initNavObserver: function(elements, type) {
            const items = document.querySelectorAll(elements);
            const config = {
                //rootMargin: '0px 0px -50% 0px',
                rootMargin: '10px',
                threshold: .75,
            };
            //let myNavObserver = new IntersectionObserver(items => {
            HELPERS.settings.nav_observer = new IntersectionObserver(items => {
                if (items[0].intersectionRatio > .75) {
                    HELPERS.inScrollview(items[0].target);
                } else {
                    HELPERS.outView(items[0].target);
                }
            }, config);
            //
            //
            //Assign effect type
            //
            //
            items.forEach(item => {
                //myNavObserver.observe(item);
                HELPERS.settings.nav_observer.observe(item);
            });
        },
            /*
            if (item.classList.contains('hide-and-show-nav')) {
                UI.hideMenu();
            }
            */        
        unobserveAll: function(elements) {
            //console.log('UNOBSERVE ALL');
            let items = document.querySelectorAll(elements);
            items.forEach(item => {
                HELPERS.settings.observer_array.unobserve(item);
            })
        },
        unobserveNav: function(elements) {
            //console.log('UNOBSERVE NAV');
            let items = document.querySelectorAll(elements);
            items.forEach(item => {
                HELPERS.settings.nav_observer.unobserve(item);
            });
        },
        /**
         *----------------------------------------------------------------------
         *
         * ::TEXT EFFECTS
         *
         *----------------------------------------------------------------------
         */
        splitLineHeading: function() {
            /*
            HELPERS.settings.split_heading_timeline = gsap.timeline(),
                mySplitText = new SplitText("#quote", {
                    type: "words,chars"
                }),
                chars = mySplitText.chars; //an array of all the divs that wrap each character
            gsap.set("#quote", {
                perspective: 400
            });
            tl.from(chars, {
                duration: 0.8,
                opacity: 0,
                scale: 0,
                y: 80,
                rotationX: 180,
                transformOrigin: "0% 50% -50",
                ease: "back",
                stagger: 0.01
            }, "+=0");
            document.getElementById("animate").onclick = function() {
                tl.restart();
            }
            */
        },
        /**
         *----------------------------------------------------------------------
         *
         * ::UNIQUE ID
         *
         *----------------------------------------------------------------------
         */
        uniqueID: function() {
            // Math.random should be unique because of its seeding algorithm.
            // Convert it to base 36 (numbers + letters), and grab the first 9 characters
            // after the decimal.
            return '-' + Math.random().toString(36).substr(2, 9);
        },
        /**
         *----------------------------------------------------------------------
         *
         * ::APPEARING FX FUNCTIONS
         *
         *----------------------------------------------------------------------
         */
        setView: function(item) {
            if (item.classList.contains('reveal-text-animation')) {
                if (item.classList.contains('content__p')) {
                    const new_item = item.querySelector('p');
                    new_item.childSplit = new SplitText(new_item, {
                        type: "words,chars,lines",
                        linesClass: "split-child",
                        //reduceWhiteSpace: false,
                    });
                    new_item.parentSplit = new SplitText(new_item, {
                        linesClass: "split-parent",
                        //reduceWhiteSpace: false,
                    });
                    gsap.set(new_item.childSplit.lines, {
                        yPercent: 108,
                        //autoAlpha: 0,
                    });
                } else {
                    item.childSplit = new SplitText(item, {
                        type: "words,chars,lines",
                        linesClass: "split-child",
                        //reduceWhiteSpace: false,
                    });
                    item.parentSplit = new SplitText(item, {
                        linesClass: "split-parent",
                        //reduceWhiteSpace: false,
                    });
                    gsap.set(item.childSplit.lines, {
                        yPercent: 108,
                        //autoAlpha: 0,
                    });
                }
            } else if (item.classList.contains('reveal-block-animation')) {
                item.classList.add('reveal-block--set');
            } else if (item.classList.contains('fade-animation')) {
                item.classList.add('fade--set');
            } else if (item.classList.contains('fade-and-slide-animation')) {
                item.classList.add('fade-in-and-slide--set');
            }
        },
        inScrollview: function(item) {
            function allDone() {
                item.parentSplit.revert();
                item.childSplit.revert();
                item.classList.add('active-item-reveal');
                //*
                if (UI.settings.locoScroll) {
                    UI.settings.locoScroll.update();
                }
                //*/
            }
            if (item.classList.contains('reveal-animation')) {
                gsap.to(item.childSplit.lines, {
                    duration: 1.5,
                    yPercent: 0,
                    ease: "Quint.easeInOut",
                    delay: .1,
                    stagger: 0.2,
                    onComplete: allDone
                });
            } else if (item.classList.contains('reveal-text-animation')) {
                if (item.classList.contains('content__p')) {
                    const new_item = item.querySelector('p');
                    gsap.to(new_item.childSplit.lines, {
                        duration: 1.5,
                        yPercent: 8,
                        ease: "Quint.easeInOut",
                        delay: .1,
                        stagger: 0.2,
                        onComplete: allDone
                    });
                } else {
                    gsap.to(item.childSplit.lines, {
                        duration: 1.5,
                        yPercent: 8,
                        ease: "Quint.easeInOut",
                        delay: .1,
                        stagger: 0.2,
                        onComplete: allDone
                    });
                }
            } else if (item.classList.contains('reveal-block-animation')) {
                gsap.to(item, {
                    duration: 1.5,
                    y: 0,
                    ease: "Quint.easeInOut",
                    delay: .1,
                    autoAlpha: 1,
                    stagger: 0.2,
                    onComplete: allDone
                });
            } else if (item.classList.contains('fade-animation')) {
                gsap.to(item, {
                    duration: 1,
                    autoAlpha: 1,
                    ease: "Quint.easeInOut",
                    delay: .25,
                });
            } else if (item.classList.contains('fade-and-slide-animation')) {
                gsap.to(item, {
                    duration: 1.5,
                    y: 0,
                    ease: "Quint.easeInOut",
                    delay: .25,
                });
                gsap.to(item, {
                    duration: 1,
                    autoAlpha: 1,
                    ease: "Quint.easeInOut",
                    delay: .1,
                });
            }
        },
        outView: function(item) {
            //console.log("The element is out of view");
            if (item.classList.contains('reveal-animation')) {
                gsap.set(item.childSplit.lines, {
                    yPercent: 100,
                    //autoAlpha: 0,
                });
            }
            if (item.classList.contains('hide-and-show-nav')) {
                UI.showMenu();
            }
        },
        /**
         *----------------------------------------------------------------------
         *
         * ::SERVICES IMAGE EFFECT
         *
         *----------------------------------------------------------------------
         */
        servicesModuleImage: function() {
            HELPERS.initServiceImageHandlers();
        },
        initServiceImageHandlers: function() {
            HELPERS.settings.services_images_triggers = document.querySelectorAll('.cis__section__services__service');
            HELPERS.settings.services_images = document.querySelectorAll('.js-service-figure');
            for (var i = HELPERS.settings.services_images_triggers.length - 1; i >= 0; i--) {
                HELPERS.settings.services_images_triggers[i].addEventListener('mouseover', HELPERS.mouseOverServiceImage, true);
                HELPERS.settings.services_images_triggers[i].addEventListener('mouseout', HELPERS.mouseOutServiceImage, true);
            }
        },
        mouseOverServiceImage: function(e) {
            let currentImage = 'service-' + e.target.dataset.imageLink;
            let images = HELPERS.settings.services_images;
            for (var i = images.length - 1; i >= 0; i--) {
                images[i].style.display = "none";
                if (images[i].querySelector('picture').classList.contains('loaded')) {
                    //*
                    //images[i].querySelector('picture').classList.remove('loaded');
                    images[i].querySelector('picture').classList.remove('--special-transform');
                    images[i].querySelector('picture').classList.remove('--fade-in-and-transform');
                    //images[i].querySelector('picture').classList.add('--special-transform');
                    //*/
                }
            }
            document.getElementById(currentImage).style.display = "block";
            if (document.getElementById(currentImage).querySelector('picture').classList.contains('loaded')) {
                let picture = document.getElementById(currentImage).querySelector('picture');
                gsap.set(picture, {
                    scale: 1.5,
                    transformOrigin: 'center center',
                });
                gsap.to(picture, {
                    duration: 1.25,
                    scale: 1.05,
                    ease: "Quint.easeOut",
                });
            }
            //console.log(currentImage);
        },
        mouseOutServiceImage: function(e) {
            let images = HELPERS.settings.services_images;
            for (var i = images.length - 1; i >= 0; i--) {
                images[i].style.display = "none";
                if (images[i].querySelector('picture').classList.contains('loaded')) {
                    images[i].querySelector('picture').classList.remove('--special-transform');
                    images[i].querySelector('picture').classList.remove('--fade-in-and-transform');
                } else {
                    //images[i].querySelector('picture').classList.add('loaded');
                }
            }
        },
        /**
         *----------------------------------------------------------------------
         *
         * ::HOVER ATTRACT FX
         *
         *----------------------------------------------------------------------
         */
        startHoverEffect: function(element) {
            class HoverButton {
                constructor(el) {
                    this.el = el;
                    this.hover = false;
                    this.calculatePosition();
                    this.attachEventsListener();
                }
                attachEventsListener() {
                    window.addEventListener('mousemove', e => this.onMouseMove(e));
                    window.addEventListener('resize', e => this.calculatePosition(e));
                }
                calculatePosition() {
                    gsap.set(this.el, {
                        x: 0,
                        y: 0,
                        scale: 1
                    });
                    const box = this.el.getBoundingClientRect();
                    this.x = box.left + (box.width * 0.5);
                    this.y = box.top + (box.height * 0.5);
                    this.width = box.width;
                    this.height = box.height;
                }
                onMouseMove(e) {
                    let hover = false;
                    let hoverArea = (this.hover ? .75 : .75);
                    let x = e.clientX - this.x;
                    let y = e.clientY - this.y;
                    let distance = Math.sqrt(x * x + y * y);
                    if (distance < (this.width * hoverArea)) {
                        hover = true;
                        if (!this.hover) {
                            this.hover = true;
                        }
                        this.onHover(e.clientX, e.clientY);
                    }
                    if (!hover && this.hover) {
                        this.onLeave();
                        this.hover = false;
                    }
                }
                onHover(x, y) {
                    gsap.to(this.el, {
                        x: (x - this.x) * .6,
                        y: (y - this.y) * .6,
                        //scale: 1.15,
                        ease: 'power2.out',
                        duration: .5,
                    });
                    this.el.style.zIndex = 10;
                }
                onLeave() {
                    gsap.to(this.el, {
                        x: 0,
                        y: 0,
                        //scale: 1,
                        //ease: 'elastic.out(1.2, 0.4)',
                        ease: 'power2.out',
                        duration: .5,
                    });
                    this.el.style.zIndex = 1;
                }
            }
            for (var i = element.length - 1; i >= 0; i--) {
                new HoverButton(element[i]);
            }
        },
        removeHoverEffect: function(element) {
            //LOGIC GOES HERE
        },
        /**
         *----------------------------------------------------------------------
         *
         * ::CTAs HOVER FX
         *
         *----------------------------------------------------------------------
         */
        initHoverAnimation: function(elements, direction) {
            for (var i = elements.length - 1; i >= 0; i--) {
                let element = elements[i];
                gsap.set(element.querySelector('.cis__attract-cta__path--bottom'), {
                    drawSVG: "0% 0%",
                });
                gsap.set(element.querySelector('.cis__attract-cta__path--top'), {
                    drawSVG: "0% 0%",
                });
                let timeline = gsap.timeline();
                /*
                 * ELEMENT TIMELINE
                 */
                if (direction == 'inverted') {
                    timeline.set(element.querySelector('.cis__attract-cta__path--bottom'), {
                        //drawSVG: "100% 50%",
                        rotation: "-180",
                        rotationX: "-180",
                        transformOrigin: 'center center',
                    }, 0);
                    timeline.to(element.querySelector('.cis__attract-cta__path--bottom'), .75, {
                        drawSVG: "100% 50%",
                        ease: Expo.easeIn,
                    }, 0);
                    timeline.to(element.querySelector('.cis__attract-cta__path--bottom'), .75, {
                        drawSVG: "100% 100%",
                        ease: Expo.easeOut,
                    }, .75);
                    timeline.to(element.querySelector('.cis__attract-cta__path--top'), 1.5, {
                        drawSVG: "100%",
                        ease: Power4.easeInOut,
                    }, .35);
                    timeline.pause();
                } else {
                    timeline.to(element.querySelector('.cis__attract-cta__path--bottom'), .75, {
                        drawSVG: "100% 50%",
                        ease: Expo.easeIn,
                    }, 0);
                    timeline.to(element.querySelector('.cis__attract-cta__path--bottom'), .75, {
                        drawSVG: "100% 100%",
                        ease: Expo.easeOut,
                    }, .75);
                    timeline.to(element.querySelector('.cis__attract-cta__path--top'), 1.5, {
                        drawSVG: "100%",
                        ease: Power4.easeInOut,
                    }, .35);
                    timeline.pause();
                }
                /*
                 * HANDLERS AND EVENTS
                 */
                element.elementMouseOver = function() {
                    timeline.timeScale(1.95).play();
                    //console.log('TESTING');
                }
                element.elementMouseOut = function() {
                    timeline.timeScale(1.75).reverse();
                }
                element.killTimeline = function() {
                    timeline.clear();
                    timeline.kill();
                    timeline = null;
                    //console.log('TIMELINE IS DEAD');
                }
                element.addEventListener('mouseover', element.elementMouseOver, true);
                element.addEventListener('mouseout', element.elementMouseOut, true);
            }
        },
        cleanHoverAnimation: function(elements) {
            for (var i = elements.length - 1; i >= 0; i--) {
                let element = elements[i];
                element.removeEventListener('mouseover', element.elementMouseOver, true);
                element.removeEventListener('mouseout', element.elementMouseOut, true);
                element.killTimeline();
            }
        },
        /**
         *----------------------------------------------------------------------
         *
         * ::CUSTOM CURSOR FOLLOW
         *
         *----------------------------------------------------------------------
         */
        initCustomCursor: function() {
            const cursor = document.querySelector('#cursor');
            let mouse = {
                x: 300,
                y: 300
            };
            let pos = {
                x: 0,
                y: 0
            };
            const speed = 0.1; // between 0 and 1
            const updatePosition = () => {
                pos.x += (mouse.x - pos.x) * speed;
                pos.y += (mouse.y - pos.y) * speed;
                cursor.style.transform = 'translate3d(' + pos.x + 'px ,' + pos.y + 'px, 0)';
            };
            const updateCoordinates = e => {
                mouse.x = e.clientX;
                mouse.y = e.clientY;
            }
            window.addEventListener('mousemove', updateCoordinates);

            function loop() {
                updatePosition();
                requestAnimationFrame(loop);
            }
            requestAnimationFrame(loop);
        },
        extraCursor: function() {
            const cursor = document.querySelector('#cursor');
            const cursorCircle = cursor.querySelector('.cursor__circle');
            const mouse = {
                x: -100,
                y: -100
            }; // mouse pointer's coordinates
            const pos = {
                x: 0,
                y: 0
            }; // cursor's coordinates
            const speed = 0.1; // between 0 and 1
            const updateCoordinates = e => {
                mouse.x = e.clientX;
                mouse.y = e.clientY;
            }
            window.addEventListener('mousemove', updateCoordinates);

            function getAngle(diffX, diffY) {
                return Math.atan2(diffY, diffX) * 180 / Math.PI;
            }

            function getSqueeze(diffX, diffY) {
                const distance = Math.sqrt(Math.pow(diffX, 2) + Math.pow(diffY, 2));
                const maxSqueeze = 0.15;
                const accelerator = 1500;
                return Math.min(distance / accelerator, maxSqueeze);
            }
            const updateCursor = () => {
                const diffX = Math.round(mouse.x - pos.x);
                const diffY = Math.round(mouse.y - pos.y);
                pos.x += diffX * speed;
                pos.y += diffY * speed;
                const angle = getAngle(diffX, diffY);
                const squeeze = getSqueeze(diffX, diffY);
                const scale = 'scale(' + (1 + squeeze) + ', ' + (1 - squeeze) + ')';
                const rotate = 'rotate(' + angle + 'deg)';
                const translate = 'translate3d(' + pos.x + 'px ,' + pos.y + 'px, 0)';
                cursor.style.transform = translate;
                cursorCircle.style.transform = rotate + scale;
            };

            function loop() {
                updateCursor();
                requestAnimationFrame(loop);
            }
            requestAnimationFrame(loop);
            const cursorModifiers = document.querySelectorAll('[cursor-class]');
            cursorModifiers.forEach(cursorModifier => {
                cursorModifier.addEventListener('mouseenter', function() {
                    const className = this.getAttribute('cursor-class');
                    cursor.classList.add(className);
                    //alert("TEST");
                });
                cursorModifier.addEventListener('mouseleave', function() {
                    const className = this.getAttribute('cursor-class');
                    cursor.classList.remove(className);
                });
            });
        },
        /**
         *----------------------------------------------------------------------
         *
         * ::CUSTOM CURSOR FOLLOW
         *
         *----------------------------------------------------------------------
         */
        getCssValuePrefix: function() {
            var rtrnVal = ''; //default to standard syntax
            var prefixes = ['-o-', '-ms-', '-moz-', '-webkit-'];
            // Create a temporary DOM object for testing
            var dom = document.createElement('div');
            for (var i = 0; i < prefixes.length; i++) {
                // Attempt to set the style
                dom.style.background = prefixes[i] + 'linear-gradient(#000000, #ffffff)';
                // Detect if the style was successfully set
                if (dom.style.background) {
                    rtrnVal = prefixes[i];
                }
            }
            dom = null;
            delete dom;
            return rtrnVal;
        },
    }
    return helpers;
}(HELPERS || {}));