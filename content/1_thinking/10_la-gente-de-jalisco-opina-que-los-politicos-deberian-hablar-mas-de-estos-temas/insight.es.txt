Title: La gente de Jalisco opina que los políticos deberían hablar más de estos temas ⬆️

----

Posttype: news

----

Blocks: [{"attrs":{"layoutcontainer":"true","colorbg":"light","paddingtop":"s40","paddingbottom":"s40","paddingtopmobile":"s40","paddingbottommobile":"s40","visibility":"true","customid":"","customclass":""},"columns":[{"blocks":[{"content":{"text":"La gente de Jalisco opina que los pol\u00edticos deber\u00edan hablar m\u00e1s de estos temas \u2b06\ufe0f","fontsize":"h3","animation":"fade_animation","aligncontent":"left","topmargin":"false","bottommargin":"true","modularpadding":"true","colorunderline":"coral","colorhighlight":"coral","width":"s12","offset":"s0","widthm":"s12","offsetm":"s0","widths":"s12","offsets":"s0","visibility":"true","customid":"","customclass":""},"id":"ffedb025-4093-4b1c-a492-0b68e5b927d9","isHidden":false,"type":"heading"},{"content":{"text":"<p>Esta encuesta fue levantada sin ning\u00fan inter\u00e9s pol\u00edtico, sin colaboraciones con terceros<\/p>","fontsize":"xs","animation":"fade_animation","aligncontent":"left","topmargin":"false","bottommargin":"false","modularpadding":"true","width":"s12","offset":"s0","widthm":"s12","offsetm":"s0","widths":"s12","offsets":"s0","visibility":"true","customid":"","customclass":""},"id":"8cfb582e-154e-40dd-aca4-46308265a5a5","isHidden":false,"type":"text"}],"id":"1c4b5615-954c-4e94-9cbb-4f7477e9f8f4","width":"1\/1"}],"id":"e9deed65-a83b-4e29-b774-3d822bac49f7"}]

----

External: https://www.instagram.com/p/CPzCA3CMbR6/

----

Report: 

----

Width: s6

----

Offset: s0

----

Widthm: s6

----

Offsetm: s0

----

Widths: s12

----

Offsets: s0

----

Positiontype: bottom

----

Date: 2021-06-06

----

Author:

- elTLu1EW

----

Tags: #jalisco, #jaliscomexico, #covid_19, #futuro, #trabajo, #feliz, #positivo, #mexico, #guadalajara, #puertovallarta, #gdl, #tequila, #visitmexico, #zapopan, #nature, #méxico, #happy

----

Cover:

- speech-balloons_t20_4j7ln8.jpg

----

Autoid: g5tyh8v6

----

Seotitle: 

----

Seodescription: 

----

Seocover: 

----

Postsize: s6