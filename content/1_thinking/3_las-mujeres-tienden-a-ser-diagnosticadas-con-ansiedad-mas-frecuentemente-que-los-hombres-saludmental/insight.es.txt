Title: Las mujeres tienden a ser diagnosticadas con ansiedad más frecuentemente que los hombres 🤔 #SaludMental

----

Posttype: news

----

Blocks: [{"attrs":{"layoutcontainer":"true","colorbg":"light","paddingtop":"s40","paddingbottom":"s40","paddingtopmobile":"s40","paddingbottommobile":"s40","visibility":"true","customid":"","customclass":""},"columns":[{"blocks":[{"content":{"text":"Las mujeres tienden a ser diagnosticadas con ansiedad m\u00e1s frecuentemente que los hombres \ud83e\udd14&nbsp;#SaludMental","fontsize":"h3","animation":"fade_animation","aligncontent":"left","topmargin":"false","bottommargin":"false","modularpadding":"true","colorunderline":"coral","colorhighlight":"coral","width":"s12","offset":"s0","widthm":"s12","offsetm":"s0","widths":"s12","offsets":"s0","visibility":"true","customid":"","customclass":""},"id":"9cc37ad4-e61e-413f-89fb-321c86e7a25f","isHidden":false,"type":"heading"},{"content":{"text":"<p>Esta encuesta fue levantada sin colaboraciones con terceros.<\/p>","fontsize":"xs","animation":"fade_animation","aligncontent":"left","topmargin":"false","bottommargin":"false","modularpadding":"true","width":"s12","offset":"s0","widthm":"s12","offsetm":"s0","widths":"s12","offsets":"s0","visibility":"true","customid":"","customclass":""},"id":"8ebed5ff-181b-4916-a1c1-a15460434983","isHidden":false,"type":"text"}],"id":"81c4b9b2-4148-4bd6-a056-4ff7022c7644","width":"1\/1"}],"id":"9ddefe27-0390-4c21-ba63-fadf87d7d9e5"}]

----

External: https://www.instagram.com/p/COgh7ByshjB/

----

Report: 

----

Width: s4

----

Offset: s0

----

Widthm: s6

----

Offsetm: s0

----

Widths: s12

----

Offsets: s0

----

Positiontype: bottom

----

Date: 2021-05-05

----

Author:

- elTLu1EW

----

Tags: #ansiedadegeneralizada, #ansiedad, #saludybienestar, #mentalhealth, #saludmental, #jalisco, #mujeres, #datos, #encuestas, #noticias, #interesante, #sabiasque, #depresión

----

Cover:

- peace-of-mind_t20_wak077.jpg

----

Autoid: 4ivsqxl2

----

Seotitle: Las mujeres tienden a ser diagnosticadas con ansiedad más frecuentemente que los hombres 🤔 #SaludMental

----

Seodescription:

Las mujeres tienden a ser diagnosticadas con ansiedad más frecuentemente que los hombres 🤔 #SaludMental

Esta encuesta fue levantada sin colaboraciones con terceros. Nuestro compromiso es mantener a la sociedad informada para que cada ciudadano pueda emitir un punto de vista basado en información precisa.

Estudio realizado por @cis.mx

----

Seocover:

- peace-of-mind_t20_wak077.jpg