Title: Bases de datos

----

Subtitle: Mejor información, mejores decisiones.

----

Blocks: [{"attrs":{"layoutcontainer":"true","colorbg":"light","paddingtop":"s240","paddingbottom":"s120","paddingtopmobile":"s80","paddingbottommobile":"s40","visibility":"true","customid":"","customclass":""},"columns":[{"blocks":[{"content":{"text":"DATOS INTELIGENTES","fontsize":"h4","animation":"reveal_text_animation","aligncontent":"left","topmargin":"false","bottommargin":"false","modularpadding":"true","textcolor":"gray","colorunderline":"green","colorhighlight":"blue","width":"s9_5","offset":"s2_5","widthm":"s9_5","offsetm":"s2_5","widths":"s12","offsets":"s0","visibility":"true","customid":"","customclass":""},"id":"b7911b6f-28b7-47c1-a2cc-bbd7f7427c21","isHidden":false,"type":"heading"},{"content":{"media":[],"alt":"Microsegmentaci\u00f3n","fullbleed":"false","position":"middle","zindex":"top","parallax":"true","scrollspeed":"2","scrolloffset":"2","width":"s5","offset":"s4_5","widthm":"s5","offsetm":"s4_5","widths":"s12","offsets":"s0","visibility":"true","customid":"","customclass":""},"id":"7d787d72-a11c-4513-bf5c-da7a0e7fa6e6","isHidden":false,"type":"dynamicimage"},{"content":{"text":"<em>Bases de datos<\/em> \u2014","fontsize":"hero__h1","animation":"reveal_block_animation","aligncontent":"left","topmargin":"true","bottommargin":"false","modularpadding":"true","colorunderline":"green","colorhighlight":"green","width":"s12","offset":"s0","widthm":"s12","offsetm":"s0","widths":"s12","offsets":"s0","visibility":"true","customid":"","customclass":""},"id":"e96aeebc-cd7c-47ef-ae2a-f25863ecce8a","isHidden":false,"type":"marquee"},{"content":{"text":"<p>Mejor informaci\u00f3n, mejores decisiones.<\/p>","fontsize":"big","animation":"fade_and_slide_animation","aligncontent":"left","topmargin":"true","bottommargin":"true","modularpadding":"true","width":"s9_5","offset":"s2_5","widthm":"s9_5","offsetm":"s2_5","widths":"s12","offsets":"s0","visibility":"true","customid":"","customclass":""},"id":"bd2ac7cf-8184-4697-abd9-b9e1f4e72bf7","isHidden":false,"type":"text"},{"content":{"tagtype":"share","related":[],"icon":"","iconcolor":"","text":"","link":"","pagelink":[],"pageurl":"","ctabuttoncolor":"","width":"s9_5","offset":"s2_5","widthm":"s9_5","offsetm":"s2_5","widths":"s12","offsets":"s0","visibility":"true","customid":"","customclass":""},"id":"2b5dc62c-c93a-47f2-929f-f8df755d44e0","isHidden":false,"type":"dynamictag"},{"content":{"spacersize":"s200","spacersizes":"s80","visibility":"true","customid":"","customclass":""},"id":"4f4c3f29-b7fa-43c4-813a-cc219f8ab4de","isHidden":false,"type":"spacer"},{"content":{"text":"SCROLL Y DESCUBRE","fontsize":"h5","animation":"reveal_text_animation","aligncontent":"left","topmargin":"true","bottommargin":"false","modularpadding":"true","textcolor":"green","colorunderline":"yellow","colorhighlight":"blue","width":"s9_5","offset":"s2_5","widthm":"s9_5","offsetm":"s2_5","widths":"s12","offsets":"s0","visibility":"true","customid":"","customclass":""},"id":"0a3bbb08-dca2-475c-ac9c-13793de1f1bb","isHidden":false,"type":"heading"}],"id":"409e9b41-8e60-497a-b1df-ffaca59b3217","width":"1\/1"}],"id":"7c153869-9c13-4869-8de9-ba03c413890c"},{"attrs":{"layoutcontainer":"true","colorbg":"light","paddingtop":"s40","paddingbottom":"s40","paddingtopmobile":"s40","paddingbottommobile":"s40","visibility":"true","customid":"","customclass":""},"columns":[{"blocks":[{"content":{"text":"<p><strong>\u00bfPara qu\u00e9 me sirve?<\/strong><\/p><p>\u2022 Tendr\u00e1s mayor precisi\u00f3n<\/p><p>\u2022 Obtendr\u00e1s resultados r\u00e1pidamente<\/p>","fontsize":"regular","animation":"fade_animation","aligncontent":"left","topmargin":"false","bottommargin":"true","modularpadding":"true","width":"s6_5","offset":"s5","widthm":"s5_5","offsetm":"s5","widths":"s12","offsets":"s0","visibility":"true","customid":"","customclass":""},"id":"23f420e2-45d8-47d4-8d44-8a8dbb1b519d","isHidden":false,"type":"text"},{"content":{"text":"<p><strong>Beneficios<\/strong><\/p><p>\u2022 Sabr\u00e1s c\u00f3mo dirigirte a cada uno de esos grupos espec\u00edficos<\/p><p>\u2022 Podr\u00e1s persuadir a cada grupo de una manera m\u00e1s efectiva<\/p><p>\u2022 Tendr\u00e1s acceso a predicciones y c\u00e1lculos con la mayor precisi\u00f3n<\/p>","fontsize":"regular","animation":"fade_animation","aligncontent":"left","topmargin":"false","bottommargin":"true","modularpadding":"true","width":"s6_5","offset":"s5","widthm":"s6_5","offsetm":"s5","widths":"s12","offsets":"s0","visibility":"true","customid":"","customclass":""},"id":"31eb02b4-7a60-4e9f-a251-f651ec4105bd","isHidden":false,"type":"text"}],"id":"a1d08de6-fc0d-4e52-9d45-a531208c5be6","width":"1\/2"},{"blocks":[{"content":{"text":"<p>Escogemos las mediciones m\u00e1s acertadas para cada pregunta, trazamos una muestra representativa de la poblaci\u00f3n y visitamos el domicilio de las personas seleccionadas para conocer sus opiniones, percepciones y comportamiento.<\/p>","fontsize":"regular","animation":"fade_animation","aligncontent":"left","topmargin":"false","bottommargin":"false","modularpadding":"true","width":"s8","offset":"s0","widthm":"s8","offsetm":"s0","widths":"s12","offsets":"s0","visibility":"true","customid":"","customclass":""},"id":"d9847949-7fad-490b-8ece-9b581afd4a6a","isHidden":false,"type":"text"},{"content":{"text":"<p>Con este servicio reconocer\u00e1s a grupos espec\u00edficos de personas con <strong>mayor profundidad<\/strong> en funci\u00f3n de variables como el estatus socioecon\u00f3mico, la personalidad, sus valores y el estilo de vida. Adem\u00e1s, sabr\u00e1s c\u00f3mo dirigirte a cada uno de esos grupos espec\u00edficos.<\/p>","fontsize":"regular","animation":"fade_animation","aligncontent":"left","topmargin":"false","bottommargin":"false","modularpadding":"true","width":"s8","offset":"s0","widthm":"s8","offsetm":"s0","widths":"s12","offsets":"s0","visibility":"true","customid":"","customclass":""},"id":"8fb1b847-0dd8-40eb-b33d-ea8ddb322aed","isHidden":false,"type":"text"},{"content":{"text":"<p>Por lo tanto, podr\u00e1s persuadir a cada grupo de una manera m\u00e1s efectiva y tendr\u00e1s acceso a predicciones y c\u00e1lculos con la mayor precisi\u00f3n.<\/p>","fontsize":"regular","animation":"fade_animation","aligncontent":"left","topmargin":"false","bottommargin":"false","modularpadding":"false","width":"s8","offset":"s0","widthm":"s8","offsetm":"s0","widths":"s12","offsets":"s0","visibility":"true","customid":"","customclass":""},"id":"e55aa123-54da-439d-8808-b0cc79a920b7","isHidden":false,"type":"text"},{"content":{"spacersize":"s40","spacersizes":"s10","width":"s8","offset":"","widthm":"s8","offsetm":"","widths":"","offsets":"","visibility":"true","customid":"","customclass":""},"id":"9c7411bd-6bcb-4638-88be-01ad1df3d8b3","isHidden":false,"type":"divider"},{"content":{"text":"PROYECTOS RELACIONADOS","fontsize":"h4","animation":"reveal_text_animation","aligncontent":"left","topmargin":"false","bottommargin":"false","modularpadding":"true","textcolor":"","colorunderline":"yellow","colorhighlight":"blue","width":"s12","offset":"s0","widthm":"s12","offsetm":"s0","widths":"s12","offsets":"s0","visibility":"true","customid":"","customclass":""},"id":"8c3a9cf8-f07c-448e-96e5-e3b62e1807e8","isHidden":false,"type":"heading"},{"content":{"tagtype":"related","related":["portafolio\/presidential-election-florida","portafolio\/encuesta-nacional-de-cubanos-que-viven-en-cuba"],"icon":"","iconcolor":"","text":"","link":"","pagelink":[],"pageurl":"","ctabuttoncolor":"","width":"s12","offset":"s0","widthm":"s12","offsetm":"s0","widths":"s12","offsets":"s0","visibility":"true","customid":"","customclass":""},"id":"5f217b8d-d47b-48a2-909b-dbd1a554e40b","isHidden":false,"type":"dynamictag"}],"id":"5aac511c-63fe-4918-bc35-3ae39e1eefd2","width":"1\/2"}],"id":"2a656f65-d9fe-4464-bb44-bb8fe3881300"},{"attrs":{"layoutcontainer":"true","colorbg":"light","paddingtop":"s200","paddingbottom":"s40","paddingtopmobile":"s80","paddingbottommobile":"s40","visibility":"true","customid":"","customclass":""},"columns":[{"blocks":[{"content":{"tagtype":"icon","related":[],"icon":"logo","iconcolor":"green","text":"","link":"","pagelink":[],"pageurl":"","ctabuttoncolor":"","width":"s10_5","offset":"s1_5","widthm":"s10_5","offsetm":"s1_5","widths":"s12","offsets":"s0","visibility":"true","customid":"","customclass":""},"id":"3395b91a-7c8d-4021-8a5a-444c471b54cf","isHidden":false,"type":"dynamictag"},{"content":{"text":"Gana conociendo a un segmento m\u00e1s <em>espec\u00edfico<\/em>: Nuestra precisi\u00f3n comprobada te acercar\u00e1 a tus metas como <strong>ning\u00fan otro servicio<\/strong> en el mercado.","fontsize":"h2","animation":"reveal_text_animation","aligncontent":"left","topmargin":"false","bottommargin":"false","modularpadding":"true","textcolor":"gray","colorunderline":"green","colorhighlight":"blue","width":"s5_5","offset":"s1_5","widthm":"s5_5","offsetm":"s1_5","widths":"s12","offsets":"s0","visibility":"true","customid":"","customclass":""},"id":"f3e9a1b4-0436-445c-8d3b-4a874bf42c69","isHidden":false,"type":"heading"},{"content":{"text":"<p>Con un margen menor a 0.01<br>Descubre m\u00e1s de esto en la secci\u00f3n de <a href=\"\/es\/servicios\" rel=\"noopener noreferrer nofollow\">Nuestros M\u00e9todos<\/a>.<\/p>","fontsize":"s","animation":"fade_animation","aligncontent":"left","topmargin":"false","bottommargin":"true","modularpadding":"true","width":"s9_5","offset":"s1_5","widthm":"s9_5","offsetm":"s1_5","widths":"s12","offsets":"s0","visibility":"true","customid":"","customclass":""},"id":"c72cb56a-5744-4629-b001-950c87bc4bc3","isHidden":false,"type":"text"},{"content":{"media":[],"alt":"","fullbleed":"false","position":"middle","zindex":"bottom","parallax":"true","scrollspeed":"2","scrolloffset":"2","width":"s5","offset":"s5","widthm":"s5","offsetm":"s5","widths":"s12","offsets":"s0","visibility":"true","customid":"","customclass":""},"id":"10765130-63ee-405e-854e-71821951c773","isHidden":false,"type":"dynamicimage"},{"content":{"spacersize":"s120","spacersizes":"s32","width":"s9","offset":"s1_5","widthm":"s9","offsetm":"s1_5","widths":"s12","offsets":"s0","visibility":"true","customid":"","customclass":""},"id":"77319c4e-1ffd-4a94-b513-433e12eea241","isHidden":false,"type":"divider"}],"id":"cddfded6-9c7e-4caa-a788-0863b3e18225","width":"1\/1"}],"id":"0bee5dfc-d35c-4425-8db4-a38ec39113d3"},{"attrs":{"layoutcontainer":"true","colorbg":"light","paddingtop":"s40","paddingbottom":"s160","paddingtopmobile":"s80","paddingbottommobile":"s80","visibility":"true","customid":"","customclass":""},"columns":[{"blocks":[{"content":{"text":"ENCUENTRA TU FOCO","fontsize":"h5","animation":"reveal_text_animation","aligncontent":"left","topmargin":"false","bottommargin":"false","modularpadding":"true","textcolor":"green","colorunderline":"green","colorhighlight":"blue","width":"s9_5","offset":"s1_5","widthm":"s9_5","offsetm":"s1_5","widths":"s12","offsets":"s0","visibility":"true","customid":"","customclass":""},"id":"9ca66f77-91e4-410b-8e9a-b48f19185d41","isHidden":false,"type":"heading"},{"content":{"text":"<p>Trabajamos con un rango amplio de presupuestos seg\u00fan la meta.<\/p>","fontsize":"regular","animation":"","aligncontent":"left","topmargin":"false","bottommargin":"true","modularpadding":"true","width":"s3","offset":"s1_5","widthm":"s3","offsetm":"s1_5","widths":"s12","offsets":"s0","visibility":"true","customid":"","customclass":""},"id":"6fda8eb8-332f-4df6-bdb1-b36c4503691f","isHidden":false,"type":"text"},{"content":{"text":"<p>Te ayudamos a bajar el objetivo ideal para obtener el resultado m\u00e1s efectivo.<\/p>","fontsize":"regular","animation":"","aligncontent":"left","topmargin":"false","bottommargin":"true","modularpadding":"true","width":"s3","offset":"s0","widthm":"s3","offsetm":"s0","widths":"s12","offsets":"s0","visibility":"true","customid":"","customclass":""},"id":"ebb2e8a6-cabc-438f-ad55-51e52aebf028","isHidden":false,"type":"text"},{"content":{"text":"Hablemos de <strong>tu proyecto<\/strong>. Podemos preparar una propuesta en <em>2 d\u00edas h\u00e1biles<\/em>.","fontsize":"h1","animation":"reveal_text_animation","aligncontent":"left","topmargin":"false","bottommargin":"false","modularpadding":"true","textcolor":"gray","colorunderline":"blue","colorhighlight":"green","width":"s9","offset":"s1_5","widthm":"s9","offsetm":"s1_5","widths":"s12","offsets":"s0","visibility":"true","customid":"","customclass":""},"id":"52dabb7c-899d-4ad7-a41d-f635a76325c0","isHidden":false,"type":"heading"},{"content":{"tagtype":"ctabutton","related":[],"icon":"","iconcolor":"","text":"Ponte en Contacto","link":"page","pagelink":["contacto"],"pageurl":"","ctabuttoncolor":"green","width":"s9","offset":"s1_5","widthm":"s9","offsetm":"s1_5","widths":"s12","offsets":"s0","visibility":"true","customid":"","customclass":""},"id":"f603849a-5dd1-4add-b95d-8a0a53d65d41","isHidden":false,"type":"dynamictag"}],"id":"acfaa29b-7f7f-494e-939c-8cad864e59f4","width":"1\/1"}],"id":"eacd5aa4-68a8-41f4-b6d3-9a43086439f2"}]

----

Customtitle: Bases de datos

----

Object: 

----

Thumbnail: 

----

Secondaryimage: 

----

Position: topleft

----

Color: green

----

Cover: 

----

Autoid: tpgks5ge

----

What: 

----

Benefits: 

----

Description: 

----

Related: 

----

Uspheading: 

----

Uspnotes: 

----

Uspimage: 

----

Layout: [{"attrs":{"class":"TESTING","id":"TESTING","image":[]},"columns":[{"blocks":[],"id":"bd541a4a-e6e2-400b-88d8-771660fb4119","width":"1\/1"}],"id":"a60c41f4-01f2-4937-bd2b-d7c00b09dc03"}]

----

Date: 2021-07-06