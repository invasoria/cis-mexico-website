<?php

/**
 * The config file is optional. It accepts a return array with config options
 * Note: Never include more than one return statement, all options go within this single return array
 * In this example, we set debugging to true, so that errors are displayed onscreen. 
 * This setting must be set to false in production.
 * All config options: https://getkirby.com/docs/reference/system/options
 */
return [
    'languages' => true,
    'debug' => true,
    //'panel.install' => true,
    'date.handler'  => 'strftime',
    //'languages.detect' => true,
    'routes' => [
        [
          'pattern' => '/es',
          //'pattern' => '/(:any)',
          'action'  => function () {
            go(page('cover')->url('es'));
          }
        ],
        [
          'pattern' => '/en',
          'action'  => function () {
            go(page('cover')->url('en'));
          }
        ],
      ],
    'hooks' => [
        'file.delete:after' => function ($status, $file) {
          if ($toDelete = $file->parent()->image($file->name() . '.webp' ) ){
            $toDelete->delete();
          }
        },
    ],
    'api' => [
    'basicAuth' => true,
    'allowInsecure' => true
  ],
  'sylvainjule.matomo.url'        => 'https://emanueltovar.mx/matomo',
  'sylvainjule.matomo.id'         => '1',
  'sylvainjule.matomo.token'      => '4da65830a387828281b3f86068da18a1',
  'kirby3-webp' => true,
  'kirby3-webp.quality' => 80,
  'kirby3-webp.defaultQuality' => 80,
];