<?php

return function($page) {

	$tags = $page->pluck('tags', ',', true);
	return compact('tags',);

};