<?php

return function($page) {

	$first_month = $page->children()->listed()->flip()->first();
	go($first_month);

	/*
	//
	//CLASSIC PAGINATION
	//
	//$episodes = page('programas')->children()->listed()->sortBy('date', 'asc')->children()->listed()->children()->listed()->flip();
	//$items = new Pages(array($posts, $episodes));

	// apply pagination
	//$pagination = $items->paginate(9);
	//$pagination = $posts->paginate();
	$months = $page->children()->listed();
	$pagination = $months->paginate(1);
	return compact('months', 'pagination',);
	//*/

	/*
	//
	//ORIGINAL MONTHLY PAGINATION
	//
	// fetch the basic set of pages
	$posts = $page->children()->listed()->flip();
    $groups = $posts->sortBy('date')->group(function($item) {
        return $item->date()->toDate('m-Y');
    })->flip();

    $pagination = Pagination::for($groups, ['limit' => 1]);

    $groups = $groups->slice($pagination->offset(), $pagination->limit()); 

	return compact('groups', 'posts', 'pagination',);
	//*/

};