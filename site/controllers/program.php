<?php

return function($page) {

	// fetch the basic set of pages
	$seasons = $page->children()->listed();

	$tags = $page->pluck('tags', ',', true);

	// apply pagination
	$pagination = $seasons->pagination();

	return compact('seasons', 'tags', 'pagination');

};