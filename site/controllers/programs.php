<?php

return function($page) {

	// fetch the basic set of pages
	$programs = $page->children()->listed();
	$episodes = $page->children()->listed()->children()->listed()->children()->listed()->sortBy('date','desc');
	$seasons = $page->children()->listed()->children()->listed()->sortBy('date','desc');

	// fetch all categories
	$categories = $programs->pluck('categories', ',', true);
	$categoriesnumber = $programs->pluck('categories', ',');

	// fetch all tags
	//$tags = $programs->children()->listed()->children()->listed()->pluck('tags', ',', true);
	//$tagsnumber = $programs->children()->listed()->children()->listed()->pluck('tags', ',');

	// add the category filter
	if($category = urldecode(param('category'))) {
		if ($category == 'Original') {
			$programs = $programs->filterBy('original', true);
		} elseif ($category == 'Temporadas') {
			$programs = $seasons;
		} elseif ($category == 'Episodios') {
			$programs = $episodes;
		} else{
			$programs = $programs->filterBy('categories', $category, ',');
		}
	}

	// add the tag filter
	if($tag = urldecode(param('tag'))) {
		$programs = $episodes->filterBy('tags', $tag, ',');
	}

	$categoriesnumber = array_count_values($categoriesnumber);
	//$tagsnumber = array_count_values($tagsnumber);

	// apply pagination

	$programs   = $programs->paginate(12);
	$pagination = $programs->pagination();

	//return compact('programs', 'categories', 'category', 'categoriesnumber', 'tags', 'tag', 'pagination', 'tagsnumber');
	return compact('programs', 'categories', 'category', 'categoriesnumber', 'tag', 'pagination', 'episodes', 'seasons');

};