<?php

return function($page) {

	// fetch the basic set of pages
	$episodes = $page->children()->listed();

	// apply pagination
	$pagination = $episodes->pagination();

	return compact('episodes', 'pagination',);

};