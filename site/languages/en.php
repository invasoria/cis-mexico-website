<?php

return [
    'code' => 'en',
    'default' => false,
    'direction' => 'ltr',
    'locale' => [
        'LC_ALL' => 'en_US'
    ],
    'name' => 'English',
    'translations' => [
        'services'=>'Services',
        'info'=>'Information',
        'social'=>'Social Media',
        'more-thinking'=>'More Thinking',
        'more-services'=>'Other Services',
        'copied'=>'Link copied!',
    ],
    'url' => NULL
];