<?php

return [
    'code' => 'es',
    'default' => true,
    'direction' => 'ltr',
    'locale' => [
        'LC_ALL' => 'es_419'
    ],
    'name' => 'Español',
    'translations' => [
        'services'=>'Servicios',
        'info'=>'Información',
        'social'=>'Redes Sociales',
        'more-thinking'=>'Más de Thinking',
        'more-services'=>'Otros Servicios',
        'copied'=>'¡Enlace copiado!',
    ],
    'url' => NULL
];