panel.plugin("tfk/block-articles", {
  blocks: {
    articles: {
      computed: {
        empty() {
          if (!this.content.source) {
            return true;
          }
          return false;
        }
      },
      template: `
        <template>
          <figure @click="open" v-if="empty" class="k-block-figure"><button class="k-block-figure-empty k-button" type="button"><span aria-hidden="true" class="k-button-icon k-icon k-icon-page"><svg viewBox="0 0 16 16"><use xlink:href="#icon-page"></use></svg></span><span class="k-button-text">Selecciona Artículos...</span></button></figure>
          <figure @click="open" v-else class="k-block-figure"><button class="k-block-figure-empty k-block-figure-source k-button" type="button"><span aria-hidden="true" class="k-button-icon k-icon k-icon-page"><svg viewBox="0 0 16 16"><use xlink:href="#icon-page"></use></svg></span><span class="k-button-text">Artículos: {{ content.source }}</span></button></figure>
        </template>
      `
    }
  }
});