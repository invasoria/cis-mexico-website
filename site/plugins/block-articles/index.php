<?php

Kirby::plugin('tfk/block-articles', [
  'blueprints' => [
    'blocks/articles' => __DIR__ . '/blueprints/articles.yml'
  ],
  'snippets' => [
    'blocks/articles' => __DIR__ . '/snippets/articles.php'
  ]
]);