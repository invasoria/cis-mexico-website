<?php

Kirby::plugin('tfk/block-articleshero', [
  'blueprints' => [
    'blocks/articleshero' => __DIR__ . '/blueprints/articleshero.yml'
  ],
  'snippets' => [
    'blocks/articleshero' => __DIR__ . '/snippets/articleshero.php'
  ]
]);