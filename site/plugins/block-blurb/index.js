panel.plugin("tfk/block-blurb", {
  blocks: {
    blurb: {
      computed: {
        classesHeading() {
          return "k-block-align-" + this.content.aligncontent + " k-block-type-blurb-heading";
        },
        classesImg() {
          return "k-block-align-" + this.content.aligncontent + " k-block-type-blurb-img";
        },
        classesText() {
          return "k-block-align-" + this.content.aligncontent + " k-block-type-blurb-text";
        },
        marks() {
          return this.field("heading", {}).marks;
        },
        placeholder() {
          return this.field("heading", {}).placeholder;
        }
      },
      methods: {
        focus() {
          this.$refs.heading.focus();
        }
      },
      template: `
        <template>
          <div>
            <img @dblclick="open" v-if="this.content.iconimage[0]"
              :class="classesImg"
              :src="this.content.iconimage[0].url"
            >
            <k-writer
              :class="classesHeading"
              :inline="true"
              :marks="marks"
              :placeholder="placeholder"
              :value="content.heading"
              ref="heading"
              @input="update({ heading: $event })"
            />
            <k-writer v-if="this.content.text"
              :class="classesText"
              :inline="true"
              :value="content.text"
              @input="update({ text: $event })"
            />
          </div>
        </template>
      `
    }
  }
});