<?php if ($block->iconType()->value() === 'image' && ($icon = $block->iconImage()->toFile())): ?>
  <img class="space-bottom-1x" loading="lazy" src="<?= $icon->resize(72)->url() ?>" srcset="<?= $icon->srcset([72 => '1x', 144 => '2x']) ?>" sizes="<?php snippet('srcset-sizes-thumb') ?>" alt="<?= $block->heading() ?>">
<?php elseif ($block->iconType()->value() === 'svg' && $block->iconSvg()->isNotEmpty()): ?>
  <div class="margin-auto-<?= $block->alignContent() ?> space-bottom-1x"><?= $block->iconSvg() ?></div>
<?php endif ?>

<?php if ($block->heading()->isNotEmpty()): ?>
  <h3 class="title-h5"><?= $block->heading() ?></h3>
<?php endif ?>

<?php if ($block->text()->isNotEmpty()): ?>
  <div class="paragraph">
    <?= $block->text() ?>
  </div>
<?php endif ?>
