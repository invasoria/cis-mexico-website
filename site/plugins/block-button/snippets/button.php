<?php if ($block->text()->isNotEmpty()): ?>

  <?php if ($block->link()->value() === 'page' && $block->linkPage()->isNotEmpty() && $block->linkPage()->toPage()): ?>
    <a href="<?= $block->linkPage()->toPage()->url() ?>" class="button button-style-<?= $block->style() ?><?php if ($block->fullWidth()->bool()): ?> full-width<?php endif ?>" role="button"><?= $block->text() ?></a>
  <?php elseif ($block->link()->value() === 'url' && $block->linkUrl()->isNotEmpty()): ?>
    <a href="<?= $block->linkUrl() ?>" class="button button-style-<?= $block->style() ?><?php if ($block->fullWidth()->bool()): ?> full-width<?php endif ?>" role="button"<?php if ($block->linkTarget()->bool()): ?> target="_blank"<?php endif ?>><?= $block->text() ?></a>
  <?php endif ?>

<?php endif ?>
