panel.plugin("tfk/block-divider", {
  blocks: {
    divider: {
      template: `
        <template>
          <figure class="k-block-figure"><button class="k-block-figure-empty k-block-figure-source k-button" type="button"><span aria-hidden="true" class="k-button-icon k-icon k-icon-bars"><svg viewBox="0 0 16 16"><use xlink:href="#icon-bars"></use></svg></span></button></figure>
        </template>
      `
    }
  }
});