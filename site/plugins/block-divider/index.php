<?php

Kirby::plugin('tfk/block-divider', [
  'blueprints' => [
    'blocks/divider' => __DIR__ . '/blueprints/divider.yml'
  ],
  'snippets' => [
    'blocks/divider' => __DIR__ . '/snippets/divider.php'
  ]
]);