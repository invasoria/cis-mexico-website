panel.plugin("tfk/block-dynamicimage", {
  blocks: {
    image: {
      computed: {
        classes() {
          if (this.content.actualsize === true) {
            return "k-block-actual-size k-block-align-" + this.content.aligncontent;
          }
          return "k-block-align-" + this.content.aligncontent;
        },
        media() {
          if (this.content.media[0]) {
            return this.content.media[0].url;
          }
          return false;
        }
      },
      template: `
        <template>
          <div v-if="media" :class="classes">
            <img @dblclick="open" :src="media">
          </div>
          <figure @click="open" v-else class="k-block-figure"><button class="k-block-figure-empty k-button" type="button"><span aria-hidden="true" class="k-button-icon k-icon k-icon-image"><svg viewBox="0 0 16 16"><use xlink:href="#icon-image"></use></svg></span><span class="k-button-text">Select image...</span></button></figure>
        </template>
      `
    }
  }
});