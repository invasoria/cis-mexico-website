<?php

Kirby::plugin('tfk/block-dynamicimage', [
  'blueprints' => [
    'blocks/dynamicimage' => __DIR__ . '/blueprints/dynamicimage.yml'
  ],
  'snippets' => [
    'blocks/dynamicimage' => __DIR__ . '/snippets/dynamicimage.php'
  ]
]);