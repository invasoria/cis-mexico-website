<?php if ($block->media()->isNotEmpty()): ?>

<?php 
  //
  // TEXT ANIMATION
  //
  if ($block->animation()->isNotEmpty()) {
    $animation = $block->animation();
    switch ($animation) {
      case 'reveal_text_animation':
        $animation = 'js-observe reveal-text-animation';
        break;
      case 'reveal_block_animation':
        $animation = 'js-observe reveal-block-animation';
        break;
      case 'fade_animation':
        $animation = 'js-observe fade-animation';
        break;
      case 'fade_and_slide_animation':
        $animation = 'js-observe fade-and-slide-animation';
        break;  
      default:
        break;
    }
  } else{
    $animation = '';
  }
?>
  
  <?php if($image = $block->media()->toFile()): ?>
      <div class="dynamic-image<?php echo $block->position()->isNotEmpty() ? ' dynamic-image--'.$block->position():''; ?><?php echo $block->zindex()->isNotEmpty() ? ' dynamic-image--index-'.$block->zindex():''; ?> <?php echo $block->fullBleed()->bool() ? ' dynamic-image--full-bleed' : ''; ?> is-col<?php echo $block->width()->isNotEmpty() ? ' col-'.ltrim($block->width(), 's'):''; ?><?php echo $block->offset()->isNotEmpty() ? ' offset-'.ltrim($block->offset(), 's'):''; ?><?php echo $block->widthm()->isNotEmpty() ? ' md-col-'.ltrim($block->widthm(), 's'):''; ?><?php echo $block->offsetm()->isNotEmpty() ? ' md-offset-'.ltrim($block->offsetm(), 's'):''; ?><?php echo $block->widths()->isNotEmpty() ? ' xs-col-'.ltrim($block->widths(), 's'):''; ?><?php echo $block->offsets()->isNotEmpty() ? ' xs-offset-'.ltrim($block->offsets(), 's'):''; ?><?php echo $block->modularPadding()->bool() ? ' content__modular-block-padding' : '';  ?>"<?php echo $block->visibility()->bool() ? '':' style="display: none;"'; ?>>
        <figure class="--figure <?php echo $block->fullBleed()->bool() ? ' fit-into-parent' : ''; ?>"<?php if($block->parallax()->bool()): ?>data-scroll data-scroll-speed="<?php echo $block->scrollspeed() ?>" data-scroll-offset="<?php echo $block->scrolloffset() ?>"<?php endif ?>>
            <div class="--figure__container"<?php echo $image ? ' style="padding-top:' . ($image->height() / $image->width()) * 100 . '%;"' : '';?>>
              <picture class="js-lazy-image --special-transform js-can-use-webp" data-alt="<?php echo $block->alt(); ?>"<?php echo $image->parent()->image($image->name().'.webp') ? ' data-webp="'.$image->parent()->image($image->name().'.webp')->url() .'"': ''; ?><?php echo $image ? ' style="padding-top:' . ($image->height() / $image->width()) * 100 . '%;"' : '';?>>
                  <source srcset="<?php echo $image->url(); ?>">
              </picture>
            </div>
        </figure>
      </div>
    <?php endif ?>

<?php endif ?>
