panel.plugin("tfk/block-dynamictag", {
  blocks: {
    dynamictag: {
      computed: {
        /*
        empty() {
          if (this.content.tagtype == 'share') {
            return "Compartir";
          }
          return false;
        }
        */
      },
      template: `
        <template>
          <figure class="k-block-figure">
            <button class="k-block-figure-empty k-block-figure-source k-button" type="button">
              <span aria-hidden="true" class="k-button-icon k-icon k-icon-bolt">
                <svg viewBox="0 0 16 16"><use xlink:href="#icon-bolt"></use></svg>
              </span>
              <span class="k-button-text">{{ content.tagtype }}</span>
            </button>
          </figure>
        </template>
      `
    }
  }
});