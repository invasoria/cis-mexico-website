<?php

Kirby::plugin('tfk/block-dynamictag', [
  'blueprints' => [
    'blocks/dynamictag' => __DIR__ . '/blueprints/dynamictag.yml'
  ],
  'snippets' => [
    'blocks/dynamictag' => __DIR__ . '/snippets/dynamictag.php'
  ]
]);