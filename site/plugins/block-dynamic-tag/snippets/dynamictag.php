<div class="is-col<?php echo $block->width()->isNotEmpty() ? ' col-'.ltrim($block->width(), 's'):''; ?><?php echo $block->offset()->isNotEmpty() ? ' offset-'.ltrim($block->offset(), 's'):''; ?><?php echo $block->widthm()->isNotEmpty() ? ' md-col-'.ltrim($block->widthm(), 's'):''; ?><?php echo $block->offsetm()->isNotEmpty() ? ' md-offset-'.ltrim($block->offsetm(), 's'):''; ?><?php echo $block->widths()->isNotEmpty() ? ' xs-col-'.ltrim($block->widths(), 's'):''; ?><?php echo $block->offsets()->isNotEmpty() ? ' xs-offset-'.ltrim($block->offsets(), 's'):''; ?><?php echo $block->modularPadding()->bool() ? ' content__modular-block-padding' : '';  ?> --z-index-30"<?php echo $block->visibility()->bool() ? '':' style="display: none;"'; ?>>
    <?php if($block->tagtype() == 'share'): ?>
    	<!-- SHARE POST -->
    	<div class="cis__card__share js-observe fade-animation">
    	    <a href="<?php echo $page ? 'https://twitter.com/intent/tweet?source='.urlencode($page->url()) . '&text=' . urlencode($page->title()) . '%20' . urlencode($page->url()) . '&via=cismx' : '#'?>" class="cis__card__icon-button cis__card__icon-button--twitter external-link" target="_blank">
    	        <svg class="" x="0px" y="0px" width="32px"
    	             height="32px" viewBox="0 0 32 32" style="overflow:visible;enable-background:new 0 0 32 32;" xml:space="preserve">
    	            <path class="ener__button__svg__icon" d="M24,11.04c-0.6,0.26-1.24,0.44-1.89,0.52c0.69-0.41,1.2-1.05,1.44-1.81c-0.64,0.38-1.35,0.65-2.08,0.79
    	            c-1.24-1.32-3.31-1.39-4.64-0.15c-0.66,0.62-1.04,1.49-1.04,2.39c0,0.25,0.02,0.5,0.08,0.75c-2.64-0.13-5.1-1.38-6.76-3.43
    	            c-0.88,1.5-0.44,3.42,1.01,4.39c-0.52-0.01-1.03-0.15-1.48-0.41v0.04c0,1.56,1.1,2.91,2.63,3.22c-0.28,0.07-0.57,0.11-0.86,0.11
    	            c-0.21,0-0.42-0.01-0.62-0.06c0.43,1.33,1.66,2.25,3.07,2.29c-1.16,0.91-2.59,1.4-4.07,1.4c-0.26,0-0.52-0.02-0.78-0.05
    	            c4.34,2.78,10.12,1.51,12.9-2.83c0.96-1.5,1.47-3.25,1.47-5.03c0-0.14,0-0.29-0.01-0.42C23,12.28,23.56,11.7,24,11.04z"/>
    	        </svg>
    	    </a>
    	    <a href="<?php echo $page ? 'https://www.facebook.com/sharer/sharer.php?u='.urlencode($page->url()) : '#'?>" class="cis__card__icon-button cis__card__icon-button--facebook external-link<?php echo $page ? '' : ' ener__button--inactive'?>" target="_blank">
    	        <svg class="" x="0px" y="0px" width="32px"
    	             height="32px" viewBox="0 0 32 32" style="overflow:visible;enable-background:new 0 0 32 32;" xml:space="preserve">
    	            <path class="ener__button__svg__icon" d="M24,16.05C24,11.6,20.42,8,16,8s-8,3.6-8,8.05c0,4.02,2.93,7.35,6.75,7.95v-5.62h-2.03v-2.33h2.03v-1.77
    	                c0-2.02,1.19-3.13,3.02-3.13c0.88,0,1.79,0.16,1.79,0.16v1.98h-1.01c-0.99,0-1.3,0.62-1.3,1.26v1.51h2.22l-0.35,2.33h-1.86V24
    	                C21.07,23.4,24,20.07,24,16.05L24,16.05z"/>
    	        </svg>
    	    </a>
    	    <a href="<?php echo $page ? 'https://www.linkedin.com/sharing/share-offsite/?url='.urlencode($page->url()) : '#'?>" class="cis__card__icon-button cis__card__icon-button--linked-in external-link<?php echo $page ? '' : ' ener__button--inactive'?>" target="_blank">
    	        <svg class="" x="0px" y="0px" width="32px"
    	             height="32px" viewBox="0 0 32 32" style="overflow:visible;enable-background:new 0 0 32 32;" xml:space="preserve">
    	            <path class="ener__button__svg__icon" d="M22.77,8H9.23C8.59,7.99,8.06,8.51,8.05,9.15c0,0,0,0,0,0v13.7c0.01,0.64,0.53,1.16,1.17,1.15c0,0,0,0,0,0 h13.54c0.64,0.01,1.17-0.51,1.17-1.15c0,0,0,0,0,0V9.15C23.94,8.51,23.41,7.99,22.77,8z M10.47,21.39v-7.22h2.4v7.22H10.47z M11.67,13.18h-0.02c-0.76,0-1.37-0.62-1.36-1.38c0-0.76,0.62-1.37,1.38-1.36c0.76,0,1.37,0.62,1.36,1.38 C13.03,12.56,12.42,13.17,11.67,13.18z M21.53,21.39h-2.4v-3.86c0-0.97-0.35-1.63-1.22-1.63c-0.56,0-1.05,0.35-1.24,0.88 c-0.06,0.19-0.09,0.39-0.08,0.59v4.03h-2.4c0,0,0.03-6.55,0-7.22h2.4v1.02c0.44-0.77,1.28-1.23,2.16-1.19 c1.58,0,2.76,1.03,2.76,3.25L21.53,21.39z"/>
    	        </svg>
    	    </a>
    	    <a href="<?php echo $page->url(); ?>" class="cis__card__icon-button cis__card__icon-button--copy-link copy-link">
    	        <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path d="M343.37,372l-6.5,6.51,0,0a9.29,9.29,0,0,1,4.78.52l4.41-4.42a3.75,3.75,0,0,1,5.31,5.31c-.25.25-6.88,6.88-6.51,6.5a3.78,3.78,0,0,1-5.3,0,1.88,1.88,0,0,0-2.65,0l-1.14,1.14a7.14,7.14,0,0,0,1.14,1.51,7.55,7.55,0,0,0,10.56,0l0,0,6.51-6.5A7.5,7.5,0,0,0,343.37,372Z" transform="translate(-324.17 -369.82)"/><path d="M338.75,392.54,334.33,397a3.75,3.75,0,1,1-5.31-5.31l6.52-6.51a3.77,3.77,0,0,1,5.3,0,1.89,1.89,0,0,0,2.66,0l1.13-1.14a7.14,7.14,0,0,0-1.13-1.52,7.56,7.56,0,0,0-10.57,0l0,0L326.37,389A7.5,7.5,0,0,0,337,399.62l6.52-6.52,0,0a9.52,9.52,0,0,1-4.78-.52Z" transform="translate(-324.17 -369.82)"/></svg>
    	        <div class="cis__card__icon-button__tooltip copy-link-tooltip"><span><?php echo t('copied'); ?></span></div>
    	    </a>
    	</div>
    	<!-- SHARE POST -->
    <?php endif ?>
    <?php if($block->tagtype() == 'icon'): ?>
    	<?php if($block->icon()->isNotEmpty() && $block->icon() == 'logo'): ?>
    		<svg class="js-observe fade-animation" xmlns="http://www.w3.org/2000/svg" width="64" height="64" viewBox="0 0 64 64">
    		  <g id="__icon-cis-logo" transform="translate(-9206 -10242)">
    		    <circle cx="32" cy="32" r="32" transform="translate(9206 10242)" fill="#fff"/>
    		    <path d="M332.293,1034.882l.15.328.084.174a54.661,54.661,0,0,1,5.548-.11,31.15,31.15,0,0,0-6.148-7.632l-.368-.286c-.938.264-1.4.426-2.358,1.992,0,0-1.63,2.888-1.95,3.464-.276.5-1.036,1.872-1.616,3.116a37.617,37.617,0,0,1,6.3-1.016Z" transform="translate(8901.099 9235.961)" class="fill--<?php echo $block->iconcolor(); ?>"/>
    		    <path data-name="Path 51" d="M336.469,1035.064l-.15-.328-.084-.174a53.96,53.96,0,0,1-5.548.11,31.651,31.651,0,0,0,6.116,7.6l.544.274c.846-.244,1.306-.466,2.218-1.95,0,0,1.63-2.888,1.95-3.464.276-.5,1.036-1.872,1.616-3.116a37.67,37.67,0,0,1-6.3,1.016Z" transform="translate(8906.151 9242.627)" class="fill--<?php echo $block->iconcolor(); ?>"/>
    		    <path data-name="Path 52" d="M335.038,1037.3l-.5-.822.626.024-.1-.174-.2-.336c-.072-.126-.15-.252-.218-.378l-.2-.346-.226-.406-.19-.35c-.078-.144-.156-.292-.236-.44l-.184-.35c-.082-.158-.166-.322-.25-.486l-.172-.338c-.092-.186-.186-.376-.28-.568-.048-.1-.1-.192-.15-.29-.062-.128-.126-.262-.188-.394a34.924,34.924,0,0,0-6.708,1.108l-.378.11c-.01.032-.024.066-.034.1-.024.072-.044.14-.062.2s-.026.114-.038.168a2.45,2.45,0,0,0-.086.648v.15a1.351,1.351,0,0,0,.02.184,3.139,3.139,0,0,0,.062.312c.01.042.02.086.03.13l.01.038.01.036c.028.1.064.2.1.318s.1.27.158.414q.069.168.15.348l.066.15c.59,1.324,1.468,2.906,1.77,3.45.322.584,1.3,2.38,2.146,3.612l.14.2c.082.114.164.222.242.324s.17.214.25.3a4.9,4.9,0,0,0,.4.372l.1.078a2.3,2.3,0,0,0,.748.434c.138.05.284.1.45.15a23.74,23.74,0,0,0,4.076.292h1.5c.8,0,2.3-.01,3.528-.109A34.47,34.47,0,0,1,335.038,1037.3Z" transform="translate(8900.732 9240.25)" class="fill--<?php echo $block->iconcolor(); ?>"/>
    		    <path data-name="Path 53" d="M335.487,1035.184l.5.826-.63-.026c.84,1.416,1.712,3.03,2.584,4.86a34.99,34.99,0,0,0,6.71-1.108l.372-.108c.038-.106.07-.21.1-.3.014-.058.026-.114.036-.17a2.5,2.5,0,0,0,0-1.3c-.012-.054-.024-.112-.036-.17a46.31,46.31,0,0,0-5.036-9.158,4.54,4.54,0,0,0-.494-.45,2.75,2.75,0,0,0-1.2-.578,32.147,32.147,0,0,0-5.226-.3c-.622,0-2.426-.006-3.872.112A34.7,34.7,0,0,1,335.487,1035.184Z" transform="translate(8904.757 9235.805)" class="fill--<?php echo $block->iconcolor(); ?>"/>
    		  </g>
    		</svg>
    		<div class="item-40"></div>
    	<?php endif ?>
    <?php endif ?>    
    <?php if($block->tagtype() == 'ctabutton'): ?>
    	<?php if($block->link() == 'page' && $block->pagelink()->toPage()): ?>
	    <a href="<?php echo $block->pagelink()->toPage()->url(); ?>" class="js-observe fade-and-slide-animation cis__arrow-cta cis__arrow-cta--<?php echo $block->ctabuttoncolor(); ?> section-link" data-uri="<?php echo $block->pagelink()->toPage()->uri(); ?>" data-title="<?php echo $block->pagelink()->toPage()->title(); ?>" data-text="<?php echo $block->text() ?>">
	        <span class="cis__arrow-cta__deco-bar"></span>
	        <span class="cis__arrow-cta__text"><?php echo $block->text() ?></span>
	        <span class="cis__arrow-cta__arrow">
	            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="19.81px"height="15.41px" viewBox="0 0 19.81 15.41" style="overflow:visible;enable-background:new 0 0 19.81 15.41;"xml:space="preserve"><path class="cis__arrow__st0" d="M9.96,2.19l3.01,3.01l1.66,0.8H0v3.33h14.55L13,10.1l-3.06,3.05l2.11,2.26l7.76-7.74 L12.15,0L9.96,2.19z"/> </svg>
	        </span>
	        <span class="cis__arrow-cta__ellipse">
	            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="80px"height="80px" viewBox="0 0 80 80" style="overflow:visible;enable-background:new 0 0 80 80;" xml:space="preserve"> <defs> </defs><circle class="cis__arrow-cta__ellipse__st0" cx="40" cy="40" r="40"/> <circle class="cis__arrow-cta__ellipse__st1" cx="40" cy="40" r="39"/> </svg>
	        </span>
	    </a>
	    <?php elseif($block->link() == 'url' && $block->pageurl()->isNotEmpty()): ?>
		    <a href="<?php echo $block->pageurl(); ?>" class="js-observe fade-and-slide-animation cis__arrow-cta cis__arrow-cta--<?php echo $block->ctabuttoncolor(); ?> external-link" data-text="<?php echo $block->text() ?>" target="_blank">
		        <span class="cis__arrow-cta__deco-bar"></span>
		        <span class="cis__arrow-cta__text"><?php echo $block->text() ?></span>
		        <span class="cis__arrow-cta__arrow">
		            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="19.81px"height="15.41px" viewBox="0 0 19.81 15.41" style="overflow:visible;enable-background:new 0 0 19.81 15.41;"xml:space="preserve"><path class="cis__arrow__st0" d="M9.96,2.19l3.01,3.01l1.66,0.8H0v3.33h14.55L13,10.1l-3.06,3.05l2.11,2.26l7.76-7.74 L12.15,0L9.96,2.19z"/> </svg>
		        </span>
		        <span class="cis__arrow-cta__ellipse">
		            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="80px"height="80px" viewBox="0 0 80 80" style="overflow:visible;enable-background:new 0 0 80 80;" xml:space="preserve"> <defs> </defs><circle class="cis__arrow-cta__ellipse__st0" cx="40" cy="40" r="40"/> <circle class="cis__arrow-cta__ellipse__st1" cx="40" cy="40" r="39"/> </svg>
		        </span>
		    </a>
    	<?php endif ?>
    <?php endif ?>

  </div>