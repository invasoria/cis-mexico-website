<?php if ($block->media()->isNotEmpty()): ?>

  <?php

    // Classes

    $class = 'row row-fill-empty-columns';

    if ($block->gutter()->bool()) {
      $class = $class . ' row-gutter-sm';
    } else {
      $class = $class . ' row-gutter-none';

      if ($block->border()->bool()) {
        $class = $class . ' border';
      }

      if ($block->rounded()->bool()) {
        $class = $class . ' rounded';
      }

      if ($block->shadow()->value() !== 'none') {
        $class = $class . ' shadow-' . $block->shadow();
      }
    }

    if ($block->columnsResponsive()->value() === 'same') {
      $class = $class . ' row-keep-proportions';
    } elseif ($block->columnsResponsive()->value() === '2') {
      $class = $class . ' row-min-two-columns';
    }
  ?>
  <div class="<?php echo $class; ?>">

    <?php

      // Define image sizes

      if (in_array($block->columns()->value(), ['3', '4', '5'])) {
        $sizes = 'srcset-sizes-half';
        $srcset = 'half';
      } else {
        $sizes = 'srcset-sizes-default';
        $srcset = '';
      }

      foreach ($block->media()->toFiles() as $mediaFile):

    ?>

      <?php

        // Classes

        $class = 'col-1-' . $block->columns() . ' align-items-' . $block->mediaPositionVertical() . ' gallery-item';

        if ($block->gutter()->bool()) {
          if ($block->border()->bool()) {
            $class = $class . ' border';
          }

          if ($block->rounded()->bool()) {
            $class = $class . ' rounded';
          }

          if ($block->shadow()->value() !== 'none') {
            $class = $class . ' shadow-' . $block->shadow();
          }
        }
      ?>
      <div class="<?php echo $class; ?>">
        <img class="full-width object-position-<?= $block->mediaPositionHorizontal() ?>" draggable="false" loading="lazy" src="<?= $mediaFile->url() ?>" srcset="<?= $mediaFile->srcset($srcset) ?>" sizes="<?php snippet($sizes) ?>" alt="<?= $mediaFile->alt() ?>">
      </div>

    <?php endforeach ?>

  </div>
<?php endif ?>
