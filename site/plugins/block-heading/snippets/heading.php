<?php if ($block->text()->isNotEmpty()): ?>
  <?php 
    $level = $block->fontSize();
    if ($level == 'hero__h1') {
      $level = 'h1';
    } else if ($level == 'jumbo__h1') {
      $level = 'h1';
    }
    //
    // TEXT ANIMATION
    //
    if ($block->animation()->isNotEmpty()) {
      $animation = $block->animation();
      switch ($animation) {
        case 'reveal_text_animation':
          $animation = 'js-observe reveal-text-animation';
          break;
        case 'reveal_block_animation':
          $animation = 'js-observe reveal-block-animation';
          break;
        case 'fade_animation':
          $animation = 'js-observe fade-animation';
          break;
        case 'fade_and_slide_animation':
          $animation = 'js-observe fade-and-slide-animation';
          break;  
        default:
          break;
      }
    } else{
      $animation = '';
    }
  ?>
  <div class="is-col<?php echo $block->width()->isNotEmpty() ? ' col-'.ltrim($block->width(), 's'):''; ?><?php echo $block->offset()->isNotEmpty() ? ' offset-'.ltrim($block->offset(), 's'):''; ?><?php echo $block->widthm()->isNotEmpty() ? ' md-col-'.ltrim($block->widthm(), 's'):''; ?><?php echo $block->offsetm()->isNotEmpty() ? ' md-offset-'.ltrim($block->offsetm(), 's'):''; ?><?php echo $block->widths()->isNotEmpty() ? ' xs-col-'.ltrim($block->widths(), 's'):''; ?><?php echo $block->offsets()->isNotEmpty() ? ' xs-offset-'.ltrim($block->offsets(), 's'):''; ?><?php echo $block->colorunderline()->isNotEmpty() ? ' --color-underline--' . $block->colorunderline() : '';  ?><?php echo $block->colorhighlight()->isNotEmpty() ? ' --color-highlight--' . $block->colorhighlight() : '';  ?><?php echo $block->modularPadding()->bool() ? ' content__modular-block-padding' : '';  ?> --z-index-30"<?php echo $block->visibility()->bool() ? '':' style="display: none;"'; ?>>
    <<?php echo $level; ?> class="text--<?php echo $block->textcolor(); ?> content__<?= $block->fontSize() ?> content__<?= $block->alignContent() ?><?php echo $block->textIndent()->bool() ? ' content__text-indent' : ''; ?><?php echo $block->topMargin()->bool() ? ' content__top-margin' : ' content__no-top-margin'; ?><?php echo $block->bottomMargin()->bool() ? '' : ' content__no-bottom-margin'; ?><?php echo $animation != '' ? ' '. $animation :''; ?>"><?= $block->text() ?></<?= $level ?>>
  </div>
<?php endif ?>
