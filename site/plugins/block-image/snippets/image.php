<?php if ($block->media()->isNotEmpty()): ?>

<?php 
  //
  // TEXT ANIMATION
  //
  if ($block->animation()->isNotEmpty()) {
    $animation = $block->animation();
    switch ($animation) {
      case 'reveal_text_animation':
        $animation = 'js-observe reveal-text-animation';
        break;
      case 'reveal_block_animation':
        $animation = 'js-observe reveal-block-animation';
        break;
      case 'fade_animation':
        $animation = 'js-observe fade-animation';
        break;
      case 'fade_and_slide_animation':
        $animation = 'js-observe fade-and-slide-animation';
        break;  
      default:
        break;
    }
  } else{
    $animation = '';
  }
?>
  
  <?php if ($block->link()->value() === 'page' && $block->linkPage()->isNotEmpty() && $block->linkPage()->toPage()): ?>
    

    <?php if($image = $block->media()->toFile()): ?>
    <a href="<?= $block->linkPage()->toPage()->url() ?>" class="is-col<?php echo $block->width()->isNotEmpty() ? ' col-'.ltrim($block->width(), 's'):''; ?><?php echo $block->offset()->isNotEmpty() ? ' offset-'.ltrim($block->offset(), 's'):''; ?><?php echo $block->widthm()->isNotEmpty() ? ' md-col-'.ltrim($block->widthm(), 's'):''; ?><?php echo $block->offsetm()->isNotEmpty() ? ' md-offset-'.ltrim($block->offsetm(), 's'):''; ?><?php echo $block->widths()->isNotEmpty() ? ' xs-col-'.ltrim($block->widths(), 's'):''; ?><?php echo $block->offsets()->isNotEmpty() ? ' xs-offset-'.ltrim($block->offsets(), 's'):''; ?> section-link<?php echo $block->modularPadding()->bool() ? ' content__modular-block-padding' : '';  ?>" data-url="<?php echo $block->linkPage()->toPage()->url() ?>" data-title="<?php echo $block->linkPage()->toPage()->title() ?>" data-uri="<?php echo $block->linkPage()->toPage()->uri() ?>"<?php echo $block->visibility()->bool() ? '':' style="display: none;"'; ?>>
      <figure class="--figure<?php echo $block->alignBlock()->isNotEmpty() ? ' --figure-align-' . $block->alignBlock() :''; ?>" <?php if(false): ?>data-scroll data-scroll-speed="3" data-scroll-offset="-500,0"<?php endif ?>>
          <div class="--figure__container"<?php echo $image ? ' style="padding-top:' . ($image->height() / $image->width()) * 100 . '%;"' : '';?>>
            <picture class="js-lazy-image --special-transform js-can-use-webp" data-alt="<?php echo $block->alt(); ?>"<?php echo $image->parent()->image($image->name().'.webp') ? ' data-webp="'.$image->parent()->image($image->name().'.webp')->url() .'"': ''; ?><?php echo $image ? ' style="padding-top:' . ($image->height() / $image->width()) * 100 . '%;"' : '';?>>
                <source srcset="<?php echo $image->url(); ?>">
            </picture>
          </div>
          <figcaption><div class="content"><?php echo $block->caption(); ?></div></figcaption>
      </figure>
    </a>
    <?php endif ?>


  <?php elseif ($block->link()->value() === 'url' && $block->linkUrl()->isNotEmpty()): ?>
    <?php if($image = $block->media()->toFile()): ?>
      <a href="<?= $block->linkUrl(); ?>" class="is-col<?php echo $block->width()->isNotEmpty() ? ' col-'.ltrim($block->width(), 's'):''; ?><?php echo $block->offset()->isNotEmpty() ? ' offset-'.ltrim($block->offset(), 's'):''; ?><?php echo $block->widthm()->isNotEmpty() ? ' md-col-'.ltrim($block->widthm(), 's'):''; ?><?php echo $block->offsetm()->isNotEmpty() ? ' md-offset-'.ltrim($block->offsetm(), 's'):''; ?><?php echo $block->widths()->isNotEmpty() ? ' xs-col-'.ltrim($block->widths(), 's'):''; ?><?php echo $block->offsets()->isNotEmpty() ? ' xs-offset-'.ltrim($block->offsets(), 's'):''; ?> external-link<?php echo $block->modularPadding()->bool() ? ' content__modular-block-padding' : '';  ?>" data-url="<?php echo $block->linkPage()->toPage()->url() ?>" data-title="<?php echo $block->linkPage()->toPage()->title() ?>" data-uri="<?php echo $block->linkPage()->toPage()->uri() ?>"<?php if ($block->linkTarget()->bool()): ?> target="_blank"<?php endif ?><?php echo $block->visibility()->bool() ? '':' style="display: none;"'; ?>>
        <figure class="--figure<?php echo $block->alignBlock()->isNotEmpty() ? ' --figure-align-' . $block->alignBlock() :''; ?>" <?php if(false): ?>data-scroll data-scroll-speed="3" data-scroll-offset="-500,0"<?php endif ?>>
            <div class="--figure__container"<?php echo $image ? ' style="padding-top:' . ($image->height() / $image->width()) * 100 . '%;"' : '';?>>
              <picture class="js-lazy-image --special-transform js-can-use-webp" data-alt="<?php echo $block->alt(); ?>"<?php echo $image->parent()->image($image->name().'.webp') ? ' data-webp="'.$image->parent()->image($image->name().'.webp')->url() .'"': ''; ?><?php echo $image ? ' style="padding-top:' . ($image->height() / $image->width()) * 100 . '%;"' : '';?>>
                  <source srcset="<?php echo $image->url(); ?>">
              </picture>
            </div>
            <figcaption><div class="content"><?php echo $block->caption(); ?></div></figcaption>
        </figure>
      </a>
    <?php endif ?>

  <?php else: ?>
    <?php if($image = $block->media()->toFile()): ?>
      <div class="is-col<?php echo $block->width()->isNotEmpty() ? ' col-'.ltrim($block->width(), 's'):''; ?><?php echo $block->offset()->isNotEmpty() ? ' offset-'.ltrim($block->offset(), 's'):''; ?><?php echo $block->widthm()->isNotEmpty() ? ' md-col-'.ltrim($block->widthm(), 's'):''; ?><?php echo $block->offsetm()->isNotEmpty() ? ' md-offset-'.ltrim($block->offsetm(), 's'):''; ?><?php echo $block->widths()->isNotEmpty() ? ' xs-col-'.ltrim($block->widths(), 's'):''; ?><?php echo $block->offsets()->isNotEmpty() ? ' xs-offset-'.ltrim($block->offsets(), 's'):''; ?><?php echo $block->modularPadding()->bool() ? ' content__modular-block-padding' : '';  ?>"<?php echo $block->visibility()->bool() ? '':' style="display: none;"'; ?>>
        <figure class="--figure<?php echo $block->alignBlock()->isNotEmpty() ? ' --figure-align-' . $block->alignBlock() :''; ?>" <?php if(false): ?>data-scroll data-scroll-speed="3" data-scroll-offset="-500,0"<?php endif ?>>
            <div class="--figure__container"<?php echo $image ? ' style="padding-top:' . ($image->height() / $image->width()) * 100 . '%;"' : '';?>>
              <picture class="js-lazy-image --special-transform js-can-use-webp" data-alt="<?php echo $block->alt(); ?>"<?php echo $image->parent()->image($image->name().'.webp') ? ' data-webp="'.$image->parent()->image($image->name().'.webp')->url() .'"': ''; ?><?php echo $image ? ' style="padding-top:' . ($image->height() / $image->width()) * 100 . '%;"' : '';?>>
                  <source srcset="<?php echo $image->url(); ?>">
              </picture>
            </div>
            <figcaption><div class="content"><?php echo $block->caption(); ?></div></figcaption>
        </figure>
      </div>
    <?php endif ?>
  <?php endif ?>

<?php endif ?>
