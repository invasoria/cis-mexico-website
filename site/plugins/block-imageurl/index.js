panel.plugin("tfk/block-imageurl", {
  blocks: {
    imageurl: {
      computed: {
        url() {
          if (this.content.url) {
            return this.content.url;
          }
          return false;
        }
      },
      template: `
        <template>
          <img @dblclick="open" v-if="url" :src="url">
          <figure @click="open" v-else class="k-block-figure"><button class="k-block-figure-empty k-button" type="button"><span aria-hidden="true" class="k-button-icon k-icon k-icon-url"><svg viewBox="0 0 16 16"><use xlink:href="#icon-url"></use></svg></span><span class="k-button-text">Link to image...</span></button></figure>
        </template>
      `
    }
  }
});