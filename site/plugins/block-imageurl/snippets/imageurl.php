<?php if ($block->url()->isNotEmpty()): ?>

  <?php

    // Classes

    $class = 'full-width';

    if ($block->border()->bool()) {
      $class = $class . ' border';
    }

    if ($block->rounded()->bool()) {
      $class = $class . ' rounded';
    }

    if ($block->shadow()->value() !== 'none') {
      $class = $class . ' shadow-' . $block->shadow();
    }
  ?>
  <?php if ($block->link()->value() === 'page' && $block->linkPage()->isNotEmpty() && $block->linkPage()->toPage()): ?>
    <a href="<?= $block->linkPage()->toPage()->url() ?>">
      <img class="<?php echo $class; ?>" loading="lazy" src="<?= $block->url() ?>" alt="">
    </a>
  <?php elseif ($block->link()->value() === 'url' && $block->linkUrl()->isNotEmpty()): ?>
    <a href="<?= $block->linkUrl() ?>"<?php if ($block->linkTarget()->bool()): ?> target="_blank"<?php endif ?>>
      <img class="<?php echo $class; ?>" loading="lazy" src="<?= $block->url() ?>" alt="">
    </a>
  <?php else: ?>
    <img class="<?php echo $class; ?>" loading="lazy" src="<?= $block->url() ?>" alt="">
  <?php endif ?>

<?php endif ?>
