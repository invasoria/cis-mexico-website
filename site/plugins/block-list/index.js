panel.plugin("tfk/block-list", {
  blocks: {
    list: {
      computed: {
        classes() {
          return "k-block-align-" + this.content.aligncontent;
        }
      },
      methods: {
        focus() {
          this.$refs.text.focus();
        }
      },
      template: `
        <template>
          <k-input
            :class="classes"
            :value="content.text"
            ref="text"
            type="list"
            @input="update({ text: $event })"
          />
        </template>
      `
    }
  }
});