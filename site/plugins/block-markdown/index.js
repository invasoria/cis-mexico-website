panel.plugin("tfk/block-markdown", {
  blocks: {
    markdown: {
      computed: {
        classes() {
          return "k-block-align-" + this.content.aligncontent + " k-block-type-markdown-text";
        },
        placeholder() {
          return this.field("text", {}).placeholder;
        }
      },
      methods: {
        focus() {
          this.$refs.text.focus();
        }
      },
      template: `
        <template>
          <k-input
            :buttons="false"
            :class="classes"
            :placeholder="placeholder"
            :value="content.text"
            ref="text"
            type="textarea"
            @input="update({ text: $event })"
          />
        </template>
      `
    }
  }
});