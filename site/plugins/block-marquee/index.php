<?php

Kirby::plugin('tfk/block-marquee', [
  'blueprints' => [
    'blocks/marquee' => __DIR__ . '/blueprints/marquee.yml'
  ],
  'snippets' => [
    'blocks/marquee' => __DIR__ . '/snippets/marquee.php'
  ]
]);