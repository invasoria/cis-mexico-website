panel.plugin("tfk/block-services", {
  blocks: {
    services: {
      computed: {
        classes() {
          return "k-block-align-" + this.content.aligncontent + " k-block-type-heading-" + this.content.fontsize;
        },
        marks() {
          return this.field("text", {}).marks;
        },
        placeholder() {
          return this.field("text", {}).placeholder;
        }
      },
      methods: {
        focus() {
          this.$refs.text.focus();
        }
      },
      template: `
        <template>
          <k-writer
            :class="classes"
            :inline="true"
            :marks="marks"
            :placeholder="placeholder"
            :value="content.text"
            ref="text"
            @input="update({ text: $event })"
          />
        </template>
      `
    }
  }
});