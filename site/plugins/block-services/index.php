<?php

Kirby::plugin('tfk/block-services', [
  'blueprints' => [
    'blocks/services' => __DIR__ . '/blueprints/services.yml'
  ],
  'snippets' => [
    'blocks/services' => __DIR__ . '/snippets/services.php'
  ]
]);