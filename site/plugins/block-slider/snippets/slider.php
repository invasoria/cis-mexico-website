<?php if ($block->media()->isNotEmpty()): ?>

  <?php 
    //
    // TEXT ANIMATION
    //
    if ($block->animation()->isNotEmpty()) {
      $animation = $block->animation();
      switch ($animation) {
        case 'reveal_text_animation':
          $animation = 'js-observe reveal-text-animation';
          break;
        case 'reveal_block_animation':
          $animation = 'js-observe reveal-block-animation';
          break;
        case 'fade_animation':
          $animation = 'js-observe fade-animation';
          break;
        case 'fade_and_slide_animation':
          $animation = 'js-observe fade-and-slide-animation';
          break;  
        default:
          break;
      }
    } else{
      $animation = '';
    }
  ?>
  <div class="is-col col-12">
    <div class="is-row is-flex">
        <div class="slider-block is-col<?php echo $block->width()->isNotEmpty() ? ' col-'.ltrim($block->width(), 's'):''; ?><?php echo $block->offset()->isNotEmpty() ? ' offset-'.ltrim($block->offset(), 's'):''; ?><?php echo $block->widthm()->isNotEmpty() ? ' md-col-'.ltrim($block->widthm(), 's'):''; ?><?php echo $block->offsetm()->isNotEmpty() ? ' md-offset-'.ltrim($block->offsetm(), 's'):''; ?><?php echo $block->widths()->isNotEmpty() ? ' xs-col-'.ltrim($block->widths(), 's'):''; ?><?php echo $block->offsets()->isNotEmpty() ? ' xs-offset-'.ltrim($block->offsets(), 's'):''; ?>">
          <?php foreach ($block->media()->toFiles() as $cell): ?>
            <div class="slider-block__cell">
              <?php $image = $cell; ?>
              <figure class="--figure<?php echo $block->alignBlock()->isNotEmpty() ? ' --figure-align-' . $block->alignBlock() :''; ?>" <?php if(false): ?>data-scroll data-scroll-speed="3" data-scroll-offset="-500,0"<?php endif ?>>
                  <div class="--figure__container"<?php echo $image ? ' style="padding-top:' . ($image->height() / $image->width()) * 100 . '%;"' : '';?>>
                    <picture class="js-lazy-image --special-transform js-can-use-webp" data-alt="<?php echo $block->alt(); ?>"<?php echo $image->parent()->image($image->name().'.webp') ? ' data-webp="'.$image->parent()->image($image->name().'.webp')->url() .'"': ''; ?><?php echo $image ? ' style="padding-top:' . ($image->height() / $image->width()) * 100 . '%;"' : '';?>>
                        <source srcset="<?php echo $image->url(); ?>">
                    </picture>
                  </div>
              </figure>
            </div>
          <?php endforeach ?>
        </div>
    </div>
    <div class="is-row is-flex --figcaption">
        <div class="is-col<?php echo $block->width()->isNotEmpty() ? ' col-'.ltrim($block->width(), 's'):''; ?><?php echo $block->offset()->isNotEmpty() ? ' offset-'.ltrim($block->offset(), 's'):''; ?><?php echo $block->widthm()->isNotEmpty() ? ' md-col-'.ltrim($block->widthm(), 's'):''; ?><?php echo $block->offsetm()->isNotEmpty() ? ' md-offset-'.ltrim($block->offsetm(), 's'):''; ?><?php echo $block->widths()->isNotEmpty() ? ' xs-col-'.ltrim($block->widths(), 's'):''; ?><?php echo $block->offsets()->isNotEmpty() ? ' xs-offset-'.ltrim($block->offsets(), 's'):''; ?>">
          <figure class="--figure<?php echo $block->alignBlock()->isNotEmpty() ? ' --figure-align-' . $block->alignBlock() :''; ?>" <?php if(false): ?>data-scroll data-scroll-speed="3" data-scroll-offset="-500,0"<?php endif ?>>
              <div class="--figure__container">
              </div>
              <figcaption><div class="content"><?php echo $block->caption(); ?></div></figcaption>
          </figure>
        </div>
    </div>
  </div>
<?php endif ?>
