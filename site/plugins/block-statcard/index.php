<?php

Kirby::plugin('tfk/block-statcard', [
  'blueprints' => [
    'blocks/statcard' => __DIR__ . '/blueprints/statcard.yml'
  ],
  'snippets' => [
    'blocks/statcard' => __DIR__ . '/snippets/statcard.php'
  ]
]);