  <figure class="cis__stat-card is-col<?php echo $block->width()->isNotEmpty() ? ' col-'.ltrim($block->width(), 's'):''; ?><?php echo $block->offset()->isNotEmpty() ? ' offset-'.ltrim($block->offset(), 's'):''; ?><?php echo $block->widthm()->isNotEmpty() ? ' md-col-'.ltrim($block->widthm(), 's'):''; ?><?php echo $block->offsetm()->isNotEmpty() ? ' md-offset-'.ltrim($block->offsetm(), 's'):''; ?><?php echo $block->widths()->isNotEmpty() ? ' xs-col-'.ltrim($block->widths(), 's'):''; ?><?php echo $block->offsets()->isNotEmpty() ? ' xs-offset-'.ltrim($block->offsets(), 's'):''; ?><?php echo $block->colorunderline()->isNotEmpty() ? ' --color-underline--' . $block->colorunderline() : '';  ?><?php echo $block->colorhighlight()->isNotEmpty() ? ' --color-highlight--' . $block->colorhighlight() : '';  ?> js-observe fade-and-slide-animation stat-card-animation --z-index-30" data-digit="<?php echo $block->digit(); ?>">
    <h6 class="cis__stat-card__eyebrow-heading"><?= $block->heading() ?></h6>
    <div class="cis__stat-card__content">
        <div class="is-row is-flex">
            <?php echo $block->blocks()->toBlocks(); ?>
        </div>
        <?php if($block->posttype()=='percent'): ?>
        <svg class="cis__stat-card__graph" viewbox="0 0 72 72" xmlns="http://www.w3.org/2000/svg">
            <circle id="circle" class="cis__stat-card__graph__track" cx="36" cy="36" r="32" />
            <path id="path" class="cis__stat-card__graph__line cis__stat-card__graph--<?php echo $block->graphcolor(); ?>" d="M576.75,328.59a32,32,0,1,0,32,32,32,32,0,0,0-32-32" transform="translate(-540.8 -324.59)" />
        </svg>
        <?php endif ?>
        <?php if($block->posttype()=='increment'): ?>
        <svg class="cis__stat-card__icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48">
            <g>
                <path class="cis__stat-card__icon--gray" d="M794.22,339.31h-4.8v48h48v-4.8h-43.2Z" transform="translate(-789.42 -339.31)"/>
                <path class="cis__stat-card__icon--gray cis__stat-card__icon--<?php echo $block->iconcolor1(); ?>" d="M825.42,346.51l4.32,4.32L820.62,360l-7.2-7.2L799,367.15l3.36,3.36,11-11,7.2,7.2,12.48-12.48,4.32,4.32v-12Z" transform="translate(-789.42 -339.31)"/>
            </g>
        </svg>
        <?php endif ?>
        <?php if($block->posttype()=='decrement'): ?>
        <svg class="cis__stat-card__icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48">
            <g>
                <path class="cis__stat-card__icon--gray" d="M873.22,339.31h-4.8v48h48v-4.8h-43.2Z" transform="translate(-868.42 -339.31)"/>
                <path class="cis__stat-card__icon--gray cis__stat-card__icon--<?php echo $block->iconcolor2(); ?>" d="M907.42,365.71,903.1,370,894,360.91l7.2-7.2-14.4-14.4-3.36,3.36,11,11-7.2,7.2,12.48,12.48-4.32,4.32h12Z" transform="translate(-868.42 -339.31)"/>
            </g>
        </svg>
        <?php endif ?>
    </div>
    <p class="cis__stat-card__subheading-p s"><?= $block->subheading() ?></p>
  </figure>