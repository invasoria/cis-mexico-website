<?php if ($block->text()->isNotEmpty() && in_array($block->alignContent()->value(), ['left', 'right'])): ?>
  <figure class="blockquote">

    <?php

      $class = 'row';

      if ($block->media()->isNotEmpty()) {
        $class = $class . ' row-keep-proportions';
      } else {
        $class = $class . ' row-fill-empty-columns';
      }
    ?>
    <div class="<?php echo $class; ?>">

      <?php if ($block->media()->isNotEmpty()): ?>
        <div class="col-1-4">
          <img class="full-width rounded" loading="lazy" src="<?= $block->media()->toFile()->url() ?>" srcset="<?= $block->media()->toFile()->srcset() ?>" sizes="<?php snippet('srcset-sizes-thumb') ?>" alt="<?= $block->media()->toFile()->alt() ?>">
        </div>
      <?php endif ?>

      <div class="col-3-4">
        <blockquote>
          <?= $block->text() ?>
        </blockquote>

        <?php if ($block->caption()->isNotEmpty()): ?>
          <figcaption><?= $block->caption() ?></figcaption>
        <?php endif ?>

      </div>
    </div>
  </figure>
<?php elseif ($block->text()->isNotEmpty()): ?>
  <figure class="blockquote">

    <?php if ($block->media()->isNotEmpty()): ?>
      <img class="actual-size full-width rounded space-bottom-15x" loading="lazy" src="<?= $block->media()->toFile()->url() ?>" srcset="<?= $block->media()->toFile()->srcset() ?>" sizes="<?php snippet('srcset-sizes-thumb') ?>" alt="<?= $block->media()->toFile()->alt() ?>">
    <?php endif ?>

    <blockquote>
      <?= $block->text() ?>
    </blockquote>

    <?php if ($block->caption()->isNotEmpty()): ?>
      <figcaption><?= $block->caption() ?></figcaption>
    <?php endif ?>

  </figure>
<?php endif ?>
