<?php if ($block->media()->isNotEmpty()): ?>

<?php 
  //
  // TEXT ANIMATION
  //
  if ($block->animation()->isNotEmpty()) {
    $animation = $block->animation();
    switch ($animation) {
      case 'reveal_text_animation':
        $animation = 'js-observe reveal-text-animation';
        break;
      case 'reveal_block_animation':
        $animation = 'js-observe reveal-block-animation';
        break;
      case 'fade_animation':
        $animation = 'js-observe fade-animation';
        break;
      case 'fade_and_slide_animation':
        $animation = 'js-observe fade-and-slide-animation';
        break;  
      default:
        break;
    }
  } else{
    $animation = '';
  }
?>
  
  <?php if ($block->link()->value() === 'page' && $block->linkPage()->isNotEmpty() && $block->linkPage()->toPage()): ?>

    <?php if($video = $block->media()->toFile()): ?>
    <a href="<?= $block->linkPage()->toPage()->url() ?>" class="is-col<?php echo $block->width()->isNotEmpty() ? ' col-'.ltrim($block->width(), 's'):''; ?><?php echo $block->offset()->isNotEmpty() ? ' offset-'.ltrim($block->offset(), 's'):''; ?><?php echo $block->widthm()->isNotEmpty() ? ' md-col-'.ltrim($block->widthm(), 's'):''; ?><?php echo $block->offsetm()->isNotEmpty() ? ' md-offset-'.ltrim($block->offsetm(), 's'):''; ?><?php echo $block->widths()->isNotEmpty() ? ' xs-col-'.ltrim($block->widths(), 's'):''; ?><?php echo $block->offsets()->isNotEmpty() ? ' xs-offset-'.ltrim($block->offsets(), 's'):''; ?><?php echo $block->topMargin()->bool() ? ' content__top-margin' : ' content__no-top-margin'; ?><?php echo $block->bottomMargin()->bool() ? ' content__bottom-margin' : ' content__no-bottom-margin'; ?> section-link<?php echo $block->modularPadding()->bool() ? ' content__modular-block-padding' : '';  ?>" data-url="<?php echo $block->linkPage()->toPage()->url() ?>" data-title="<?php echo $block->linkPage()->toPage()->title() ?>" data-uri="<?php echo $block->linkPage()->toPage()->uri() ?>"<?php echo $block->visibility()->bool() ? '':' style="display: none;"'; ?>>
      <?php 
          require_once('assets/php/getid3/getid3.php');
          $getID3 = new getID3;
          $file = $getID3->analyze($video->mediaRoot());
          $video_width = $file['video']['resolution_x'];
          $video_height = $file['video']['resolution_y'];
       ?>
       <figure class="--figure"<?php echo isset($video_width) && isset($video_height) ? ' style="padding-top:' . ($video_height / $video_width) * 100 . '%;"' : '';?>>
           <video class="js-lazy-image --special-transform" data-poster="" webkit-playsinline="webkit-playsinline" playsinline="playsinline" autoplay="autoplay" loop="loop" muted="muted">
               <source data-src="<?php echo $video->url() ?>" type="<?php echo $video->mime() ?>">
           </video>
           </picture>
       </figure>
    </a>
    <?php endif ?>


  <?php elseif ($block->link()->value() === 'url' && $block->linkUrl()->isNotEmpty()): ?>
    <?php if($video = $block->media()->toFile()): ?>
      <a href="<?= $block->linkUrl(); ?>" class="is-col<?php echo $block->width()->isNotEmpty() ? ' col-'.ltrim($block->width(), 's'):''; ?><?php echo $block->offset()->isNotEmpty() ? ' offset-'.ltrim($block->offset(), 's'):''; ?><?php echo $block->widthm()->isNotEmpty() ? ' md-col-'.ltrim($block->widthm(), 's'):''; ?><?php echo $block->offsetm()->isNotEmpty() ? ' md-offset-'.ltrim($block->offsetm(), 's'):''; ?><?php echo $block->widths()->isNotEmpty() ? ' xs-col-'.ltrim($block->widths(), 's'):''; ?><?php echo $block->offsets()->isNotEmpty() ? ' xs-offset-'.ltrim($block->offsets(), 's'):''; ?><?php echo $block->topMargin()->bool() ? ' content__top-margin' : ' content__no-top-margin'; ?><?php echo $block->bottomMargin()->bool() ? ' content__bottom-margin' : ' content__no-bottom-margin'; ?> external-link<?php echo $block->modularPadding()->bool() ? ' content__modular-block-padding' : '';  ?>" data-url="<?php echo $block->linkPage()->toPage()->url() ?>" data-title="<?php echo $block->linkPage()->toPage()->title() ?>" data-uri="<?php echo $block->linkPage()->toPage()->uri() ?>"<?php if ($block->linkTarget()->bool()): ?> target="_blank"<?php endif ?><?php echo $block->visibility()->bool() ? '':' style="display: none;"'; ?>>
        <?php 
            require_once('assets/php/getid3/getid3.php');
            $getID3 = new getID3;
            $file = $getID3->analyze($video->mediaRoot());
            $video_width = $file['video']['resolution_x'];
            $video_height = $file['video']['resolution_y'];
         ?>
         <figure class="--figure"<?php echo isset($video_width) && isset($video_height) ? ' style="padding-top:' . ($video_height / $video_width) * 100 . '%;"' : '';?>>
             <video class="js-lazy-image --special-transform" data-poster="" webkit-playsinline="webkit-playsinline" playsinline="playsinline" autoplay="autoplay" loop="loop" muted="muted">
                 <source data-src="<?php echo $video->url() ?>" type="<?php echo $video->mime() ?>">
             </video>
             </picture>
         </figure>
      </a>
    <?php endif ?>

  <?php else: ?>
    <?php if($video = $block->media()->toFile()): ?>
      <div class="is-col<?php echo $block->width()->isNotEmpty() ? ' col-'.ltrim($block->width(), 's'):''; ?><?php echo $block->offset()->isNotEmpty() ? ' offset-'.ltrim($block->offset(), 's'):''; ?><?php echo $block->widthm()->isNotEmpty() ? ' md-col-'.ltrim($block->widthm(), 's'):''; ?><?php echo $block->offsetm()->isNotEmpty() ? ' md-offset-'.ltrim($block->offsetm(), 's'):''; ?><?php echo $block->widths()->isNotEmpty() ? ' xs-col-'.ltrim($block->widths(), 's'):''; ?><?php echo $block->offsets()->isNotEmpty() ? ' xs-offset-'.ltrim($block->offsets(), 's'):''; ?><?php echo $block->topMargin()->bool() ? ' content__top-margin' : ' content__no-top-margin'; ?><?php echo $block->bottomMargin()->bool() ? ' content__bottom-margin' : ' content__no-bottom-margin'; ?><?php echo $block->modularPadding()->bool() ? ' content__modular-block-padding' : '';  ?>"<?php echo $block->visibility()->bool() ? '':' style="display: none;"'; ?>>
        <?php 
            require_once('assets/php/getid3/getid3.php');
            $getID3 = new getID3;
            $file = $getID3->analyze($video->mediaRoot());
            $video_width = $file['video']['resolution_x'];
            $video_height = $file['video']['resolution_y'];
         ?>
         <figure class="--figure"<?php echo isset($video_width) && isset($video_height) ? ' style="padding-top:' . ($video_height / $video_width) * 100 . '%;"' : '';?>>
             <video class="js-lazy-image --special-transform" data-poster="" webkit-playsinline="webkit-playsinline" playsinline="playsinline" autoplay="autoplay" loop="loop" muted="muted">
                 <source data-src="<?php echo $video->url() ?>" type="<?php echo $video->mime() ?>">
             </video>
             </picture>
         </figure>
      </div>
    <?php endif ?>
  <?php endif ?>

<?php endif ?>
