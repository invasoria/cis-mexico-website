<?php $numRow = 0; foreach ($page->toLayouts() as $row):
  $numRow = ++$numRow;
?>
  <!-- LAYOUT SECTION -->
  <section class="--section<?php echo $row->layoutContainer()->bool() ? ' grid-margin' : ''; ?><?php echo $row->colorbg()->isNotEmpty() ? ' --section-bg--' . $row->colorbg() : ' --section-bg--light';  ?>"<?php echo $row->visibility()->bool() ? '':' style="display: none;"'; ?>>
    <div class="is-row is-flex<?php echo $row->layoutContainer()->bool() ? ' column-padding' : ' is-gapless'; ?>">
      <div class="is-col<?php echo $row->layoutContainer()->bool() ? ' col-11 offset-_5' : ' col-12'; ?>">

        <!-- LAYOUT ROW -->
        <div class="is-row is-flex<?php echo $row->paddingTop()->isNotEmpty() ? ' --padding-top-'.ltrim($row->paddingTop(), 's'):''; ?><?php echo $row->paddingBottom()->isNotEmpty() ? ' --padding-bottom-'.ltrim($row->paddingBottom(), 's'):''; ?><?php echo $row->paddingMobileTop()->isNotEmpty() ? ' --padding-mobile-top-'.ltrim($row->paddingMobileTop(), 's'):''; ?><?php echo $row->paddingMobileBottom()->isNotEmpty() ? ' --padding-mobile-bottom-'.ltrim($row->paddingMobileBottom(), 's'):''; ?>"<?php echo $row->customId()->isNotEmpty() ? ' id="' . $row->customId() . '"' : ''; ?>>
            <?php $numColumn = 0; foreach ($row->columns() as $column): $numColumn = ++$numColumn; ?>
              <?php if ($column->blocks()->isNotEmpty()): ?>
                <?php
                  // Classes
                  if ($column->span() === 12) {
                    $class = '12';
                  } elseif ($column->span() === 6) {
                    $class = '6';
                  } elseif ($column->span() === 4) {
                    $class = '4';
                  } elseif ($column->span() === 3) {
                    $class = '3';
                  } elseif ($column->span() === 8) {
                    $class = '8';
                  }
                ?>
                <!-- COLUMN -->
                  <div class="is-col col-<?php echo $class; ?>"<?php echo $row->flexColumn()->bool() ? ' style="display: flex; flex-direction: column;"':''; ?>>
                    <div class="is-row is-row--responsive is-flex">
                      <?php $numBlock = 0; foreach ($column->blocks() as $block): $numBlock = ++$numBlock; ?>
                        <?php
                          echo $block;
                        ?>
                      <?php endforeach ?>
                    </div>
                  </div>
                <!-- COLUMN -->
              <?php endif ?>
            <?php endforeach ?>
        </div>
        <!-- LAYOUT ROW -->

      </div>
    </div>
  </section>
  <!-- LAYOUT SECTION -->
<?php endforeach ?>
