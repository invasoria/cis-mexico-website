<div class="main-carousel__cell grid-margin">
    <?php if($data->bgtype() == 'image' && $slide_image = $data->image()->toFile()): ?>
    <div class="main-carousel__cell__container">
        <div class="is-row is-flex column-padding">
            <div class="is-col col-4 offset-7" data-scroll data-scroll-speed="2">
                <picture class="js-lazy-image --special-transform js-can-use-webp" style="display: block; min-height: 1rem;" data-alt="<?php echo $site->title(); ?>"<?php echo $page->image($slide_image->name().'.webp') ? ' data-webp="'.$page->image($slide_image->name().'.webp')->url() .'"': ''; ?>>
                    <source srcset="<?php echo $slide_image->url(); ?>">
                </picture>
            </div>
        </div>
    </div>
    <?php endif ?>
    <div class="is-row is-flex column-padding" style="position: relative; z-index: 50;">
        <div class="is-col col-10 offset-1">
            <?php echo $data->heading()->toBlocks(); ?>
            <div class="is-row is-flex">
                <div class="is-col col-8">
                    <?php if($data->counter()->bool()): ?>
                    <?php 
                        $programs = page('programas')->children()->listed();
                        $seasons = page('programas')->grandChildren()->listed();
                        $episodes = page('programas')->children()->listed()->children()->listed()->children()->listed();
                     ?>
                    <h2><a href="<?php echo page('programas')->url(); ?>" class="section-link" data-title="<?php echo page('programas')->title(); ?>"><?php echo $programs->count(); ?> Programas</a>, <a href="<?= url(page('programas')->url(), ['params' => ['category' => urlencode('Temporadas')]]) ?>" class="section-link" data-title="<?php echo page('programas')->title(); ?>"><?php echo $seasons->count(); ?> Temporadas</a>, <a href="<?= url(page('programas')->url(), ['params' => ['category' => urlencode('Episodios')]]) ?>" class="section-link" data-title="<?php echo page('programas')->title(); ?>"><?php echo $episodes->count(); ?> Episodios</a>.</h2>
                    <?php endif ?>
                    <?php echo $data->subheading()->toBlocks(); ?>
                    <div class="item-24"></div>
                    <?php 
                        foreach ($data->ctas()->toStructure() as $cta) {
                            if($data->ctas()->isnotEmpty()) {
                                foreach ($cta->intpage()->yaml() as $item) {
                                    $int_page = $item;
                                }
                                echo '<a href="' . ($cta->link() == 'url' ? $cta->url() : (page($int_page) ? page($int_page)->url() : 'error')) . '" class="main-cta' . ($cta->style() == 'secondary' ? ' main-cta--outline' : ' ') . ($cta->link() == 'url' ? ' external-link' : ' section-link') . '" data-uri="" data-title="" data-text="' . $cta->text() . '"><span>' . $cta->text() . '</span></a>';
                            }
                        }
                    ?>
                </div>
                <?php if($data->latest()->bool()): ?>
                <div class="is-col col-3 offset-_5">
                    <?php snippet("modules/latest.stuff") ?>
                </div>
                <?php endif ?>
            </div>
        </div>
    </div>
</div>