<div class="is-row is-flex column-padding">
    <div class="is-col offset-1 col-4_5">
        <div class="ener__product-tour__picture-container" data-scroll data-scroll-speed="-1" data-scroll-offset="-500,0">
                <?php if($image_1 = $data->image()->toFile()): ?>
                <picture class="ener__product-tour__picture-container__picture js-lazy-image --special-transform js-can-use-webp" data-alt="<?php echo ($data->interaction() == true && $data->mode() == 'listen' ? 'Prueba ya: escucha un episodio' : 'Prueba ya: descargar archivo') ?>"<?php echo page('acerca-de')->image($image_1->name().'.webp') ? ' data-webp="'.page('acerca-de')->image($image_1->name().'.webp')->url() .'"': ''; ?><?php echo $image_1 ? 'style="padding-top:' . ($image_1->height() / $image_1->width()) * 100 . '%;"' : '';?>>
                    <source srcset="<?php echo $image_1->url(); ?>">
                </picture>
                <?php endif ?>
                <?php if($data->decoration()->bool()): ?>
                <figure class="ener__product-tour__falling ener__product-tour__falling-11" data-scroll data-scroll-speed="-1" data-scroll-offset="-1500,0">
                    <picture class="js-lazy-image js-can-use-webp" data-alt="Falling Petal" data-webp="<?php echo $site->url(); ?>/assets/images/petal_11.webp" data-scroll data-scroll-speed="-1.25" data-scroll-offset="-500,0" <?php $image_1 = image($site->url().'/assets/images/petal_11.png');  ?> style="padding-top:<?php echo (image($image_1)->height() / image($image_1)->width()) * 100 . '%;'?>">
                        <source srcset="<?php echo $site->url(); ?>/assets/images/petal_11.png">
                    </picture>
                </figure>

                <figure class="ener__product-tour__falling ener__product-tour__falling-12" data-scroll data-scroll-speed="2" data-scroll-offset="-1500,0">
                    <picture class="js-lazy-image js-can-use-webp" data-alt="Falling Petal" data-webp="<?php echo $site->url(); ?>/assets/images/petal_12.webp" data-scroll data-scroll-speed="-1.25" data-scroll-offset="-500,0" <?php $image_2 = image($site->url().'/assets/images/petal_12.png');  ?> style="padding-top:<?php echo (image($image_2)->height() / image($image_2)->width()) * 100 . '%;'?>">
                        <source srcset="<?php echo $site->url(); ?>/assets/images/petal_12.png">
                    </picture>
                </figure>
                <?php endif ?>
        </div>
    </div>
    <div class="is-col offset-1 col-4 ener__product-tour__widget-3" data-scroll data-scroll-speed="3" data-scroll-offset="-800,0">
        <?php echo $data->heading()->toBlocks(); ?>

        <?php echo $data->interaction() == true && $data->mode() == 'listen' ? '<h3>Prueba ya: escucha un episodio</h3>' : '<h3>Prueba ya: descargar archivo</h3>'; ?>
        <?php $data_checked = $data; ?>
        <?php if($data_checked->interaction()->bool() && $data_checked->episode()->isNotEmpty() && $interaction_page = $data_checked->episode()->toPage()): ?>
        <?php if($file_int = $interaction_page->audiofile()->toFile()): ?>
        <?php 
            $program_target = '';
            $program_target = $interaction_page->autoid();
        ?>
        <button class="ener__widget ener__widget--light-bg <?php echo 'id-' . $program_target ?><?php echo $file_int == false || $file_int == 'false'  ? ' ener__button--inactive' : ' play-target'; ?>"<?php echo $file_int == false || $file_int == 'false'  ? ' disabled' : '' ; ?> data-episode="<?php echo $file_int; ?>" data-csrf="<?= csrf() ?>" data-target="<?php echo $program_target; ?>">
            <ul class="ener__widget__list">
                <li class="ener__widget__item ener__widget__item--button">
                    <div class="ener__button ener__button--line">
                        <svg class="ener__button__svg" x="0px" y="0px" width="48px"
                             height="48px" viewBox="0 0 48 48" style="overflow:visible;enable-background:new 0 0 48 48;" xml:space="preserve">
                            <circle class="ener__button__svg__transparent-bg__bg" cx="24" cy="24" r="24"/>
                            <circle class="ener__button__svg__transparent-bg__border" cx="24" cy="24" r="23"/>
                            <circle class="ener__button__svg__white-bg" cx="24" cy="24" r="24"/>
                            <path class="ener__button__svg__icon play-icon" d="M21.67,30l8-6l-8-6V30z"/>
                            <g class="ener__button__svg__icon display-none pause-icon">
                                <rect x="19" y="19" width="2.86" height="10"/>
                                <rect x="26.14" y="19" width="2.86" height="10"/>
                            </g>
                        </svg>
                    </div>
                </li>
                <li class="ener__widget__item ener__widget__item--info">
                    <p class="ener__widget__item__episode"><?php echo $interaction_page->title(); ?></p>
                    <p class="ener__widget__item__file-info"><?php if($interaction_page->parentprogram()->isNotEmpty()): ?><?php echo $interaction_page->parentprogram(); ?><?php endif ?> <span>–</span> <?php 
                            if (page($interaction_page) && $interaction_page->audiofile()->isnotEmpty() && $interaction_page->audiofile()->toFile()) {
                                $audio_file = $interaction_page->audiofile()->toFile();
                                require_once('assets/php/getid3/getid3.php');
                                $getID3 = new getID3;
                                $file_duration = $getID3->analyze($audio_file->mediaRoot());
                                $audio_file_playtime = floor(@$file_duration['playtime_seconds'] / 60); // playtime in minutes:seconds, formatted string
                                echo '<time datetime="' . $audio_file_playtime . '">' . $audio_file_playtime . ' min</time>';
                            }
                         ?><span class=""> /<?php if($file_int): ?> <?php echo $file_int->extension(); ?> / <?php echo floor(($file_int->size() / 1024) / 1000); ?> MB<?php endif ?></span></p>
                </li>
            </ul>
        </button>
        <?php endif ?>
        <?php endif ?>
    </div>
</div>