<div class="is-row is-flex column-padding">
    <div class="is-col offset-1 col-4_5">
        <div class="ener__product-tour__picture-container" data-scroll data-scroll-speed="-1" data-scroll-offset="-500,0">
                <?php if($image_1 = $data->image()->toFile()): ?>
                <picture class="ener__product-tour__picture-container__picture js-lazy-image --special-transform js-can-use-webp" data-alt="<?php echo ($data->interaction() == true && $data->mode() == 'listen' ? 'Prueba ya: escucha un episodio' : 'Prueba ya: descargar archivo') ?>"<?php echo page('acerca-de')->image($image_1->name().'.webp') ? ' data-webp="'.page('acerca-de')->image($image_1->name().'.webp')->url() .'"': ''; ?><?php echo $image_1 ? 'style="padding-top:' . ($image_1->height() / $image_1->width()) * 100 . '%;"' : '';?>>
                    <source srcset="<?php echo $image_1->url(); ?>">
                </picture>
                <?php endif ?>
                <?php if($data->decoration()->bool()): ?>
                <figure class="ener__product-tour__falling ener__product-tour__falling-11" data-scroll data-scroll-speed="-1" data-scroll-offset="-1500,0">
                    <picture class="js-lazy-image js-can-use-webp" data-alt="Falling Petal" data-webp="<?php echo $site->url(); ?>/assets/images/petal_11.webp" data-scroll data-scroll-speed="-1.25" data-scroll-offset="-500,0" <?php $image_1 = image($site->url().'/assets/images/petal_11.png');  ?> style="padding-top:<?php echo (image($image_1)->height() / image($image_1)->width()) * 100 . '%;'?>">
                        <source srcset="<?php echo $site->url(); ?>/assets/images/petal_11.png">
                    </picture>
                </figure>

                <figure class="ener__product-tour__falling ener__product-tour__falling-12" data-scroll data-scroll-speed="2" data-scroll-offset="-1500,0">
                    <picture class="js-lazy-image js-can-use-webp" data-alt="Falling Petal" data-webp="<?php echo $site->url(); ?>/assets/images/petal_12.webp" data-scroll data-scroll-speed="-1.25" data-scroll-offset="-500,0" <?php $image_2 = image($site->url().'/assets/images/petal_12.png');  ?> style="padding-top:<?php echo (image($image_2)->height() / image($image_2)->width()) * 100 . '%;'?>">
                        <source srcset="<?php echo $site->url(); ?>/assets/images/petal_12.png">
                    </picture>
                </figure>
                <?php endif ?>
        </div>
    </div>
    <div class="is-col offset-1 col-4 ener__product-tour__widget-3 ener__product-tour__widget-3--services" data-scroll data-scroll-speed="2" data-scroll-offset="-800,0">
        <?php echo $data->heading()->toBlocks(); ?>
        <?php $data_checked = $data; ?>
        <?php if($data_checked->interaction()->bool()): ?>
        <a href="mailto:info@estonoesradio.mx" class="main-cta external-link" target="_blank" data-text="PONTE EN CONTACTO"><span>PONTE EN CONTACTO</span></a>
        <?php endif ?>
    </div>
</div>