<?php if (false && $page_checked = $block->minicard()->toPage()): ?>
<?php 
    /*****
    *****
    Mini Card Snippet

    Needed Parameters:
    
    $page_checked -> Post page to get.
    $number -> Footnote ID

    *****
    *****/
    if ($block->randomepisode()->toBool() && $page_checked->hasListedChildren()) {
    	$check = $page_checked->children()->listed()->first();
    	if ($check->hasListedChildren()) {
    		$page_checked = $page_checked->children()->listed()->children()->listed()->shuffle()->limit(2)->first();
    	}
    }
    $url = $page_checked->url();
    $uri = $page_checked->uri();
    $title = $page_checked->title();
    $cover = $page_checked->cover();
    $number = $block->numberfootnote();
    //setlocale(LC_TIME, 'es_MX.utf8');
?>
<article class="ener__card--mini">
    <a href="<?php echo $url; ?>" data-uri="<?php echo $uri; ?>" data-title="<?php echo $title; ?>" class="ener__card--mini__container section-link">
        <?php echo $cover->toFile() ? '<img src="'.$cover->toFile()->resize(80)->url().'" alt="'.$title.'">':''; ?>
    </a>
    <a href="#note-<?php echo isset($number) ? $number:''; ?>" data-id="note-<?php echo isset($number) ? $number:''; ?>" class="ener__card--mini__footnote-link scroll-link">( <?php echo isset($number) ? $number:''; ?> )</a>
</article>
<?php endif ?>