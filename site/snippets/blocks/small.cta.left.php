<div class="is-row is-flex column-padding ener__product-tour__ad">
    <div class="is-col offset-1 col-2_5">
        <div class="item-80 is-shown-mobile"></div>
        <!-- TEXT AD: BUILD YOUR IDEA -->
        <div class="ener__text-ad">
                <div class="item-20"></div>
                <?php echo $data->heading()->toBlocks(); ?>
                <div class="item-32"></div>
                <?php if($int_link = page($data->pagelink()->toPage())): ?>
                <a href="<?php echo $int_link->url() ?>" class="main-cta main-cta--gray section-link" data-uri="<?php echo $int_link->uri(); ?>" data-title="<?php echo $int_link->seotitle(); ?>" data-text="<?php echo $data->ctatext() ?>"><span><?php echo $data->ctatext() ?></span></a>
                <?php endif ?>
        </div>
        <!-- TEXT AD: BUILD YOUR IDEA -->
    </div>
</div>