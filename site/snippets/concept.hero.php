<section class="cis__concept-hero cis__special-backgound" id="cis-concept-hero">
    <div class="cis__concept-hero__section__container<?php echo $site->heroPaddingTop()->isNotEmpty() && $site->heroPaddingTop()->bool() === true ? ' cis--padding-top-200':''; ?><?php echo $site->heroPaddingBottom()->isNotEmpty() && $site->heroPaddingBottom()->bool() === true ? ' cis--padding-bottom-200 ':' '; ?>grid-margin">
        <?php 
            $special_hero = $site->specialHero()->toStructure();
            $i = 1;
            foreach ($special_hero as $line) {
                ${'line_' . $i} = $line;
                $i++;
            }
         ?>
        <!-- CONCEPT NAV -->
        <div class="cis__concept-hero__special-nav" id="concept-nav">
            <nav class="is-row is-flex">
                <?php if(isset($line_1)): ?>
                <?php $page_link = $line_1->pagelink()->toPage(); ?>
                <div class="is-col col-12 js-intro-animation js-disabled-anchor">
                    <a href="<?php echo $page_link->url(); ?>" class="cis__concept-link--anchor cis__concept-link--green section-link" data-uri="<?php echo $page_link->uri(); ?>" data-title="<?php echo $page_link->title(); ?>">
                        <div class="cis__concept-link">
                            <span class="cis__concept-link__top"><?php echo $line_1->text(); ?></span>
                            <span class="cis__concept-link__bottom"><span class="cis__concept-link__container"><span class="cis__concept-link__bg"></span><?php echo $page_link->title(); ?></span></span>
                        </div>
                    </a>
                </div>
                <?php endif ?>

                <?php if(isset($line_2)): ?>
                <?php $page_link = $line_2->pagelink()->toPage(); ?>
                <div class="is-col col-12 js-intro-animation js-disabled-anchor">
                    <a href="<?php echo $page_link->url(); ?>" class="cis__concept-link--anchor cis__concept-link--anchor-2 cis__concept-link--blue section-link" data-uri="<?php echo $page_link->uri(); ?>" data-title="<?php echo $page_link->title(); ?>">
                        <div class="cis__concept-link">
                            <span class="cis__concept-link__top"><?php echo $line_2->text(); ?></span>
                            <span class="cis__concept-link__bottom"><span class="cis__concept-link__container"><span class="cis__concept-link__bg"></span><?php echo $page_link->title(); ?></span></span>
                        </div>
                    </a>
                </div>
                <?php endif ?>

                <?php if(isset($line_3)): ?>
                <?php $page_link = $line_3->pagelink()->toPage(); ?>
                <div class="is-col col-12 js-intro-animation js-disabled-anchor cis__concept-link-video-container">
                    <a href="<?php echo $page_link->url(); ?>" class="cis__concept-link--anchor cis__concept-link--anchor-3 cis__concept-link--coral section-link" data-uri="<?php echo $page_link->uri(); ?>" data-title="<?php echo $page_link->title(); ?>"> 
                        <div class="cis__concept-link">
                            <span class="cis__concept-link__top"><?php echo $line_3->text(); ?></span>
                            <span class="cis__concept-link__bottom cis__concept-link__bottom--right-aligned"><span class="cis__concept-link__container"><span class="cis__concept-link__bg"></span><?php echo $page_link->title(); ?></span></span>
                        </div>
                    </a>
                    <ul class="cis__concept-link-video">
                        <li class="js-intro-animation">
                            <?php if($video = $page->file('hero_stamp_01.mp4')): ?>
                                <?php 
                                    //echo $video->url();
                                    require_once('assets/php/getid3/getid3.php');
                                    $getID3 = new getID3;
                                    $file = $getID3->analyze($video->mediaRoot());
                                    $video_width = $file['video']['resolution_x'];
                                    $video_height = $file['video']['resolution_y'];
                                 ?>
                                 <figure class="--figure"<?php echo isset($video_width) && isset($video_height) ? ' style="padding-top:' . ($video_height / $video_width) * 100 . '%;"' : '';?>>
                                     <video class="js-lazy-image --special-transform" data-poster="" webkit-playsinline="webkit-playsinline" playsinline="playsinline" autoplay="autoplay" loop="loop" muted="muted">
                                         <source data-src="<?php echo $video->url() ?>" type="<?php echo $video->mime() ?>">
                                     </video>
                                 </figure>
                            <?php endif ?>
                        </li>
                        <li class="js-intro-animation">
                            <?php if($video = $page->file('hero_stamp_02.mp4')): ?>
                                <?php 
                                    //echo $video->url();
                                    require_once('assets/php/getid3/getid3.php');
                                    $getID3 = new getID3;
                                    $file = $getID3->analyze($video->mediaRoot());
                                    $video_width = $file['video']['resolution_x'];
                                    $video_height = $file['video']['resolution_y'];
                                 ?>
                                 <figure class="--figure"<?php echo isset($video_width) && isset($video_height) ? ' style="padding-top:' . ($video_height / $video_width) * 100 . '%;"' : '';?>>
                                     <video class="js-lazy-image --special-transform" data-poster="" webkit-playsinline="webkit-playsinline" playsinline="playsinline" autoplay="autoplay" loop="loop" muted="muted">
                                         <source data-src="<?php echo $video->url() ?>" type="<?php echo $video->mime() ?>">
                                     </video>
                                 </figure>
                            <?php endif ?>
                        </li>
                    </ul>
                </div>
                <?php endif ?>

                <?php if(isset($line_4)): ?>
                <?php $page_link = $line_4->pagelink()->toPage(); ?>
                <div class="is-col col-12 js-intro-animation js-disabled-anchor">
                    <a href="<?php echo $page_link->url(); ?>" class="cis__concept-link--anchor cis__concept-link--anchor-4 cis__concept-link--orange section-link" data-uri="<?php echo $page_link->uri(); ?>" data-title="<?php echo $page_link->title(); ?>">
                        <div class="cis__concept-link">
                            <span class="cis__concept-link__top"><?php echo $line_4->text(); ?></span>
                            <span class="cis__concept-link__bottom"><span class="cis__concept-link__container"><span class="cis__concept-link__bg"></span><?php echo $page_link->title(); ?></span></span>
                        </div>
                    </a>
                </div>
                <?php endif ?>
            </nav>
            <p class="js-intro-animation-with-delay" role="tagline"><span><?php echo $site->heroSubheading(); ?></span></p>
        </div>
        <!-- CONCEPT NAV -->
    </div>
</section>