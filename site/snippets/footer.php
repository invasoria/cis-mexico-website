<footer class="cis__footer js-nav-observe hide-and-show-nav"<?php echo true ? ' data-scroll data-scroll-speed="-4" data-scroll-offset="-500,0"' : ''; ?> id="footer">
	<div class="cis__footer__container grid-margin">
		<div class="is-row is-flex cis__footer__level-1 column-padding">
		    <div class="is-col col-6 offset-_5">
		    	<h1 class="cis__footer__logo">
		    		<a href="<?php echo page('cover')->url(); ?>" data-uri="<?php echo page('cover')->uri(); ?>" data-title="<?php echo page('cover')->title(); ?>" class="section-link">
		    			<!-- 
		    			<svg viewbox="0 0 176.01 72" xmlns="http://www.w3.org/2000/svg"> <g> <g> <rect class="cis-logo__st1" height="16" width="16" y="19"> </rect> </g> <g> <rect class="cis-logo__st1" height="16" width="40" x="136" y="38"> </rect> </g> <g> <g> <path class="cis-logo__st1" d="M329.89,183.56a5.1,5.1,0,0,1,5.48,4.72c0,.12,0,.23,0,.34l-2.2.09a3.16,3.16,0,0,0-3.17-3.14h-.13c-2.06,0-3.41,1.55-3.41,4s1.35,4,3.41,4a3.17,3.17,0,0,0,3.3-3v-.13l2.2.09a5.11,5.11,0,0,1-5.14,5.08h-.36a6,6,0,0,1,0-12Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st1" d="M345.35,195.34h-8.64V183.78h8.48v1.9H339v2.82h4.88v1.89H339v3h6.38Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st1" d="M354.56,191.48v-7.7h2.13v11.56h-1.84l-5.9-8.15v8.15h-2.13V183.78h2.27Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st1" d="M362.06,195.34v-9.66H358v-1.9H368.5v1.9h-4.15v9.66Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st1" d="M377.23,195.34a5.62,5.62,0,0,1-.48-2.39,1.73,1.73,0,0,0-1.57-1.9,1.56,1.56,0,0,0-.43,0H372v4.28h-2.26V183.78H375c3,0,4.4,1.37,4.4,3.52a2.8,2.8,0,0,1-2.55,2.93,2.41,2.41,0,0,1,2.13,2.39c.15,1.71.2,2.29.59,2.72Zm-2.53-6.08c1.67,0,2.44-.55,2.44-1.8s-.77-1.83-2.44-1.83H372v3.63Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st1" d="M386.21,195.56a6,6,0,1,1,5.86-6.15v.15a5.71,5.71,0,0,1-5.41,6Zm0-10c-2.12,0-3.48,1.56-3.48,4s1.36,4,3.48,4,3.49-1.56,3.49-4S388.35,185.57,386.21,185.57Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st1" d="M396.56,183.78h4.37c3.72,0,6,2.16,6,5.78s-2.32,5.78-6,5.78h-4.37Zm4.22,9.72c2.44,0,3.82-1.47,3.82-4s-1.38-3.93-3.82-3.93h-1.92v7.89Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st1" d="M417,195.34h-8.64V183.78h8.47v1.9h-6.21v2.82h4.88v1.89h-4.88v3H417Z" transform="translate(-323.62 -181.56)"> </path> </g> </g> <g> <g> <path class="cis-logo__st1" d="M355.51,202.77v11.56h-2.3V202.77Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st1" d="M365.14,210.48v-7.71h2.13v11.56h-1.83l-5.91-8.14v8.14H357.4V202.77h2.28Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st1" d="M372.8,214.33l-4.57-11.56h2.4l3.42,8.78h.07l3.38-8.78h2.29l-4.55,11.56Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st1" d="M389.42,214.33h-8.64V202.77h8.48v1.91H383v2.82h4.88v1.89H383v3h6.38Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st1" d="M392.19,210.13a2.69,2.69,0,0,0,2.74,2.63l.27,0c1.43,0,2.35-.57,2.35-1.54s-.7-1.22-1.91-1.45l-2.2-.4c-1.8-.33-3.05-1.32-3.05-3.23,0-2.17,1.71-3.56,4.4-3.56,3,0,4.74,1.56,4.77,4.13l-2.11.07a2.39,2.39,0,0,0-2.36-2.42l-.32,0c-1.35,0-2.12.61-2.12,1.6,0,.84.6,1.14,1.65,1.34l2.13.38c2.27.43,3.33,1.5,3.33,3.38,0,2.28-2,3.5-4.56,3.5-3,0-5.12-1.54-5.12-4.33Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st1" d="M404.15,214.33v-9.65h-4.09v-1.91h10.53v1.91h-4.15v9.65Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st1" d="M414.16,202.77v11.56h-2.29V202.77Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st1" d="M415.52,208.55a5.72,5.72,0,0,1,5.44-6h.36c3.34,0,5.27,1.84,5.5,4.76l-2.24.09a2.94,2.94,0,0,0-3-2.84h-.17c-2.11,0-3.47,1.56-3.47,4s1.4,4.06,3.66,4.06a3,3,0,0,0,3.15-2.4h-3.06V208.5h5.21v5.83h-2v-1.7a3.77,3.77,0,0,1-3.67,1.92C417.93,214.55,415.52,212.13,415.52,208.55Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st1" d="M435,202.77l4.71,11.56h-2.44l-1.16-2.86h-4.91l-1.12,2.86h-2.31l4.67-11.56Zm-3.1,6.94h3.52l-1.75-4.33h-.07Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st1" d="M445.37,202.55a5.12,5.12,0,0,1,5.48,4.73c0,.12,0,.23,0,.34l-2.2.09a3.16,3.16,0,0,0-3.17-3.14h-.13c-2.06,0-3.42,1.55-3.42,4s1.36,4,3.42,4a3.15,3.15,0,0,0,3.29-3s0-.09,0-.13l2.2.09a5.12,5.12,0,0,1-5.15,5.08l-.35,0a6,6,0,0,1,0-12Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st1" d="M454.48,202.77v11.56h-2.29V202.77Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st1" d="M461.71,214.55a6,6,0,1,1,5.85-6.14v.14a5.71,5.71,0,0,1-5.41,6Zm0-10c-2.13,0-3.49,1.56-3.49,4s1.36,4,3.49,4,3.49-1.56,3.49-4S463.84,204.57,461.71,204.57Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st1" d="M476.64,210.48v-7.71h2.13v11.56h-1.84L471,206.19v8.14h-2.13V202.77h2.28Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st1" d="M489.28,214.33h-8.64V202.77h8.47v1.91h-6.22v2.82h4.88v1.89h-4.88v3h6.39Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st1" d="M492.05,210.13a2.68,2.68,0,0,0,2.73,2.63l.27,0c1.44,0,2.35-.57,2.35-1.54s-.7-1.22-1.91-1.45l-2.2-.4c-1.8-.33-3-1.32-3-3.23,0-2.17,1.71-3.56,4.4-3.56,3,0,4.73,1.56,4.77,4.13l-2.11.07a2.39,2.39,0,0,0-2.36-2.42l-.32,0c-1.36,0-2.13.61-2.13,1.6,0,.84.61,1.14,1.65,1.34l2.13.38c2.28.43,3.34,1.5,3.34,3.38,0,2.28-2,3.5-4.57,3.5-3,0-5.12-1.54-5.12-4.33Z" transform="translate(-323.62 -181.56)"> </path> </g> </g> <g> <g> <path class="cis-logo__st1" d="M326.11,229.13a2.69,2.69,0,0,0,2.74,2.63l.27,0c1.43,0,2.35-.57,2.35-1.54s-.7-1.22-1.91-1.45l-2.2-.4c-1.8-.33-3.05-1.32-3.05-3.23,0-2.17,1.71-3.56,4.4-3.56,3,0,4.74,1.56,4.78,4.13l-2.11.07a2.41,2.41,0,0,0-2.37-2.42l-.31,0c-1.36,0-2.13.6-2.13,1.6,0,.84.6,1.14,1.65,1.34l2.13.38c2.27.43,3.34,1.5,3.34,3.38,0,2.27-2,3.5-4.57,3.5-3,0-5.12-1.54-5.12-4.33Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st1" d="M340.49,233.55a6,6,0,1,1,5.85-6.14.71.71,0,0,1,0,.14,5.72,5.72,0,0,1-5.41,6Zm0-10c-2.13,0-3.48,1.56-3.48,4s1.35,4,3.48,4,3.49-1.55,3.49-4S342.62,223.57,340.49,223.57Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st1" d="M353,221.55a5.11,5.11,0,0,1,5.48,4.73c0,.11,0,.23,0,.34l-2.2.09a3.16,3.16,0,0,0-3.17-3.14H353c-2.06,0-3.42,1.54-3.42,4s1.36,4,3.42,4a3.14,3.14,0,0,0,3.29-3,.57.57,0,0,0,0-.13l2.2.09a5.11,5.11,0,0,1-5.14,5.08l-.36,0a6,6,0,0,1,0-12Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st1" d="M362.12,221.77v11.56h-2.3V221.77Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st1" d="M369.35,233.55a6,6,0,1,1,5.85-6.14v.14a5.71,5.71,0,0,1-5.41,6Zm0-10c-2.13,0-3.49,1.56-3.49,4s1.36,4,3.49,4,3.48-1.55,3.48-4S371.47,223.57,369.35,223.57Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st1" d="M378.83,221.77v9.65h6.11v1.91h-8.4V221.77Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st1" d="M397.65,227.55a5.72,5.72,0,0,1,5.44-6h.36c3.33,0,5.26,1.84,5.5,4.76l-2.24.09a3,3,0,0,0-3-2.84h-.17c-2.11,0-3.47,1.56-3.47,4s1.4,4.06,3.65,4.06a3,3,0,0,0,3.15-2.4h-3.06V227.5H409v5.84h-2v-1.71a3.78,3.78,0,0,1-3.67,1.93C400.05,233.55,397.65,231.13,397.65,227.55Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st1" d="M413.05,221.77v11.56h-2.29V221.77Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st1" d="M420.21,221.55a5.12,5.12,0,0,1,5.48,4.74c0,.11,0,.22,0,.33l-2.2.09a3.16,3.16,0,0,0-3.17-3.14h-.13c-2.06,0-3.42,1.54-3.42,4s1.36,4,3.42,4a3.14,3.14,0,0,0,3.29-3,.57.57,0,0,0,0-.13l2.2.09a5.12,5.12,0,0,1-5.15,5.08l-.35,0a6,6,0,0,1,0-12Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st1" d="M432.88,221.77l4.72,11.56h-2.44L434,230.47h-4.92L428,233.33h-2.31l4.68-11.56Zm-3.1,6.94h3.53l-1.75-4.33h-.07Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st1" d="M439.85,229.13a2.69,2.69,0,0,0,2.74,2.63l.27,0c1.43,0,2.35-.57,2.35-1.54s-.7-1.22-1.91-1.45l-2.2-.4c-1.8-.33-3.05-1.32-3.05-3.23,0-2.17,1.71-3.56,4.4-3.56,3,0,4.74,1.56,4.77,4.13l-2.11.07a2.39,2.39,0,0,0-2.36-2.42l-.32,0c-1.35,0-2.12.6-2.12,1.6,0,.84.6,1.14,1.65,1.34l2.13.38c2.27.43,3.33,1.5,3.33,3.38,0,2.27-2,3.5-4.56,3.5-3,0-5.12-1.54-5.12-4.33Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st1" d="M390.93,233.56a6,6,0,1,1,5.85-6.15v.15a5.69,5.69,0,0,1-5.4,6Zm0-10c-2.12,0-3.48,1.56-3.48,4s1.36,4,3.48,4,3.49-1.56,3.49-4S393.06,223.58,390.93,223.58Zm-.8-2.57,1-2.45h2.36L391.71,221Z" transform="translate(-323.62 -181.56)"> </path> </g> </g> <g> <rect class="cis-logo__st1" height="16" width="72" y="56"> </rect> </g> <g> <rect class="cis-logo__st1" height="16" width="72" x="104"> </rect> </g> <g> <g> <path class="cis-logo__st1" d="M411.43,239.77h4.37c3.72,0,6,2.17,6,5.78s-2.31,5.78-6,5.78h-4.37Zm4.22,9.73c2.44,0,3.82-1.47,3.82-4s-1.38-3.93-3.82-3.93h-1.93v7.89Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st1" d="M431.83,251.33h-8.64V239.77h8.47v1.91h-6.22v2.82h4.88v1.89h-4.88v3h6.39Z" transform="translate(-323.62 -181.56)"> </path> </g> <g> <path class="cis-logo__st2" d="M441.48,251.33l-2.83-9.1h-.07v9.1h-2.13V239.77h3.61l2.53,8.4h.06l2.4-8.4h3.54v11.56h-2.31v-9.1h-.07l-2.77,9.1Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st2" d="M466.71,245.26l4.46,6.07h-2.7l-3.13-4.27-3.12,4.27h-2.55l4.45-5.92L460,239.77h2.7l2.77,3.86,2.76-3.86h2.55Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st2" d="M474.45,239.77v11.56h-2.29V239.77Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st2" d="M481.61,239.55a5.13,5.13,0,0,1,5.49,4.73c0,.12,0,.23,0,.34l-2.2.09a3.16,3.16,0,0,0-3.17-3.14h-.13c-2.06,0-3.41,1.55-3.41,4s1.35,4,3.41,4a3.17,3.17,0,0,0,3.3-3v-.13l2.2.09a5.11,5.11,0,0,1-5.14,5.08l-.36,0a6,6,0,0,1,0-12Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st2" d="M493.77,251.55a6,6,0,1,1,5.85-6.14v.14a5.71,5.71,0,0,1-5.41,6Zm0-10c-2.13,0-3.49,1.56-3.49,4s1.36,4,3.49,4,3.49-1.56,3.49-4S495.9,241.57,493.77,241.57Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st2" d="M459.08,251.34h-8.64V239.77h8.48v1.91H452.7v2.81h4.88v1.89H452.7v3.05h6.38ZM454,239l1-2.46h2.37L455.6,239Z" transform="translate(-323.62 -181.56)"> </path> </g> </g> </g> </svg>
		    			 -->
		    			 <svg xmlns="http://www.w3.org/2000/svg" width="146.45" height="68.065" viewBox="0 0 146.45 68.065">
		    			   <g id="Group_237" data-name="Group 237" transform="translate(0 -1.985)">
		    			     <g id="Group_227" data-name="Group 227" transform="translate(-0.25)">
		    			       <g id="Group_226" data-name="Group 226">
		    			         <path class="cis-logo__st1" id="Path_3970" data-name="Path 3970" d="M329.89,183.56a5.1,5.1,0,0,1,5.48,4.72v.34l-2.2.09a3.16,3.16,0,0,0-3.17-3.14h-.13c-2.06,0-3.41,1.55-3.41,4s1.35,4,3.41,4a3.17,3.17,0,0,0,3.3-3v-.13l2.2.09a5.11,5.11,0,0,1-5.14,5.08h-.36a6,6,0,0,1,0-12Z" transform="translate(-323.62 -181.56)"/>
		    			         <path class="cis-logo__st1" id="Path_3971" data-name="Path 3971" d="M345.35,195.34h-8.64V183.78h8.48v1.9H339v2.82h4.88v1.89H339v3h6.38Z" transform="translate(-323.62 -181.56)"/>
		    			         <path class="cis-logo__st1" id="Path_3972" data-name="Path 3972" d="M354.56,191.48v-7.7h2.13v11.56h-1.84l-5.9-8.15v8.15h-2.13V183.78h2.27Z" transform="translate(-323.62 -181.56)"/>
		    			         <path class="cis-logo__st1" id="Path_3973" data-name="Path 3973" d="M362.06,195.34v-9.66H358v-1.9h10.5v1.9h-4.15v9.66Z" transform="translate(-323.62 -181.56)"/>
		    			         <path class="cis-logo__st1" id="Path_3974" data-name="Path 3974" d="M377.23,195.34a5.62,5.62,0,0,1-.48-2.39,1.73,1.73,0,0,0-1.57-1.9,1.56,1.56,0,0,0-.43,0H372v4.28h-2.26V183.78H375c3,0,4.4,1.37,4.4,3.52a2.8,2.8,0,0,1-2.55,2.93,2.41,2.41,0,0,1,2.13,2.39c.15,1.71.2,2.29.59,2.72Zm-2.53-6.08c1.67,0,2.44-.55,2.44-1.8s-.77-1.83-2.44-1.83H372v3.63Z" transform="translate(-323.62 -181.56)"/>
		    			         <path class="cis-logo__st1" id="Path_3975" data-name="Path 3975" d="M386.21,195.56a6,6,0,1,1,5.86-6.15v.15a5.71,5.71,0,0,1-5.41,6Zm0-10c-2.12,0-3.48,1.56-3.48,4s1.36,4,3.48,4,3.49-1.56,3.49-4-1.35-3.99-3.49-3.99Z" transform="translate(-323.62 -181.56)"/>
		    			         <path class="cis-logo__st1" id="Path_3976" data-name="Path 3976" d="M396.56,183.78h4.37c3.72,0,6,2.16,6,5.78s-2.32,5.78-6,5.78h-4.37Zm4.22,9.72c2.44,0,3.82-1.47,3.82-4s-1.38-3.93-3.82-3.93h-1.92v7.89Z" transform="translate(-323.62 -181.56)"/>
		    			         <path class="cis-logo__st1" id="Path_3977" data-name="Path 3977" d="M417,195.34h-8.64V183.78h8.47v1.9h-6.21v2.82h4.88v1.89h-4.88v3H417Z" transform="translate(-323.62 -181.56)"/>
		    			       </g>
		    			     </g>
		    			     <g id="Group_229" data-name="Group 229" transform="translate(-29.59)">
		    			       <g id="Group_228" data-name="Group 228">
		    			         <path class="cis-logo__st1" id="Path_3978" data-name="Path 3978" d="M355.51,202.77v11.56h-2.3V202.77Z" transform="translate(-323.62 -181.56)"/>
		    			         <path class="cis-logo__st1" id="Path_3979" data-name="Path 3979" d="M365.14,210.48v-7.71h2.13v11.56h-1.83l-5.91-8.14v8.14H357.4V202.77h2.28Z" transform="translate(-323.62 -181.56)"/>
		    			         <path class="cis-logo__st1" id="Path_3980" data-name="Path 3980" d="M372.8,214.33l-4.57-11.56h2.4l3.42,8.78h.07l3.38-8.78h2.29l-4.55,11.56Z" transform="translate(-323.62 -181.56)"/>
		    			         <path class="cis-logo__st1" id="Path_3981" data-name="Path 3981" d="M389.42,214.33h-8.64V202.77h8.48v1.91H383v2.82h4.88v1.89H383v3h6.38Z" transform="translate(-323.62 -181.56)"/>
		    			         <path class="cis-logo__st1" id="Path_3982" data-name="Path 3982" d="M392.19,210.13a2.69,2.69,0,0,0,2.74,2.63h.27c1.43,0,2.35-.57,2.35-1.54s-.7-1.22-1.91-1.45l-2.2-.4c-1.8-.33-3.05-1.32-3.05-3.23,0-2.17,1.71-3.56,4.4-3.56,3,0,4.74,1.56,4.77,4.13l-2.11.07a2.39,2.39,0,0,0-2.36-2.42h-.32c-1.35,0-2.12.61-2.12,1.6,0,.84.6,1.14,1.65,1.34l2.13.38c2.27.43,3.33,1.5,3.33,3.38,0,2.28-2,3.5-4.56,3.5-3,0-5.12-1.54-5.12-4.33Z" transform="translate(-323.62 -181.56)"/>
		    			         <path class="cis-logo__st1" id="Path_3983" data-name="Path 3983" d="M404.15,214.33v-9.65h-4.09v-1.91h10.53v1.91h-4.15v9.65Z" transform="translate(-323.62 -181.56)"/>
		    			         <path class="cis-logo__st1" id="Path_3984" data-name="Path 3984" d="M414.16,202.77v11.56h-2.29V202.77Z" transform="translate(-323.62 -181.56)"/>
		    			         <path class="cis-logo__st1" id="Path_3985" data-name="Path 3985" d="M415.52,208.55a5.72,5.72,0,0,1,5.44-6h.36c3.34,0,5.27,1.84,5.5,4.76l-2.24.09a2.94,2.94,0,0,0-3-2.84h-.17c-2.11,0-3.47,1.56-3.47,4s1.4,4.06,3.66,4.06a3,3,0,0,0,3.15-2.4h-3.06V208.5h5.21v5.83h-2v-1.7a3.77,3.77,0,0,1-3.67,1.92C417.93,214.55,415.52,212.13,415.52,208.55Z" transform="translate(-323.62 -181.56)"/>
		    			         <path class="cis-logo__st1" id="Path_3986" data-name="Path 3986" d="M435,202.77l4.71,11.56h-2.44l-1.16-2.86H431.2l-1.12,2.86h-2.31l4.67-11.56Zm-3.1,6.94h3.52l-1.75-4.33h-.07Z" transform="translate(-323.62 -181.56)"/>
		    			         <path class="cis-logo__st1" id="Path_3987" data-name="Path 3987" d="M445.37,202.55a5.12,5.12,0,0,1,5.48,4.73v.34l-2.2.09a3.16,3.16,0,0,0-3.17-3.14h-.13c-2.06,0-3.42,1.55-3.42,4s1.36,4,3.42,4a3.15,3.15,0,0,0,3.29-3s0-.09,0-.13l2.2.09a5.12,5.12,0,0,1-5.15,5.08h-.35a6,6,0,0,1,0-12Z" transform="translate(-323.62 -181.56)"/>
		    			         <path class="cis-logo__st1" id="Path_3988" data-name="Path 3988" d="M454.48,202.77v11.56h-2.29V202.77Z" transform="translate(-323.62 -181.56)"/>
		    			         <path class="cis-logo__st1" id="Path_3989" data-name="Path 3989" d="M461.71,214.55a6,6,0,1,1,5.85-6.14v.14a5.71,5.71,0,0,1-5.41,6Zm0-10c-2.13,0-3.49,1.56-3.49,4s1.36,4,3.49,4,3.49-1.56,3.49-4-1.36-3.98-3.49-3.98Z" transform="translate(-323.62 -181.56)"/>
		    			         <path class="cis-logo__st1" id="Path_3990" data-name="Path 3990" d="M476.64,210.48v-7.71h2.13v11.56h-1.84L471,206.19v8.14h-2.13V202.77h2.28Z" transform="translate(-323.62 -181.56)"/>
		    			         <path class="cis-logo__st1" id="Path_3991" data-name="Path 3991" d="M489.28,214.33h-8.64V202.77h8.47v1.91h-6.22v2.82h4.88v1.89h-4.88v3h6.39Z" transform="translate(-323.62 -181.56)"/>
		    			         <path class="cis-logo__st1" id="Path_3992" data-name="Path 3992" d="M492.05,210.13a2.68,2.68,0,0,0,2.73,2.63h.27c1.44,0,2.35-.57,2.35-1.54s-.7-1.22-1.91-1.45l-2.2-.4c-1.8-.33-3-1.32-3-3.23,0-2.17,1.71-3.56,4.4-3.56,3,0,4.73,1.56,4.77,4.13l-2.11.07a2.39,2.39,0,0,0-2.36-2.42h-.32c-1.36,0-2.13.61-2.13,1.6,0,.84.61,1.14,1.65,1.34l2.13.38c2.28.43,3.34,1.5,3.34,3.38,0,2.28-2,3.5-4.57,3.5-3,0-5.12-1.54-5.12-4.33Z" transform="translate(-323.62 -181.56)"/>
		    			       </g>
		    			     </g>
		    			     <g id="Group_231" data-name="Group 231" transform="translate(-0.38)">
		    			       <g id="Group_230" data-name="Group 230">
		    			         <path class="cis-logo__st1" id="Path_3993" data-name="Path 3993" d="M326.11,229.13a2.69,2.69,0,0,0,2.74,2.63h.27c1.43,0,2.35-.57,2.35-1.54s-.7-1.22-1.91-1.45l-2.2-.4c-1.8-.33-3.05-1.32-3.05-3.23,0-2.17,1.71-3.56,4.4-3.56,3,0,4.74,1.56,4.78,4.13l-2.11.07a2.41,2.41,0,0,0-2.37-2.42h-.31c-1.36,0-2.13.6-2.13,1.6,0,.84.6,1.14,1.65,1.34l2.13.38c2.27.43,3.34,1.5,3.34,3.38,0,2.27-2,3.5-4.57,3.5-3,0-5.12-1.54-5.12-4.33Z" transform="translate(-323.62 -181.56)"/>
		    			         <path class="cis-logo__st1" id="Path_3994" data-name="Path 3994" d="M340.49,233.55a6,6,0,1,1,5.85-6.14.711.711,0,0,1,0,.14,5.72,5.72,0,0,1-5.41,6Zm0-10c-2.13,0-3.48,1.56-3.48,4s1.35,4,3.48,4,3.49-1.55,3.49-4-1.36-3.98-3.49-3.98Z" transform="translate(-323.62 -181.56)"/>
		    			         <path class="cis-logo__st1" id="Path_3995" data-name="Path 3995" d="M353,221.55a5.11,5.11,0,0,1,5.48,4.73v.34l-2.2.09a3.16,3.16,0,0,0-3.17-3.14H353c-2.06,0-3.42,1.54-3.42,4s1.36,4,3.42,4a3.14,3.14,0,0,0,3.29-3,.571.571,0,0,0,0-.13l2.2.09a5.11,5.11,0,0,1-5.14,5.08h-.36a6,6,0,0,1,0-12Z" transform="translate(-323.62 -181.56)"/>
		    			         <path class="cis-logo__st1" id="Path_3996" data-name="Path 3996" d="M362.12,221.77v11.56h-2.3V221.77Z" transform="translate(-323.62 -181.56)"/>
		    			         <path class="cis-logo__st1" id="Path_3997" data-name="Path 3997" d="M369.35,233.55a6,6,0,1,1,5.85-6.14v.14a5.71,5.71,0,0,1-5.41,6Zm0-10c-2.13,0-3.49,1.56-3.49,4s1.36,4,3.49,4,3.48-1.55,3.48-4-1.36-3.98-3.48-3.98Z" transform="translate(-323.62 -181.56)"/>
		    			         <path class="cis-logo__st1" id="Path_3998" data-name="Path 3998" d="M378.83,221.77v9.65h6.11v1.91h-8.4V221.77Z" transform="translate(-323.62 -181.56)"/>
		    			         <path class="cis-logo__st1" id="Path_3999" data-name="Path 3999" d="M397.65,227.55a5.72,5.72,0,0,1,5.44-6h.36c3.33,0,5.26,1.84,5.5,4.76l-2.24.09a3,3,0,0,0-3-2.84h-.17c-2.11,0-3.47,1.56-3.47,4s1.4,4.06,3.65,4.06a3,3,0,0,0,3.15-2.4h-3.06V227.5H409v5.84h-2v-1.71a3.78,3.78,0,0,1-3.67,1.93C400.05,233.55,397.65,231.13,397.65,227.55Z" transform="translate(-323.62 -181.56)"/>
		    			         <path class="cis-logo__st1" id="Path_4000" data-name="Path 4000" d="M413.05,221.77v11.56h-2.29V221.77Z" transform="translate(-323.62 -181.56)"/>
		    			         <path class="cis-logo__st1" id="Path_4001" data-name="Path 4001" d="M420.21,221.55a5.12,5.12,0,0,1,5.48,4.74v.33l-2.2.09a3.16,3.16,0,0,0-3.17-3.14h-.13c-2.06,0-3.42,1.54-3.42,4s1.36,4,3.42,4a3.14,3.14,0,0,0,3.29-3,.571.571,0,0,0,0-.13l2.2.09a5.12,5.12,0,0,1-5.15,5.08h-.35a6,6,0,1,1,0-12Z" transform="translate(-323.62 -181.56)"/>
		    			         <path class="cis-logo__st1" id="Path_4002" data-name="Path 4002" d="M432.88,221.77l4.72,11.56h-2.44L434,230.47h-4.92L428,233.33h-2.31l4.68-11.56Zm-3.1,6.94h3.53l-1.75-4.33h-.07Z" transform="translate(-323.62 -181.56)"/>
		    			         <path class="cis-logo__st1" id="Path_4003" data-name="Path 4003" d="M439.85,229.13a2.69,2.69,0,0,0,2.74,2.63h.27c1.43,0,2.35-.57,2.35-1.54s-.7-1.22-1.91-1.45l-2.2-.4c-1.8-.33-3.05-1.32-3.05-3.23,0-2.17,1.71-3.56,4.4-3.56,3,0,4.74,1.56,4.77,4.13l-2.11.07a2.39,2.39,0,0,0-2.36-2.42h-.32c-1.35,0-2.12.6-2.12,1.6,0,.84.6,1.14,1.65,1.34l2.13.38c2.27.43,3.33,1.5,3.33,3.38,0,2.27-2,3.5-4.56,3.5-3,0-5.12-1.54-5.12-4.33Z" transform="translate(-323.62 -181.56)"/>
		    			         <path class="cis-logo__st1" id="Path_4004" data-name="Path 4004" d="M390.93,233.56a6,6,0,1,1,5.85-6.15v.15a5.69,5.69,0,0,1-5.4,6Zm0-10c-2.12,0-3.48,1.56-3.48,4s1.36,4,3.48,4,3.49-1.56,3.49-4-1.36-3.98-3.49-3.98Zm-.8-2.57,1-2.45h2.36L391.71,221Z" transform="translate(-323.62 -181.56)"/>
		    			       </g>
		    			     </g>
		    			     <g id="Group_236" data-name="Group 236" transform="translate(-87.81)">
		    			       <g id="Group_234" data-name="Group 234">
		    			         <path class="cis-logo__st1" id="Path_4005" data-name="Path 4005" d="M411.43,239.77h4.37c3.72,0,6,2.17,6,5.78s-2.31,5.78-6,5.78h-4.37Zm4.22,9.73c2.44,0,3.82-1.47,3.82-4s-1.38-3.93-3.82-3.93h-1.93v7.89Z" transform="translate(-323.62 -181.56)"/>
		    			         <path class="cis-logo__st1" id="Path_4006" data-name="Path 4006" d="M431.83,251.33h-8.64V239.77h8.47v1.91h-6.22v2.82h4.88v1.89h-4.88v3h6.39Z" transform="translate(-323.62 -181.56)"/>
		    			       </g>
		    			       <g id="Group_235" data-name="Group 235">
		    			         <path class="cis-logo__st2" id="Path_4007" data-name="Path 4007" d="M441.48,251.33l-2.83-9.1h-.07v9.1h-2.13V239.77h3.61l2.53,8.4h.06l2.4-8.4h3.54v11.56h-2.31v-9.1h-.07l-2.77,9.1Z" transform="translate(-323.62 -181.56)"/>
		    			         <path class="cis-logo__st2" id="Path_4008" data-name="Path 4008" d="M466.71,245.26l4.46,6.07h-2.7l-3.13-4.27-3.12,4.27h-2.55l4.45-5.92L460,239.77h2.7l2.77,3.86,2.76-3.86h2.55Z" transform="translate(-323.62 -181.56)"/>
		    			         <path class="cis-logo__st2" id="Path_4009" data-name="Path 4009" d="M474.45,239.77v11.56h-2.29V239.77Z" transform="translate(-323.62 -181.56)"/>
		    			         <path class="cis-logo__st2" id="Path_4010" data-name="Path 4010" d="M481.61,239.55a5.13,5.13,0,0,1,5.49,4.73c0,.12,0,.23,0,.34l-2.2.09a3.16,3.16,0,0,0-3.17-3.14h-.13c-2.06,0-3.41,1.55-3.41,4s1.35,4,3.41,4a3.17,3.17,0,0,0,3.3-3v-.13l2.2.09a5.11,5.11,0,0,1-5.14,5.08h-.36a6,6,0,0,1,0-12Z" transform="translate(-323.62 -181.56)"/>
		    			         <path class="cis-logo__st2" id="Path_4011" data-name="Path 4011" d="M493.77,251.55a6,6,0,1,1,5.85-6.14v.14a5.71,5.71,0,0,1-5.41,6Zm0-10c-2.13,0-3.49,1.56-3.49,4s1.36,4,3.49,4,3.49-1.56,3.49-4-1.36-3.98-3.49-3.98Z" transform="translate(-323.62 -181.56)"/>
		    			         <path class="cis-logo__st2" id="Path_4012" data-name="Path 4012" d="M459.08,251.34h-8.64V239.77h8.48v1.91H452.7v2.81h4.88v1.89H452.7v3.05h6.38ZM454,239l1-2.46h2.37L455.6,239Z" transform="translate(-323.62 -181.56)"/>
		    			       </g>
		    			     </g>
		    			   </g>
		    			 </svg>
		    			<!-- CENTRO DE<br>INVESTIGACIONES<br>SOCIOLÓGICAS<br>DE <span>MÉXICO</span> --></a></h1>
		    </div>
		</div>
		<div class="is-row is-flex cis__footer__level-2 column-padding">
		    <div class="is-col col-5 offset-_5">
		    	<div class="is-row is-flex is-row--responsive">
		    	    <div class="is-col col-5 xs-col-6">
		    	    	<h4><?php echo t('services'); ?></h4>
				    	<nav class="cis__footer__nav">
				    		<?php
				    		    $items = $site->footermenus();
				    		?>
				    		<?php if($items->isNotEmpty()): ?>
				    		<ul class="cis__footer__list" id="footer-nav-list">
				    		    <?php foreach ($items->toStructure() as $item): ?>
				    		        <?php if($item->link() == 'page'): ?>
				    		            <?php foreach ($item->pagelink()->yaml() as $pagelink): ?>
				    		                <?php if(page($pagelink) && page($pagelink)->isListed()): ?>
				    		                <?php $pageitem = page($pagelink); ?>
				    		                <li class="cis__footer__list__item">
				    		                	<a href="<?php echo $pageitem->url(); ?>" data-uri="<?php echo $pageitem->uri(); ?>" data-title="<?php echo $pageitem->title(); ?>" class="main-nav-item section-link main-nav-<?php echo $pageitem->autoid(); ?>"><?php echo $item->pagetitle() != '' ? $item->pagetitle() : $pageitem->title() ; ?></a>
				    		                </li>
				    		                <?php endif ?>
				    		            <?php endforeach ?>
				    		        <?php elseif(($item->link() == 'url')): ?>
				    		        	<li class="cis__footer__list__item">
				    		        		<a href="<?php echo $item->pageurl(); ?>" data-title="<?php echo $item->label(); ?>" class="main-nav-item external-link" target="_blank"><?php echo $item->label(); ?></a>
				    		        	</li>
				    		        <?php endif ?>
				    		    <?php endforeach ?>
					    	</ul>
					    	<?php endif ?>
				    	</nav>
		    	    </div>
		    	    <div class="is-col col-5 xs-col-6">
		    	    	<h4><?php echo t('social'); ?></h4>
		    	    	<?php 
		    	    		$platforms = $site->social()->toStructure();
		    	    		$limit_count = $platforms->count() / 2;
		    	    	 ?>
				    	<ul class="cis__footer__list">
	        	    	<?php foreach ($platforms as $platform): ?>
	        	    		<?php 
	        	    			switch ($platform->account()) {
	        	    				case 'instagram':
	        	    					$social_network = 'Instagram';
	        	    					$social_url = 'https://www.instagram.com/'.$platform->username();
        	    	        			$social_icon = '<g class="ener__button__svg__icon" transform="translate(-12 2)"> <path d="M43.95,18.7c-0.01-0.66-0.14-1.32-0.37-1.94c-0.42-1.08-1.27-1.92-2.34-2.34 c-0.62-0.23-1.28-0.36-1.94-0.37C38.44,14.01,38.17,14,36,14s-2.44,0.01-3.3,0.05c-0.66,0.01-1.32,0.14-1.94,0.37 c-1.07,0.42-1.92,1.27-2.34,2.34c-0.23,0.62-0.36,1.28-0.37,1.94C28.01,19.55,28,19.83,28,22s0.01,2.45,0.05,3.3 c0.01,0.66,0.14,1.32,0.37,1.94c0.42,1.08,1.27,1.93,2.34,2.34c0.62,0.23,1.28,0.36,1.94,0.37C33.55,29.99,33.83,30,36,30 s2.44-0.01,3.3-0.05c0.66-0.01,1.32-0.14,1.94-0.37c1.08-0.42,1.92-1.27,2.34-2.34c0.23-0.62,0.36-1.28,0.37-1.94 C43.99,24.45,44,24.17,44,22S43.99,19.56,43.95,18.7z M42.51,25.24c0,0.51-0.1,1.01-0.27,1.48c-0.27,0.7-0.82,1.25-1.52,1.52 c-0.48,0.18-0.98,0.27-1.48,0.27c-0.84,0.04-1.1,0.05-3.23,0.05s-2.39-0.01-3.23-0.05c-0.51,0-1.01-0.1-1.48-0.27 c-0.7-0.27-1.25-0.82-1.52-1.52c-0.18-0.48-0.27-0.98-0.27-1.48c-0.04-0.84-0.05-1.1-0.05-3.23s0.01-2.39,0.05-3.23 c0-0.51,0.1-1.01,0.27-1.48c0.13-0.35,0.33-0.66,0.6-0.92c0.26-0.27,0.57-0.47,0.92-0.6c0.48-0.18,0.98-0.27,1.48-0.27 c0.84-0.04,1.1-0.05,3.23-0.05s2.39,0.01,3.23,0.05c0.51,0.01,1.01,0.1,1.48,0.27c0.35,0.13,0.66,0.33,0.92,0.6 c0.27,0.26,0.47,0.57,0.6,0.92c0.18,0.48,0.27,0.98,0.27,1.48c0.04,0.84,0.05,1.1,0.05,3.23S42.55,24.39,42.51,25.24L42.51,25.24z "/> <path d="M36,17.89c-2.27,0-4.11,1.84-4.11,4.11c0,2.27,1.84,4.11,4.11,4.11c2.27,0,4.11-1.84,4.11-4.11l0,0 C40.11,19.73,38.27,17.89,36,17.89z M36,24.67c-1.47,0-2.67-1.19-2.67-2.67s1.19-2.67,2.67-2.67s2.67,1.19,2.67,2.67l0,0 C38.66,23.47,37.47,24.66,36,24.67L36,24.67z"/> <path d="M41.23,17.73c0,0.53-0.43,0.96-0.96,0.96s-0.96-0.43-0.96-0.96s0.43-0.96,0.96-0.96l0,0l0,0 C40.8,16.77,41.23,17.2,41.23,17.73z"/> </g>';
        	    	        		break;
        	    	        		case 'twitter':
        	    	        		$social_network = 'Twitter';
        	    	        			$social_url = 'https://www.twitter.com/'.$platform->username();
        	    	        			$social_icon = '<path class="ener__button__svg__icon" d="M33.85,17.89c-0.74,0.32-1.52,0.54-2.33,0.64c0.85-0.5,1.48-1.3,1.78-2.23c-0.79,0.47-1.66,0.8-2.56,0.98 c-1.52-1.63-4.08-1.71-5.71-0.18c-0.81,0.76-1.28,1.83-1.28,2.94c0,0.31,0.03,0.62,0.09,0.92c-3.25-0.16-6.28-1.69-8.32-4.22 c-1.08,1.84-0.54,4.21,1.24,5.4c-0.64-0.02-1.26-0.19-1.82-0.5v0.04c0,1.92,1.35,3.58,3.23,3.97c-0.35,0.09-0.7,0.14-1.06,0.13 c-0.26,0-0.51-0.02-0.77-0.07c0.53,1.64,2.05,2.77,3.77,2.81c-1.43,1.12-3.19,1.72-5.01,1.72c-0.32,0-0.65-0.02-0.97-0.06 c5.35,3.42,12.45,1.86,15.87-3.49c1.18-1.85,1.81-4,1.81-6.19c0-0.18,0-0.35-0.02-0.52C32.62,19.42,33.31,18.71,33.85,17.89z"/>';
        	    	        		break;
        	    	        		case 'facebook':
        	    	        			$social_network = 'Facebook';
        	    	        			if ($platform->fburl()->isNotEmpty()) {
        	    	        				$social_url = $platform->fburl();
        	    	        			} else {
        	    	        				$social_url = 'https://www.facebook.com/'.$platform->username();
        	    	        			}
        	    	        			$social_icon = '<path class="ener__button__svg__icon" d="M36.07,24.07C36.07,17.41,30.67,12,24,12s-12.07,5.41-12.07,12.07c0,6.03,4.42,11.02,10.19,11.93v-8.44h-3.07v-3.49h3.07 v-2.66c0-3.03,1.8-4.7,4.56-4.7c1.32,0,2.7,0.24,2.7,0.24v2.97h-1.52c-1.5,0-1.97,0.93-1.97,1.89v2.26h3.35l-0.54,3.49h-2.81V36 C31.66,35.09,36.07,30.1,36.07,24.07L36.07,24.07z"/>';
        	    	        		break;
        	    	        		case 'youtube':
    	    	        				$social_network = 'YouTube';
        	    	        			if ($platform->yturl()->isNotEmpty()) {
        	    	        				$social_url = $platform->yturl();
        	    	        			} else {
        	    	        				$social_url = 'https://youtube.com/'.$platform->username();
        	    	        			}
        	    	        			$social_icon = '<g class="ener__button__svg__icon" transform="matrix(7.8701756,0,0,7.8701756,695.19553,-948.4235)"> <path d="M-85.28,122.49c0,0-0.95,0-1.19,0.06c-0.13,0.04-0.23,0.14-0.27,0.27c-0.06,0.24-0.06,0.73-0.06,0.73 s0,0.5,0.06,0.73c0.04,0.13,0.14,0.23,0.27,0.27c0.24,0.06,1.19,0.06,1.19,0.06s0.95,0,1.19-0.06c0.13-0.04,0.23-0.14,0.27-0.27 c0.06-0.24,0.06-0.73,0.06-0.73s0-0.5-0.06-0.74c-0.03-0.13-0.14-0.23-0.27-0.27C-84.33,122.49-85.28,122.49-85.28,122.49z M-85.59,123.1l0.79,0.46l-0.79,0.46V123.1z"/> </g>';
        	    	        		break;
        	    	        		case 'linkedin':
    	    	        				$social_network = 'LinkedIn';
        	    	        			if ($platform->yturl()->isNotEmpty()) {
        	    	        				$social_url = $platform->yturl();
        	    	        			} else {
        	    	        				$social_url = 'https://www.linkedin.com/company/'.$platform->username();
        	    	        			}
        	    	        			$social_icon = '<g class="ener__button__svg__icon" transform="matrix(7.8701756,0,0,7.8701756,695.19553,-948.4235)"> <path d="M-85.28,122.49c0,0-0.95,0-1.19,0.06c-0.13,0.04-0.23,0.14-0.27,0.27c-0.06,0.24-0.06,0.73-0.06,0.73 s0,0.5,0.06,0.73c0.04,0.13,0.14,0.23,0.27,0.27c0.24,0.06,1.19,0.06,1.19,0.06s0.95,0,1.19-0.06c0.13-0.04,0.23-0.14,0.27-0.27 c0.06-0.24,0.06-0.73,0.06-0.73s0-0.5-0.06-0.74c-0.03-0.13-0.14-0.23-0.27-0.27C-84.33,122.49-85.28,122.49-85.28,122.49z M-85.59,123.1l0.79,0.46l-0.79,0.46V123.1z"/> </g>';
        	    	        		break;
	        	    				default:
	        	    					# code...
	        	    					break;
	        	    			}
	        	    		 ?>
	        	    		<?php if($platform->footer()->bool()): ?>
	        	    		<li class="cis__footer__list__item">
	        	    			<a href="<?php echo $social_url; ?>" target="_blank"><span><?php echo $social_network; ?></span></a>
	        	    		</li>
	        	    		<?php //echo $social_icon; ?>
		        	    	<?php endif ?>
	        	    	<?php endforeach ?>
    	    			</ul>
		    	    </div>
		    	</div>
		    </div>
		    <div class="is-col col-5 offset-1">
		    	<div class="item-64 is-shown-mobile"></div>
		    	<?php 
		    		$email_addresses = $site->emailaddress()->toStructure();
		    	 ?>
		    	<?php if($email_addresses->count()): ?>
		    	<ul class="cis__nav-header__list--column">
			    	<?php foreach ($email_addresses as $email_address): ?>
			    		<?php foreach ($email_address->emaillink()->yaml() as $email): ?>
			    		    <li class="cis__nav-header__list__item--column">
			    		    	<?php 
			    		    		$newoutput = preg_replace('/(@)/i', '<span class="blue-1">$1</span>', $email); 
			    		    	 ?>
			    		    	<a href="mailto:<?php echo $email; ?>" class="cis__nav-header__nav__big-anchor external-link" target="_blank"><span class="underline-anchor"><?php echo $newoutput; ?></span></a>
			    		    </li>
			    		<?php endforeach ?>
			    	<?php endforeach ?>
		    	</ul>
		    	<?php endif ?>

		    	<?php if($site->address()->isNotEmpty()): ?>
		    	<h4><?php echo t('info'); ?></h4>
		    	<p class="cis__footer__white-text"><?php echo $site->address(); ?></p>
		    	<?php endif ?>

		    	<?php if($site->phone()->isNotEmpty()): ?>
		    		<?php 
		    			$newoutput = preg_replace('/(\+)/i', '<span class="blue-1">$1</span>', $site->phone()); 
		    		 ?>
		    		<p class="cis__footer__text"><?php echo $newoutput; ?></p>
		    	<?php endif ?>
		    </div>
		</div>
		<div class="is-row is-flex cis__footer__level-3 column-padding">
		    <div class="is-col col-4 offset-_5">
		    	<p class="cis__footer__legal-credits">Somos una consultoría estratégica que genera valor a partir de un enfoque inteligente en investigación de mercados y opinión pública. Nuestro secreto es entender a las personas y obtener Estadísticas más Humanas™.</p>
		    </div>
		</div>
		<div class="is-row is-flex cis__footer__level-4 column-padding">
		    <div class="is-col col-5_5 offset-_5">
		    	<p class="cis__footer__small"><?php echo $site->footerCopyright() == 'custom' ? $site->footerCopyrightCustom()->kirbytextinline() . ' ' : '©'. date('Y').', '.$site->title().'.' ?></p>
		    	<!-- <p class="cis__footer__legal-credits">Diseñado en colaboración con <a href="https://invasoria.com" class="hover-underline-animation external-link" target="_blank" title="invasoria.com">Iván Soria</a></p> -->
		    </div>
		    <div class="is-col col-5 offset-_5">
		    	<?php 
			    	$legal_links = $site->footerLegal()->toStructure();
		    	?>
		    	<?php if($legal_links->count()): ?>
		    	<ul class="cis__nav-header__list">
	    		    <?php $languages = $kirby->languages(); ?>
			    	<?php foreach ($legal_links as $legal_item): ?>

			    		<?php foreach ($legal_item->pagelink()->yaml() as $legal_item_pagelink): ?>
			    		    <?php if(page($legal_item_pagelink)): ?>
			    			<?php $legal_item_page = page($legal_item_pagelink); ?>	
			    		    <li class="cis__nav-header__list__item">
			    		    	<a href="<?php echo $legal_item_page->url(); ?>" class="section-link"><?php echo $legal_item_page->title(); ?></a>
			    		    </li>
			    		    <?php endif ?>
			    		<?php endforeach ?>

			    	<?php endforeach ?>
			    	<li class="cis__nav-header__list__item">—</li>
    		    	<?php foreach ($languages->flip() as $language): ?>
    		    		<li class="cis__nav-header__list__item<?php echo $language == $kirby->language() ? ' active-language' :''; ?>">
    		    			<a href="<?php echo $language->url() ?>" class="same-window"><?php echo strtoupper($language->code()); ?></a>
    		    		</li>
    		    	<?php endforeach ?>
		    	</ul>
		    	<?php endif ?>
		    </div>
		</div>
	</div>
</footer>