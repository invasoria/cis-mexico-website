<!DOCTYPE html>
<html lang="<?=$kirby->language() ? $kirby->language() : 'es'?>">
<head>
  <meta charset="UTF-8">
  <!--
  Made by

  Iván Soria
  For a Better Website™

  INVASORIA.COM
  -->

  <!-- THE TITLE -->
  <title><?php echo $page->seotitle()->isNotEmpty() ? $page->seotitle() : $page->title() . ' - ' . $site->title()->html();?></title>

  <!-- THE METATAGS -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="<?php echo $page->seodescription()->isNotEmpty() ? html::encode($page->seodescription()) : html::encode($page->description()->or($page->text())->or($site->seodescription())); ?>">  
  <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=5,user-scalable=yes">
  <meta name="application-name" content="<?php echo $site->title()->html(); ?>">
  <meta name="mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="msapplication-tap-highlight" content="no">

  <!-- THE FACEBOOK (DROP THE _THE_) -->
  <meta property="og:site_name" content="<?php echo html::decode($site->title()); ?>">
  <meta property="og:title" content="<?php echo $page->seotitle()->isNotEmpty() ? $page->seotitle() : $page->title() . ' - ' . $site->title()->html();?>">
  <meta property="og:type" content="website">
  <meta property="og:url" content="<?php echo $page->url() ?>">
  <meta property="og:description" content="<?php echo $page->seodescription()->isNotEmpty() ? html::encode($page->seodescription()) : html::encode($page->description()->or($page->text())->or($site->seodescription())); ?>">
  <meta property="og:image" content="<?php echo ($page->cover()->isNotEmpty() && $page->cover()->toFile()) ? $page->cover()->toFile()->url() . '?v=' . time() : (($site->cover()->toFile()) ? $site->cover()->toFile()->url() . '?v=' . time() : $site->url() . '/assets/images/share_image.jpg' . '?v=' . time())?>">

  <!-- THE TWITTER -->
  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:title" content="<?php echo $page->seotitle()->isNotEmpty() ? $page->seotitle() : $page->title() . ' - ' . $site->title()->html();?>">
  <meta name="twitter:description" content="<?php echo $page->seodescription()->isNotEmpty() ? html::encode($page->seodescription()) : html::encode($page->description()->or($page->text())->or($site->seodescription())); ?>">
  <meta name="twitter:image" content="<?php echo ($page->cover()->isNotEmpty() && $page->cover()->toFile()) ? $page->cover()->toFile()->url() . '?v=' . time() : (($site->cover()->toFile()) ? $site->cover()->toFile()->url() . '?v=' . time() : $site->url() . '/assets/images/share_image.jpg' . '?v=' . time())?>">

  <link rel="apple-touch-icon" sizes="180x180" href="/assets/images/icons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/assets/images/icons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/assets/images/icons/favicon-16x16.png">
  <link rel="manifest" href="/assets/images/icons/site.webmanifest">
  <link rel="mask-icon" href="/assets/images/icons/safari-pinned-tab.svg" color="#2250b8">
  <link rel="shortcut icon" href="/assets/images/icons/favicon.ico">
  <meta name="msapplication-TileColor" content="#f7f8f8">
  <meta name="msapplication-config" content="/assets/images/icons/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">

  <!--THE CSS-->
  <?php echo css('assets/css/main.min.css' . '?v=' . time()) ?>

  </head>

  <body>
