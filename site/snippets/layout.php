<?php $numRow = 0; foreach ($page->toLayouts() as $row):
  $numRow = ++$numRow;
?>
<div class="is-row is-flex">
  <div class="is-col col-11 offset-_5">
    <!-- ROW -->
    <div class="is-row is-flex column-padding">
        <?php $numColumn = 0; foreach ($row->columns() as $column): $numColumn = ++$numColumn; ?>
          <?php if ($column->blocks()->isNotEmpty()): ?>
            <?php
              // Classes
              if ($column->span() === 12) {
                $class = '12';
              } elseif ($column->span() === 6) {
                $class = '6';
              } elseif ($column->span() === 4) {
                $class = '4';
              } elseif ($column->span() === 3) {
                $class = '3';
              } elseif ($column->span() === 8) {
                $class = '8';
              }
            ?>
            <!-- COLUMN -->
            <div class="is-col col-<?php echo $class; ?>">
              <?php $numBlock = 0; foreach ($column->blocks() as $block): $numBlock = ++$numBlock; ?>
                <?php
                  echo $block;
                ?>
              <?php endforeach ?>
            </div>
            <!-- COLUMN -->
          <?php endif ?>
        <?php endforeach ?>
    </div>
    <!-- ROW -->
  </div>
</div>
<?php endforeach ?>
