<?php if(isset($source)): ?>
<section class="--section --section-bg--light overflow-hidden">
    <div class="cis__section__container<?php echo $site->aboutAtHomePaddingTop()->isNotEmpty() && $site->aboutAtHomePaddingTop()->bool() === true ? ' --padding-top-120':''; ?><?php echo $site->aboutAtHomePaddingBottom()->isNotEmpty() && $site->aboutAtHomePaddingBottom()->bool() === true ? ' --padding-bottom-120 ':' '; ?>">

        <?php if(false): ?>
        <div class="is-row is-flex cis__picture-container-row">
            <div class="is-col offset-1 col-2">
                <?php if(false): ?>
                    <?php if($image_1 = $page->image('gif-6.gif')): ?>
                    <figure class="--figure" data-scroll data-scroll-speed="3" data-scroll-offset="-100,0"<?php echo $image_1 ? ' style="padding-top:' . ($image_1->height() / $image_1->width()) * 100 . '%;"' : '';?>>
                        <?php //if($image_1 = $page->main_image()->toFile()): ?>
                        <picture class="js-lazy-image --special-transform js-can-use-webp" data-alt="CIS México"<?php echo page('cover')->image($image_1->name().'.webp') ? ' data-webp="'.page('cover')->image($image_1->name().'.webp')->url() .'"': ''; ?><?php echo $image_1 ? ' style="padding-top:' . ($image_1->height() / $image_1->width()) * 100 . '%;"' : '';?>>
                            <source srcset="<?php echo $image_1->url(); ?>">
                        </picture>
                    </figure>
                    <?php endif ?>
                <?php endif ?>
                
                <?php if(true): ?>
                    <?php if($video = $page->file('hero_stamp_01.mp4')): ?>
                        <?php 
                            //echo $video->url();
                            require_once('assets/php/getid3/getid3.php');
                            $getID3 = new getID3;
                            $file = $getID3->analyze($video->mediaRoot());
                            $video_width = $file['video']['resolution_x'];
                            $video_height = $file['video']['resolution_y'];
                         ?>
                         <figure class="--figure"<?php echo isset($video_width) && isset($video_height) ? ' style="padding-top:' . ($video_height / $video_width) * 100 . '%;"' : '';?> data-scroll data-scroll-speed="3" data-scroll-offset="-100,0">
                             <video class="js-lazy-image --special-transform" data-poster="" webkit-playsinline="webkit-playsinline" playsinline="playsinline" autoplay="autoplay" loop="loop" muted="muted">
                                 <source data-src="<?php echo $video->url() ?>" type="<?php echo $video->mime() ?>">
                             </video>
                             </picture>
                         </figure>
                    <?php endif ?>
                <?php endif ?>
            </div>
        </div>
        <?php endif ?>

        <div class="is-row is-flex column-padding">
            <div class="is-col col-7_5 offset-2 cis__special-text cis__special-text--orange">
                <h4 class="js-observe fade-and-slide-animation"><?php echo $source->aboutAtHomeEyebrow(); ?></h4>
                <h2 class="js-observe reveal-text-animation"><?php echo $source->aboutAtHomeText(); ?></h2>
                
                <div class="item-48"></div>
                <?php $buttons = $source->aboutAtHomeButtons()->toStructure(); ?>

                <?php if ($buttons->isNotEmpty()): ?>
                    <?php foreach ($buttons as $button): ?>

                        <?php if ($button->link()->value() === 'page' && $button->pagelink()->isNotEmpty() && $button->pagelink()->toPage()): ?>
                            <!-- SECTION LINK -->
                            <a href="<?php echo $button->pagelink()->toPage()->url(); ?>" class="js-observe fade-and-slide-animation cis__arrow-cta cis__arrow-cta--orange section-link" data-uri="<?php echo $button->pagelink()->toPage()->uri(); ?>" data-title="<?php echo $button->pagelink()->toPage()->title(); ?>" data-text="<?php echo $button->text(); ?>">
                                <span class="cis__arrow-cta__deco-bar"></span>
                                <span class="cis__arrow-cta__text"><?php echo $button->text(); ?></span>
                                <span class="cis__arrow-cta__arrow">
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="19.81px"height="15.41px" viewBox="0 0 19.81 15.41" style="overflow:visible;enable-background:new 0 0 19.81 15.41;"xml:space="preserve"><path class="cis__arrow__st0" d="M9.96,2.19l3.01,3.01l1.66,0.8H0v3.33h14.55L13,10.1l-3.06,3.05l2.11,2.26l7.76-7.74 L12.15,0L9.96,2.19z"/> </svg>
                                </span>
                                <span class="cis__arrow-cta__ellipse">
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="80px"height="80px" viewBox="0 0 80 80" style="overflow:visible;enable-background:new 0 0 80 80;" xml:space="preserve"> <defs> </defs><circle class="cis__arrow-cta__ellipse__st0" cx="40" cy="40" r="40"/> <circle class="cis__arrow-cta__ellipse__st1" cx="40" cy="40" r="39"/> </svg>
                                </span>
                            </a>
                            <!-- SECTION LINK -->
                        <?php elseif ($button->link()->value() === 'url' && $button->url()->isNotEmpty()): ?>
                        <?php endif ?>
                    <?php endforeach ?>
                <?php endif ?>
                
            </div>
        </div>
    </div>
</section>
<?php endif ?>