<?php if (isset($article)): ?>
<?php 
    /*****
    *****
    Card (Instagram) Snippet

    Needed Parameters:
    
    $article -> Article page to get.

    *****
    *****/
    $url = $article->url();
    $uri = $article->uri();
    $title = $article->title();
    $image = $article->cover()->toFile();
    $external = $article->external();
    
    setlocale(LC_TIME, 'es_MX.utf8');
?>
<article class="is-col<?php echo $article->postSize()->isNotEmpty() ? ' col-'.ltrim($article->postSize(), 's'):''; ?> js-observe fade-animation cis__thinking__card__instagram">
    <a href="<?php echo $external; ?>" class="cis__thinking__card__instagram__anchor external-link">
        <?php if($image): ?>
            <figure class="--figure">
                <div class="--figure__container"<?php echo $image ? ' style="padding-top:' . ($image->height() / $image->width()) * 100 . '%;"' : '';?>>
                  <picture class="js-lazy-image --special-transform js-can-use-webp" data-alt="<?php echo $title; ?>"<?php echo $image->parent()->image($image->name().'.webp') ? ' data-webp="'.$image->parent()->image($image->name().'.webp')->url() .'"': ''; ?><?php echo $image ? ' style="padding-top:' . ($image->height() / $image->width()) * 100 . '%;"' : '';?>>
                      <source srcset="<?php echo $image->url(); ?>">
                  </picture>
                </div>
            </figure>
        <?php endif ?>
    </a>
    <a href="<?php echo $external; ?>" class="cis__thinking__card__instagram__anchor external-link">
        <h5 class="content__no-bottom-margin --margin-bottom-16"><?php echo $article->postType(); ?></h5>
        <?php echo $article->blocks()->toBlocks(); ?>
    </a>
</article>
<?php endif ?>