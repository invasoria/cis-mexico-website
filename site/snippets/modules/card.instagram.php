<?php if (isset($article)): ?>
<?php 
    /*****
    *****
    Card (Instagram) Snippet

    Needed Parameters:
    
    $article -> Article page to get.

    *****
    *****/
    $url = $article->url();
    $uri = $article->uri();
    $title = $article->title();
    if ($article->cover()->isNotEmpty() && $article->cover()->toFile()) {
        $image = $article->cover()->toFile();
    } else{
        $image = '';
    }
    $external = $article->external();
    $report = $article->report();

    setlocale(LC_TIME, 'es_MX.utf8');
?>
<article class="is-col <?php echo isset($special_grid) && $special_grid == true ? 'is-col--special-gap ' :'';?><?php echo $article->width()->isNotEmpty() ? ' col-'.ltrim($article->width(), 's'):''; ?><?php echo $article->offset()->isNotEmpty() ? ' offset-'.ltrim($article->offset(), 's'):''; ?><?php echo $article->widthm()->isNotEmpty() ? ' md-col-'.ltrim($article->widthm(), 's'):''; ?><?php echo $article->offsetm()->isNotEmpty() ? ' md-offset-'.ltrim($article->offsetm(), 's'):''; ?><?php echo $article->widths()->isNotEmpty() ? ' xs-col-'.ltrim($article->widths(), 's'):''; ?><?php echo $article->offsets()->isNotEmpty() ? ' xs-offset-'.ltrim($article->offsets(), 's'):''; ?> js-observe fade-animation cis__card<?php echo $article->positiontype()->isNotEmpty() ? ' cis__card--' . $article->positiontype() : '';  ?>"<?php echo $article->tags()->isNotEmpty() ? ' data-seotags="' . $article->tags() .'"':''; ?>>
    <!-- BG CONTAINER -->
    <?php if($report_page = $report->toPage()): ?>
    <a href="<?php echo $report_page->url(); ?>" class="cis__card__bg js-lazy-image js-lazy-image--background js-can-use-webp"<?php echo isset($image) && $image != '' ? ' data-background-image="' . $image->url() . '"' : ''; ?><?php echo isset($image) && $image != '' && $image->parent()->image($image->name().'.webp') ? ' data-webp="'.$image->parent()->image($image->name().'.webp')->url() .'"': ''; ?>></a>
    <?php elseif($article->external()->isNotEmpty()): ?>
    <a href="<?php echo $article->external()->isNotEmpty(); ?>" class="cis__card__bg js-lazy-image js-lazy-image--background js-can-use-webp"<?php echo isset($image) && $image != '' ? ' data-background-image="' . $image->url() . '"' : ''; ?><?php echo isset($image) && $image != '' && $image->parent()->image($image->name().'.webp') ? ' data-webp="'.$image->parent()->image($image->name().'.webp')->url() .'"': ''; ?>></a>
    <?php endif ?>
    <!-- BG CONTAINER -->

    <!-- CARD CONTENT -->
    <div class="cis__card__container">
        <?php if($report_page = $report->toPage()): ?>
        <!-- REPORT ANCHOR -->
        <a href="<?php echo $report_page->url(); ?>" class="cis__card__anchor section-link" id="section-<?php echo $report_page->autoid(); ?>" data-url="<?php echo $report_page->url(); ?>" data-uri="<?php echo $report_page->uri(); ?>" data-title="<?php echo $report_page->seotitle()->isNotEmpty() ? $report_page->seotitle() : $report_page->title(); ?>" data-description="<?php echo $report_page->seodescription()->isNotEmpty() ? $report_page->seodescription() : $site->description(); ?>">
            <h6 class="content__no-bottom-margin --margin-bottom-16 js-observe fade-animation">REPORT</h6>
            <?php echo $article->blocks()->toBlocks(); ?>
        </a>
        <!-- REPORT ANCHOR -->
        <?php elseif($article->external()->isNotEmpty()): ?>
        <!-- EXTERNAL LINK -->
        <a href="<?php echo $external; ?>" class="cis__card__anchor external-link">
            <h6 class="content__no-bottom-margin --margin-bottom-16 js-observe fade-animation">INSIGHTS</h6>
            <?php echo $article->blocks()->toBlocks(); ?>
        </a>
        <!-- EXTERNAL LINK -->
        <?php else: ?>
        <!-- NO LINK CARD -->
        <div class="cis__card__anchor">
            <h6 class="content__no-bottom-margin --margin-bottom-16 js-observe fade-animation">INSIGHTS</h6>
            <?php echo $article->blocks()->toBlocks(); ?>
        </div>
        <!-- NO LINK CARD -->
        <?php endif ?>

        <!-- SHARE CARD -->
        <p class="cis__card__share-title js-observe fade-animation">COMPARTIR</p>
        <div class="cis__card__share js-observe fade-animation">
            <a href="<?php echo $article ? 'https://twitter.com/intent/tweet?source='.urlencode($article->url()) . '&text=' . urlencode($article->title()) . '%20' . urlencode($article->url()) . '&via=cismx' : '#'?>" class="cis__card__icon-button cis__card__icon-button--twitter external-link" target="_blank">
                <svg class="" x="0px" y="0px" width="32px"
                     height="32px" viewBox="0 0 32 32" style="overflow:visible;enable-background:new 0 0 32 32;" xml:space="preserve">
                    <path class="ener__button__svg__icon" d="M24,11.04c-0.6,0.26-1.24,0.44-1.89,0.52c0.69-0.41,1.2-1.05,1.44-1.81c-0.64,0.38-1.35,0.65-2.08,0.79
                    c-1.24-1.32-3.31-1.39-4.64-0.15c-0.66,0.62-1.04,1.49-1.04,2.39c0,0.25,0.02,0.5,0.08,0.75c-2.64-0.13-5.1-1.38-6.76-3.43
                    c-0.88,1.5-0.44,3.42,1.01,4.39c-0.52-0.01-1.03-0.15-1.48-0.41v0.04c0,1.56,1.1,2.91,2.63,3.22c-0.28,0.07-0.57,0.11-0.86,0.11
                    c-0.21,0-0.42-0.01-0.62-0.06c0.43,1.33,1.66,2.25,3.07,2.29c-1.16,0.91-2.59,1.4-4.07,1.4c-0.26,0-0.52-0.02-0.78-0.05
                    c4.34,2.78,10.12,1.51,12.9-2.83c0.96-1.5,1.47-3.25,1.47-5.03c0-0.14,0-0.29-0.01-0.42C23,12.28,23.56,11.7,24,11.04z"/>
                </svg>
            </a>
            <a href="<?php echo $article ? 'https://www.facebook.com/sharer/sharer.php?u='.urlencode($article->url()) : '#'?>" class="cis__card__icon-button cis__card__icon-button--facebook external-link<?php echo $article ? '' : ' ener__button--inactive'?>" target="_blank">
                <svg class="" x="0px" y="0px" width="32px"
                     height="32px" viewBox="0 0 32 32" style="overflow:visible;enable-background:new 0 0 32 32;" xml:space="preserve">
                    <path class="ener__button__svg__icon" d="M24,16.05C24,11.6,20.42,8,16,8s-8,3.6-8,8.05c0,4.02,2.93,7.35,6.75,7.95v-5.62h-2.03v-2.33h2.03v-1.77
                        c0-2.02,1.19-3.13,3.02-3.13c0.88,0,1.79,0.16,1.79,0.16v1.98h-1.01c-0.99,0-1.3,0.62-1.3,1.26v1.51h2.22l-0.35,2.33h-1.86V24
                        C21.07,23.4,24,20.07,24,16.05L24,16.05z"/>
                </svg>
            </a>
            <a href="<?php echo $article ? 'https://www.linkedin.com/sharing/share-offsite/?url='.urlencode($article->url()) : '#'?>" class="cis__card__icon-button cis__card__icon-button--linked-in external-link<?php echo $article ? '' : ' ener__button--inactive'?>" target="_blank">
                <svg class="" x="0px" y="0px" width="32px"
                     height="32px" viewBox="0 0 32 32" style="overflow:visible;enable-background:new 0 0 32 32;" xml:space="preserve">
                    <path class="ener__button__svg__icon" d="M22.77,8H9.23C8.59,7.99,8.06,8.51,8.05,9.15c0,0,0,0,0,0v13.7c0.01,0.64,0.53,1.16,1.17,1.15c0,0,0,0,0,0 h13.54c0.64,0.01,1.17-0.51,1.17-1.15c0,0,0,0,0,0V9.15C23.94,8.51,23.41,7.99,22.77,8z M10.47,21.39v-7.22h2.4v7.22H10.47z M11.67,13.18h-0.02c-0.76,0-1.37-0.62-1.36-1.38c0-0.76,0.62-1.37,1.38-1.36c0.76,0,1.37,0.62,1.36,1.38 C13.03,12.56,12.42,13.17,11.67,13.18z M21.53,21.39h-2.4v-3.86c0-0.97-0.35-1.63-1.22-1.63c-0.56,0-1.05,0.35-1.24,0.88 c-0.06,0.19-0.09,0.39-0.08,0.59v4.03h-2.4c0,0,0.03-6.55,0-7.22h2.4v1.02c0.44-0.77,1.28-1.23,2.16-1.19 c1.58,0,2.76,1.03,2.76,3.25L21.53,21.39z"/>
                </svg>
            </a>
        </div>
        <!-- SHARE CARD -->
    </div>
    <!-- CARD CONTENT -->
</article>
<?php endif ?>