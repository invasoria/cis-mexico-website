
<article class="is-col<?php echo $article->postSize()->isNotEmpty() ? ' col-'.ltrim($article->postSize(), 's'):''; ?>">
    <a href="<?php echo $article->url(); ?>">
        <h5><?php echo $article->type(); ?></h5>
        <h3><?php echo $article->title(); ?></h3>
    </a>
</article>