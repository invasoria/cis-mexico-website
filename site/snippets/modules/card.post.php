<?php if (isset($article)): ?>
<?php 
    /*****
    *****
    Card (Instagram) Snippet

    Needed Parameters:
    
    $article -> Article page to get.

    *****
    *****/
    $url = $article->url();
    $uri = $article->uri();
    $title = $article->title();
    $intro = $article->intro();
    if ($article->cover()->isNotEmpty() && $article->cover()->toFile()) {
        $image = $article->cover()->toFile();
    } else{
        $image = '';
    }
    $external = $article->external();
    $report = $article->report();

    setlocale(LC_TIME, 'es_MX.utf8');
?>
<article class="is-col <?php echo isset($special_grid) && $special_grid == true ? 'is-col--special-gap ' :'';?><?php echo $article->width()->isNotEmpty() ? ' col-'.ltrim($article->width(), 's'):''; ?><?php echo $article->offset()->isNotEmpty() ? ' offset-'.ltrim($article->offset(), 's'):''; ?><?php echo $article->widthm()->isNotEmpty() ? ' md-col-'.ltrim($article->widthm(), 's'):''; ?><?php echo $article->offsetm()->isNotEmpty() ? ' md-offset-'.ltrim($article->offsetm(), 's'):''; ?><?php echo $article->widths()->isNotEmpty() ? ' xs-col-'.ltrim($article->widths(), 's'):''; ?><?php echo $article->offsets()->isNotEmpty() ? ' xs-offset-'.ltrim($article->offsets(), 's'):''; ?> js-observe fade-and-slide-animation cis__post">

    <!-- CARD CONTENT -->
    <a href="<?php echo $url; ?>" class="cis__post__anchor section-link" data-url="<?php echo $article->url(); ?>" data-uri="<?php echo $article->uri(); ?>" data-title="<?php echo $article->seotitle()->isNotEmpty() ? $article->seotitle() : $article->title(); ?>" data-description="<?php echo $article->seodescription()->isNotEmpty() ? $article->seodescription() : $site->description(); ?>">
        <?php if($image): ?>
            <figure class="--figure">
                <div class="--figure__container"<?php echo $image ? ' style="padding-top:' . ($image->height() / $image->width()) * 100 . '%;"' : '';?>>
                  <picture class="js-lazy-image --special-transform js-can-use-webp" data-alt="<?php echo $title; ?>"<?php echo $image->parent()->image($image->name().'.webp') ? ' data-webp="'.$image->parent()->image($image->name().'.webp')->url() .'"': ''; ?><?php echo $image ? ' style="padding-top:' . ($image->height() / $image->width()) * 100 . '%;"' : '';?>>
                      <source srcset="<?php echo $image->url(); ?>">
                  </picture>
                </div>
            </figure>
        <?php endif ?>
    </a>
    <a href="<?php echo $url; ?>" class="cis__post__anchor cis__post__anchor--content section-link" data-url="<?php echo $article->url(); ?>" data-uri="<?php echo $article->uri(); ?>" data-title="<?php echo $article->seotitle()->isNotEmpty() ? $article->seotitle() : $article->title(); ?>" data-description="<?php echo $article->seodescription()->isNotEmpty() ? $article->seodescription() : $site->description(); ?>">
        <h5 class="content__no-bottom-margin --margin-bottom-16"><?php echo $article->postType()->isNotEmpty() ? $article->postType() : 'REPORTE'; ?></h5>
        <p class="big"><?php echo $title; ?></p>
    </a>
    <!-- CARD CONTENT -->
</article>
<?php endif ?>