<!-- CARD -->
<?php 
	switch ($size) {case 'xs': $size = '2'; break; case 's': $size = '3'; break; case 'm': $size = '4'; break; case 'l': $size = '6'; break; case 'xl': $size = '8'; break; case 's1': $size = '1'; break; case 's2': $size = '2'; break; case 's3': $size = '3'; break; case 's4': $size = '4'; break; case 's5': $size = '5'; break; case 's6': $size = '6'; break; case 's7': $size = '7'; break; case 's8': $size = '8'; break; case 's10': $size = '10'; break; case 's11': $size = '11'; break; case 's12': $size = '12'; break; default: $size = $size; break; } 
    if (isset($xs_size)) {
        switch ($xs_size) {
            case '1':
                $xs_size = '1';
                break;
            case '2':
                $xs_size = '2';
                break;
            case '3':
                $xs_size = '3';
                break;
            case '4':
                $xs_size = '4';
                break;
            case '5':
                $xs_size = '5';
                break;
            case '6':
                $xs_size = '6';
                break;
            case '7':
                $xs_size = '7';
                break;
            case '8':
                $xs_size = '8';
                break;
            case '10':
                $xs_size = '10';
                break;
            case '11':
                $xs_size = '11';
                break;
            case '12':
                $xs_size = '12';
                break;

            default:
                $xs_size = $xs_size;
                break;
        }
    }
    if (isset($md_size)) {
        switch ($md_size) {
            case '1':
                $md_size = '1';
                break;
            case '2':
                $md_size = '2';
                break;
            case '3':
                $md_size = '3';
                break;
            case '4':
                $md_size = '4';
                break;
            case '5':
                $md_size = '5';
                break;
            case '6':
                $md_size = '6';
                break;
            case '7':
                $md_size = '7';
                break;
            case '8':
                $md_size = '8';
                break;
            case '10':
                $md_size = '10';
                break;
            case '11':
                $md_size = '11';
                break;
            case '12':
                $md_size = '12';
                break;

            default:
                $md_size = $md_size;
                break;
        }
    }
    if (isset($offset)) {
        switch ($offset) {case 's1': $offset = '1'; break;case 's2': $offset = '2'; break; case 's3': $offset = '3'; break; case 's4': $offset = '4'; break; case 's5': $offset = '5'; break; case 's6': $offset = '6'; break; case 's7': $offset = '7'; break; case 's8': $offset = '8'; break; case 's9': $offset = '9'; break; case 's10': $offset = '10'; break; case 's11': $offset = '11'; break; default: $offset = $offset; break;
        }
    }
    $program_template = $program->intendedTemplate();
    $program_episode = '';
    $program_target = '';

    if ($program_template == 'program') {
        //$program_episode = $program->children()->listed()->first()->children()->first()->audiofile()->toFile()->url();
        $program_children_listed_first = $program->children()->listed()->first();

        if ($program_children_listed_first && $program_children_listed_first->children()->listed()->first()) {
            $program_episode = $program_children_listed_first->children()->listed()->first()->audiofile()->toFile() ? $program_children_listed_first->children()->listed()->first()->audiofile()->toFile()->url() : $program_episode = 'false';
            $episode_cover = $program_children_listed_first->children()->listed()->first()->cover()->toFile() ? $program_children_listed_first->children()->listed()->first()->cover()->toFile()->url() : $episode_cover = 'false';
            $program_target = $program_children_listed_first->children()->listed()->first()->audiofile()->toFile() ? $program_children_listed_first->children()->listed()->first()->autoid() : $program_episode = 'false';
        } else {
            $program_episode = 'false';
            $program_target = 'false';
        }
    } elseif ($program_template == 'episode') {
        if ($program->audiofile() !='' && $program->audiofile()->toFile()) {
            $program_episode = $program->audiofile()->toFile();
            $program_episode = $program->audiofile()->toFile()->url();
            $program_target = $program->autoid();
        }
    }
?>
<article class="is-col <?php echo isset($special_grid) && $special_grid == true ? 'is-col--special-gap ' :'';?>col-<?php echo $size; ?><?php echo isset($xs_size) ? ' xs-col-'.$xs_size :'';?><?php echo isset($md_size) ? ' md-col-'.$md_size :'';?> offset-<?php echo isset($offset) ? $offset : '0'; ?><?php echo isset($extra_margin) && $extra_margin == true ? ' special-margin-for-card':'' ?>"<?php echo isset($data_scroll) ? $data_scroll : ''; ?>>
    
    <?php if(isset($extra_decoration) && $extra_decoration): ?>
    <figure class="ener__card__deco --deco-01" data-scroll data-scroll-speed="1">
        <picture class="js-lazy-image --special-transform js-can-use-webp" style="display: inline-block; min-height: 1rem;" data-alt="Decorative petal">
            <source srcset="<?php echo $site->url(); ?>/assets/images/petal_02.png">
        </picture>
    </figure>
    <figure class="ener__card__deco --deco-02" data-scroll data-scroll-speed="1.75" data-scroll-offset="-500,0">
        <picture class="js-lazy-image --special-transform js-can-use-webp" style="display: inline-block; min-height: 1rem;" data-alt="Decorative petal">
            <source srcset="<?php echo $site->url(); ?>/assets/images/petal_01.png">
        </picture>
    </figure>
    <figure class="ener__card__deco --deco-03" data-scroll data-scroll-speed="-2" data-scroll-offset="-500,0">
        <picture class="js-lazy-image --special-transform js-can-use-webp" style="display: inline-block; min-height: 1rem;" data-alt="Decorative petal">
            <source srcset="<?php echo $site->url(); ?>/assets/images/petal_06.png">
        </picture>
    </figure>
    <?php endif ?>
    <div class="ener__card">
        <div class="ener__card__image">
            <div class="ener__card__image__controls">
                <button class="ener__button ener__button--fill ener__card__image__controls__play margin-right-8 <?php echo 'id-' . $program_target ?><?php echo $program_episode == false || $program_episode == 'false'  ? ' ener__button--inactive' : ' play-target' ; ?>"<?php echo $program_episode == false || $program_episode == 'false'  ? ' disabled' : '' ; ?> data-episode="<?php echo $program_episode; ?>" data-csrf="<?= csrf() ?>" data-target="<?php echo $program_target; ?>">
                    <svg class="ener__button__svg" x="0px" y="0px" width="48px"
                         height="48px" viewBox="0 0 48 48" style="overflow:visible;enable-background:new 0 0 48 48;" xml:space="preserve">
                        <circle class="ener__button__svg__transparent-bg__bg" cx="24" cy="24" r="24"/>
                        <circle class="ener__button__svg__transparent-bg__border" cx="24" cy="24" r="23"/>
                        <circle class="ener__button__svg__white-bg" cx="24" cy="24" r="24"/>
                        <path class="ener__button__svg__icon play-icon" d="M21.67,30l8-6l-8-6V30z"/>
                        <g class="ener__button__svg__icon display-none pause-icon">
                            <rect x="19" y="19" width="2.86" height="10"/>
                            <rect x="26.14" y="19" width="2.86" height="10"/>
                        </g>
                    </svg>
                </button>
                <a href="<?php echo $program->url(); ?>" class="ener__button ener__card__image__controls__info section-link" data-uri="<?php echo $program->uri(); ?>" data-title="<?php echo htmlspecialchars($program->title()); ?>">
                    <svg class="ener__button__svg" x="0px" y="0px" width="48px"
                         height="48px" viewBox="0 0 48 48" style="overflow:visible;enable-background:new 0 0 48 48;" xml:space="preserve">
                        <circle class="ener__button__svg__transparent-bg__bg" cx="24" cy="24" r="24"/>
                        <circle class="ener__button__svg__transparent-bg__border" cx="24" cy="24" r="23"/>
                        <circle class="ener__button__svg__white-bg" cx="24" cy="24" r="24"/>
                        <g class="ener__button__svg__icon">
                            <g transform="translate(5 -64.267)">
                                <path d="M17,80.88h9c0.55,0,1,0.45,1,1l0,0c0,0.55-0.45,1-1,1h-9c-0.55,0-1-0.45-1-1l0,0C16,81.32,16.45,80.88,17,80.88z"/>
                                <path d="M17,86.88h9c0.55,0,1,0.45,1,1l0,0c0,0.55-0.45,1-1,1h-9c-0.55,0-1-0.45-1-1l0,0C16,87.32,16.45,86.88,17,86.88z"/>
                                <path d="M17,92.88h9c0.55,0,1,0.45,1,1l0,0c0,0.55-0.45,1-1,1h-9c-0.55,0-1-0.45-1-1l0,0C16,93.32,16.45,92.88,17,92.88z"/>
                            </g>
                            <g transform="translate(0 -64.267)">
                                <path d="M17,80.88h1c0.55,0,1,0.45,1,1l0,0c0,0.55-0.45,1-1,1h-1c-0.55,0-1-0.45-1-1l0,0C16,81.32,16.45,80.88,17,80.88z"/>
                                <path d="M17,86.88h1c0.55,0,1,0.45,1,1l0,0c0,0.55-0.45,1-1,1h-1c-0.55,0-1-0.45-1-1l0,0C16,87.32,16.45,86.88,17,86.88z"/>
                                <path d="M17,92.88h1c0.55,0,1,0.45,1,1l0,0c0,0.55-0.45,1-1,1h-1c-0.55,0-1-0.45-1-1l0,0C16,93.32,16.45,92.88,17,92.88z"/>
                            </g>
                        </g>
                    </svg>
                </a>
            </div>
            <div class="ener__card__image__container">
            	<?php //if($program->original()->bool() === true): ?>
    			<?php
                    if ($program_template == 'program' && $program->hasChildren() && $program->children()->first()->islisted()) {
                        foreach ($program->grandChildren()->listed()->flip()->limit(1) as $latest_episode){
            				$today = date("Y-m-d");
            				$daysago = date("Y-m-d", strtotime("-14 day"));
            				$today = date('Y-m-d', strtotime($today));
            				$latest_episode_date = $latest_episode->date("Y-m-d");
            				if (($latest_episode_date >= $daysago) && ($latest_episode_date <= $today)){
            				    echo '<div class="ener__card__image__floating-container"><span class="ener__tag--original">';
            				    echo "NUEVOS EPISODIOS";
            				    echo '</span></div>';
            				}
            			 }
                    } else if ($program_template == 'episode') {
                        $today = date("Y-m-d");
                        $daysago = date("Y-m-d", strtotime("-14 day"));
                        $today = date('Y-m-d', strtotime($today));
                        $latest_episode_date = $program->date("Y-m-d");
                        if (($latest_episode_date >= $daysago) && ($latest_episode_date <= $today)){
                            echo '<div class="ener__card__image__floating-container"><span class="ener__tag--original">';
                            echo "NUEVO";
                            echo '</span></div>';
                        }
                    }
    			?>
            	<?php //endif ?>
                <?php if($program->cover()!='' && $cover = $program->cover()->toFile()): ?>
                    <picture class="js-lazy-image --special-transform js-can-use-webp" style="display: block; min-height: 1rem;" data-alt="<?php echo $program->title(); ?>"<?php echo $program->image($cover->name().'.webp') ? ' data-webp="'.$program->image($cover->name().'.webp')->url() .'"': ''; ?>>
                        <source srcset="<?php echo $cover->url(); ?>">
                    </picture>
                <?php endif ?>
            </div>
            <a href="<?php echo $program->url(); ?>" class="ener__card__image__bg-anchor section-link" data-uri="<?php echo $program->uri(); ?>" data-title="<?php echo htmlspecialchars($program->title()); ?>">
                <svg class="ener__card__image__bg" x="0px" y="0px" width="175px"height="175px" viewBox="0 0 175 175"> <rect width="175" height="175"/> </svg>
            </a>
            <?php
            	/*
	            echo '<div class="ener__card__image__texture">';
	            echo '<img src="' . $site->url() . '/assets/images/pit/bg_texture_01.jpg" alt="Texture Background">';
	            echo '</div>';
	            //*/
            ?>
            <?php
            	//*
                if ($program_template == 'episode') {
                    echo '<div class="ener__card__image__marquee" data-text="' . $program->parentprogram() . '">';
                    echo $program->parentprogram();
                    echo '</div>';
                } else {
            	   echo '<div class="ener__card__image__marquee" data-text="' . $program->title() . '">';
            	   echo $program->title();
            	   echo '</div>';
                }
            	//*/
            ?>
        </div>
        <a href="<?php echo $program->url(); ?>" class="ener__card__content section-link" data-uri="<?php echo $program->uri(); ?>" data-title="<?php echo htmlspecialchars($program->title()); ?>">
            <?php
                //*
                if ($program->intendedTemplate() == 'episode') {
                    setlocale(LC_TIME, 'es_ES');
                    if (isset($continue) && $continue == true) {
                        echo '<span class="ener__date-tag ener__date-tag--dark no-margin">' . $program->title() . '</span>';
                    } else {
                        echo '<span class="ener__date-tag no-margin">' . $program->parent()->parent()->title() . ' – </span>';
                    }
                    //echo '<span> – </span>';
                    if (page($program) && $program->audiofile()->isnotEmpty() && $program->audiofile()->toFile()) {
                        $audio_file = $program->audiofile()->toFile();
                        require_once('assets/php/getid3/getid3.php');
                        $getID3 = new getID3;
                        $file_duration = $getID3->analyze($audio_file->mediaRoot());
                        $audio_file_playtime = floor(@$file_duration['playtime_seconds'] / 60); // playtime in minutes:seconds, formatted string
                        echo '<span class="ener__date-tag no-margin"><time datetime="' . $audio_file_playtime . '">' . $audio_file_playtime . ' min</time></span>';
                    }
                    //echo '<span class="ener__date-tag no-margin"><time datetime="' . $program->duration() . '">' . $program->duration() . ' min</time></span>';
                    //echo '<span class="ener__season-tag no-margin">' . strftime("%b %Y", strtotime($program->date()->toDate("d-m-Y"))) . '</span>';
                    //echo '<span class="ener__rating-tag no-margin">' . $program->rating() . '</span>';
                    echo '<div class="item-4"></div>';
                }
                //*/
             ?>

            <!-- PROGRAM TITLE -->
            <?php if(isset($continue) && $continue == true): ?>
            <?php else: ?>
            <p class="ener__card__content__program"><?php echo $program->title(); ?><?php echo $program->rating() == 'e' ? '<span class="ener__rating-tag--mini ener__rating__tag--after-title ener__rating__tag--after-title-small">E</span>' : '' ?></p>
            <?php endif ?>
            <?php 
                /*
                    echo '<p class="ener__card__content__program">' . $program_template . '</p>';
                    echo '<p class="ener__card__content__program">' . $program_episode . '</p>';
                //*/
            ?>           
            <?php 
            	//*
                if ($program->intendedTemplate() != 'episode') {
                    # code...
                	echo '<!-- PROGRAM INTRO -->';
                	echo '<p class="ener__card__content__description">' . $program->intro() . '</p>';
                	echo '<!-- PROGRAM INTRO -->';
                }
                //*/
            ?>
            <?php 
            	/*
                $seasons = 0;
                foreach ($program->children()->listed() as $season) {
                    $seasons++;
                }
                echo '<!-- SEASONS AND RATING TAG -->';
                echo '<span class="ener__season-tag no-margin">';
                if ($seasons>0) {
                	echo $seasons > 1 ? $seasons.' TEMPORADAS' : $seasons.' TEMPORADA' ;
                } else {
                	echo 'SIN TEMPORADAS';
                }
                echo '</span>';
                if ($program->rating()!='') {
                	echo '<span class="ener__rating-tag no-margin">';
                	echo $program->rating();
                	echo '</span>';
                	echo '<!-- SEASONS AND RATING TAG -->';
                }
                //*/
            ?>
            <?php 
            	/*
            	echo '<!-- PROGRAM TAGS -->';
            	foreach ($program->tags()->split() as $category) {
            		echo '<span class="ener__tag ener__tag--non-interactive">' . $category . '</span>';
            	}
            	echo '<!-- PROGRAM TAGS -->';
            	//*/
            ?>
        </a>
    </div>
</article>
<!-- CARD -->