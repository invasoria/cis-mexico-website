<?php if(isset($source)): ?>
<section class="cis__section cis__special-text">
    <div class="cis__section__container<?php echo $site->clientsBlockPaddingTop()->isNotEmpty() && $site->clientsBlockPaddingTop()->bool() === true ? ' --padding-top-120':''; ?><?php echo $site->clientsBlockPaddingBottom()->isNotEmpty() && $site->clientsBlockPaddingBottom()->bool() === true ? ' --padding-bottom-120 ':' '; ?>">
        <div class="is-row is-flex is-row--responsive column-padding">
            <!-- LEFT BLOCK -->
            <div class="is-col col-3_5 offset-_5 xs-col-12">
                <div class="is-row is-flex is-row--responsive">
                    <div class="is-col col-3_5 xs-col-4">
                        <?php if($image_1 = $page->image('2570612_sq.jpg')): ?>
                        <figure class="--figure"<?php echo $image_1 ? ' style="padding-top:' . ($image_1->height() / $image_1->width()) * 100 . '%;"' : '';?>>
                            <?php //if($image_1 = $page->main_image()->toFile()): ?>
                            <picture class="js-lazy-image --special-transform js-can-use-webp" data-alt="CIS México"<?php echo page('cover')->image($image_1->name().'.webp') ? ' data-webp="'.page('cover')->image($image_1->name().'.webp')->url() .'"': ''; ?><?php echo $image_1 ? ' style="padding-top:' . ($image_1->height() / $image_1->width()) * 100 . '%;"' : '';?>>
                                <source srcset="<?php echo $image_1->url(); ?>">
                            </picture>
                        </figure>
                        <?php endif ?>
                        <div class="item-24"></div>
                    </div>
                    <div class="is-col col-7 offset-1_5 xs-col-8">
                        <?php if($image_1 = $page->image('1154404.jpg')): ?>
                        <figure class="--figure"<?php echo $image_1 ? ' style="padding-top:' . ($image_1->height() / $image_1->width()) * 100 . '%;"' : '';?>>
                            <?php //if($image_1 = $page->main_image()->toFile()): ?>
                            <picture class="js-lazy-image --special-transform js-can-use-webp" data-alt="CIS México"<?php echo page('cover')->image($image_1->name().'.webp') ? ' data-webp="'.page('cover')->image($image_1->name().'.webp')->url() .'"': ''; ?><?php echo $image_1 ? ' style="padding-top:' . ($image_1->height() / $image_1->width()) * 100 . '%;"' : '';?>>
                                <source srcset="<?php echo $image_1->url(); ?>">
                            </picture>
                        </figure>
                        <?php endif ?>
                        <div class="item-48"></div>
                    </div>
                </div>
            </div>
            <!-- LEFT BLOCK -->

            <!-- RIGHT BLOCK -->
            <div class="is-col col-6 xs-col-12">
                <div class="is-row is-flex is-row--responsive">
                    <div class="is-col col-8 xs-col-10 cis__special-text cis__special-text--orange">
                        <h2 class="js-observe reveal-text-animation cis__special-text--serif-heading"><!-- gggyy --><?php echo $source->clientsBlockHeading(); ?></h2>
                        <div class="item-48"></div>
                    </div>
                    <div class="is-col col-2 offset-2 xs-col-2">
                        <?php if($image_1 = $page->image('3448707_sq.jpg')): ?>
                        <figure class="--figure"<?php echo $image_1 ? ' style="padding-top:' . ($image_1->height() / $image_1->width()) * 100 . '%;"' : '';?>>
                            <?php //if($image_1 = $page->main_image()->toFile()): ?>
                            <picture class="js-lazy-image --special-transform js-can-use-webp" data-alt="CIS México"<?php echo page('cover')->image($image_1->name().'.webp') ? ' data-webp="'.page('cover')->image($image_1->name().'.webp')->url() .'"': ''; ?><?php echo $image_1 ? ' style="padding-top:' . ($image_1->height() / $image_1->width()) * 100 . '%;"' : '';?>>
                                <source srcset="<?php echo $image_1->url(); ?>">
                            </picture>
                        </figure>
                        <?php endif ?>
                        <div class="item-24"></div>
                    </div>

                    <div class="is-col col-12 xs-col-12">
                        <div class="cis__value-block--two-columns">
                            <div class="js-observe fade-animation">
                                <?php echo $source->clientsBlockText()->kt(); ?>
                            </div>
                        </div>
                        <div class="item-40"></div>
                    </div>
                        
                    <div class="is-col col-12 xs-col-12">
                        <h4 class="end">ALGUNOS DE NUESTROS CLIENTES</h4>
                        <div class="item-8"></div>
                        
                        <!-- CLIENTS LIST -->
                        <ul class="cis__value-block__list">
                            <!-- CLIENT -->
                            <li class="cis__value-block__list__item">
                                <?php if($image_1 = $page->image('B_A.jpg')): ?>
                                <figure class="--figure">
                                    <picture class="js-lazy-image --special-transform js-can-use-webp" data-alt="CIS México"<?php echo page('cover')->image($image_1->name().'.webp') ? ' data-webp="'.page('cover')->image($image_1->name().'.webp')->url() .'"': ''; ?>>
                                        <source srcset="<?php echo $image_1->url(); ?>">
                                    </picture>
                                </figure>
                                <?php endif ?>
                            </li>
                            <!-- CLIENT -->

                            <!-- CLIENT -->
                            <li class="cis__value-block__list__item">
                                <?php if($image_1 = $page->image('bid_banco.jpg')): ?>
                                <figure class="--figure">
                                    <picture class="js-lazy-image --special-transform js-can-use-webp" data-alt="CIS México"<?php echo page('cover')->image($image_1->name().'.webp') ? ' data-webp="'.page('cover')->image($image_1->name().'.webp')->url() .'"': ''; ?>>
                                        <source srcset="<?php echo $image_1->url(); ?>">
                                    </picture>
                                </figure>
                                <?php endif ?>
                            </li>
                            <!-- CLIENT -->

                            <!-- CLIENT -->
                            <li class="cis__value-block__list__item">
                                <?php if($image_1 = $page->image('santander.jpg')): ?>
                                <figure class="--figure">
                                    <picture class="js-lazy-image --special-transform js-can-use-webp" data-alt="CIS México"<?php echo page('cover')->image($image_1->name().'.webp') ? ' data-webp="'.page('cover')->image($image_1->name().'.webp')->url() .'"': ''; ?>>
                                        <source srcset="<?php echo $image_1->url(); ?>">
                                    </picture>
                                </figure>
                                <?php endif ?>
                            </li>
                            <!-- CLIENT -->
                            
                            <!-- CLIENT -->
                            <li class="cis__value-block__list__item">
                                <?php if($image_1 = $page->image('telcel.jpg')): ?>
                                <figure class="--figure">
                                    <picture class="js-lazy-image --special-transform js-can-use-webp" data-alt="CIS México"<?php echo page('cover')->image($image_1->name().'.webp') ? ' data-webp="'.page('cover')->image($image_1->name().'.webp')->url() .'"': ''; ?>>
                                        <source srcset="<?php echo $image_1->url(); ?>">
                                    </picture>
                                </figure>
                                <?php endif ?>
                            </li>
                            <!-- CLIENT -->

                            <!-- CLIENT -->
                            <li class="cis__value-block__list__item">
                                <?php if($image_1 = $page->image('adeprotur.jpg')): ?>
                                <figure class="--figure">
                                    <picture class="js-lazy-image --special-transform js-can-use-webp" data-alt="CIS México"<?php echo page('cover')->image($image_1->name().'.webp') ? ' data-webp="'.page('cover')->image($image_1->name().'.webp')->url() .'"': ''; ?>>
                                        <source srcset="<?php echo $image_1->url(); ?>">
                                    </picture>
                                </figure>
                                <?php endif ?>
                            </li>
                            <!-- CLIENT -->
                        </ul>
                        <!-- CLIENTS LIST -->
                    </div>
                </div>
            </div>
            <!-- RIGHT BLOCK -->
        </div>
    </div>
</section>
<?php endif ?>