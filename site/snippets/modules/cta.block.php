<?php if(isset($source)): ?>
    <section class="--section grid-margin --section-bg--light overflow-hidden">
        <div class="is-row is-flex column-padding">
            <div class="is-col col-11 offset-_5">
                <!-- LAYOUT ROW -->
                <div class="is-row is-flex --padding-top-160 --padding-bottom-120 --padding-mobile-top-80 --padding-mobile-bottom-80">
                    <!-- COLUMN -->
                    <div class="is-col col-12">
                        
                        <div class="is-row is-flex">
                            <div class="is-col col-7_5 cis__special-text cis__special-text--purple">
                                <h4 class="js-observe fade-and-slide-animation"><?php echo $source->ctaBlockEyebrow(); ?></h4>
                                <h1 class="js-observe reveal-text-animation"><?php echo $source->ctaBlockHeading(); ?></h1>
                                <div class="item-80"></div>
                            </div>
                            <div class="is-col col-3 cis--align-items-end">
                                <p class="js-observe reveal-text-animation"><?php echo $source->ctaBlockText(); ?></p>
                            </div>
                        </div>
                        <div class="is-row is-flex">
                            <div class="is-col col-7_5">
                                <a href="<?php echo page('contacto')->url(); ?>" class="js-observe fade-and-slide-animation cis__arrow-cta cis__arrow-cta--purple section-link" data-uri="<?php echo page('contacto')->uri(); ?>" data-title="<?php echo page('contacto')->title(); ?>" data-text="Ponte en Contacto">
                                    <span class="cis__arrow-cta__deco-bar"></span>
                                    <span class="cis__arrow-cta__text">Ponte en Contacto</span>
                                    <span class="cis__arrow-cta__arrow">
                                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="19.81px"height="15.41px" viewBox="0 0 19.81 15.41" style="overflow:visible;enable-background:new 0 0 19.81 15.41;"xml:space="preserve"><path class="cis__arrow__st0" d="M9.96,2.19l3.01,3.01l1.66,0.8H0v3.33h14.55L13,10.1l-3.06,3.05l2.11,2.26l7.76-7.74 L12.15,0L9.96,2.19z"/> </svg>
                                    </span>
                                    <span class="cis__arrow-cta__ellipse">
                                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="80px"height="80px" viewBox="0 0 80 80" style="overflow:visible;enable-background:new 0 0 80 80;" xml:space="preserve"> <defs> </defs><circle class="cis__arrow-cta__ellipse__st0" cx="40" cy="40" r="40"/> <circle class="cis__arrow-cta__ellipse__st1" cx="40" cy="40" r="39"/> </svg>
                                    </span>
                                </a>
                            </div>
                        </div>

                    </div>
                    <!-- COLUMN -->
                </div>
                <!-- LAYOUT ROW -->
            </div>
        </div>
    </section>
<?php endif ?>