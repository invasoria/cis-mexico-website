<?php if($page_checked && $page_checked->interaction()->bool() && $page_checked->episode()->isNotEmpty() && $interaction_page = $page_checked->episode()->toPage()): ?>
<?php if($file_int = $interaction_page->audiofile()->toFile()): ?>
<?php 
    $program_target = '';
    $program_target = $interaction_page->autoid();
?>
<section class="ener__feature-cta">
    <div class="container grid-margin">
        <div class="is-row is-flex column-padding">
            <div class="is-col offset-1 col-5 ener__feature-cta__left-container">
                <h1><?php echo $page_checked->heading()->kirbytextinline(); ?></h1>
                <h2><?php echo $page_checked->subheading()->kirbytextinline(); ?></h2>
                
                <?php echo $page_checked->interaction() == true && $page_checked->mode()->isNotEmpty() && $page_checked->mode() == 'listen' ? '<h3>Prueba ya: escucha un episodio</h3>' : '<h3>Prueba ya: descargar archivo</h3>'; ?>

                <?php if($page_checked->mode()->isNotEmpty() && $page_checked->mode() == 'download'): ?>
                <?php if($interaction_page->downloadable()->toBool() !== false || !$interaction_page->downloadable()->isNotEmpty()): ?>
                <a href="<?php echo $file_int->url(); ?>" class="ener__widget ener__widget--dark-bg download-target" target="_blank" data-episode="<?php echo $file_int; ?>">
                    <ul class="ener__widget__list">
                        <li class="ener__widget__item ener__widget__item--button">
                            <div class="ener__button">
                                <svg class="ener__button__svg" x="0px" y="0px" width="48px"
                                     height="48px" viewBox="0 0 48 48" style="overflow:visible;enable-background:new 0 0 48 48;" xml:space="preserve">
                                    <circle class="ener__button__svg__transparent-bg__bg" cx="24" cy="24" r="24"/>
                                    <circle class="ener__button__svg__transparent-bg__border" cx="24" cy="24" r="23"/>
                                    <circle class="ener__button__svg__white-bg" cx="24" cy="24" r="24"/>
                                    <path class="ener__button__svg__icon" d="M31.35,22.03c-0.81-4.06-4.76-6.7-8.82-5.89c-2.23,0.44-4.13,1.87-5.18,3.89 c-3.29,0.36-5.68,3.32-5.32,6.61C12.36,29.69,14.93,32,18,32h13c2.75,0.01,5-2.21,5.01-4.96C36.02,24.4,33.98,22.21,31.35,22.03z M24,30l-5-5h3v-4h4v4h3L24,30z"/>
                                </svg>
                            </div>
                        </li>
                        <li class="ener__widget__item ener__widget__item--info">
                            <p class="ener__widget__item__episode"><?php echo $interaction_page->title(); ?></p>
                            <p class="ener__widget__item__file-info"><?php if($interaction_page->parentprogram()->isNotEmpty()): ?><?php echo $interaction_page->parentprogram(); ?><?php endif ?> <span>–</span> <?php 
                                    if (page($interaction_page) && $interaction_page->audiofile()->isnotEmpty() && $interaction_page->audiofile()->toFile()) {
                                        $audio_file = $interaction_page->audiofile()->toFile();
                                        require_once('assets/php/getid3/getid3.php');
                                        $getID3 = new getID3;
                                        $file_duration = $getID3->analyze($audio_file->mediaRoot());
                                        $audio_file_playtime = floor(@$file_duration['playtime_seconds'] / 60); // playtime in minutes:seconds, formatted string
                                        echo '<time datetime="' . $audio_file_playtime . '">' . $audio_file_playtime . ' min</time>';
                                    }
                                 ?><span class=""> /<?php if($file_int): ?> <?php echo $file_int->extension(); ?> / <?php echo floor(($file_int->size() / 1024) / 1000); ?> MB<?php endif ?></span></p>
                        </li>
                    </ul>
                </a>
                <?php else: ?>
                    <p>Lo sentimos: este archivo no tiene permisos para ser descargado.</p>
                <?php endif ?>
                <?php elseif($page_checked->mode()->isNotEmpty() && $page_checked->mode() == 'listen'): ?>
                <button class="ener__widget ener__widget--dark-bg <?php echo 'id-' . $program_target ?><?php echo $file_int == false || $file_int == 'false'  ? ' ener__button--inactive' : ' play-target'; ?>"<?php echo $file_int == false || $file_int == 'false'  ? ' disabled' : '' ; ?> data-episode="<?php echo $file_int; ?>" data-csrf="<?= csrf() ?>" data-target="<?php echo $program_target; ?>">
                    <ul class="ener__widget__list">
                        <li class="ener__widget__item ener__widget__item--button">
                            <div class="ener__button">
                                <svg class="ener__button__svg" x="0px" y="0px" width="48px"
                                     height="48px" viewBox="0 0 48 48" style="overflow:visible;enable-background:new 0 0 48 48;" xml:space="preserve">
                                    <circle class="ener__button__svg__transparent-bg__bg" cx="24" cy="24" r="24"/>
                                    <circle class="ener__button__svg__transparent-bg__border" cx="24" cy="24" r="23"/>
                                    <circle class="ener__button__svg__white-bg" cx="24" cy="24" r="24"/>
                                    <path class="ener__button__svg__icon play-icon" d="M21.67,30l8-6l-8-6V30z"/>
                                    <g class="ener__button__svg__icon display-none pause-icon">
                                        <rect x="19" y="19" width="2.86" height="10"/>
                                        <rect x="26.14" y="19" width="2.86" height="10"/>
                                    </g>
                                </svg>
                            </div>
                        </li>
                        <li class="ener__widget__item ener__widget__item--info">
                            <p class="ener__widget__item__episode"><?php echo $interaction_page->title(); ?></p>
                            <p class="ener__widget__item__file-info"><?php if($interaction_page->parentprogram()->isNotEmpty()): ?><?php echo $interaction_page->parentprogram(); ?><?php endif ?> <span>–</span> <?php 
                                    if (page($interaction_page) && $interaction_page->audiofile()->isnotEmpty() && $interaction_page->audiofile()->toFile()) {
                                        $audio_file = $interaction_page->audiofile()->toFile();
                                        require_once('assets/php/getid3/getid3.php');
                                        $getID3 = new getID3;
                                        $file_duration = $getID3->analyze($audio_file->mediaRoot());
                                        $audio_file_playtime = floor(@$file_duration['playtime_seconds'] / 60); // playtime in minutes:seconds, formatted string
                                        echo '<time datetime="' . $audio_file_playtime . '">' . $audio_file_playtime . ' min</time>';
                                    }
                                 ?><span class=""> /<?php if($file_int): ?> <?php echo $file_int->extension(); ?> / <?php echo floor(($file_int->size() / 1024) / 1000); ?> MB<?php endif ?></span></p>
                        </li>
                    </ul>
                </button>
                <?php elseif($page_checked->mode()->isNotEmpty() && $page_checked->mode() == 'listenexternal'): ?>
                <?php endif ?>
            </div>
            <div class="is-col offset-1 col-4 ener__feature-cta__right-container">
                <!-- INTERACTION CARD -->
                <div class="ener__card no-margin">
                    <div class="ener__card__image no-margin">
                        <div class="ener__card__image__controls ener__card__image__controls--single">
                            <?php if($page_checked->mode()->isNotEmpty() && $page_checked->mode() == 'download'): ?>
                            <a href="<?php echo $file_int->url(); ?>" class="ener__button ener__button--fill download-target" target="_blank" data-episode="<?php echo $file_int; ?>">
                                <svg class="ener__button__svg" x="0px" y="0px" width="48px"
                                     height="48px" viewBox="0 0 48 48" style="overflow:visible;enable-background:new 0 0 48 48;" xml:space="preserve">
                                    <circle class="ener__button__svg__transparent-bg__bg" cx="24" cy="24" r="24"/>
                                    <circle class="ener__button__svg__transparent-bg__border" cx="24" cy="24" r="23"/>
                                    <circle class="ener__button__svg__white-bg" cx="24" cy="24" r="24"/>
                                    <path class="ener__button__svg__icon" d="M31.35,22.03c-0.81-4.06-4.76-6.7-8.82-5.89c-2.23,0.44-4.13,1.87-5.18,3.89 c-3.29,0.36-5.68,3.32-5.32,6.61C12.36,29.69,14.93,32,18,32h13c2.75,0.01,5-2.21,5.01-4.96C36.02,24.4,33.98,22.21,31.35,22.03z M24,30l-5-5h3v-4h4v4h3L24,30z"/>
                                </svg>
                            </a>
                            <?php elseif($page_checked->mode()->isNotEmpty() && $page_checked->mode() == 'listen'): ?>
                            <button class="ener__button ener__button--fill <?php echo 'id-' . $program_target ?><?php echo $file_int == false || $file_int == 'false'  ? ' ener__button--inactive' : ' play-target'; ?>"<?php echo $file_int == false || $file_int == 'false'  ? ' disabled' : '' ; ?> data-episode="<?php echo $file_int; ?>" data-csrf="<?= csrf() ?>" data-target="<?php echo $program_target; ?>">
                                <svg class="ener__button__svg" x="0px" y="0px" width="48px"
                                     height="48px" viewBox="0 0 48 48" style="overflow:visible;enable-background:new 0 0 48 48;" xml:space="preserve">
                                    <circle class="ener__button__svg__transparent-bg__bg" cx="24" cy="24" r="24"/>
                                    <circle class="ener__button__svg__transparent-bg__border" cx="24" cy="24" r="23"/>
                                    <circle class="ener__button__svg__white-bg" cx="24" cy="24" r="24"/>
                                    <path class="ener__button__svg__icon play-icon" d="M21.67,30l8-6l-8-6V30z"/>
                                    <g class="ener__button__svg__icon display-none pause-icon">
                                        <rect x="19" y="19" width="2.86" height="10"/>
                                        <rect x="26.14" y="19" width="2.86" height="10"/>
                                    </g>
                                </svg>
                            </button>
                            <?php elseif($page_checked->mode()->isNotEmpty() && $page_checked->mode() == 'listenexternal'): ?>
                            <?php endif ?>
                        </div>
                        <div class="ener__card__image__container --dark-bg">
                            <?php if($interaction_page->cover()->isNotEmpty() && $int_cover = $interaction_page->cover()->toFile()): ?>
                                <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" class="js-lazy-image --special-transform" data-src="<?php echo $int_cover->url(); ?>" loading="lazy" alt="<?php echo $interaction_page->title(); ?>">
                            <?php else: ?>
                                <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" class="js-lazy-image --special-transform" data-src="<?php echo $site->url(); ?>/assets/images/pit/no_cover_gray.png" alt="<?php echo $interaction_page->title(); ?>">
                            <?php endif ?>
                        </div>
                        <a href="<?php echo $interaction_page->url(); ?>" class="ener__card__image__bg-anchor section-link" data-uri="<?php echo $interaction_page->uri(); ?>" data-title="<?php echo $interaction_page->title(); ?>">
                            <svg class="ener__card__image__bg" x="0px" y="0px" width="175px"height="175px" viewBox="0 0 175 175"> <rect width="175" height="175"/> </svg>
                        </a>
                        <div class="ener__card__image__marquee" data-text="<?php echo $interaction_page->parentprogram(); ?>"><?php echo $interaction_page->parentprogram(); ?></div>
                    </div>
                </div>
                <!-- INTERACTION CARD -->
            </div>
        </div>
    </div>
</section>
<?php endif ?>
<?php endif ?>