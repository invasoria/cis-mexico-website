<!-- LATEST STUFF MODULE -->
<div class="ener__latest-stuff">
        <h3>Lo Nuevo</h3>
        <ul class="ener__latest-stuff__list">
                <!-- ITEM -->
                <li class="ener__latest-stuff__list__item">
                        <a href="#" class="ener__latest-stuff__list__item__anchor">
                                <span class="ener__latest-stuff__list__item__icon"><svg x="0px" y="0px" width="16px"height="16px" viewBox="0 0 16 16"> <defs> </defs> <g transform="translate(-24 -1008)"> <g transform="translate(24 1008)"> <circle class="svg-no-fill" cx="8" cy="8" r="8"/> <circle class="svg-stroke-width-2-gray-6-no-fill" cx="8" cy="8" r="7"/> </g> <path class="svg-fill-gray-1" d="M30.67,1019.11l4-3l-4-3V1019.11z"/> </g> </svg> </span>
                                <p class="ener__latest-stuff__list__item__episode">S1e1: La crisis que fracturó la crisis</p>
                                <p class="ener__latest-stuff__list__item__program">2050: El fin que no fue <span>–</span> <time datetime="3m 30s">30:57</time><span class=""> / 48MB</span></p>
                        </a>
                </li>
                <!-- ITEM -->

                <!-- ITEM -->
                <li class="ener__latest-stuff__list__item">
                        <a href="#" class="ener__latest-stuff__list__item__anchor">
                                <span class="ener__latest-stuff__list__item__icon"><svg x="0px" y="0px" width="16px"height="16px" viewBox="0 0 16 16"> <defs> </defs> <g transform="translate(-24 -1008)"> <g transform="translate(24 1008)"> <circle class="svg-no-fill" cx="8" cy="8" r="8"/> <circle class="svg-stroke-width-2-gray-6-no-fill" cx="8" cy="8" r="7"/> </g> <path class="svg-fill-gray-1" d="M30.67,1019.11l4-3l-4-3V1019.11z"/> </g> </svg> </span>
                                <p class="ener__latest-stuff__list__item__episode">S1e3:  Mis deseos sexuales ¿Reales o fantasías?</p>
                                <p class="ener__latest-stuff__list__item__program">Entre hermanas <span>–</span> <time datetime="28m 59s">28:59</time><span class=""> / 32MB</span></p>
                        </a>
                </li>
                <!-- ITEM -->

                <!-- ITEM -->
                <li class="ener__latest-stuff__list__item">
                        <a href="#" class="ener__latest-stuff__list__item__anchor">
                                <span class="ener__latest-stuff__list__item__icon"><svg x="0px" y="0px" width="16px"height="16px" viewBox="0 0 16 16"> <defs> </defs> <g transform="translate(-24 -1008)"> <g transform="translate(24 1008)"> <circle class="svg-no-fill" cx="8" cy="8" r="8"/> <circle class="svg-stroke-width-2-gray-6-no-fill" cx="8" cy="8" r="7"/> </g> <path class="svg-fill-gray-1" d="M30.67,1019.11l4-3l-4-3V1019.11z"/> </g> </svg> </span>
                                <p class="ener__latest-stuff__list__item__episode">S2e3: La niña y el Capitán O’Flynn</p>
                                <p class="ener__latest-stuff__list__item__program">Esto NO es radio <span>–</span> <time datetime="32m 20s">32:20</time><span class=""> / 54MB</span></p>
                        </a>
                </li>
                <!-- ITEM -->
        </ul>
</div>
<!-- LATEST STUFF MODULE -->