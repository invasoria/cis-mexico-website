<?php 
        
        //$latest_releases = page('programas')->children()->listed()->children()->listed()->children()->listed()->sortBy('date', 'desc')->limit(4);
        //$title = 'Lanzamientos Recientes';
        $latest_programs = page('programas')->children()->listed()->shuffle()->limit(4);
        //$latest_releases = page('programas')->children()->listed()->children()->listed()->children()->listed()->shuffle()->limit(4);
        $title = 'Te recomendamos';
 ?>
<section class="ener__latest-releases grid-margin<?php echo isset($padding) && $padding == false ? ' ener__latest-releases--short' : '';?>">
        <div class="is-row is-flex column-padding">
                <div class="is-col offset-1 col-10">
                        <h3><?php echo $title; ?></h3>
                            <div class="is-row is-row--responsive is-flex">

                                <?php if($latest_programs->count()==0): ?>
                                    <div class="is-col col-12 xs-col-12">
                                        <?php snippet('modules/empty.state') ?>
                                    </div>
                                <?php else: ?>
                                    <?php foreach ($latest_programs as $program): ?>
                                        <?php $show = $program->children()->listed()->children()->listed()->shuffle()->limit(1); ?>
                                        <?php foreach ($show as $episode): ?>
                                        <?php snippet('modules/card.program', ['program' => $episode, 'size' => 's', 'xs_size' => '6',]) ?>
                                        <?php endforeach ?>
                                    <?php endforeach ?>
                                <?php endif ?>                                
                        </div>
                        <div class="centered-content">
                            <div class="item-48 is-shown-mobile"></div>
                            <a href="<?= url(page('programas')->url(), ['params' => ['category' => urlencode('Episodios')]]) ?>" data-uri="<?php echo page('programas')->uri(); ?>" data-title="<?php echo page('programas')->title(); ?>" class="main-cta main-cta--dark section-link" data-text="Ve más episodios"><span>Ve más episodios</span></a>
                        </div>
                </div>
        </div>
</section>