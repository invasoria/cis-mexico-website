<?php 
    $latest_release = page('programas')->children()->listed()->children()->listed()->children()->listed()->sortBy('date', 'desc')->limit(3);
 ?>
<!-- LATEST STUFF MODULE -->
<div class="ener__latest-stuff<?php echo isset($no_margin) ? ' ener__latest-stuff--no-margin' : ''; ?>">
        <h3>Nuevos episodios</h3>
        <ul class="ener__latest-stuff__list">
                <?php foreach ($latest_release as $release): ?>
                <!-- ITEM -->
                <li class="ener__latest-stuff__list__item">
                    <a href="<?php echo $release->url(); ?>" class="is-row is-row--responsive is-flex ener__latest-stuff__list__item__anchor section-link" data-uri="<?php echo $release->uri(); ?>" data-title="<?php echo $release->title(); ?>">
                        <span class="is-col col-3 xs-col-2_5 md-col-5">
                            <?php if($release->cover()->isnotEmpty() && $cover = $release->cover()->toFile()): ?>
                                <picture class="js-lazy-image js-can-use-webp" style="display: block; min-height: 1rem;" data-alt="<?php echo $release->title(); ?>"<?php echo $release->image($cover->name().'.webp') ? ' data-webp="'.$release->image($cover->name().'.webp')->resize(80)->url() .'"': ''; ?>>
                                    <source srcset="<?php echo $cover->resize(80)->url(); ?>">
                                </picture>
                            <?php endif ?>
                        </span>
                        <span class="is-col col-9 xs-col-9_5 md-col-7">
                            <?php if(false): ?>
                            <span class="ener__latest-stuff__list__item__icon"><svg x="0px" y="0px" width="16px"height="16px" viewBox="0 0 16 16"> <defs> </defs> <g transform="translate(-24 -1008)"> <g transform="translate(24 1008)"> <circle class="svg-no-fill" cx="8" cy="8" r="8"/> <circle class="svg-stroke-width-2-gray-6-no-fill" cx="8" cy="8" r="7"/> </g> <path class="svg-fill-gray-1" d="M30.67,1019.11l4-3l-4-3V1019.11z"/> </g> </svg> </span>
                            <?php endif ?>
                            <p class="ener__latest-stuff__list__item__episode"><?php echo $release->title(); ?></p>
                            <p class="ener__latest-stuff__list__item__program"><?php echo $release->parentprogram(); ?> <span>–</span> <?php 
                                        $file_int = $release->audiofile()->toFile();
                                        if (page($release) && $release->audiofile()->isnotEmpty() && $release->audiofile()->toFile()) {
                                            $audio_file = $release->audiofile()->toFile();
                                            require_once('assets/php/getid3/getid3.php');
                                            $getID3 = new getID3;
                                            $file_duration = $getID3->analyze($audio_file->mediaRoot());
                                            $audio_file_playtime = floor(@$file_duration['playtime_seconds'] / 60); // playtime in minutes:seconds, formatted string
                                            echo '<time datetime="' . $audio_file_playtime . '">' . $audio_file_playtime . ' min</time>';
                                        }
                                     ?><span class=""> /<?php if($file_int): ?> <?php echo $file_int->extension(); ?> / <?php echo floor(($file_int->size() / 1024) / 1000); ?> MB<?php endif ?></span></p>
                        </span>
                    </a>
                </li>
                <!-- ITEM -->
                <?php endforeach ?>
                <?php if(false): ?>
                <div class="is-row is-flex">
                        <?php snippet('modules/card.program', ['program' => $latest_release, 'size' => 's8', 'continue' => true]) ?>
                </div>
                <?php endif ?>
        </ul>
</div>
<!-- LATEST STUFF MODULE -->