<?php 
    // fetch the basic set of pages
    $original_programs = page('programas')->children()->listed()->filterBy('original', true)->shuffle()->limit(3);
    $size = [];
    $size[0] = '3';
    $size[1] = '5 offset-_5';
    $size[2] = '2_5 offset-1';
    $data_scroll = [];
    $data_scroll[0] = ' data-scroll data-scroll-speed="1"';
    $data_scroll[1] = ' data-scroll data-scroll-speed="-1.75" data-scroll-offset="-500,0"';
    $data_scroll[2] = ' data-scroll data-scroll-speed="2" data-scroll-offset="-500,0"';
    $data_decoration = [];
    $data_decoration[0] = false;
    $data_decoration[1] = false;
    $data_decoration[2] = false;
    $i = 0;
 ?>
<!-- ESTO NO ES RADIO ORIGINALS -->
<section class="cover__featured grid-margin<?php echo isset($padding) && $padding == false ? ' cover__featured--short' : '';?>">
    <div class="is-row is-flex column-padding">
        <div class="is-col offset-1 col-10">
            <h3>Programas Originales</h3>
            <div class="is-row is-flex">
                <?php foreach ($original_programs as $program): ?>
                  <?php snippet('modules/card.program', ['program' => $program, 'size' => $size[$i], 'data_scroll' => $data_scroll[$i], 'extra_decoration' => $data_decoration[$i] ]) ?>
                  <?php $i++; ?>
                <?php endforeach ?>
            </div>
            <div class="is-row is-flex ener__centered-button-block">
                <div class="is-col offset-3_5 col-5">
                    <a href="<?php echo page('programas')->url(); ?>" class="main-cta main-cta--dark section-link" data-uri="<?php echo page('programas')->uri(); ?>" data-title="<?php echo page('programas')->title(); ?>" data-text="Ve Nuestro Catálogo"><span>Ve Nuestro Catálogo</span></a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ESTO NO ES RADIO ORIGINALS -->