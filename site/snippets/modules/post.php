<?php 
    /*****
    *****
    Post Module Snippet

    Needed Parameters:
    
    $page_checked -> Post page to get.
    $size -> Post size for columns and offsets.
    $offset -> Post left offset in columns.

    *****
    *****/
    if (isset($size)) {
		switch ($size) {case 's1': $offset = '1'; break; case 's2': $size = '2'; break; case 's3': $size = '3'; break; case 's4': $size = '4'; break; case 's5': $size = '5'; break; case 's6': $size = '6'; break; case 's7': $size = '7'; break; case 's8': $size = '8'; break; case 's9': $size = '9'; break; case 's10': $size = '10'; break; case 's11': $size = '11'; break; default: $size = $size; break;
		}
    }
	if (isset($offset)) {
		switch ($offset) {case 's1': $offset = '1'; break;case 's2': $offset = '2'; break; case 's3': $offset = '3'; break; case 's4': $offset = '4'; break; case 's5': $offset = '5'; break; case 's6': $offset = '6'; break; case 's7': $offset = '7'; break; case 's8': $offset = '8'; break; case 's9': $offset = '9'; break; case 's10': $offset = '10'; break; case 's11': $offset = '11'; break; default: $offset = $offset; break;
		}
    }
    $title = $page_checked->title();
    $social_url = $page_checked->external();
    $author = '';
    $profile = '';
    $avatar = '';
    $date = $page_checked->date();
    $cover = $page_checked->cover()->toFile();
    foreach ($page_checked->author()->toStructure() as $current_author) {
    	$author = $current_author->title();
    	$profile = $current_author->link();
    	if ($current_author->avatar()->isNotEmpty()) {
    		$avatar = $current_author->avatar()->toFile();
    	} else{
    		$avatar = false;
    	}
    }
    setlocale(LC_TIME, 'es_MX.utf8');
?>
<!-- POST -->
	<div class="is-col is-col--special-gap col-<?php echo isset($size) ? $size : ''; ?> offset-<?php echo isset($offset) ? $offset : ''; ?>">
		<?php if($page_checked->posttype() == 'tweet'): ?>
		<article class="ener__latest-releases__post ener__latest-releases__post--tweet">
			<a href="<?php echo $social_url; ?>" class="ener__button ener__button--line ener__button--margin ener__button--margin-bottom external-link" target="_blank">
	    	    <svg class="ener__button__svg" x="0px" y="0px" width="48px"
	    	         height="48px" viewBox="0 0 48 48" style="overflow:visible;enable-background:new 0 0 48 48;" xml:space="preserve">
	    	        <circle class="ener__button__svg__transparent-bg__bg" cx="24" cy="24" r="24"/>
	    	        <circle class="ener__button__svg__transparent-bg__border" cx="24" cy="24" r="23"/>
	    	        <circle class="ener__button__svg__white-bg" cx="24" cy="24" r="24"/>
	    	        <path class="ener__button__svg__icon" d="M33.85,17.89c-0.74,0.32-1.52,0.54-2.33,0.64c0.85-0.5,1.48-1.3,1.78-2.23c-0.79,0.47-1.66,0.8-2.56,0.98 c-1.52-1.63-4.08-1.71-5.71-0.18c-0.81,0.76-1.28,1.83-1.28,2.94c0,0.31,0.03,0.62,0.09,0.92c-3.25-0.16-6.28-1.69-8.32-4.22 c-1.08,1.84-0.54,4.21,1.24,5.4c-0.64-0.02-1.26-0.19-1.82-0.5v0.04c0,1.92,1.35,3.58,3.23,3.97c-0.35,0.09-0.7,0.14-1.06,0.13 c-0.26,0-0.51-0.02-0.77-0.07c0.53,1.64,2.05,2.77,3.77,2.81c-1.43,1.12-3.19,1.72-5.01,1.72c-0.32,0-0.65-0.02-0.97-0.06 c5.35,3.42,12.45,1.86,15.87-3.49c1.18-1.85,1.81-4,1.81-6.19c0-0.18,0-0.35-0.02-0.52C32.62,19.42,33.31,18.71,33.85,17.89z"/>
    	        </svg>
	    	</a>
	    	<a href="<?php echo $social_url; ?>" class="ener__latest-releases__post__anchor external-link" target="_blank">
	    		<span class="ener__latest-releases__post__date ener__season-tag"><?php echo strftime('%B %e, %Y', strtotime($date->toDate('d-m-Y'))); ?></span>
	    		<?php if($cover && $cover!=''): ?>
	    		<figure class="ener__latest-releases__post__figure" style="padding-top: <?php echo ($cover->height()/$cover->width())*100;?>%">
    			    <picture class="js-lazy-image --special-transform js-can-use-webp" style="display: block; min-height: 1rem;" data-alt="<?php echo $title; ?>"<?php echo $page_checked->image($cover->name().'.webp') ? ' data-webp="'.$page_checked->image($cover->name().'.webp')->url() .'"': ''; ?>>
    			        <source srcset="<?php echo $cover->url(); ?>">
    			    </picture>
	    		</figure>
	    		<?php endif ?>
	    		<div class="ener__latest-releases__post__container">
	    			<?php echo $page_checked->description()->toBlocks(); ?>
	    		</div>
	    		<ul class="ener__latest-releases__post__list">
	    			<?php if($avatar): ?>
	    			<li class="ener__latest-releases__post__list__item ener__latest-releases__post__list__item--avatar">
	    				<picture class="js-lazy-image js-can-use-webp" data-alt="<?php echo $author; ?>"<?php echo $page_checked->image($avatar->name().'.webp') ? ' data-webp="'.$page_checked->image($avatar->name().'.webp')->url() .'"': ''; ?><?php echo $avatar ? 'style="padding-top:' . ($avatar->height() / $avatar->width()) * 100 . '%;"' : '';?>>
	    				    <source srcset="<?php echo $avatar->url(); ?>">
	    				</picture>
	    			</li>
	    			<?php else: ?>
	    			<li class="ener__latest-releases__post__list__item ener__latest-releases__post__list__item--avatar">
	    				<picture class="js-lazy-image js-can-use-webp" data-alt="Esto no es radio" data-webp="<?php echo $site->url(); ?>/assets/images/twitter_avatar.webp" data-scroll data-scroll-speed="-1.25" data-scroll-offset="-500,0">
	    				    <source srcset="<?php echo $site->url(); ?>/assets/images/twitter_avatar.png">
	    				</picture>
	    			</li>
	    			<?php endif ?>
	    			<li class="ener__latest-releases__post__list__item ener__latest-releases__post__list__item--info">
			    		<p class="ener__latest-releases__post__author"><strong><?php echo $author; ?></strong></p>
			    		<?php echo $profile !='' ? '<p class="ener__latest-releases__post__profile">@'.$profile.'</p>':''; ?>
	    			</li>
	    		</ul>
	    	</a>
		</article>
		<?php endif ?>
		<?php if($page_checked->posttype() == 'instagram'): ?>
		<article class="ener__latest-releases__post ener__latest-releases__post--instagram">
			<a href="<?php echo $social_url; ?>" class="ener__button ener__button--line ener__button--margin ener__button--margin-bottom external-link" target="_blank">
	    	    <svg class="ener__button__svg" x="0px" y="0px" width="48px"
	    	         height="48px" viewBox="0 0 48 48" style="overflow:visible;enable-background:new 0 0 48 48;" xml:space="preserve">
	    	        <circle class="ener__button__svg__transparent-bg__bg" cx="24" cy="24" r="24"/>
	    	        <circle class="ener__button__svg__transparent-bg__border" cx="24" cy="24" r="23"/>
	    	        <circle class="ener__button__svg__white-bg" cx="24" cy="24" r="24"/>
	    	        <g class="ener__button__svg__icon" transform="translate(-12 2)"> <path d="M43.95,18.7c-0.01-0.66-0.14-1.32-0.37-1.94c-0.42-1.08-1.27-1.92-2.34-2.34 c-0.62-0.23-1.28-0.36-1.94-0.37C38.44,14.01,38.17,14,36,14s-2.44,0.01-3.3,0.05c-0.66,0.01-1.32,0.14-1.94,0.37 c-1.07,0.42-1.92,1.27-2.34,2.34c-0.23,0.62-0.36,1.28-0.37,1.94C28.01,19.55,28,19.83,28,22s0.01,2.45,0.05,3.3 c0.01,0.66,0.14,1.32,0.37,1.94c0.42,1.08,1.27,1.93,2.34,2.34c0.62,0.23,1.28,0.36,1.94,0.37C33.55,29.99,33.83,30,36,30 s2.44-0.01,3.3-0.05c0.66-0.01,1.32-0.14,1.94-0.37c1.08-0.42,1.92-1.27,2.34-2.34c0.23-0.62,0.36-1.28,0.37-1.94 C43.99,24.45,44,24.17,44,22S43.99,19.56,43.95,18.7z M42.51,25.24c0,0.51-0.1,1.01-0.27,1.48c-0.27,0.7-0.82,1.25-1.52,1.52 c-0.48,0.18-0.98,0.27-1.48,0.27c-0.84,0.04-1.1,0.05-3.23,0.05s-2.39-0.01-3.23-0.05c-0.51,0-1.01-0.1-1.48-0.27 c-0.7-0.27-1.25-0.82-1.52-1.52c-0.18-0.48-0.27-0.98-0.27-1.48c-0.04-0.84-0.05-1.1-0.05-3.23s0.01-2.39,0.05-3.23 c0-0.51,0.1-1.01,0.27-1.48c0.13-0.35,0.33-0.66,0.6-0.92c0.26-0.27,0.57-0.47,0.92-0.6c0.48-0.18,0.98-0.27,1.48-0.27 c0.84-0.04,1.1-0.05,3.23-0.05s2.39,0.01,3.23,0.05c0.51,0.01,1.01,0.1,1.48,0.27c0.35,0.13,0.66,0.33,0.92,0.6 c0.27,0.26,0.47,0.57,0.6,0.92c0.18,0.48,0.27,0.98,0.27,1.48c0.04,0.84,0.05,1.1,0.05,3.23S42.55,24.39,42.51,25.24L42.51,25.24z "/> <path d="M36,17.89c-2.27,0-4.11,1.84-4.11,4.11c0,2.27,1.84,4.11,4.11,4.11c2.27,0,4.11-1.84,4.11-4.11l0,0 C40.11,19.73,38.27,17.89,36,17.89z M36,24.67c-1.47,0-2.67-1.19-2.67-2.67s1.19-2.67,2.67-2.67s2.67,1.19,2.67,2.67l0,0 C38.66,23.47,37.47,24.66,36,24.67L36,24.67z"/> <path d="M41.23,17.73c0,0.53-0.43,0.96-0.96,0.96s-0.96-0.43-0.96-0.96s0.43-0.96,0.96-0.96l0,0l0,0 C40.8,16.77,41.23,17.2,41.23,17.73z"/> </g>
    	        </svg>
	    	</a>
	    	<a href="<?php echo $social_url; ?>" class="ener__latest-releases__post__anchor ener__latest-releases__post__anchor--instagram external-link" target="_blank">
	    		<span class="ener__latest-releases__post__date ener__season-tag"><?php echo strftime('%B %e, %Y', strtotime($date->toDate('d-m-Y'))); ?></span>
		    	<?php if($cover && $cover!=''): ?>
	    		<figure class="ener__latest-releases__post__figure" style="padding-top: <?php echo ($cover->height()/$cover->width())*100;?>%">
    			    <picture class="js-lazy-image --special-transform js-can-use-webp" style="display: block; min-height: 1rem;" data-alt="<?php echo $title; ?>"<?php echo $page_checked->image($cover->name().'.webp') ? ' data-webp="'.$page_checked->image($cover->name().'.webp')->url() .'"': ''; ?>>
    			        <source srcset="<?php echo $cover->url(); ?>">
    			    </picture>
	    		</figure>
	    		<?php endif ?>
		    	<div class="ener__latest-releases__post__content-container">
		    		<div class="ener__latest-releases__post__container">
		    			<?php echo $page_checked->description()->toBlocks(); ?>
		    		</div>
		    		<ul class="ener__latest-releases__post__list">
		    			<?php if($avatar): ?>
		    			<li class="ener__latest-releases__post__list__item ener__latest-releases__post__list__item--avatar">
		    				<picture class="js-lazy-image js-can-use-webp" data-alt="<?php echo $author; ?>"<?php echo $page_checked->image($avatar->name().'.webp') ? ' data-webp="'.$page_checked->image($avatar->name().'.webp')->url() .'"': ''; ?><?php echo $avatar ? 'style="padding-top:' . ($avatar->height() / $avatar->width()) * 100 . '%;"' : '';?>>
		    				    <source srcset="<?php echo $avatar->url(); ?>">
		    				</picture>
		    			</li>
		    			<?php else: ?>
		    			<li class="ener__latest-releases__post__list__item ener__latest-releases__post__list__item--avatar">
		    				<picture class="js-lazy-image js-can-use-webp" data-alt="Esto no es radio" data-webp="<?php echo $site->url(); ?>/assets/images/instagram_avatar.webp" data-scroll data-scroll-speed="-1.25" data-scroll-offset="-500,0">
		    				    <source srcset="<?php echo $site->url(); ?>/assets/images/instagram_avatar.png">
		    				</picture>
		    			</li>
		    			<?php endif ?>
		    			<li class="ener__latest-releases__post__list__item ener__latest-releases__post__list__item--info">
				    		<p class="ener__latest-releases__post__author"><strong><?php echo $author; ?></strong></p>
				    		<?php echo $profile !='' ? '<p class="ener__latest-releases__post__profile">@'.$profile.'</p>':''; ?>
		    			</li>
		    		</ul>
		    	</div>
	    	</a>
		</article>
		<?php endif ?>
		<?php if($page_checked->posttype() == 'news'): ?>
		<article class="ener__latest-releases__post ener__latest-releases__post--news">
	    	<a href="<?php echo $social_url; ?>" class="ener__latest-releases__post__anchor ener__latest-releases__post__anchor--instagram external-link" target="_blank">
	    		<span class="ener__latest-releases__post__date ener__season-tag"><?php echo strftime('%B %e, %Y', strtotime($date->toDate('d-m-Y'))); ?></span>
		    	<?php if($cover && $cover!=''): ?>
	    		<figure class="ener__latest-releases__post__figure" style="padding-top: <?php echo ($cover->height()/$cover->width())*100;?>%">
    			    <picture class="js-lazy-image --special-transform js-can-use-webp" style="display: block; min-height: 1rem;" data-alt="<?php echo $title; ?>"<?php echo $page_checked->image($cover->name().'.webp') ? ' data-webp="'.$page_checked->image($cover->name().'.webp')->url() .'"': ''; ?>>
    			        <source srcset="<?php echo $cover->url(); ?>">
    			    </picture>
	    		</figure>
	    		<?php endif ?>
	    		<div class="ener__latest-releases__post__content-container">
		    		<div class="ener__latest-releases__post__container">
		    			<?php echo $page_checked->description()->toBlocks(); ?>
		    		</div>
		    		<?php if($author && $author!=''): ?>
		    		<ul class="ener__latest-releases__post__list">
		    			<?php if($avatar): ?>
		    			<li class="ener__latest-releases__post__list__item ener__latest-releases__post__list__item--avatar">
		    				<picture class="js-lazy-image js-can-use-webp" data-alt="<?php echo $author; ?>"<?php echo $page_checked->image($avatar->name().'.webp') ? ' data-webp="'.$page_checked->image($avatar->name().'.webp')->url() .'"': ''; ?><?php echo $avatar ? 'style="padding-top:' . ($avatar->height() / $avatar->width()) * 100 . '%;"' : '';?>>
		    				    <source srcset="<?php echo $avatar->url(); ?>">
		    				</picture>
		    			</li>
		    			<?php else: ?>
		    			<li class="ener__latest-releases__post__list__item ener__latest-releases__post__list__item--avatar">
		    				<picture class="js-lazy-image js-can-use-webp" data-alt="Esto no es radio" data-webp="<?php echo $site->url(); ?>/assets/images/instagram_avatar.webp" data-scroll data-scroll-speed="-1.25" data-scroll-offset="-500,0">
		    				    <source srcset="<?php echo $site->url(); ?>/assets/images/instagram_avatar.png">
		    				</picture>
		    			</li>
		    			<?php endif ?>
		    			<li class="ener__latest-releases__post__list__item ener__latest-releases__post__list__item--info">
				    		<p class="ener__latest-releases__post__author"><strong><?php echo $author; ?></strong></p>
				    		<?php echo $profile !='' ? '<p class="ener__latest-releases__post__profile">'.$profile.'</p>':''; ?>
		    			</li>
		    		</ul>
		    		<?php endif ?>
	    		</div>
	    	</a>
		</article>
		<?php endif ?>
	</div>
<!-- POST -->