<?php 
    /*****
    *****
    Privacy Banner

    Needed Parameters:
    
    None

    *****
    *****/
?>
<?php if(!Cookie::get('privacy')): ?>
<aside class="project__privacy-banner" id="privacy-banner">
    <div class="project__privacy-banner__container">
        <h3>Aviso de Privacidad <?php echo Cookie::get('privacy'); ?></h3>
        <p><?php echo page('privacidad') ? 'Utilizamos <a href="'.page('privacidad')->url().'" data-uri="'.page('privacidad')->uri().'" data-title="'.page('privacidad')->title().'">cookies y otras tecnologías</a> para mejorar tu experiencia en nuestro sitio.' : 'Utilizamos cookies y otras tecnologías para mejorar tu experiencia en nuestro sitio.'; ?></p>
        <button class="close-privacy-banner" data-action="true" data-text="De acuerdo"><span>De acuerdo</span></button>
    </div>
</aside>
<?php endif ?>