<?php if(page('acerca-de')->tour()->isNotEmpty()): ?>
<section class="ener__product-tour<?php echo isset($short) && $short == true ? ' ener__product-tour--short':''?>">
    <div class="container grid-margin">
        <div class="is-row is-flex column-padding">
            <div class="is-col col-6 offset-1">
                <h3>Tour de Plataforma</h3>
            </div>
        </div>
        <?php # /site/templates/yourtemplate.php
        foreach(page('acerca-de')->tour()->toBlocks() as $block):
          snippet('blocks/' . $block->_key(), array('data' => $block));
        endforeach;
        ?>
    </div>
</section>
<?php endif ?>