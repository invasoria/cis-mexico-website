<?php if (isset($items)): ?>
<?php 
    /*****
    *****
    Services Block Snippet

    Needed Parameters:
    
    $items -> Post page to get.
    $number -> Footnote ID

    *****
    *****/
    //$url = $page_checked->url();
    // /$uri = $page_checked->uri();
    // /$title = $page_checked->title();
    // /$cover = $page_checked->cover();
    
    setlocale(LC_TIME, 'es_MX.utf8');
?>
<!-- SERVICES LIST -->
<section class="cis__section cis__section__services">
    <div class="cis__section__container --padding-160">
        <div class="is-row is-flex column-padding">
            <div class="is-col offset-_5 col-6_5 cis__special-text cis__special-text--purple">
                <h4 class="js-observe fade-and-slide-animation">Nuestros Servicios</h4>
                <h1 class="js-observe reveal-text-animation">Estamos potenciados por nuestra propia <strong>humanidad</strong>.</h1>
            </div>
        </div>
        <div class="item-64"></div>
        <div class="is-row is-row--responsive is-flex column-padding relative">
            <div class="is-col offset-_5 col-11 relative">
                <?php 
                    $services = page('servicios')->children()->listed()->children()->listed();
                    $i = 1;
                 ?>
                <?php foreach ($services as $service): ?>
                    <!-- SERVICE -->
                    <a href="<?php echo $service->url(); ?>" class="cis__section__services__service js-observe fade-and-slide-animation section-link" data-uri="<?php echo page('servicios')->uri(); ?>" data-title="<?php echo page('servicios')->title(); ?>" data-image-link="<?php echo $i++; ?>">
                        <ul class="cis__section__services__service__list">
                            <li class="cis__section__services__service__left cis__section__services__service__list__item">
                                <p class="cis__section__services__service--text"><?php echo $service->title(); ?></p>
                                <p class="cis__section__services__service__list__item--small-text cis__section__services__service--text"><?php echo $service->subtitle(); ?></p>
                            </li>
                            <li class="cis__section__services__service__right">
                                <!-- <p>Ver Servicio</p> -->
                                <div class="cis__arrow-cta--compressed">
                                    <span class="cis__arrow-cta__arrow">
                                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="19.81px"height="15.41px" viewBox="0 0 19.81 15.41" style="overflow:visible;enable-background:new 0 0 19.81 15.41;"xml:space="preserve"><path class="cis__arrow__st0" d="M9.96,2.19l3.01,3.01l1.66,0.8H0v3.33h14.55L13,10.1l-3.06,3.05l2.11,2.26l7.76-7.74 L12.15,0L9.96,2.19z"/> </svg>
                                    </span>
                                    <span class="cis__arrow-cta__ellipse">
                                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="80px"height="80px" viewBox="0 0 80 80" style="overflow:visible;enable-background:new 0 0 80 80;" xml:space="preserve"> <defs> </defs><circle class="cis__arrow-cta__ellipse__st0" cx="40" cy="40" r="40"/> <circle class="cis__arrow-cta__ellipse__st1" cx="40" cy="40" r="39"/> </svg>
                                    </span>
                                </div>
                            </li>
                        </ul>
                        <span class="cis__section__services__service__line"></span>
                    </a>
                    <!-- SERVICE -->
                <?php endforeach ?>

                
                <?php if($services->count() > 0): ?>
                <div class="is-row is-row--responsive is-flex cis__section__services__service__image">
                    <div class="is-col offset-5 col-5 xl-col-5 xl-offset-5">
                        <?php if($image_1 = $page->image('1.jpg')): ?>
                        <figure class="--figure js-service-figure" id="service-1"<?php echo $image_1 ? ' style="padding-top:' . ($image_1->height() / $image_1->width()) * 100 . '%;"' : '';?>>
                            <?php //if($image_1 = $page->main_image()->toFile()): ?>
                            <picture class="js-lazy-service-image --special-transform js-can-use-webp" data-alt="CIS México"<?php echo page('cover')->image($image_1->name().'.webp') ? ' data-webp="'.page('cover')->image($image_1->name().'.webp')->url() .'"': ''; ?><?php echo $image_1 ? ' style="padding-top:' . ($image_1->height() / $image_1->width()) * 100 . '%;"' : '';?>>
                                <source srcset="<?php echo $image_1->url(); ?>">
                            </picture>    
                        </figure>
                        <?php endif ?>
                        <?php if($image_1 = $page->image('2.jpg')): ?>
                        <figure class="--figure js-service-figure" id="service-2"<?php echo $image_1 ? ' style="padding-top:' . ($image_1->height() / $image_1->width()) * 100 . '%;"' : '';?>>
                            <?php //if($image_1 = $page->main_image()->toFile()): ?>
                            <picture class="js-lazy-service-image --special-transform js-can-use-webp" data-alt="CIS México"<?php echo page('cover')->image($image_1->name().'.webp') ? ' data-webp="'.page('cover')->image($image_1->name().'.webp')->url() .'"': ''; ?><?php echo $image_1 ? ' style="padding-top:' . ($image_1->height() / $image_1->width()) * 100 . '%;"' : '';?>>
                                <source srcset="<?php echo $image_1->url(); ?>">
                            </picture>    
                        </figure>
                        <?php endif ?>
                        <?php if($image_1 = $page->image('3.jpg')): ?>
                        <figure class="--figure js-service-figure" id="service-3"<?php echo $image_1 ? ' style="padding-top:' . ($image_1->height() / $image_1->width()) * 100 . '%;"' : '';?>>
                            <?php //if($image_1 = $page->main_image()->toFile()): ?>
                            <picture class="js-lazy-service-image --special-transform js-can-use-webp" data-alt="CIS México"<?php echo page('cover')->image($image_1->name().'.webp') ? ' data-webp="'.page('cover')->image($image_1->name().'.webp')->url() .'"': ''; ?><?php echo $image_1 ? ' style="padding-top:' . ($image_1->height() / $image_1->width()) * 100 . '%;"' : '';?>>
                                <source srcset="<?php echo $image_1->url(); ?>">
                            </picture>    
                        </figure>
                        <?php endif ?>
                        <?php if($image_1 = $page->image('4.jpg')): ?>
                        <figure class="--figure js-service-figure" id="service-4"<?php echo $image_1 ? ' style="padding-top:' . ($image_1->height() / $image_1->width()) * 100 . '%;"' : '';?>>
                            <?php //if($image_1 = $page->main_image()->toFile()): ?>
                            <picture class="js-lazy-service-image --special-transform js-can-use-webp" data-alt="CIS México"<?php echo page('cover')->image($image_1->name().'.webp') ? ' data-webp="'.page('cover')->image($image_1->name().'.webp')->url() .'"': ''; ?><?php echo $image_1 ? ' style="padding-top:' . ($image_1->height() / $image_1->width()) * 100 . '%;"' : '';?>>
                                <source srcset="<?php echo $image_1->url(); ?>">
                            </picture>    
                        </figure>
                        <?php endif ?>
                        <?php if($image_1 = $page->image('5.jpg')): ?>
                        <figure class="--figure js-service-figure" id="service-5"<?php echo $image_1 ? ' style="padding-top:' . ($image_1->height() / $image_1->width()) * 100 . '%;"' : '';?>>
                            <?php //if($image_1 = $page->main_image()->toFile()): ?>
                            <picture class="js-lazy-service-image --special-transform js-can-use-webp" data-alt="CIS México"<?php echo page('cover')->image($image_1->name().'.webp') ? ' data-webp="'.page('cover')->image($image_1->name().'.webp')->url() .'"': ''; ?><?php echo $image_1 ? ' style="padding-top:' . ($image_1->height() / $image_1->width()) * 100 . '%;"' : '';?>>
                                <source srcset="<?php echo $image_1->url(); ?>">
                            </picture>    
                        </figure>
                        <?php endif ?>
                        <?php if($image_1 = $page->image('6.jpg')): ?>
                        <figure class="--figure js-service-figure" id="service-6"<?php echo $image_1 ? ' style="padding-top:' . ($image_1->height() / $image_1->width()) * 100 . '%;"' : '';?>>
                            <?php //if($image_1 = $page->main_image()->toFile()): ?>
                            <picture class="js-lazy-service-image --special-transform js-can-use-webp" data-alt="CIS México"<?php echo page('cover')->image($image_1->name().'.webp') ? ' data-webp="'.page('cover')->image($image_1->name().'.webp')->url() .'"': ''; ?><?php echo $image_1 ? ' style="padding-top:' . ($image_1->height() / $image_1->width()) * 100 . '%;"' : '';?>>
                                <source srcset="<?php echo $image_1->url(); ?>">
                            </picture>    
                        </figure>
                        <?php endif ?>
                        <?php if($image_1 = $page->image('7.jpg')): ?>
                        <figure class="--figure js-service-figure" id="service-7"<?php echo $image_1 ? ' style="padding-top:' . ($image_1->height() / $image_1->width()) * 100 . '%;"' : '';?>>
                            <?php //if($image_1 = $page->main_image()->toFile()): ?>
                            <picture class="js-lazy-service-image --special-transform js-can-use-webp" data-alt="CIS México"<?php echo page('cover')->image($image_1->name().'.webp') ? ' data-webp="'.page('cover')->image($image_1->name().'.webp')->url() .'"': ''; ?><?php echo $image_1 ? ' style="padding-top:' . ($image_1->height() / $image_1->width()) * 100 . '%;"' : '';?>>
                                <source srcset="<?php echo $image_1->url(); ?>">
                            </picture>    
                        </figure>
                        <?php endif ?>
                        <?php if($image_1 = $page->image('1.jpg')): ?>
                        <figure class="--figure js-service-figure" id="service-8"<?php echo $image_1 ? ' style="padding-top:' . ($image_1->height() / $image_1->width()) * 100 . '%;"' : '';?>>
                            <?php //if($image_1 = $page->main_image()->toFile()): ?>
                            <picture class="js-lazy-service-image --special-transform js-can-use-webp" data-alt="CIS México"<?php echo page('cover')->image($image_1->name().'.webp') ? ' data-webp="'.page('cover')->image($image_1->name().'.webp')->url() .'"': ''; ?><?php echo $image_1 ? ' style="padding-top:' . ($image_1->height() / $image_1->width()) * 100 . '%;"' : '';?>>
                                <source srcset="<?php echo $image_1->url(); ?>">
                            </picture>    
                        </figure>
                        <?php endif ?>
                        <?php if($image_1 = $page->image('2.jpg')): ?>
                        <figure class="--figure js-service-figure" id="service-9"<?php echo $image_1 ? ' style="padding-top:' . ($image_1->height() / $image_1->width()) * 100 . '%;"' : '';?>>
                            <?php //if($image_1 = $page->main_image()->toFile()): ?>
                            <picture class="js-lazy-service-image --special-transform js-can-use-webp" data-alt="CIS México"<?php echo page('cover')->image($image_1->name().'.webp') ? ' data-webp="'.page('cover')->image($image_1->name().'.webp')->url() .'"': ''; ?><?php echo $image_1 ? ' style="padding-top:' . ($image_1->height() / $image_1->width()) * 100 . '%;"' : '';?>>
                                <source srcset="<?php echo $image_1->url(); ?>">
                            </picture>    
                        </figure>
                        <?php endif ?>
                        <?php if($image_1 = $page->image('3.jpg')): ?>
                        <figure class="--figure js-service-figure" id="service-10"<?php echo $image_1 ? ' style="padding-top:' . ($image_1->height() / $image_1->width()) * 100 . '%;"' : '';?>>
                            <?php //if($image_1 = $page->main_image()->toFile()): ?>
                            <picture class="js-lazy-service-image --special-transform js-can-use-webp" data-alt="CIS México"<?php echo page('cover')->image($image_1->name().'.webp') ? ' data-webp="'.page('cover')->image($image_1->name().'.webp')->url() .'"': ''; ?><?php echo $image_1 ? ' style="padding-top:' . ($image_1->height() / $image_1->width()) * 100 . '%;"' : '';?>>
                                <source srcset="<?php echo $image_1->url(); ?>">
                            </picture>    
                        </figure>
                        <?php endif ?>
                    </div>
                </div>
                <?php endif ?>
                
            </div>
        </div>
        <div class="is-row is-flex column-padding">
            <div class="is-col offset-_5 col-11 --padding-80 centered-content">
                <!-- SECTION LINK -->
                <a href="<?php echo page('servicios')->url(); ?>" class="js-observe fade-and-slide-animation cis__arrow-cta cis__arrow-cta--blue section-link" data-uri="<?php echo page('servicios')->uri(); ?>" data-title="<?php echo page('servicios')->title(); ?>" data-text="Conoce más de nuestros Servicios">
                    <span class="cis__arrow-cta__deco-bar"></span>
                    <span class="cis__arrow-cta__text">Conoce más de nuestros Servicios</span>
                    <span class="cis__arrow-cta__arrow">
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="19.81px"height="15.41px" viewBox="0 0 19.81 15.41" style="overflow:visible;enable-background:new 0 0 19.81 15.41;"xml:space="preserve"><path class="cis__arrow__st0" d="M9.96,2.19l3.01,3.01l1.66,0.8H0v3.33h14.55L13,10.1l-3.06,3.05l2.11,2.26l7.76-7.74 L12.15,0L9.96,2.19z"/> </svg>
                    </span>
                    <span class="cis__arrow-cta__ellipse">
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="80px"height="80px" viewBox="0 0 80 80" style="overflow:visible;enable-background:new 0 0 80 80;" xml:space="preserve"> <defs> </defs><circle class="cis__arrow-cta__ellipse__st0" cx="40" cy="40" r="40"/> <circle class="cis__arrow-cta__ellipse__st1" cx="40" cy="40" r="39"/> </svg>
                    </span>
                </a>
                <!-- SECTION LINK -->
            </div>
        </div>
    </div>
</section>
<!-- SERVICES LIST -->
<?php endif ?>