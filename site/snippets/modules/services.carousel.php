<button class="cis__services__button open-carousel" id="services-button">
  <svg viewbox="0 0 65 65" xmlns="http://www.w3.org/2000/svg">
      <!-- CIRCLE BG -->
      <circle class="cis__services__button__circle-bg" cx="32.5" cy="32.5" r="25.6" />
      <!-- MARQUEE TEXT -->
      <g id="services-button-group" class="cis__services__button__marquee">
        <g class="cis__services__button__marquee__container">
          <path class="cis__services__button--svg-4" d="M300.15,956.84c0-.33,0-.67,0-1a30.9,30.9,0,0,1,21.28-29.41l.47,1.42a29.4,29.4,0,0,0-20.25,28c0,.32,0,.64,0,.95Z" transform="translate(-298.58 -923.34)">
          </path>
          <path class="cis__services__button--svg-1" d="M324.94,926.28a.81.81,0,0,0,1,.66c.44-.07.68-.3.63-.62s-.24-.37-.64-.38l-.59,0a.77.77,0,0,1-.86-.65c-.08-.51.26-.92.9-1a1,1,0,0,1,1.34.86l-.37.08a.73.73,0,0,0-.91-.63c-.42.07-.63.31-.57.64s.25.34.6.35l.54,0c.56,0,.86.2.94.67s-.32.92-.95,1a1.12,1.12,0,0,1-1.43-.9Z" transform="translate(-298.58 -923.34)">
          </path>
          <path class="cis__services__button--svg-1" d="M330.51,926.8l-2.11.09-.14-3,2.07-.09,0,.34-1.67.07,0,.93,1.33-.06v.34l-1.33.06,0,1,1.72-.08Z" transform="translate(-298.58 -923.34)">
          </path>
          <path class="cis__services__button--svg-1" d="M333.76,927a1.43,1.43,0,0,1-.08-.65.51.51,0,0,0-.54-.59l-.83-.06-.09,1.19-.4,0,.22-3,1.2.09c.75.06,1.09.43,1.05,1a.7.7,0,0,1-.73.68.64.64,0,0,1,.52.68c0,.44,0,.61.1.72Zm-.65-1.57c.5,0,.74-.13.77-.5s-.18-.57-.68-.61l-.78-.06-.09,1.11Z" transform="translate(-298.58 -923.34)">
          </path>
          <path class="cis__services__button--svg-1" d="M336.34,927.36l-.58-3.14.41.08.49,2.65h0l1.45-2.25.41.08-1.77,2.67Z" transform="translate(-298.58 -923.34)">
          </path>
          <path class="cis__services__button--svg-1" d="M340.37,925.33l-.88,2.83-.39-.12.87-2.83Z" transform="translate(-298.58 -923.34)">
          </path>
          <path class="cis__services__button--svg-1" d="M343.26,926.34a1.28,1.28,0,0,1,.77,1.72l-.37-.13a.94.94,0,0,0-.54-1.27,1.2,1.2,0,0,0-.93,2.19,1,1,0,0,0,1.29-.49l.35.17a1.28,1.28,0,0,1-1.77.64,1.54,1.54,0,0,1,1.2-2.83Z" transform="translate(-298.58 -923.34)">
          </path>
          <path class="cis__services__button--svg-1" d="M346.28,927.88l-1.43,2.6-.36-.2,1.43-2.6Z" transform="translate(-298.58 -923.34)">
          </path>
          <path class="cis__services__button--svg-1" d="M347.18,932a1.54,1.54,0,1,1,2.09-.43A1.44,1.44,0,0,1,347.18,932Zm1.55-2.24a1.19,1.19,0,1,0,.19,1.57A1,1,0,0,0,348.73,929.76Z" transform="translate(-298.58 -923.34)">
          </path>
          <path class="cis__services__button--svg-1" d="M350.27,932.92a.81.81,0,0,0,.11,1.21c.33.3.66.34.88.1s.14-.42-.1-.73l-.36-.48a.78.78,0,0,1,0-1.07.91.91,0,0,1,1.37.06,1,1,0,0,1,.16,1.57l-.29-.23a.72.72,0,0,0-.07-1.1c-.32-.29-.64-.3-.86-.06s-.11.42.1.7l.33.43c.33.44.37.8.06,1.15s-.93.33-1.4-.1a1.12,1.12,0,0,1-.19-1.67Z" transform="translate(-298.58 -923.34)">
          </path>
          <path class="cis__services__button--svg-1" d="M354.14,936.49l1.12,1.43-.23.18-1.12-1.43Z" transform="translate(-298.58 -923.34)">
          </path>
          <path class="cis__services__button--svg-1" d="M357,941.05a.83.83,0,0,0-.31,1.18c.21.39.5.55.79.39s.27-.34.16-.72l-.18-.57a.78.78,0,0,1,.35-1,.92.92,0,0,1,1.27.53,1,1,0,0,1-.4,1.54l-.19-.32a.73.73,0,0,0,.31-1.07c-.2-.38-.49-.5-.78-.34s-.25.35-.15.68l.16.53c.16.52.08.87-.34,1.1s-1,0-1.28-.58a1.11,1.11,0,0,1,.39-1.64Z" transform="translate(-298.58 -923.34)">
          </path>
          <path class="cis__services__button--svg-1" d="M358.23,946.5l-.76-2,2.77-1,.74,1.93-.32.12-.59-1.56-.87.33.47,1.24-.31.12-.48-1.24-.95.36.61,1.61Z" transform="translate(-298.58 -923.34)">
          </path>
          <path class="cis__services__button--svg-1" d="M359.09,949.63a1.52,1.52,0,0,1,.59-.27.52.52,0,0,0,.4-.7l-.2-.81-1.16.29-.1-.39,2.88-.71.29,1.17c.18.73-.07,1.16-.58,1.29a.72.72,0,0,1-.88-.48.63.63,0,0,1-.49.7c-.41.15-.57.2-.65.32Zm1.29-1.1c.12.49.36.66.72.57s.48-.35.36-.84l-.19-.75-1.08.26Z" transform="translate(-298.58 -923.34)">
          </path>
          <path class="cis__services__button--svg-1" d="M359.52,952.21l2.8-1.53.05.41L360,952.38v0l2.59.68,0,.41-3.08-.85Z" transform="translate(-298.58 -923.34)">
          </path>
          <path class="cis__services__button--svg-1" d="M362.7,955.41l-3,.06v-.41l3-.06Z" transform="translate(-298.58 -923.34)">
          </path>
          <path class="cis__services__button--svg-1" d="M362.66,958.45a1.29,1.29,0,0,1-1.4,1.28l0-.39a1,1,0,0,0,1-.91,1.21,1.21,0,0,0-2.38-.19,1,1,0,0,0,.88,1.06l-.05.39a1.29,1.29,0,0,1-1.17-1.48,1.54,1.54,0,0,1,3.07.24Z" transform="translate(-298.58 -923.34)">
          </path>
          <path class="cis__services__button--svg-1" d="M362.15,961.81l-2.92-.54.07-.4,2.92.53Z" transform="translate(-298.58 -923.34)">
          </path>
          <path class="cis__services__button--svg-1" d="M358.52,964a1.44,1.44,0,0,1,1.89-1,1.47,1.47,0,1,1-.82,2.82A1.44,1.44,0,0,1,358.52,964Zm2.62.76a1.2,1.2,0,1,0-1.44.68A1.05,1.05,0,0,0,361.14,964.73Z" transform="translate(-298.58 -923.34)">
          </path>
          <path class="cis__services__button--svg-1" d="M358.62,967.18a.82.82,0,0,0-1.11.49c-.18.41-.12.74.18.87s.44,0,.66-.33l.34-.49a.8.8,0,0,1,1-.36.92.92,0,0,1,.37,1.32,1,1,0,0,1-1.45.65l.13-.35a.73.73,0,0,0,1-.42c.17-.39.08-.69-.22-.83s-.43,0-.63.31l-.31.45c-.32.45-.64.61-1.08.42s-.59-.78-.34-1.37a1.12,1.12,0,0,1,1.53-.7Z" transform="translate(-298.58 -923.34)">
          </path>
          <path class="cis__services__button--svg-1" d="M356.44,972l-1,1.5-.24-.16,1-1.5Z" transform="translate(-298.58 -923.34)">
          </path>
          <path class="cis__services__button--svg-1" d="M353,976.09a.81.81,0,0,0-1.21.08c-.3.31-.36.65-.13.87s.42.15.74-.07l.49-.35a.79.79,0,0,1,1.08,0,.93.93,0,0,1-.11,1.37,1,1,0,0,1-1.58.11l.24-.28a.73.73,0,0,0,1.11-.05c.29-.31.32-.62.08-.85s-.41-.13-.7.08l-.45.31c-.45.32-.81.35-1.15,0s-.29-.93.15-1.4a1.12,1.12,0,0,1,1.68-.13Z" transform="translate(-298.58 -923.34)">
          </path>
          <path class="cis__services__button--svg-1" d="M348.22,979l1.64-1.34,1.87,2.29-1.6,1.31-.21-.26,1.29-1.06-.59-.72-1,.84-.21-.25,1-.85-.64-.79-1.33,1.09Z" transform="translate(-298.58 -923.34)">
          </path>
          <path class="cis__services__button--svg-1" d="M345.52,980.8a1.74,1.74,0,0,1,.45.48.52.52,0,0,0,.78.16l.71-.45-.64-1,.34-.22,1.58,2.51-1,.64c-.63.4-1.12.3-1.41-.14a.72.72,0,0,1,.18-1,.64.64,0,0,1-.82-.23c-.26-.35-.37-.49-.51-.52Zm1.45.88c-.42.27-.51.55-.32.86s.49.35.92.08l.65-.42-.59-.94Z" transform="translate(-298.58 -923.34)">
          </path>
          <path class="cis__services__button--svg-1" d="M343.21,982l2.33,2.18-.37.18-2-1.84h0l.18,2.67-.38.18-.17-3.19Z" transform="translate(-298.58 -923.34)">
          </path>
          <path class="cis__services__button--svg-1" d="M341.19,986.05l-1-2.79.39-.14,1,2.79Z" transform="translate(-298.58 -923.34)">
          </path>
          <path class="cis__services__button--svg-1" d="M338.26,987a1.29,1.29,0,0,1-1.65-.92l.38-.11a1,1,0,0,0,1.19.7,1.2,1.2,0,0,0-.56-2.32.94.94,0,0,0-.74,1.16l-.39.08a1.3,1.3,0,0,1,1.05-1.58,1.54,1.54,0,0,1,.72,3Z" transform="translate(-298.58 -923.34)">
          </path>
          <path class="cis__services__button--svg-1" d="M334.91,987.53l-.4-2.94.4-.05.4,2.93Z" transform="translate(-298.58 -923.34)">
          </path>
          <path class="cis__services__button--svg-1" d="M331.73,984.76a1.54,1.54,0,1,1-1.42,1.59A1.44,1.44,0,0,1,331.73,984.76Zm.1,2.73a1.2,1.2,0,1,0-1.09-1.15A1.06,1.06,0,0,0,331.83,987.49Z" transform="translate(-298.58 -923.34)">
          </path>
          <path class="cis__services__button--svg-1" d="M328.72,985.86a.83.83,0,0,0-.82-.9c-.44,0-.73.12-.76.45s.14.42.51.53l.58.17c.43.12.71.38.66.84s-.49.83-1.13.77a1,1,0,0,1-1.07-1.17l.37,0a.71.71,0,0,0,.72.83c.43.05.69-.13.72-.46s-.16-.4-.5-.5l-.52-.15c-.53-.16-.78-.42-.73-.89s.55-.81,1.18-.75a1.1,1.1,0,0,1,1.15,1.22Z" transform="translate(-298.58 -923.34)">
          </path>
          <path class="cis__services__button--svg-1" d="M323.48,985.32l-1.75-.49.08-.27,1.75.48Z" transform="translate(-298.58 -923.34)">
          </path>
          <path class="cis__services__button--svg-1" d="M318.49,983.34a.82.82,0,0,0-.45-1.13c-.4-.19-.73-.14-.87.15s0,.44.3.68l.48.36a.77.77,0,0,1,.33,1,.92.92,0,0,1-1.33.33,1,1,0,0,1-.6-1.47l.34.14a.73.73,0,0,0,.39,1c.39.19.69.11.83-.19s0-.43-.29-.64l-.43-.33c-.45-.32-.59-.65-.39-1.08s.8-.57,1.38-.3a1.11,1.11,0,0,1,.65,1.55Z" transform="translate(-298.58 -923.34)">
          </path>
          <path class="cis__services__button--svg-1" d="M314.23,979.71l1.79,1.14-1.6,2.5-1.74-1.11.18-.29,1.41.9.5-.78-1.12-.72.18-.28,1.12.71.55-.86-1.45-.92Z" transform="translate(-298.58 -923.34)">
          </path>
          <path class="cis__services__button--svg-1" d="M311.67,977.71a1.8,1.8,0,0,1-.32.57.53.53,0,0,0,.1.8l.64.53.76-.93.31.26-1.88,2.29-.93-.77c-.58-.48-.64-1-.31-1.38a.71.71,0,0,1,1-.14.65.65,0,0,1,0-.86c.25-.36.35-.5.34-.64Zm-.38,1.65c-.39-.32-.69-.32-.92,0s-.18.57.21.89l.6.5.71-.86Z" transform="translate(-298.58 -923.34)">
          </path>
          <path class="cis__services__button--svg-1" d="M309.78,975.89l-1.33,2.9-.29-.3,1.12-2.44v0l-2.48,1-.29-.3,3-1.17Z" transform="translate(-298.58 -923.34)">
          </path>
          <path class="cis__services__button--svg-1" d="M305.32,975.23l2.34-1.81.26.32-2.34,1.82Z" transform="translate(-298.58 -923.34)">
          </path>
          <path class="cis__services__button--svg-1" d="M303.53,972.75a1.29,1.29,0,0,1,.36-1.85l.23.32a1,1,0,0,0-.29,1.35,1.2,1.2,0,0,0,2-1.26.94.94,0,0,0-1.33-.34l-.2-.34a1.29,1.29,0,0,1,1.83.49,1.55,1.55,0,0,1-2.62,1.63Z" transform="translate(-298.58 -923.34)">
          </path>
          <path class="cis__services__button--svg-1" d="M302,969.76l2.65-1.31.19.36-2.66,1.32Z" transform="translate(-298.58 -923.34)">
          </path>
          <path class="cis__services__button--svg-1" d="M303.57,965.88a1.54,1.54,0,1,1-1.95-.85A1.43,1.43,0,0,1,303.57,965.88Zm-2.56,1a1.2,1.2,0,1,0,.76-1.4A1.06,1.06,0,0,0,301,966.83Z" transform="translate(-298.58 -923.34)">
          </path>
          <path class="cis__services__button--svg-1" d="M301.57,963.34a.82.82,0,0,0,.6-1.06c-.09-.43-.34-.66-.65-.58s-.36.26-.35.65l0,.6a.77.77,0,0,1-.59.89c-.51.12-.94-.2-1.09-.84a1,1,0,0,1,.78-1.38l.1.36a.73.73,0,0,0-.57.95c.09.42.34.61.66.53s.34-.27.33-.62l0-.55c0-.55.15-.86.61-1s.94.27,1.09.89a1.12,1.12,0,0,1-.81,1.48Z" transform="translate(-298.58 -923.34)">
          </path>
        </g>
      </g>    

      <!-- LOGO -->
      <g class="cis__services__button__logo" id="services-button-logo">
        <path class="cis__services__button--svg-4" d="M328.2,954.08l.1.22.05.12a35.15,35.15,0,0,1,3.7-.07,20.57,20.57,0,0,0-4.1-5.09l-.24-.19c-.63.17-.94.28-1.58,1.32,0,0-1.08,1.93-1.3,2.31s-.69,1.25-1.07,2.08a25.89,25.89,0,0,1,4.2-.68Z" data-name="Path 50" id="Path_50" transform="translate(-298.77 -923.86)">
        </path>
        <path class="cis__services__button--svg-4" d="M334.35,958.65l-.1-.22-.06-.12a35.09,35.09,0,0,1-3.7.08,21.25,21.25,0,0,0,4.08,5.07l.36.18c.57-.16.87-.31,1.48-1.3l1.3-2.31c.19-.33.69-1.25,1.08-2.08a24.85,24.85,0,0,1-4.2.68Z" data-name="Path 51" id="Path_51" transform="translate(-298.77 -923.86)">
        </path>
        <path class="cis__services__button--svg-4" d="M329.78,958.55l-.33-.55.41,0-.06-.12-.13-.22-.15-.25-.13-.23-.15-.27-.13-.24-.16-.29-.12-.23-.16-.33-.12-.22-.19-.38a1.14,1.14,0,0,0-.1-.19l-.12-.27a23.31,23.31,0,0,0-4.47.74l-.25.07,0,.07c0,.05,0,.09,0,.14a.43.43,0,0,0,0,.11,1.54,1.54,0,0,0-.06.43v.1s0,.08,0,.12,0,.14,0,.21a.29.29,0,0,1,0,.09v0c0,.06,0,.13.07.21a2.33,2.33,0,0,0,.1.27l.1.24,0,.1c.4.88,1,1.93,1.18,2.3s.87,1.58,1.44,2.4l.09.14.16.21a1.12,1.12,0,0,0,.17.2,3.07,3.07,0,0,0,.26.25l.07,0a1.32,1.32,0,0,0,.5.29l.3.1a16.32,16.32,0,0,0,2.71.2h1c.53,0,1.53,0,2.35-.08A22.76,22.76,0,0,1,329.78,958.55Z" data-name="Path 52" id="Path_52" transform="translate(-298.77 -923.86)">
        </path>
        <path class="cis__services__button--svg-4" d="M332.76,954.18l.34.55-.42,0c.56.95,1.14,2,1.72,3.24a22.56,22.56,0,0,0,4.47-.74l.25-.07c0-.07.05-.14.07-.2s0-.07,0-.11a1.57,1.57,0,0,0,0-.86s0-.08,0-.12a31,31,0,0,0-3.36-6.1,2.35,2.35,0,0,0-.33-.3,1.87,1.87,0,0,0-.8-.39,21.32,21.32,0,0,0-3.48-.2c-.42,0-1.62,0-2.59.08A22.74,22.74,0,0,1,332.76,954.18Z" data-name="Path 53" id="Path_53" transform="translate(-298.77 -923.86)">
        </path>
      </g>

      <!-- CROSS -->
      <polygon id="services-button-cross" class="cis__services__button--cross" points="38.86 27.55 37.45 26.14 32.5 31.09 27.55 26.14 26.14 27.55 31.09 32.5 26.14 37.45 27.55 38.86 32.5 33.91 37.45 38.86 38.86 37.45 33.91 32.5 38.86 27.55" />
  </svg>
  <span class="cis__services__button__label">Explorar Servicios</span>
</button>
<button class="cis__services__trigger open-carousel" id="carousel-trigger"></button>

<!-- COVER BACKGROUND -->
<section class="cis__services__carousel cis__services__carousel--inactive cis__services__carousel--section-active project-type" id="carousel-container">
  <div class="cis__services__carousel__container">
    <div class="cis__services__carousel__bg carousel-change-bg-2"></div>
    <div class="cis__services__carousel__marquee">
      <div class="cis__services__carousel__marquee__container" id="servicios-marquee" data-text="— Servicios — Servicios">— Servicios — Servicios</div>
    </div>
    <div class="services-carousel" id="services-carousel">
      <?php
        $services = page('servicios')->children()->listed()->children()->listed();
        $i = 1;
      ?>
      <?php foreach ($services as $service) : ?>
        <!-- SERVICE CELL -->
        <div class="services-carousel__cell">
          <div class="services-carousel__cell__container grid-margin">
            <div class="is-row is-row--responsive is-flex services-carousel__cell__container__row column-padding">
              <div class="is-col col-5 offset-3_5
                          xs-col-10 xs-offset-1
                          md-col-8 md-offset-2
                          l-col-3 l-offset-4_5
                          xl-col-3 xl-offset-4_5
                          services-carousel__cell__container__col">

                <!-- WHITE BOX CONTAINER -->
                <a href="<?php echo $service->url(); ?>" class="services-carousel__white-box js-carousel-link section-link carousel-cell" data-uri="<?php echo $service->uri(); ?>" data-title="<?php echo $service->title(); ?>" data-customtitle="<?php echo html::encode($service->customtitle()->html()); ?>" data-id="service-<?php echo $i; ?>" data-category="<?php echo $service->parent()->title(); ?>" data-cover="<?php echo $service->cover()->toFile() ? $service->cover()->toFile()->url() : ''; ?>" data-object="<?php echo $service->object()->toFile() ? $service->object()->toFile()->url() : ''; ?>" data-webp="<?php echo $service->object()->toFile() ? ($service->image($service->object()->toFile()->name() . '.webp') ? $service->image($service->object()->toFile()->name() . '.webp')->url() : '') : '' ?>" data-ratio="<?php echo $service->object()->toFile() ? ($service->object()->toFile()->height() / $service->object()->toFile()->width()) * 100 : ''; ?>" data-color="<?php echo $service->color(); ?>">

                  <?php if ($service->object()->toFile()) : ?>
                    <img src="<?php echo $service->object()->toFile()->url(); ?>" class="services-carousel__main-image-small" alt="<?php echo $service->title(); ?>">
                  <?php endif ?>
                  <?php 
                    switch ($service->position()) {
                      case 'topleft':
                      $text_position = ' services-carousel__figure--top-left';
                      break;

                      case 'bottomleft':
                      $text_position = ' services-carousel__figure--bottom-left';
                      break;

                      case 'topright':
                      $text_position = ' services-carousel__figure--top-right';
                      break;

                      case 'bottomright':
                      $text_position = ' services-carousel__figure--top-left';
                      break;
                    
                      default:
                      $text_position = '';
                      break;
                    }
                  ?>
                  <div class="services-carousel__white-box__border carousel-change-bg-1" id="carousel-box-border-<?php echo $i - 1; ?>"></div>
                  <div class="sevices-carousel__image-container<?php echo $text_position != '' ? $text_position : ''; ?>" id="second-image-<?php echo $i; ?>">
                    <?php
                    $element = $service;
                    $image_1 = $element->secondaryimage()->toFile();
                    $title = $element->title();
                    $ratio = $image_1 ? ' style="padding-top:' . ($image_1->height() / $image_1->width()) * 100 . '%;"' : '';
                    $webp = $image_1 ? ($element->image($image_1->name() . '.webp') ? ' data-webp="' . $element->image($image_1->name() . '.webp')->url() . '"' : '') : '';
                    ?>
                    <?php if ($image_1) : ?>
                      <figure class="services-carousel__figure" <?php echo $ratio; ?>>
                        <picture class="js-lazy-image --special-transform js-can-use-webp" data-alt="<?php echo $title; ?>" <?php echo $webp; ?><?php echo $ratio; ?>>
                          <source srcset="<?php echo $image_1->url(); ?>">
                        </picture>
                      </figure>
                    <?php endif ?>
                  </div>
                  <div class="services-carousel__content">
                    <p class="services-carousel__digit"><?php echo $i >= 10 ? $i : '0' . $i; ?></p>
                    <div class="services-carousel__content__headings">
                      <h5 class="services-carousel__category"><?php echo $service->parent()->title(); ?></h5>
                      <h3 class="services-carousel__heading"><?php echo $service->title(); ?></h3>
                    </div>
                  </div>
                </a>
                <!-- WHITE BOX CONTAINER -->

              </div>
            </div>
          </div>
        </div>
        <!-- SERVICE CELL -->
        <?php $i++; ?>
      <?php endforeach ?>
    </div>

    <div class="cis__services__carousel__title">
      <div class="container grid-margin">
        <div class="is-row is-flex">
          <div class="is-col col-2_5 offset-7 relative" id="js-new-titles"></div>
        </div>
      </div>
    </div>

    <div class="cis__services__carousel__image">
      <div class="container grid-margin">
        <div class="is-row is-row--responsive is-flex">
          <div class="is-col col-6_5 offset-2_5
                          md-col-10 md-offset-1
                          l-col-4 l-offset-4
                          xl-col-4 xl-offset-4 relative" id="js-new-image">
            <?php if (false) : ?>
              <!-- <img src="<?php echo page('cover')->image('03_micro.png')->url(); ?>" class="cis__services__carousel__image__image" alt=""> -->
              <?php
              $element = page('cover');
              $image_1 = page('cover')->image('03_micro.png');
              $title = $element->title();
              $ratio = $image_1 ? ' style="padding-top:' . ($image_1->height() / $image_1->width()) * 100 . '%;"' : '';
              $webp = $image_1 ? ($element->image($image_1->name() . '.webp') ? ' data-webp="' . $element->image($image_1->name() . '.webp')->url() . '"' : '') : '';
              ?>
              <?php if ($image_1) : ?>
                <figure class="cis__services__carousel__image__figure">
                  <picture class="js-lazy-image js-can-use-webp" data-alt="<?php echo $title; ?>" <?php echo $webp; ?><?php echo $ratio; ?>>
                    <source srcset="<?php echo $image_1->url(); ?>">
                  </picture>
                </figure>
              <?php endif ?>
            <?php endif ?>
          </div>
        </div>
      </div>
    </div>

    <div class="services-carousel__page-dots" id="services-carousel-dots">
      
      <!-- SERVICES THUMB-->
      <ul class="services-carousel__page-dots__list">
        <?php
          $i = 0;
        ?>
        <?php if(true): ?>
        <li>
          <!-- PREVIOUS SERVICE -->
          <button class="cis__attract-cta services-carousel__previous js-hover-animation-ui-item prev-service" id="prev-service">
            <svg class="cis__attract-cta__icon cis__attract-cta__icon--16" viewbox="0 0 16 12.45" xmlns="http://www.w3.org/2000/svg">
                <path d="M149.12,881.7l-2.43-2.43-1.35-.65h11.82v-2.69H145.41l1.25-.62,2.47-2.46-1.7-1.83-6.27,6.26,6.18,6.2Z" data-name="  Arrow Path" id="_Arrow_Path" transform="translate(-141.16 -871.02)">
                </path>
            </svg>
            <svg class="cis__attract-cta__svg cis__attract-cta__svg--a" x="0px" y="0px" width="106px" height="106px" viewBox="0 0 106 106" style="enable-background:new 0 0 106 106;">
                <circle class="cis__attract-cta__path" cx="53" cy="53" r="50"/>
            </svg>
            <svg class="cis__attract-cta__svg cis__attract-cta__svg--b" x="0px" y="0px" width="106px" height="106px" viewBox="0 0 106 106" style="enable-background:new 0 0 106 106;">
                <circle class="cis__attract-cta__path cis__attract-cta__path--bottom cis__attract-cta__path--b" cx="53" cy="53" r="50"/>
            </svg>
            <svg class="cis__attract-cta__svg cis__attract-cta__svg--c" x="0px" y="0px" width="106px" height="106px" viewBox="0 0 106 106" style="enable-background:new 0 0 106 106;">
                <circle class="cis__attract-cta__path cis__attract-cta__path--top cis__attract-cta__path--c" cx="53" cy="53" r="50"/>
            </svg>
            <svg class="cis__attract-cta__bg__circle" x="0px" y="0px" width="106px" height="106px" viewBox="0 0 106 106">
                <circle cx="53" cy="53" r="50"/>
            </svg>
          </button>
          <!-- PREVIOUS SERVICE -->
        </li>
        <?php endif ?>
        <?php foreach ($services as $service) : ?>
        <li class="services-carousel__page-dots__list__item services-carousel__page-dots__list__item--<?php echo $i; ?>" id="page-dot-<?php echo $i ?>">
          <button class="services-carousel__page-dots__button go-to-service" data-id="<?php echo $i ?>" id="page-dot-button-<?php echo $i ?>">
            <div class="services-carousel__page-dots__button__container carousel-change-bg-1">
              <?php
              $element = $service;
              $image_1 = $element->thumbnail()->toFile();
              $title = $element->title();
              $ratio = $image_1 ? ' style="padding-top:' . ($image_1->height() / $image_1->width()) * 100 . '%;"' : '';
              $webp = $image_1 ? ($element->image($image_1->name() . '.webp') ? ' data-webp="' . $element->image($image_1->name() . '.webp')->url() . '"' : '') : '';
              ?>
              <?php if ($image_1) : ?>
                <figure class="services-carousel__page-dots__button__figure" <?php echo $ratio; ?>>
                  <picture class="js-lazy-image --special-transform js-can-use-webp" data-alt="<?php echo $title; ?>" <?php echo $webp; ?><?php echo $ratio; ?>>
                    <source srcset="<?php echo $image_1->url(); ?>">
                  </picture>
                </figure>
              <?php endif ?>
            </div>
          </button>
        </li>
        <?php $i++; ?>
        <?php endforeach ?>
        <?php if(true): ?>
        <li>
          <!-- NEXT SERVICE -->
          <button class="cis__attract-cta services-carousel__next js-hover-animation-ui-item next-service" id="next-service">
            <svg class="cis__attract-cta__icon cis__attract-cta__icon--16" viewbox="0 0 16 12.45" xmlns="http://www.w3.org/2000/svg">
                <path d="M257.48,881.7l2.43-2.43,1.35-.65H249.44v-2.69h11.75l-1.25-.62-2.47-2.46,1.7-1.83,6.27,6.26-6.18,6.2Z" transform="translate(-249.44 -871.02)" />
            </svg>
            <svg class="cis__attract-cta__svg cis__attract-cta__svg--a" x="0px" y="0px" width="106px" height="106px" viewBox="0 0 106 106" style="enable-background:new 0 0 106 106;">
                <circle class="cis__attract-cta__path" cx="53" cy="53" r="50"/>
            </svg>
            <svg class="cis__attract-cta__svg cis__attract-cta__svg--b" x="0px" y="0px" width="106px" height="106px" viewBox="0 0 106 106" style="enable-background:new 0 0 106 106;">
                <circle class="cis__attract-cta__path cis__attract-cta__path--bottom cis__attract-cta__path--b" cx="53" cy="53" r="50"/>
            </svg>
            <svg class="cis__attract-cta__svg cis__attract-cta__svg--c" x="0px" y="0px" width="106px" height="106px" viewBox="0 0 106 106" style="enable-background:new 0 0 106 106;">
                <circle class="cis__attract-cta__path cis__attract-cta__path--top cis__attract-cta__path--c" cx="53" cy="53" r="50"/>
            </svg>
            <svg class="cis__attract-cta__bg__circle" x="0px" y="0px" width="106px" height="106px" viewBox="0 0 106 106">
                <circle cx="53" cy="53" r="50"/>
            </svg>
          </button>
          <!-- NEXT SERVICE -->
        </li>
        <?php endif ?>
      </ul>
      <!-- SERVICES THUMB-->

    </div>

  </div>
</section>
<!-- COVER BACKGROUND -->