<nav>
    <ul>
        <li>
            <a href="<?php echo page('insights')->url(); ?>" data-uri="<?php echo page('insights')->uri(); ?>" data-title="<?php echo page('insights')->title(); ?>">
                <div class="cis__concept-link">
                    <span class="cis__concept-link__top"><span class="cis__block"></span>La <span class="cis__underline">Inspiración</span></span>
                    <span class="cis__concept-link__bottom"><?php echo page('insights')->title(); ?></span>
                </div>
            </a>
        </li>
        <li>
            <a href="<?php echo page('servicios')->url(); ?>" data-uri="<?php echo page('servicios')->uri(); ?>" data-title="<?php echo page('servicios')->title(); ?>"> <div class="cis__concept-link"> <span class="cis__concept-link__top">de <span class="cis__underline">lo que hacemos</span></span> <span class="cis__concept-link__bottom"><?php echo page('servicios')->title(); ?></span> </div> </a>
        </li>
        <li>
            <a href="<?php echo page('portafolio')->url(); ?>" data-uri="<?php echo page('portafolio')->uri(); ?>" data-title="<?php echo page('portafolio')->title(); ?>"> <div class="cis__concept-link"> <span class="cis__concept-link__top">se <span class="cis__underline">encuentra</span></span> <span class="cis__concept-link__bottom"><?php echo page('portafolio')->title(); ?></span> </div> </a>
        </li>
        <li>
            <a href="<?php echo page('nosotros')->url(); ?>" data-uri="<?php echo page('nosotros')->uri(); ?>" data-title="<?php echo page('nosotros')->title(); ?>"> <div class="cis__concept-link"> <span class="cis__concept-link__top">en <span class="cis__underline">las personas.</span><span class="cis__block"></span></span> <span class="cis__concept-link__bottom"><?php echo page('nosotros')->title(); ?></span> </div> </a>
        </li>
        <!-- OLD STRUCTURE -->
        <!-- <li><a href="<?php echo page('nosotros')->url(); ?>"><span data-text="Nosotros">En las personas.</span></a></li> -->
    </ul>
</nav>