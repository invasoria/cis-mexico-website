<!-- MAIN HEADER AND NAV -->
<header class="cis__header cis__header--main" id="main-nav">
	<nav class="cis__header__nav grid-margin">
		<div class="is-row is-row--responsive is-flex column-padding">
		    <div class="is-col col-2 offset-_5 xs-col-6">
		    	<h1 class="cis__header__nav__type-logo">
		    		<a href="<?php echo page('cover')->url(); ?>" data-uri="<?php echo page('cover')->uri(); ?>" data-title="<?php echo page('cover')->title(); ?>" class="section-link">
						<!--
		    			<svg viewbox="0 0 176.01 72" xmlns="http://www.w3.org/2000/svg"> <g> <g> <rect class="cis-logo__st1" height="16" width="16" y="19"> </rect> </g> <g> <rect class="cis-logo__st1" height="16" width="40" x="136" y="38"> </rect> </g> <g> <g> <path class="cis-logo__st0" d="M329.89,183.56a5.1,5.1,0,0,1,5.48,4.72c0,.12,0,.23,0,.34l-2.2.09a3.16,3.16,0,0,0-3.17-3.14h-.13c-2.06,0-3.41,1.55-3.41,4s1.35,4,3.41,4a3.17,3.17,0,0,0,3.3-3v-.13l2.2.09a5.11,5.11,0,0,1-5.14,5.08h-.36a6,6,0,0,1,0-12Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st0" d="M345.35,195.34h-8.64V183.78h8.48v1.9H339v2.82h4.88v1.89H339v3h6.38Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st0" d="M354.56,191.48v-7.7h2.13v11.56h-1.84l-5.9-8.15v8.15h-2.13V183.78h2.27Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st0" d="M362.06,195.34v-9.66H358v-1.9H368.5v1.9h-4.15v9.66Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st0" d="M377.23,195.34a5.62,5.62,0,0,1-.48-2.39,1.73,1.73,0,0,0-1.57-1.9,1.56,1.56,0,0,0-.43,0H372v4.28h-2.26V183.78H375c3,0,4.4,1.37,4.4,3.52a2.8,2.8,0,0,1-2.55,2.93,2.41,2.41,0,0,1,2.13,2.39c.15,1.71.2,2.29.59,2.72Zm-2.53-6.08c1.67,0,2.44-.55,2.44-1.8s-.77-1.83-2.44-1.83H372v3.63Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st0" d="M386.21,195.56a6,6,0,1,1,5.86-6.15v.15a5.71,5.71,0,0,1-5.41,6Zm0-10c-2.12,0-3.48,1.56-3.48,4s1.36,4,3.48,4,3.49-1.56,3.49-4S388.35,185.57,386.21,185.57Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st0" d="M396.56,183.78h4.37c3.72,0,6,2.16,6,5.78s-2.32,5.78-6,5.78h-4.37Zm4.22,9.72c2.44,0,3.82-1.47,3.82-4s-1.38-3.93-3.82-3.93h-1.92v7.89Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st0" d="M417,195.34h-8.64V183.78h8.47v1.9h-6.21v2.82h4.88v1.89h-4.88v3H417Z" transform="translate(-323.62 -181.56)"> </path> </g> </g> <g> <g> <path class="cis-logo__st0" d="M355.51,202.77v11.56h-2.3V202.77Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st0" d="M365.14,210.48v-7.71h2.13v11.56h-1.83l-5.91-8.14v8.14H357.4V202.77h2.28Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st0" d="M372.8,214.33l-4.57-11.56h2.4l3.42,8.78h.07l3.38-8.78h2.29l-4.55,11.56Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st0" d="M389.42,214.33h-8.64V202.77h8.48v1.91H383v2.82h4.88v1.89H383v3h6.38Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st0" d="M392.19,210.13a2.69,2.69,0,0,0,2.74,2.63l.27,0c1.43,0,2.35-.57,2.35-1.54s-.7-1.22-1.91-1.45l-2.2-.4c-1.8-.33-3.05-1.32-3.05-3.23,0-2.17,1.71-3.56,4.4-3.56,3,0,4.74,1.56,4.77,4.13l-2.11.07a2.39,2.39,0,0,0-2.36-2.42l-.32,0c-1.35,0-2.12.61-2.12,1.6,0,.84.6,1.14,1.65,1.34l2.13.38c2.27.43,3.33,1.5,3.33,3.38,0,2.28-2,3.5-4.56,3.5-3,0-5.12-1.54-5.12-4.33Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st0" d="M404.15,214.33v-9.65h-4.09v-1.91h10.53v1.91h-4.15v9.65Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st0" d="M414.16,202.77v11.56h-2.29V202.77Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st0" d="M415.52,208.55a5.72,5.72,0,0,1,5.44-6h.36c3.34,0,5.27,1.84,5.5,4.76l-2.24.09a2.94,2.94,0,0,0-3-2.84h-.17c-2.11,0-3.47,1.56-3.47,4s1.4,4.06,3.66,4.06a3,3,0,0,0,3.15-2.4h-3.06V208.5h5.21v5.83h-2v-1.7a3.77,3.77,0,0,1-3.67,1.92C417.93,214.55,415.52,212.13,415.52,208.55Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st0" d="M435,202.77l4.71,11.56h-2.44l-1.16-2.86h-4.91l-1.12,2.86h-2.31l4.67-11.56Zm-3.1,6.94h3.52l-1.75-4.33h-.07Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st0" d="M445.37,202.55a5.12,5.12,0,0,1,5.48,4.73c0,.12,0,.23,0,.34l-2.2.09a3.16,3.16,0,0,0-3.17-3.14h-.13c-2.06,0-3.42,1.55-3.42,4s1.36,4,3.42,4a3.15,3.15,0,0,0,3.29-3s0-.09,0-.13l2.2.09a5.12,5.12,0,0,1-5.15,5.08l-.35,0a6,6,0,0,1,0-12Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st0" d="M454.48,202.77v11.56h-2.29V202.77Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st0" d="M461.71,214.55a6,6,0,1,1,5.85-6.14v.14a5.71,5.71,0,0,1-5.41,6Zm0-10c-2.13,0-3.49,1.56-3.49,4s1.36,4,3.49,4,3.49-1.56,3.49-4S463.84,204.57,461.71,204.57Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st0" d="M476.64,210.48v-7.71h2.13v11.56h-1.84L471,206.19v8.14h-2.13V202.77h2.28Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st0" d="M489.28,214.33h-8.64V202.77h8.47v1.91h-6.22v2.82h4.88v1.89h-4.88v3h6.39Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st0" d="M492.05,210.13a2.68,2.68,0,0,0,2.73,2.63l.27,0c1.44,0,2.35-.57,2.35-1.54s-.7-1.22-1.91-1.45l-2.2-.4c-1.8-.33-3-1.32-3-3.23,0-2.17,1.71-3.56,4.4-3.56,3,0,4.73,1.56,4.77,4.13l-2.11.07a2.39,2.39,0,0,0-2.36-2.42l-.32,0c-1.36,0-2.13.61-2.13,1.6,0,.84.61,1.14,1.65,1.34l2.13.38c2.28.43,3.34,1.5,3.34,3.38,0,2.28-2,3.5-4.57,3.5-3,0-5.12-1.54-5.12-4.33Z" transform="translate(-323.62 -181.56)"> </path> </g> </g> <g> <g> <path class="cis-logo__st0" d="M326.11,229.13a2.69,2.69,0,0,0,2.74,2.63l.27,0c1.43,0,2.35-.57,2.35-1.54s-.7-1.22-1.91-1.45l-2.2-.4c-1.8-.33-3.05-1.32-3.05-3.23,0-2.17,1.71-3.56,4.4-3.56,3,0,4.74,1.56,4.78,4.13l-2.11.07a2.41,2.41,0,0,0-2.37-2.42l-.31,0c-1.36,0-2.13.6-2.13,1.6,0,.84.6,1.14,1.65,1.34l2.13.38c2.27.43,3.34,1.5,3.34,3.38,0,2.27-2,3.5-4.57,3.5-3,0-5.12-1.54-5.12-4.33Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st0" d="M340.49,233.55a6,6,0,1,1,5.85-6.14.71.71,0,0,1,0,.14,5.72,5.72,0,0,1-5.41,6Zm0-10c-2.13,0-3.48,1.56-3.48,4s1.35,4,3.48,4,3.49-1.55,3.49-4S342.62,223.57,340.49,223.57Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st0" d="M353,221.55a5.11,5.11,0,0,1,5.48,4.73c0,.11,0,.23,0,.34l-2.2.09a3.16,3.16,0,0,0-3.17-3.14H353c-2.06,0-3.42,1.54-3.42,4s1.36,4,3.42,4a3.14,3.14,0,0,0,3.29-3,.57.57,0,0,0,0-.13l2.2.09a5.11,5.11,0,0,1-5.14,5.08l-.36,0a6,6,0,0,1,0-12Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st0" d="M362.12,221.77v11.56h-2.3V221.77Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st0" d="M369.35,233.55a6,6,0,1,1,5.85-6.14v.14a5.71,5.71,0,0,1-5.41,6Zm0-10c-2.13,0-3.49,1.56-3.49,4s1.36,4,3.49,4,3.48-1.55,3.48-4S371.47,223.57,369.35,223.57Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st0" d="M378.83,221.77v9.65h6.11v1.91h-8.4V221.77Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st0" d="M397.65,227.55a5.72,5.72,0,0,1,5.44-6h.36c3.33,0,5.26,1.84,5.5,4.76l-2.24.09a3,3,0,0,0-3-2.84h-.17c-2.11,0-3.47,1.56-3.47,4s1.4,4.06,3.65,4.06a3,3,0,0,0,3.15-2.4h-3.06V227.5H409v5.84h-2v-1.71a3.78,3.78,0,0,1-3.67,1.93C400.05,233.55,397.65,231.13,397.65,227.55Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st0" d="M413.05,221.77v11.56h-2.29V221.77Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st0" d="M420.21,221.55a5.12,5.12,0,0,1,5.48,4.74c0,.11,0,.22,0,.33l-2.2.09a3.16,3.16,0,0,0-3.17-3.14h-.13c-2.06,0-3.42,1.54-3.42,4s1.36,4,3.42,4a3.14,3.14,0,0,0,3.29-3,.57.57,0,0,0,0-.13l2.2.09a5.12,5.12,0,0,1-5.15,5.08l-.35,0a6,6,0,0,1,0-12Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st0" d="M432.88,221.77l4.72,11.56h-2.44L434,230.47h-4.92L428,233.33h-2.31l4.68-11.56Zm-3.1,6.94h3.53l-1.75-4.33h-.07Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st0" d="M439.85,229.13a2.69,2.69,0,0,0,2.74,2.63l.27,0c1.43,0,2.35-.57,2.35-1.54s-.7-1.22-1.91-1.45l-2.2-.4c-1.8-.33-3.05-1.32-3.05-3.23,0-2.17,1.71-3.56,4.4-3.56,3,0,4.74,1.56,4.77,4.13l-2.11.07a2.39,2.39,0,0,0-2.36-2.42l-.32,0c-1.35,0-2.12.6-2.12,1.6,0,.84.6,1.14,1.65,1.34l2.13.38c2.27.43,3.33,1.5,3.33,3.38,0,2.27-2,3.5-4.56,3.5-3,0-5.12-1.54-5.12-4.33Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st0" d="M390.93,233.56a6,6,0,1,1,5.85-6.15v.15a5.69,5.69,0,0,1-5.4,6Zm0-10c-2.12,0-3.48,1.56-3.48,4s1.36,4,3.48,4,3.49-1.56,3.49-4S393.06,223.58,390.93,223.58Zm-.8-2.57,1-2.45h2.36L391.71,221Z" transform="translate(-323.62 -181.56)"> </path> </g> </g> <g> <rect class="cis-logo__st1" height="16" width="72" y="56"> </rect> </g> <g> <rect class="cis-logo__st1" height="16" width="72" x="104"> </rect> </g> <g> <g> <path class="cis-logo__st0" d="M411.43,239.77h4.37c3.72,0,6,2.17,6,5.78s-2.31,5.78-6,5.78h-4.37Zm4.22,9.73c2.44,0,3.82-1.47,3.82-4s-1.38-3.93-3.82-3.93h-1.93v7.89Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st0" d="M431.83,251.33h-8.64V239.77h8.47v1.91h-6.22v2.82h4.88v1.89h-4.88v3h6.39Z" transform="translate(-323.62 -181.56)"> </path> </g> <g> <path class="cis-logo__st2" d="M441.48,251.33l-2.83-9.1h-.07v9.1h-2.13V239.77h3.61l2.53,8.4h.06l2.4-8.4h3.54v11.56h-2.31v-9.1h-.07l-2.77,9.1Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st2" d="M466.71,245.26l4.46,6.07h-2.7l-3.13-4.27-3.12,4.27h-2.55l4.45-5.92L460,239.77h2.7l2.77,3.86,2.76-3.86h2.55Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st2" d="M474.45,239.77v11.56h-2.29V239.77Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st2" d="M481.61,239.55a5.13,5.13,0,0,1,5.49,4.73c0,.12,0,.23,0,.34l-2.2.09a3.16,3.16,0,0,0-3.17-3.14h-.13c-2.06,0-3.41,1.55-3.41,4s1.35,4,3.41,4a3.17,3.17,0,0,0,3.3-3v-.13l2.2.09a5.11,5.11,0,0,1-5.14,5.08l-.36,0a6,6,0,0,1,0-12Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st2" d="M493.77,251.55a6,6,0,1,1,5.85-6.14v.14a5.71,5.71,0,0,1-5.41,6Zm0-10c-2.13,0-3.49,1.56-3.49,4s1.36,4,3.49,4,3.49-1.56,3.49-4S495.9,241.57,493.77,241.57Z" transform="translate(-323.62 -181.56)"> </path> <path class="cis-logo__st2" d="M459.08,251.34h-8.64V239.77h8.48v1.91H452.7v2.81h4.88v1.89H452.7v3.05h6.38ZM454,239l1-2.46h2.37L455.6,239Z" transform="translate(-323.62 -181.56)"> </path> </g> </g> </g> </svg>
						-->
						<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 144.29 69.78"><g class="cis-logo__st0"><path class="cls-2" d="M612,348.73a5,5,0,0,1,5.4,5l-2.16.09a3.1,3.1,0,0,0-3.24-3.08c-2,0-3.35,1.52-3.35,3.91s1.33,3.91,3.35,3.91a3.1,3.1,0,0,0,3.24-3.08l2.16.09a5,5,0,0,1-5.4,5,5.89,5.89,0,0,1,0-11.78Z" transform="translate(-606.18 -348.73)"/><path class="cls-2" d="M627.13,360.29h-8.48V349H627v1.87h-6.1v2.76h4.78v1.85h-4.78v3h6.26Z" transform="translate(-606.18 -348.73)"/><path class="cls-2" d="M636.17,356.51V349h2.09v11.34h-1.81l-5.79-8v8h-2.09V349h2.23Z" transform="translate(-606.18 -348.73)"/><path class="cls-2" d="M643.53,360.29v-9.47h-4V349h10.34v1.87h-4.07v9.47Z" transform="translate(-606.18 -348.73)"/><path class="cls-2" d="M658.41,360.29A5.46,5.46,0,0,1,658,358a1.71,1.71,0,0,0-2-1.85h-2.66v4.19H651.1V349h5.14c2.89,0,4.32,1.35,4.32,3.46a2.75,2.75,0,0,1-2.51,2.88,2.35,2.35,0,0,1,2.09,2.34c.15,1.67.2,2.25.58,2.66Zm-2.48-6c1.64,0,2.39-.54,2.39-1.76s-.75-1.8-2.39-1.8h-2.61v3.56Z" transform="translate(-606.18 -348.73)"/><path class="cls-2" d="M667.23,360.51a5.89,5.89,0,1,1,5.75-5.89A5.61,5.61,0,0,1,667.23,360.51Zm0-9.8c-2.08,0-3.42,1.53-3.42,3.91s1.34,3.91,3.42,3.91,3.42-1.53,3.42-3.91S669.32,350.71,667.23,350.71Z" transform="translate(-606.18 -348.73)"/><path class="cls-2" d="M677.38,349h4.29c3.65,0,5.92,2.12,5.92,5.67s-2.27,5.67-5.92,5.67h-4.29Zm4.14,9.54c2.4,0,3.75-1.44,3.75-3.89s-1.35-3.85-3.75-3.85h-1.89v7.74Z" transform="translate(-606.18 -348.73)"/><path class="cls-2" d="M697.4,360.29h-8.48V349h8.32v1.87h-6.1v2.76h4.78v1.85h-4.78v3h6.26Z" transform="translate(-606.18 -348.73)"/></g><g class="cis-logo__st0"><path class="cls-2" d="M609,368v11.34h-2.25V368Z" transform="translate(-606.18 -348.73)"/><path class="cls-2" d="M618.49,375.51V368h2.09v11.34h-1.8l-5.8-8v8h-2.09V368h2.24Z" transform="translate(-606.18 -348.73)"/><path class="cls-2" d="M626,379.29,621.53,368h2.36l3.35,8.61h.07l3.31-8.61h2.25l-4.46,11.34Z" transform="translate(-606.18 -348.73)"/><path class="cls-2" d="M642.32,379.29h-8.48V368h8.32v1.87h-6.1v2.76h4.79v1.85h-4.79v3h6.26Z" transform="translate(-606.18 -348.73)"/><path class="cls-2" d="M645,375.17a2.64,2.64,0,0,0,3,2.56c1.41,0,2.31-.56,2.31-1.52s-.69-1.18-1.88-1.42l-2.16-.39c-1.76-.33-3-1.3-3-3.17,0-2.13,1.68-3.5,4.33-3.5,3,0,4.64,1.53,4.68,4.06l-2.07.07a2.35,2.35,0,0,0-2.63-2.36c-1.33,0-2.09.59-2.09,1.56s.59,1.12,1.62,1.32l2.09.38c2.23.41,3.27,1.47,3.27,3.31,0,2.23-1.92,3.44-4.48,3.44-2.92,0-5-1.51-5-4.25Z" transform="translate(-606.18 -348.73)"/><path class="cls-2" d="M656.77,379.29v-9.47h-4V368h10.33v1.87H659v9.47Z" transform="translate(-606.18 -348.73)"/><path class="cls-2" d="M666.6,368v11.34h-2.25V368Z" transform="translate(-606.18 -348.73)"/><path class="cls-2" d="M667.93,373.62a5.63,5.63,0,0,1,5.69-5.89c3.28,0,5.17,1.8,5.4,4.67l-2.19.09a2.89,2.89,0,0,0-3.15-2.78c-2.07,0-3.41,1.53-3.41,3.91s1.37,4,3.59,4a3,3,0,0,0,3.09-2.36h-3v-1.67h5.11v5.72h-2v-1.67a3.72,3.72,0,0,1-3.6,1.89C670.29,379.51,667.93,377.13,667.93,373.62Z" transform="translate(-606.18 -348.73)"/><path class="cls-2" d="M687,368l4.63,11.34h-2.39l-1.14-2.81h-4.82l-1.1,2.81h-2.27L684.51,368Zm-3,6.81h3.46l-1.71-4.25h-.07Z" transform="translate(-606.18 -348.73)"/><path class="cls-2" d="M697.22,367.73a5,5,0,0,1,5.4,5l-2.16.09a3.1,3.1,0,0,0-3.24-3.08c-2,0-3.35,1.52-3.35,3.91s1.33,3.91,3.35,3.91a3.1,3.1,0,0,0,3.24-3.08l2.16.09a5,5,0,0,1-5.4,5,5.89,5.89,0,0,1,0-11.78Z" transform="translate(-606.18 -348.73)"/><path class="cls-2" d="M706.17,368v11.34h-2.25V368Z" transform="translate(-606.18 -348.73)"/><path class="cls-2" d="M713.26,379.51a5.89,5.89,0,1,1,5.74-5.89A5.61,5.61,0,0,1,713.26,379.51Zm0-9.8c-2.09,0-3.42,1.53-3.42,3.91s1.33,3.91,3.42,3.91,3.42-1.53,3.42-3.91S715.35,369.71,713.26,369.71Z" transform="translate(-606.18 -348.73)"/><path class="cls-2" d="M727.91,375.51V368H730v11.34h-1.8l-5.8-8v8h-2.09V368h2.24Z" transform="translate(-606.18 -348.73)"/><path class="cls-2" d="M740.31,379.29h-8.48V368h8.32v1.87h-6.1v2.76h4.79v1.85h-4.79v3h6.26Z" transform="translate(-606.18 -348.73)"/><path class="cls-2" d="M743,375.17a2.64,2.64,0,0,0,3,2.56c1.41,0,2.31-.56,2.31-1.52s-.69-1.18-1.88-1.42l-2.16-.39c-1.76-.33-3-1.3-3-3.17,0-2.13,1.68-3.5,4.33-3.5,3,0,4.64,1.53,4.68,4.06l-2.07.07a2.35,2.35,0,0,0-2.63-2.36c-1.33,0-2.09.59-2.09,1.56s.59,1.12,1.62,1.32l2.09.38c2.23.41,3.27,1.47,3.27,3.31,0,2.23-1.92,3.44-4.48,3.44-2.91,0-5-1.51-5-4.25Z" transform="translate(-606.18 -348.73)"/></g><g class="cis-logo__st0"><path class="cls-2" d="M608.25,394.17a2.64,2.64,0,0,0,3,2.56c1.4,0,2.3-.56,2.3-1.52s-.68-1.18-1.87-1.42l-2.16-.39c-1.76-.33-3-1.3-3-3.17,0-2.13,1.68-3.5,4.32-3.5,3,0,4.65,1.53,4.69,4.06l-2.08.07a2.34,2.34,0,0,0-2.62-2.36c-1.34,0-2.09.59-2.09,1.56s.59,1.12,1.62,1.32l2.09.38c2.23.41,3.27,1.47,3.27,3.31,0,2.23-1.92,3.44-4.48,3.44-2.92,0-5-1.51-5-4.25Z" transform="translate(-606.18 -348.73)"/><path class="cls-2" d="M622.36,398.51a5.89,5.89,0,1,1,5.74-5.89A5.61,5.61,0,0,1,622.36,398.51Zm0-9.8c-2.09,0-3.42,1.53-3.42,3.91s1.33,3.91,3.42,3.91,3.42-1.53,3.42-3.91S624.45,388.71,622.36,388.71Z" transform="translate(-606.18 -348.73)"/><path class="cls-2" d="M634.64,386.73a5,5,0,0,1,5.4,5l-2.16.09a3.1,3.1,0,0,0-3.24-3.08c-2,0-3.35,1.52-3.35,3.91s1.33,3.91,3.35,3.91a3.1,3.1,0,0,0,3.24-3.08l2.16.09a5,5,0,0,1-5.4,5,5.89,5.89,0,0,1,0-11.78Z" transform="translate(-606.18 -348.73)"/><path class="cls-2" d="M643.58,387v11.34h-2.25V387Z" transform="translate(-606.18 -348.73)"/><path class="cls-2" d="M650.67,398.51a5.89,5.89,0,1,1,5.75-5.89A5.61,5.61,0,0,1,650.67,398.51Zm0-9.8c-2.09,0-3.42,1.53-3.42,3.91s1.33,3.91,3.42,3.91,3.42-1.53,3.42-3.91S652.76,388.71,650.67,388.71Z" transform="translate(-606.18 -348.73)"/><path class="cls-2" d="M660,387v9.47h6v1.87h-8.24V387Z" transform="translate(-606.18 -348.73)"/><path class="cls-2" d="M671.86,398.51a5.89,5.89,0,1,1,5.74-5.89A5.61,5.61,0,0,1,671.86,398.51Zm0-9.8c-2.09,0-3.42,1.53-3.42,3.91s1.33,3.91,3.42,3.91,3.42-1.53,3.42-3.91S674,388.71,671.86,388.71Zm-.79-2.52,1-2.41h2.32l-1.74,2.41Z" transform="translate(-606.18 -348.73)"/><path class="cls-2" d="M678.45,392.62a5.62,5.62,0,0,1,5.68-5.89c3.28,0,5.17,1.8,5.41,4.67l-2.2.09a2.88,2.88,0,0,0-3.15-2.78c-2.07,0-3.4,1.53-3.4,3.91s1.36,4,3.58,4a3,3,0,0,0,3.1-2.36h-3v-1.67h5.11v5.72h-2v-1.67a3.71,3.71,0,0,1-3.6,1.89C680.8,398.51,678.45,396.13,678.45,392.62Z" transform="translate(-606.18 -348.73)"/><path class="cls-2" d="M693.57,387v11.34h-2.26V387Z" transform="translate(-606.18 -348.73)"/><path class="cls-2" d="M700.59,386.73a5,5,0,0,1,5.4,5l-2.16.09a3.1,3.1,0,0,0-3.24-3.08c-2,0-3.35,1.52-3.35,3.91s1.33,3.91,3.35,3.91a3.1,3.1,0,0,0,3.24-3.08l2.16.09a5,5,0,0,1-5.4,5,5.89,5.89,0,0,1,0-11.78Z" transform="translate(-606.18 -348.73)"/><path class="cls-2" d="M713,387l4.63,11.34h-2.39l-1.14-2.81H709.3l-1.1,2.81h-2.27L710.52,387Zm-3,6.81h3.46l-1.71-4.25h-.07Z" transform="translate(-606.18 -348.73)"/><path class="cls-2" d="M719.86,394.17a2.64,2.64,0,0,0,3,2.56c1.4,0,2.3-.56,2.3-1.52s-.68-1.18-1.87-1.42l-2.16-.39c-1.77-.33-3-1.3-3-3.17,0-2.13,1.67-3.5,4.32-3.5,3,0,4.65,1.53,4.68,4.06l-2.07.07a2.34,2.34,0,0,0-2.63-2.36c-1.33,0-2.09.59-2.09,1.56s.6,1.12,1.62,1.32l2.09.38c2.24.41,3.28,1.47,3.28,3.31,0,2.23-1.93,3.44-4.48,3.44-2.92,0-5-1.51-5-4.25Z" transform="translate(-606.18 -348.73)"/></g><g class="cis-logo__st0"><path class="cls-2" d="M606.79,407h4.28c3.66,0,5.93,2.12,5.93,5.67s-2.27,5.67-5.93,5.67h-4.28Zm4.14,9.54c2.39,0,3.74-1.44,3.74-3.89s-1.35-3.85-3.74-3.85H609v7.74Z" transform="translate(-606.18 -348.73)"/><path class="cls-2" d="M626.81,418.29h-8.48V407h8.31v1.87h-6.1v2.76h4.79v1.85h-4.79v3h6.27Z" transform="translate(-606.18 -348.73)"/></g><g class="cis-logo__st0"><path class="cis-logo__st2" d="M636.18,418.29l-2.32-8.37h-.07v8.37h-2.52V407h4.25l2,7.58h.05l1.91-7.58h4.16v11.34H640.9v-8.37h-.07l-2.31,8.37Z" transform="translate(-606.18 -348.73)"/><path class="cis-logo__st2" d="M654.08,418.29h-8.75V407h8.58v2.25H648v2.23h4.59v2.22H648V416h6.07Zm-5.26-12.06,1-2.49h2.65l-1.75,2.49Z" transform="translate(-606.18 -348.73)"/><path class="cis-logo__st2" d="M662,412.32l4.37,6h-3.2l-2.83-3.85-2.77,3.85h-3l4.36-5.81-4-5.53h3.2l2.47,3.42L663,407h3Z" transform="translate(-606.18 -348.73)"/><path class="cis-logo__st2" d="M669.88,407v11.34h-2.74V407Z" transform="translate(-606.18 -348.73)"/><path class="cis-logo__st2" d="M676.83,406.73a5.08,5.08,0,0,1,5.47,5l-2.61.11a2.74,2.74,0,0,0-2.88-2.7c-1.82,0-2.94,1.35-2.94,3.47s1.12,3.48,2.94,3.48a2.74,2.74,0,0,0,2.88-2.7l2.61.1a5.08,5.08,0,0,1-5.47,5,5.89,5.89,0,0,1,0-11.78Z" transform="translate(-606.18 -348.73)"/><path class="cis-logo__st2" d="M688.76,418.51a5.89,5.89,0,1,1,5.83-5.89A5.67,5.67,0,0,1,688.76,418.51Zm0-9.38c-1.84,0-3,1.38-3,3.49s1.17,3.49,3,3.49,3-1.38,3-3.49S690.62,409.13,688.76,409.13Z" transform="translate(-606.18 -348.73)"/></g></svg>
		    			<!-- CENTRO DE<br>INVESTIGACIONES<br>SOCIOLÓGICAS<br>DE <span>MÉXICO</span> -->
		    		</a>
		    	</h1>
		    </div>
		    <div class="is-col col-7_5 is-hidden-mobile">
	    		<?php
	    		    $items = $site->navmenus();
	    		?>
	    		<?php if($items->isNotEmpty()): ?>
	    		<ul class="cis__header__nav__list cis__header__nav__list--right-align" id="main-nav-list">
	    		    <?php foreach ($items->toStructure() as $item): ?>
	    		        <?php if($item->link() == 'page'): ?>
	    		            <?php foreach ($item->pagelink()->yaml() as $pagelink): ?>
	    		                <?php if(page($pagelink) && page($pagelink)->isListed()): ?>
	    		                <?php $pageitem = page($pagelink); ?>
	    		                <li class="cis__header__nav__list__item">
	    		                	<a href="<?php echo $pageitem->url(); ?>" data-uri="<?php echo $pageitem->uri(); ?>" data-title="<?php echo $pageitem->title(); ?>" class="main-nav-item section-link main-nav-<?php echo $pageitem->autoid(); ?>"><?php echo $item->pagetitle() != '' ? $item->pagetitle() : $pageitem->title() ; ?></a>
	    		                </li>
	    		                <?php endif ?>
	    		            <?php endforeach ?>
	    		        <?php elseif(($item->link() == 'url')): ?>
	    		        	<li class="cis__header__nav__list__item">
	    		        		<a href="<?php echo $item->pageurl(); ?>" data-title="<?php echo $item->label(); ?>" class="main-nav-item external-link" target="_blank"><?php echo $item->label(); ?></a>
	    		        	</li>
	    		        <?php endif ?>
	    		    <?php endforeach ?>
		    	</ul>
		    	<?php endif ?>
		    </div>
		    <div class="is-col col-1_5 xs-col-3 xs-offset-3 cis__header__nav--right-cta">
		    	<button class="cis__attract-cta open-menu js-hover-effect-ui-item" id="menu-toggle">
		    		<svg class="cis__attract-cta__icon" id="menu-toggle-svg" viewbox="0 0 25 25" xmlns="http://www.w3.org/2000/svg">
		    		    <g>
		    		        <g>
		    		            <rect id="menu-toggle-bar-1" class="cis__hamburger__fill" height="2" width="20" x="2.5" y="5.5" />
		    		            <rect id="menu-toggle-bar-2" class="cis__hamburger__fill" height="2" width="20" x="2.5" y="11.5" />
		    		            <rect id="menu-toggle-bar-3" class="cis__hamburger__fill" height="2" width="14" x="8.5" y="17.5" />
		    		        </g>
		    		    </g>
		    		</svg>
		    	    <svg class="cis__attract-cta__svg cis__attract-cta__svg--a" x="0px" y="0px" width="106px" height="106px" viewBox="0 0 106 106" style="enable-background:new 0 0 106 106;">
		    	        <circle class="cis__attract-cta__path" cx="53" cy="53" r="50"/>
		    	    </svg>
		    	    <svg class="cis__attract-cta__svg cis__attract-cta__svg--b" x="0px" y="0px" width="106px" height="106px" viewBox="0 0 106 106" style="enable-background:new 0 0 106 106;">
		    	        <circle class="cis__attract-cta__path cis__attract-cta__path--bottom cis__attract-cta__path--b" cx="53" cy="53" r="50"/>
		    	    </svg>
		    	    <svg class="cis__attract-cta__svg cis__attract-cta__svg--c" x="0px" y="0px" width="106px" height="106px" viewBox="0 0 106 106" style="enable-background:new 0 0 106 106;">
		    	        <circle class="cis__attract-cta__path cis__attract-cta__path--top cis__attract-cta__path--c" cx="53" cy="53" r="50"/>
		    	    </svg>
		    	    <svg class="cis__attract-cta__bg__circle" x="0px" y="0px" width="106px" height="106px" viewBox="0 0 106 106">
		    	        <circle cx="53" cy="53" r="50"/>
		    	    </svg>
		    	</button>
		    </div>
		</div>
	</nav>
</header>

<header class="cis__nav-header" id="nav-header">
	<div class="cis__nav-header__container">
		<div class="cis__nav-header__container__wrapper grid-margin">
			<div class="is-row is-flex column-padding">
			    <div class="is-col col-5_5 offset-_5">
			    	<nav class="cis__nav-header__nav">
			    		<h4 class="js-appear-on-menu-open">Menu</h4>
			    		<ul class="cis__nav-header__nav__menu-list">
			    		<?php
			    		    $items = $site->footermenus();
			    		    //$items = $pages->listed();
			    		?>
		    		    <?php foreach ($items->toStructure() as $item): ?>
		    		        <?php if($item->link() == 'page'): ?>
		    		            <?php foreach ($item->pagelink()->yaml() as $pagelink): ?>
		    		                <?php if(page($pagelink) && page($pagelink)->isListed()): ?>
		    		                <?php $pageitem = page($pagelink); ?>
		    		                <li class="cis__nav-header__nav__menu-list__item js-appear-on-menu-open">
		    		                	<a href="<?php echo $pageitem->url(); ?>" data-uri="<?php echo $pageitem->uri(); ?>" data-title="<?php echo $pageitem->title(); ?>" class="main-nav-item section-link main-nav-<?php echo $pageitem->autoid(); ?>"><?php echo $item->pagetitle() != '' ? $item->pagetitle() : $pageitem->title() ; ?></a>
		    		                </li>
		    		                <?php endif ?>
		    		            <?php endforeach ?>
		    		        <?php elseif(($item->link() == 'url')): ?>
		    		            <li class="main-nav__list__item"><a class="external-link" href="<?php echo $item->pageurl(); ?>" data-title="<?php echo $item->label(); ?>"><?php echo $item->label(); ?></a></li>
		    		        <?php endif ?>
		    		    <?php endforeach ?>
			    	</ul>
			    	</nav>
			    </div>
			    <div class="is-col col-5 cis__nav-header__right-side">
			    	<?php 
			    		$email_addresses = $site->emailaddress()->toStructure();
			    	 ?>
			    	<?php if($email_addresses->count()): ?>
			    	<ul class="cis__nav-header__list--column">
				    	<?php foreach ($email_addresses as $email_address): ?>
				    		<?php foreach ($email_address->emaillink()->yaml() as $email): ?>
				    		    <li class="cis__nav-header__nav__big-anchor">
				    		    	<?php 
				    		    		$newoutput = preg_replace('/(@)/i', '<span class="blue-1">$1</span>', $email); 
				    		    	 ?>
				    		    	<a href="mailto:<?php echo $email; ?>" class="cis__nav-header__nav__big-anchor js-appear-on-menu-open external-link" target="_blank"><span class="underline-anchor"><?php echo $newoutput; ?></span></a>
				    		    </li>
				    		<?php endforeach ?>
				    	<?php endforeach ?>
			    	</ul>
			    	<?php endif ?>

			    	<?php if($site->address()!=''): ?>
			    	<div class="item-24"></div>
			    	<h4 class="js-appear-on-menu-open"><?php echo t('info'); ?></h4>
			    	<p class="js-appear-on-menu-open"><?php echo $site->address(); ?></p>
			    	<?php endif ?>

			    	<?php if($site->phone()->isNotEmpty()): ?>
			    		<?php 
			    			$newoutput = preg_replace('/(\+)/i', '<span class="blue-1">$1</span>', $site->phone()); 
			    		 ?>
			    		<p class="js-appear-on-menu-open"><?php echo $newoutput; ?></p>
			    	<?php endif ?>

			    	<div class="item-16"></div>
        	    	<?php if($site->social()->isNotEmpty()): ?>
        	    		<h4 class="js-appear-on-menu-open">Social Media</h4>
	        	    	<?php 
	        	    		$platforms = $site->social()->toStructure();
	        	    	 ?>
					    	<ul class="cis__nav-header__nav__list js-appear-on-menu-open">
		        	    	<?php foreach ($platforms as $platform): ?>
		        	    		<?php 
		        	    			switch ($platform->account()) {
		        	    				case 'instagram':
		        	    					$social_network = 'Instagram';
		        	    					$social_url = 'https://www.instagram.com/'.$platform->username();
	        	    	        			$social_icon = '<path class="button__svg__icon" d="M211,544.9a5.86,5.86,0,0,0-.37-1.94,4.11,4.11,0,0,0-2.34-2.34,5.86,5.86,0,0,0-1.94-.37c-.86,0-1.13,0-3.3,0s-2.44,0-3.3,0a5.86,5.86,0,0,0-1.94.37,4.14,4.14,0,0,0-2.34,2.34,5.86,5.86,0,0,0-.37,1.94c0,.85-.05,1.13-.05,3.3s0,2.45.05,3.3a5.86,5.86,0,0,0,.37,1.94,4.09,4.09,0,0,0,2.34,2.34,5.86,5.86,0,0,0,1.94.37c.85,0,1.13.05,3.3.05s2.44,0,3.3-.05a5.86,5.86,0,0,0,1.94-.37,4.11,4.11,0,0,0,2.34-2.34,5.86,5.86,0,0,0,.37-1.94c0-.85.05-1.13.05-3.3S211,545.76,211,544.9Zm-1.44,6.54a4.36,4.36,0,0,1-.27,1.48,2.65,2.65,0,0,1-1.52,1.52,4.21,4.21,0,0,1-1.48.27c-.84,0-1.1,0-3.23,0s-2.39,0-3.23,0a4.36,4.36,0,0,1-1.48-.27,2.65,2.65,0,0,1-1.52-1.52,4.21,4.21,0,0,1-.27-1.48c0-.84,0-1.1,0-3.23s0-2.39,0-3.23a4.36,4.36,0,0,1,.27-1.48A2.67,2.67,0,0,1,198.3,542a4.21,4.21,0,0,1,1.48-.27c.84,0,1.1-.05,3.23-.05s2.39,0,3.23.05a4.61,4.61,0,0,1,1.48.27,2.67,2.67,0,0,1,1.52,1.52,4.21,4.21,0,0,1,.27,1.48c0,.84.05,1.1.05,3.23s0,2.38-.05,3.23Z" transform="translate(-186.5 -531.7)"> </path> <path d="M203,544.09a4.11,4.11,0,1,0,4.11,4.11h0A4.11,4.11,0,0,0,203,544.09Zm0,6.78a2.67,2.67,0,1,1,2.67-2.67h0a2.69,2.69,0,0,1-2.67,2.67Z" transform="translate(-186.5 -531.7)"> </path> <path d="M208.23,543.93a1,1,0,1,1-1-1h0A1,1,0,0,1,208.23,543.93Z" transform="translate(-186.5 -531.7)"> </path>';
	        	    	        		break;
	        	    	        		case 'twitter':
	        	    	        		$social_network = 'Twitter';
	        	    	        			$social_url = 'https://www.twitter.com/'.$platform->username();
	        	    	        			$social_icon = '<path class="button__svg__icon" d="M152.15,614.71a7.68,7.68,0,0,1-2,.55,3.45,3.45,0,0,0,1.54-1.92,7,7,0,0,1-2.21.84,3.49,3.49,0,0,0-6,2.38,4.15,4.15,0,0,0,.08.79,9.8,9.8,0,0,1-7.17-3.63,3.47,3.47,0,0,0,1.07,4.65,3.45,3.45,0,0,1-1.57-.43v0a3.5,3.5,0,0,0,2.78,3.43,3.49,3.49,0,0,1-.91.11,3.68,3.68,0,0,1-.66-.06,3.49,3.49,0,0,0,3.25,2.42,7,7,0,0,1-4.32,1.48,5.92,5.92,0,0,1-.84-.05A9.91,9.91,0,0,0,150.39,617c0-.16,0-.31,0-.45A6.45,6.45,0,0,0,152.15,614.71Z" transform="translate(-127.15 -603.47)"></path>';
	        	    	        		break;
	        	    	        		case 'facebook':
	        	    	        			$social_network = 'Facebook';
	        	    	        			if ($platform->fburl()->isNotEmpty()) {
	        	    	        				$social_url = $platform->fburl();
	        	    	        			} else {
	        	    	        				$social_url = 'https://www.facebook.com/'.$platform->username();
	        	    	        			}
	        	    	        			$social_icon = '<path class="button__svg__icon" d="M251.27,605.29a9.06,9.06,0,1,0-10.47,9v-6.33h-2.3v-2.62h2.3v-2a3.19,3.19,0,0,1,3.42-3.52,13.77,13.77,0,0,1,2,.18v2.23h-1.14a1.31,1.31,0,0,0-1.48,1.41v1.7h2.51l-.4,2.62h-2.11v6.33a9.07,9.07,0,0,0,7.64-9Z" transform="translate(-225.71 -588.74)" />';
	        	    	        		break;
	        	    	        		case 'youtube':
	    	    	        				$social_network = 'YouTube';
	        	    	        			if ($platform->yturl()->isNotEmpty()) {
	        	    	        				$social_url = $platform->yturl();
	        	    	        			} else {
	        	    	        				$social_url = 'https://youtube.com/'.$platform->username();
	        	    	        			}
	        	    	        			$social_icon = '<g class="button__svg__icon" transform="matrix(7.8701756,0,0,7.8701756,695.19553,-948.4235)"> <path d="M-85.28,122.49c0,0-0.95,0-1.19,0.06c-0.13,0.04-0.23,0.14-0.27,0.27c-0.06,0.24-0.06,0.73-0.06,0.73 s0,0.5,0.06,0.73c0.04,0.13,0.14,0.23,0.27,0.27c0.24,0.06,1.19,0.06,1.19,0.06s0.95,0,1.19-0.06c0.13-0.04,0.23-0.14,0.27-0.27 c0.06-0.24,0.06-0.73,0.06-0.73s0-0.5-0.06-0.74c-0.03-0.13-0.14-0.23-0.27-0.27C-84.33,122.49-85.28,122.49-85.28,122.49z M-85.59,123.1l0.79,0.46l-0.79,0.46V123.1z"/> </g>';
	        	    	        		break;
	        	    	        		case 'linkedin':
	    	    	        				$social_network = 'LinkedIn';
	        	    	        			if ($platform->yturl()->isNotEmpty()) {
	        	    	        				$social_url = $platform->yturl();
	        	    	        			} else {
	        	    	        				$social_url = 'https://www.linkedin.com/company/'.$platform->username();
	        	    	        			}
	        	    	        			$social_icon = '<path class="button__svg__icon" d="M91,559.93H77.87a2.46,2.46,0,0,0-2.46,2.46v13.09a2.46,2.46,0,0,0,2.46,2.45H91a2.45,2.45,0,0,0,2.45-2.45V562.39A2.46,2.46,0,0,0,91,559.93Zm-9.41,14.24a.39.39,0,0,1-.38.39H79.55a.38.38,0,0,1-.38-.38h0V567.4a.38.38,0,0,1,.38-.38h1.62a.38.38,0,0,1,.38.38Zm-1.19-7.8a1.53,1.53,0,1,1,1.53-1.53A1.52,1.52,0,0,1,80.36,566.37ZM90,574.21a.35.35,0,0,1-.35.35H87.91a.35.35,0,0,1-.35-.35h0V571c0-.48.14-2.08-1.24-2.08-1.07,0-1.29,1.1-1.33,1.59v3.67a.35.35,0,0,1-.34.35H83a.35.35,0,0,1-.35-.35h0v-6.84A.34.34,0,0,1,83,567h1.68a.35.35,0,0,1,.35.35V568a2.36,2.36,0,0,1,2.24-1.06c2.78,0,2.76,2.6,2.76,4Z" transform="translate(-67.91 -552.43)"></path>';
	        	    	        		break;
		        	    				default:
		        	    					# code...
		        	    					break;
		        	    			}
		        	    		 ?>
		        	    		<?php if($platform->footer()->bool()): ?>
		        	    		<li class="cis__nav-header__nav__list__item--no-animation">
		        	    			<a href="<?php echo $social_url; ?>" target="_blank">
		        	    				<svg viewbox="0 0 33 33" xmlns="http://www.w3.org/2000/svg">
		        	    				    <circle class="social-svg__circle" cx="16.5" cy="16.5" r="16" />
		        	    				    <?php echo $social_icon; ?>
		        	    				</svg>
		        	    			</a>
		        	    		</li>
		        	    		<?php //echo $social_icon; ?>
			        	    	<?php endif ?>
		        	    	<?php endforeach ?>
	        	    		</ul>
        	    	<?php endif ?>
			    </div>
			</div>
			<div class="item-64"></div>
			<div class="is-row is-flex column-padding js-appear-on-menu-open">
			    <div class="is-col col-5_5 offset-_5">
			    	<?php 
			    		$legal_links = $site->footerLegal()->toStructure();
			    	 ?>
			    	<?php if($legal_links->count()): ?>
			    	<ul class="cis__nav-header__list">
		    		    <?php $languages = $kirby->languages(); ?>
				    	<?php foreach ($legal_links as $legal_item): ?>

				    		<?php foreach ($legal_item->pagelink()->yaml() as $legal_item_pagelink): ?>
				    		    <?php if(page($legal_item_pagelink)): ?>
				    			<?php $legal_item_page = page($legal_item_pagelink); ?>	
				    		    <li class="cis__nav-header__list__item">
				    		    	<a href="<?php echo $legal_item_page->url(); ?>" class="section-link"><?php echo $legal_item_page->title(); ?></a>
				    		    </li>
				    		    <?php endif ?>
				    		<?php endforeach ?>

				    	<?php endforeach ?>
	    		    	
	    		    	<li class="cis__nav-header__list__item">—</li>

	    		    	<?php foreach ($languages->flip() as $language): ?>
	    		    		<li class="cis__nav-header__list__item cis__nav-header__list__item--language<?php echo $language == $kirby->language() ? ' active-language' :''; ?>">
	    		    			<a href="<?php echo $language->url() ?>" class="same-window"><?php echo strtoupper($language->code()); ?></a>
	    		    		</li>
	    		    	<?php endforeach ?>
			    	</ul>
			    	<?php endif ?>
			    </div>
			    <div class="is-col col-5">
			    	<div class="is-row is-flex">
			    	    <div class="is-col col-6">
			    	    	<p class="cis__nav-header__small"><?php echo $site->footerCopyright() == 'custom' ? $site->footerCopyrightCustom()->kirbytextinline() . ' ' : '©'. date('Y').', '.$site->title().'.' ?></p>			    	    	
			    	    </div>
			    	    <div class="is-col col-6">
			    	    	<!-- <p class="cis__footer__legal-credits">Designed in collaboration with <a href="https://invasoria.com" class="hover-underline-animation external-link" target="_blank" title="invasoria.com">Iván Soria</a></p> -->
			    	    </div>
			    	</div>
			    </div>
			</div>
		</div>
	</div>
	<div class="cis__nav-header__column-container">
		<div class="cis__nav-header__column js-nav-header-column"></div>
		<div class="cis__nav-header__column js-nav-header-column"></div>
		<div class="cis__nav-header__column js-nav-header-column"></div>
		<div class="cis__nav-header__column js-nav-header-column"></div>
		<div class="cis__nav-header__column js-nav-header-column"></div>
	</div>
</header>
<!-- MAIN HEADER AND NAV -->