<?php if(!array_key_exists('HTTP_X_KIRBY_FETCH', $_SERVER)): ?>

    <?php snippet('default.header'); ?>
    <?php snippet('default.content'); ?>

    <!-- CONTENT LOADING CONTAINER -->
    <main class="main fetch-container" id="fetch-container" data-id="main" data-root="<?php echo $site->url(); ?>" data-title="<?php echo $site->title(); ?>"></main>
    <!-- CONTENT LOADING CONTAINER -->

<?php endif ?>


<?php if(array_key_exists('HTTP_X_KIRBY_FETCH', $_SERVER)): ?>

    <div class="page-builder" id="new-section" data-id="<?php echo $page->autoid(); ?>" data-template="<?php echo $page->intendedTemplate(); ?>" data-url="<?php echo $page->url(); ?>" data-uri="<?php echo htmlspecialchars($page->uri()); ?>" data-title="<?php echo htmlspecialchars($page->title()); ?>">

        <section class="--section grid-margin --section-bg--light --padding-top-160">
            
            
            <div class="is-row is-flex column-padding --padding-bottom-120">
                <div class="is-col col-11 offset-_5">
                    <h1 class="js-observe reveal-text-animation"><?php echo $page->title(); ?></h1>
                    <!-- LAYOUT ROW -->
                    <div class="is-row is-row--special-gap is-flex --padding-top-80  --padding-bottom-80 --padding-mobile-top-40 --padding-mobile-bottom-40">
                        <!-- SERVICES BLOCK -->
                        <?php snippet('modules/services.dummy.block', ['items' => $page]); ?>
                        <!-- SERVICES BLOCK -->
                    </div>
                    <!-- LAYOUT ROW -->
                    <div class="item-40"></div>
                    <div class="--divider"></div>
                </div>
            </div>
            <!-- CTA BLOCK -->
            <?php snippet('modules/cta.block', ['source' => $site]); ?>
            <!-- CTA BLOCK -->
        </section>
    </div>
    

<?php endif ?>

<?php if(!array_key_exists('HTTP_X_KIRBY_FETCH', $_SERVER)): ?>

    <?php snippet('footer')?>
    <?php snippet('footer.code')?>

<?php endif ?>