<?php if(!array_key_exists('HTTP_X_KIRBY_FETCH', $_SERVER)): ?>

    <?php snippet('default.header'); ?>
    <?php snippet('default.content'); ?>

    <!-- CONTENT LOADING CONTAINER -->
    <main class="main fetch-container" id="fetch-container" data-id="main" data-root="<?php echo $site->url(); ?>" data-title="<?php echo $site->title(); ?>"></main>
    <!-- CONTENT LOADING CONTAINER -->

<?php endif ?>


<?php if(array_key_exists('HTTP_X_KIRBY_FETCH', $_SERVER)): ?>

    <div class="page-builder cis__contact" id="new-section" data-id="<?php echo $page->autoid(); ?>" data-template="<?php echo $page->intendedTemplate(); ?>" data-url="<?php echo $page->url(); ?>" data-uri="<?php echo htmlspecialchars($page->uri()); ?>" data-title="<?php echo htmlspecialchars($page->title()); ?>">


        <section class="--section grid-margin --section-bg--light">
            <div class="is-row is-flex column-padding">
                <div class="is-col col-11 offset-_5">
                    <!-- LAYOUT ROW -->
                    <div class="is-row is-flex --padding-top-240 --padding-bottom-80 --padding-mobile-top-160 --padding-mobile-bottom-80">
                        <!-- COLUMN -->
                        <div class="is-col col-12 offset-0 md-col-12 md-offset-0 xs-col-12 xs-offset-0 --color-underline--yellow --color-highlight--blue content__modular-block-padding --z-index-30">
                            
                            <form class="cis__form">
                                <div class="is-row is-flex">
                                    <div class="is-col col-12">
                                        <h1>Hola,</h1>
                                    </div>
                                </div>

                                <div class="is-row is-flex">
                                    <div class="is-col col-12">
                                        <h1>Soy</h1>


                                        <div class="input-sizer">
                                            <input type="text" class="form-control" name="name" id="name" onInput="this.parentNode.dataset.value = this.value" size="7" placeholder="Nombre" required>
                                            <label for="user" class="animated-label">Nombre</label>
                                        </div>

                                        <div class="input-sizer">
                                          <input type="text" class="form-control" name="lastname" id="lastname" onInput="this.parentNode.dataset.value = this.value" size="7" placeholder="Apellido" required>
                                          <label for="user" class="animated-label">Apellido</label>
                                        </div>
                                        
                                        <h1>,</h1>
                                        <h1>trabajo en</h1>

                                        <div class="input-sizer">
                                          <input type="text" class="form-control" name="company" id="company" onInput="this.parentNode.dataset.value = this.value" size="9" placeholder="Compañía" required>
                                          <label for="user" class="animated-label">Compañía</label>
                                        </div>

                                        <h1>,</h1>
                                        <h1>donde</h1>
                                        <h1>soy</h1>

                                        <div class="input-sizer">
                                          <input type="text" class="form-control" name="position" id="position" onInput="this.parentNode.dataset.value = this.value" size="6" placeholder="Puesto" required>
                                          <label for="user" class="animated-label">Puesto</label>
                                        </div>

                                        <h1>.</h1>

                                        <h1>Mi</h1>
                                        <h1>correo</h1>
                                        <h1>es</h1>

                                        <div class="input-sizer">
                                          <input type="email" class="form-control" name="email" id="email" onInput="this.parentNode.dataset.value = this.value" size="5" placeholder="Email" required>
                                          <label for="user" class="animated-label">Email</label>
                                        </div>

                                        <h1>&nbsp;</h1>
                                        <h1>y</h1>
                                        <h1>mi</h1>
                                        <h1>teléfono</h1>
                                        <h1>es</h1>

                                        <div class="input-sizer">
                                          <input type="tel" class="form-control" id="phone" name="phone" onInput="this.parentNode.dataset.value = this.value" size="7" placeholder="Número" required>
                                          <label for="user" class="animated-label">Número</label>
                                        </div>
                                        <h1>.</h1>

                                        <h1>Estoy</h1>
                                        <h1>interesadx</h1>
                                        <h1>en</h1>
                                        <h1>recibir</h1>
                                        <h1>información de</h1>

                                        <div class="input-sizer">
                                          <input type="text" class="form-control" name="service" id="service" onInput="this.parentNode.dataset.value = this.value" size="7" placeholder="Servicio" required>
                                          <label for="user" class="animated-label">Servicio</label>
                                        </div>

                                        <h1>.</h1>



                                        <!-- 

                                        <label class="input-sizer">
                                          <select onInput="this.parentNode.dataset.value = this.value" size="8" >
                                              <option value="a">a</option>
                                              <?php foreach (page('servicios')->children()->children()->listed() as $item): ?>
                                                  <option value="<?php echo $item->autoid(); ?>"><?php echo $item->title(); ?></option>
                                              <?php endforeach ?>
                                          </select>
                                        </label>
                                         -->

                                         


                                    </div>
                                </div>
                            </form>

                            <!-- 
                            <form class="test__form">
                                <h1>Hola,</h1>
                                <h1>Soy <span class="input-wrap"><span class="width-machine" aria-hidden="true">Nombre</span><input class="input" value="Nombre" type="text"></span>, trabajo en <span class="input-wrap"><span class="width-machine" aria-hidden="true">Compañía</span><input class="input" name="company" placeholder="Company" value="" type="text"></span></h1>
                            </form>
                             -->

                        </div>
                        <!-- COLUMN -->
                    </div>
                </div>
            </div>
        </section>

        <!-- BLOCKS -->
        <?php snippet('blocks', ['page' => $page->blocks()->blocks()]) ?>
        <!-- BLOCKS -->

    </div>
    

<?php endif ?>

<?php if(!array_key_exists('HTTP_X_KIRBY_FETCH', $_SERVER)): ?>

    <?php snippet('footer')?>
    <?php snippet('footer.code')?>

<?php endif ?>