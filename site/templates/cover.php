<?php if(!array_key_exists('HTTP_X_KIRBY_FETCH', $_SERVER)): ?>

    <?php snippet('default.header'); ?>
    <?php snippet('default.content'); ?>

    <!-- CONTENT LOADING CONTAINER -->
    <main class="main fetch-container" id="fetch-container" data-id="main" data-root="<?php echo $site->url(); ?>" data-title="<?php echo $site->title(); ?>"></main>
    <!-- CONTENT LOADING CONTAINER -->

<?php endif ?>


<?php if(array_key_exists('HTTP_X_KIRBY_FETCH', $_SERVER)): ?>

    <div class="cover cis__page" id="new-section" data-id="<?php echo $page->autoid(); ?>" data-template="<?php echo $page->intendedTemplate(); ?>" data-url="<?php echo $page->url(); ?>" data-uri="<?php echo htmlspecialchars($page->uri()); ?>" data-title="<?php echo htmlspecialchars($page->title()); ?>">
        
        <!-- HERO SECTION -->
        <?php snippet('concept.hero') ?>
        <!-- HERO SECTION -->

        <!-- PAGE CONTAINER -->
        <div class="cis__page__container">
            <div class="cis__container grid-margin">
                <!-- NAVIGATION -->
                <?php //snippet('nav.section') ?>
                <!-- NAVIGATION -->

                <!-- OUR PROMISE -->
                <?php snippet('modules/about.block', ['source' => $site]); ?>
                <!-- OUR PROMISE -->

                <!-- VALUE BLOCK -->
                <?php snippet('modules/clients.block', ['source' => $site]); ?>
                <!-- VALUE BLOCK -->

                <?php 
                    $articles_count = page('thinking')->children()->listed()->count();
                    $articles = page('thinking')->children()->listed()->flip()->paginate(12);
                 ?>
                <section class="--section grid-margin --section-bg--light overflow-hidden">    
                    <div class="is-row is-flex column-padding">
                        <div class="is-col col-11 offset-_5">

                            <!-- LAYOUT ROW -->
                            <div class="is-row is-row--special-gap is-flex --padding-top-80  --padding-bottom-0 --padding-mobile-top-40 --padding-mobile-bottom-0">
                                <!-- <div class="is-col col-6 red-bg">...</div> -->
                                <?php foreach ($articles as $article): ?>
                                <!-- ARTICLE COLUMN -->
                                <?php if($article->posttype()->isNotEmpty() && $article->posttype() == 'news'): ?>
                                    <?php snippet('modules/card.news', ['article' => $article, 'special_grid' => true ]); ?>
                                <?php elseif($article->posttype()->isNotEmpty() && $article->posttype() == 'page'): ?>
                                    <?php snippet('modules/card.page', ['article' => $article, 'special_grid' => true ]); ?>
                                <?php elseif($article->posttype()->isNotEmpty() && $article->posttype() == 'post'): ?>
                                    <?php snippet('modules/card.post', ['article' => $article, 'special_grid' => true ]); ?>
                                <?php endif ?>
                                <!-- ARTICLE COLUMN -->
                                <?php endforeach ?>
                            </div>
                          <!-- LAYOUT ROW -->

                        </div>
                    </div>

                    <div class="is-row is-flex column-padding">
                        <div class="is-col offset-_5 col-11 --padding-80 centered-content">
                            <!-- SECTION LINK -->
                            <a href="<?php echo page('thinking')->url(); ?>" class="js-observe fade-and-slide-animation cis__arrow-cta cis__arrow-cta--red section-link" data-uri="<?php echo page('servicios')->uri(); ?>" data-title="<?php echo page('servicios')->title(); ?>" data-text="Descubre más Insights e Historias">
                                <span class="cis__arrow-cta__deco-bar"></span>
                                <span class="cis__arrow-cta__text">Descubre más Insights e Historias</span>
                                <span class="cis__arrow-cta__arrow">
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="19.81px"height="15.41px" viewBox="0 0 19.81 15.41" style="overflow:visible;enable-background:new 0 0 19.81 15.41;"xml:space="preserve"><path class="cis__arrow__st0" d="M9.96,2.19l3.01,3.01l1.66,0.8H0v3.33h14.55L13,10.1l-3.06,3.05l2.11,2.26l7.76-7.74 L12.15,0L9.96,2.19z"/> </svg>
                                </span>
                                <span class="cis__arrow-cta__ellipse">
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="80px"height="80px" viewBox="0 0 80 80" style="overflow:visible;enable-background:new 0 0 80 80;" xml:space="preserve"> <defs> </defs><circle class="cis__arrow-cta__ellipse__st0" cx="40" cy="40" r="40"/> <circle class="cis__arrow-cta__ellipse__st1" cx="40" cy="40" r="39"/> </svg>
                                </span>
                            </a>
                            <!-- SECTION LINK -->
                        </div>
                    </div>
                </section>

                <!-- SERVICES BLOCK -->
                <?php snippet('modules/services.block', ['items' => page('servicios')]); ?>
                <!-- SERVICES BLOCK -->

                <!-- SERVICES BLOCK -->
                <?php snippet('modules/cta.block', ['source' => $site]); ?>
                <!-- SERVICES BLOCK -->

            </div>
        </div>
        <!-- PAGE CONTAINER -->
    </div>


<?php endif ?>

<?php if(!array_key_exists('HTTP_X_KIRBY_FETCH', $_SERVER)): ?>

    <?php snippet('footer')?>
    <?php snippet('footer.code')?>

<?php endif ?>