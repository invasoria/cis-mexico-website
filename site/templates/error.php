<?php if(!array_key_exists('HTTP_X_KIRBY_FETCH', $_SERVER)): ?>

    <?php snippet('default.header'); ?>
    <?php snippet('default.content'); ?>

    <!-- CONTENT LOADING CONTAINER -->
    <main class="main fetch-container" id="fetch-container" data-id="main" data-root="<?php echo $site->url(); ?>" data-title="<?php echo $site->title(); ?>"></main>
    <!-- CONTENT LOADING CONTAINER -->

<?php endif ?>


<?php if(array_key_exists('HTTP_X_KIRBY_FETCH', $_SERVER)): ?>

    <section class="default" id="new-section" data-id="<?php echo $page->autoid(); ?>" data-template="<?php echo $page->intendedTemplate(); ?>" data-url="<?php echo $page->url(); ?>" data-uri="<?php echo htmlspecialchars($page->uri()); ?>" data-title="<?php echo htmlspecialchars($page->title()); ?>" style="position: relative; height: 100%; width: 100%;">
        <section class="cis__hero grid-margin" id="cis-hero">
            <div class="cis__hero__heading cis--padding-200">
                <div class="is-row is-flex column-padding">
                    <div class="is-col col-5 offset-1">
                        <h1><?php echo $page->title(); ?></h1>
                        <?php foreach ($site->pages() as $item): ?>
                        <a href="<?php echo $item->url(); ?>" data-title="<?php echo $item->title(); ?>" data-uri="<?php echo $item->uri(); ?>"><?php echo $item->title(); ?></a><br>
                        <?php endforeach ?>
                        <div class="item-160"></div>
                        <div class="item-160"></div>
                    </div>
                    <div class="is-col col-5 offset-1">
                        
                    </div>
                </div>
            </div>
        </section>
    </section>
    
    <?php snippet('footer')?>

<?php endif ?>

<?php if(!array_key_exists('HTTP_X_KIRBY_FETCH', $_SERVER)): ?>

    <?php snippet('footer.code')?>

<?php endif ?>