<?php if(!array_key_exists('HTTP_X_KIRBY_FETCH', $_SERVER)): ?>

    <?php snippet('default.header'); ?>
    <?php snippet('default.content'); ?>

    <!-- CONTENT LOADING CONTAINER -->
    <main class="main fetch-container" id="fetch-container" data-id="main" data-root="<?php echo $site->url(); ?>" data-title="<?php echo $site->title(); ?>"></main>
    <!-- CONTENT LOADING CONTAINER -->

<?php endif ?>


<?php if(array_key_exists('HTTP_X_KIRBY_FETCH', $_SERVER)): ?>

    <section class="default" id="new-section" data-template="<?php echo $page->intendedTemplate(); ?>" data-url="<?php echo $page->url(); ?>" data-uri="<?php echo htmlspecialchars($page->uri()); ?>" data-title="<?php echo htmlspecialchars($page->title()); ?>">
        <div class="container grid-margin">
            <div class="is-row is-flex">
                <div class="is-col col-6">
                    <h1><?php echo $page->title(); ?></h1>
                    <?php foreach ($site->pages() as $item): ?>
                        <a href="<?php echo $item->url(); ?>" class="section-link" data-uri="<?php echo $item->uri() ?>" data-title="<?php echo $item->title() ?>"><?php echo $item->title(); ?></a>
                    <?php endforeach ?>
                    <?php foreach ($page->children() as $item): ?>
                        <a href="<?php echo $item->url(); ?>" class="section-link" data-uri="<?php echo $item->uri() ?>" data-title="<?php echo $item->title() ?>"><?php echo $item->title(); ?></a>
                    <?php endforeach ?>
                </div>
            </div>
        </div>
    </section>

<?php endif ?>

<?php if(!array_key_exists('HTTP_X_KIRBY_FETCH', $_SERVER)): ?>

    <?php snippet('footer')?>
    <?php snippet('footer.code')?>

<?php endif ?>