<?php if(!array_key_exists('HTTP_X_KIRBY_FETCH', $_SERVER)): ?>

    <?php snippet('default.header'); ?>
    <?php snippet('default.content'); ?>

    <!-- CONTENT LOADING CONTAINER -->
    <main class="main fetch-container" id="fetch-container" data-id="main" data-root="<?php echo $site->url(); ?>" data-title="<?php echo $site->title(); ?>"></main>
    <!-- CONTENT LOADING CONTAINER -->

<?php endif ?>


<?php if(array_key_exists('HTTP_X_KIRBY_FETCH', $_SERVER)): ?>

    <div class="page-builder" id="new-section" data-id="<?php echo $page->autoid(); ?>" data-template="<?php echo $page->intendedTemplate(); ?>" data-url="<?php echo $page->url(); ?>" data-uri="<?php echo htmlspecialchars($page->uri()); ?>" data-title="<?php echo htmlspecialchars($page->title()); ?>">
        <?php 
            $articles_count = $page->children()->listed()->count();
            $articles = $page->children()->listed()->flip()->paginate(12);
         ?>
        <section class="--section">
            <div class="container">

                <!-- BLOCKS -->
                <?php snippet('blocks', ['page' => $page->blocks()->blocks()]) ?>
                <!-- BLOCKS -->

            </div>
        </section>

        <section class="--section grid-margin --section-bg--light overflow-hidden">
            <div class="is-row is-flex column-padding">
                <div class="is-col col-11 offset-_5">

                    <!-- LAYOUT ROW -->
                    <div class="is-row is-row--special-gap is-flex --padding-top-80  --padding-bottom-80 --padding-mobile-top-40 --padding-mobile-bottom-40">
                        <!-- <div class="is-col col-6 red-bg">...</div> -->
                        <?php foreach ($articles as $article): ?>
                        <?php if($article->intendedTemplate() == 'reporte'): ?>
                        <!-- ARTICLE COLUMN -->
                        <?php snippet('modules/card.post', ['article' => $article, 'special_grid' => true ]); ?>
                        <!-- ARTICLE COLUMN -->
                        <?php endif ?>
                        <?php endforeach ?>
                    </div>
                  <!-- LAYOUT ROW -->

                </div>
            </div>

            <!-- SERVICES BLOCK -->
            <?php snippet('modules/cases.dummy.block', ['items' => page('portafolio')]); ?>
            <!-- SERVICES BLOCK -->

            <!-- CTA BLOCK -->
            <?php //snippet('modules/cta.block', ['source' => $site]); ?>
            <!-- CTA BLOCK -->

        </section>
    </div>
    

<?php endif ?>

<?php if(!array_key_exists('HTTP_X_KIRBY_FETCH', $_SERVER)): ?>

    <?php snippet('footer')?>
    <?php snippet('footer.code')?>

<?php endif ?>