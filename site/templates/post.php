<?php if(!array_key_exists('HTTP_X_KIRBY_FETCH', $_SERVER)): ?>

    <?php snippet('default.header'); ?>
    <?php snippet('default.content'); ?>

    <!-- CONTENT LOADING CONTAINER -->
    <main class="main fetch-container" id="fetch-container" data-id="main" data-root="<?php echo $site->url(); ?>" data-title="<?php echo $site->title(); ?>"></main>
    <!-- CONTENT LOADING CONTAINER -->

<?php endif ?>


<?php if(array_key_exists('HTTP_X_KIRBY_FETCH', $_SERVER)): ?>

    <article class="article" id="new-section" data-id="<?php echo $page->autoid(); ?>" data-template="<?php echo $page->intendedTemplate(); ?>" data-url="<?php echo $page->url(); ?>" data-uri="<?php echo htmlspecialchars($page->uri()); ?>" data-title="<?php echo htmlspecialchars($page->title()); ?>">

        <!-- LAYOUT SECTION -->
        <section class="--section grid-margin --section-bg--light overflow-hidden">
            <div class="is-row is-flex column-padding">
                <div class="is-col col-11 offset-_5">

                    <!-- LAYOUT ROW -->
                    <div class="is-row is-flex --padding-top-240 --padding-bottom-80 --padding-mobile-top-120 --padding-mobile-bottom-40">
                        <!-- COLUMN -->
                        <div class="is-col col-7 offset-2_5">
                            <p class="article__category xs js-observe fade-animation">
                            <?php foreach ($page->categories()->split(',') as $category): ?>
                                <strong><?php echo $category; ?></strong>
                            <?php endforeach ?>
                            </p>
                            <h1 class="content__h1 content__left content__no-top-margin content__no-bottom-margin js-observe fade-animation reveal-text-animation"><?php echo $page->title(); ?></h1>
                            <?php if($page->author()->isNotEmpty() && $author = $page->author()->toUser()): ?>
                            
                            <?php setlocale(LC_TIME, 'es_MX.utf8'); ?>
                            <div class="article__author-list">
                                <?php if($author->picture()->isNotEmpty() && $avatar = $author->picture()->toFile()): ?>
                                <li class="article__author-list__item article__author-list__item--avatar"><?php echo $avatar; ?></li>
                                <?php endif ?>
                                <li class="article__author-list__item article__author-list__item--name"><p class="s content__no-bottom-margin js-observe fade-animation">Por <?php echo $author->firstName(); ?> <?php echo $author->lastName(); ?>. <?php echo strftime('%B %e, %Y', strtotime($page->date())); ?>.</p></li>
                            </div>

                            <?php endif ?>

                            <!-- SHARE POST -->
                            <div class="cis__card__share js-observe fade-animation">
                                <a href="<?php echo $page ? 'https://twitter.com/intent/tweet?source='.urlencode($page->url()) . '&text=' . urlencode($page->title()) . '%20' . urlencode($page->url()) . '&via=cismx' : '#'?>" class="cis__card__icon-button cis__card__icon-button--twitter external-link" target="_blank">
                                    <svg class="" x="0px" y="0px" width="32px"
                                         height="32px" viewBox="0 0 32 32" style="overflow:visible;enable-background:new 0 0 32 32;" xml:space="preserve">
                                        <path class="ener__button__svg__icon" d="M24,11.04c-0.6,0.26-1.24,0.44-1.89,0.52c0.69-0.41,1.2-1.05,1.44-1.81c-0.64,0.38-1.35,0.65-2.08,0.79
                                        c-1.24-1.32-3.31-1.39-4.64-0.15c-0.66,0.62-1.04,1.49-1.04,2.39c0,0.25,0.02,0.5,0.08,0.75c-2.64-0.13-5.1-1.38-6.76-3.43
                                        c-0.88,1.5-0.44,3.42,1.01,4.39c-0.52-0.01-1.03-0.15-1.48-0.41v0.04c0,1.56,1.1,2.91,2.63,3.22c-0.28,0.07-0.57,0.11-0.86,0.11
                                        c-0.21,0-0.42-0.01-0.62-0.06c0.43,1.33,1.66,2.25,3.07,2.29c-1.16,0.91-2.59,1.4-4.07,1.4c-0.26,0-0.52-0.02-0.78-0.05
                                        c4.34,2.78,10.12,1.51,12.9-2.83c0.96-1.5,1.47-3.25,1.47-5.03c0-0.14,0-0.29-0.01-0.42C23,12.28,23.56,11.7,24,11.04z"/>
                                    </svg>
                                </a>
                                <a href="<?php echo $page ? 'https://www.facebook.com/sharer/sharer.php?u='.urlencode($page->url()) : '#'?>" class="cis__card__icon-button cis__card__icon-button--facebook external-link<?php echo $page ? '' : ' ener__button--inactive'?>" target="_blank">
                                    <svg class="" x="0px" y="0px" width="32px"
                                         height="32px" viewBox="0 0 32 32" style="overflow:visible;enable-background:new 0 0 32 32;" xml:space="preserve">
                                        <path class="ener__button__svg__icon" d="M24,16.05C24,11.6,20.42,8,16,8s-8,3.6-8,8.05c0,4.02,2.93,7.35,6.75,7.95v-5.62h-2.03v-2.33h2.03v-1.77
                                            c0-2.02,1.19-3.13,3.02-3.13c0.88,0,1.79,0.16,1.79,0.16v1.98h-1.01c-0.99,0-1.3,0.62-1.3,1.26v1.51h2.22l-0.35,2.33h-1.86V24
                                            C21.07,23.4,24,20.07,24,16.05L24,16.05z"/>
                                    </svg>
                                </a>
                                <a href="<?php echo $page ? 'https://www.linkedin.com/sharing/share-offsite/?url='.urlencode($page->url()) : '#'?>" class="cis__card__icon-button cis__card__icon-button--linked-in external-link<?php echo $page ? '' : ' ener__button--inactive'?>" target="_blank">
                                    <svg class="" x="0px" y="0px" width="32px"
                                         height="32px" viewBox="0 0 32 32" style="overflow:visible;enable-background:new 0 0 32 32;" xml:space="preserve">
                                        <path class="ener__button__svg__icon" d="M22.77,8H9.23C8.59,7.99,8.06,8.51,8.05,9.15c0,0,0,0,0,0v13.7c0.01,0.64,0.53,1.16,1.17,1.15c0,0,0,0,0,0 h13.54c0.64,0.01,1.17-0.51,1.17-1.15c0,0,0,0,0,0V9.15C23.94,8.51,23.41,7.99,22.77,8z M10.47,21.39v-7.22h2.4v7.22H10.47z M11.67,13.18h-0.02c-0.76,0-1.37-0.62-1.36-1.38c0-0.76,0.62-1.37,1.38-1.36c0.76,0,1.37,0.62,1.36,1.38 C13.03,12.56,12.42,13.17,11.67,13.18z M21.53,21.39h-2.4v-3.86c0-0.97-0.35-1.63-1.22-1.63c-0.56,0-1.05,0.35-1.24,0.88 c-0.06,0.19-0.09,0.39-0.08,0.59v4.03h-2.4c0,0,0.03-6.55,0-7.22h2.4v1.02c0.44-0.77,1.28-1.23,2.16-1.19 c1.58,0,2.76,1.03,2.76,3.25L21.53,21.39z"/>
                                    </svg>
                                </a>
                                <a href="<?php echo $page->url(); ?>" class="cis__card__icon-button cis__card__icon-button--copy-link copy-link">
                                    <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path d="M343.37,372l-6.5,6.51,0,0a9.29,9.29,0,0,1,4.78.52l4.41-4.42a3.75,3.75,0,0,1,5.31,5.31c-.25.25-6.88,6.88-6.51,6.5a3.78,3.78,0,0,1-5.3,0,1.88,1.88,0,0,0-2.65,0l-1.14,1.14a7.14,7.14,0,0,0,1.14,1.51,7.55,7.55,0,0,0,10.56,0l0,0,6.51-6.5A7.5,7.5,0,0,0,343.37,372Z" transform="translate(-324.17 -369.82)"/><path d="M338.75,392.54,334.33,397a3.75,3.75,0,1,1-5.31-5.31l6.52-6.51a3.77,3.77,0,0,1,5.3,0,1.89,1.89,0,0,0,2.66,0l1.13-1.14a7.14,7.14,0,0,0-1.13-1.52,7.56,7.56,0,0,0-10.57,0l0,0L326.37,389A7.5,7.5,0,0,0,337,399.62l6.52-6.52,0,0a9.52,9.52,0,0,1-4.78-.52Z" transform="translate(-324.17 -369.82)"/></svg>
                                    <div class="cis__card__icon-button__tooltip copy-link-tooltip"><span><?php echo t('copied'); ?></span></div>
                                </a>
                            </div>
                            <!-- SHARE POST -->
                        </div>
                        <!-- COLUMN -->
                    </div>
                    <!-- LAYOUT ROW -->

                    <!-- LAYOUT ROW -->
                    <div class="is-row is-flex --padding-bottom-80 --padding-mobile-bottom-80">
                        <!-- COLUMN -->
                        <div class="is-col col-7 offset-2_5">
                            <h3 class="js-observe reveal-text-animation --color-underline--yellow --color-hightlight--blue"><?php echo $page->intro(); ?></h3>
                            <div class="is-row is-flex">
                                <?php echo $page->blocks()->toBlocks(); ?>
                            </div>
                        </div>
                    </div>
                    <!-- BLOCKS -->
                    <?php snippet('blocks', ['page' => $page->blocks()->blocks()]) ?>
                    <!-- BLOCKS -->

                </div>
            </div>
        </section>
        <!-- LAYOUT SECTION -->


        <!-- MORE ARTICLES -->
        <section class="article__functions --section grid-margin --section-bg--light overflow-hidden">
            <div class="is-row is-flex column-padding">
                <div class="is-col col-11 offset-_5">
                    <!-- LAYOUT ROW -->
                    <div class="is-row is-flex">
                        <!-- COLUMN -->
                        <div class="is-col col-2_5">
                            <h3 class="js-observe reveal-text-animation --color-underline--yellow --color-hightlight--blue"><?php echo t('more-thinking'); ?></h3>
                        </div>
                        <div class="is-col col-9_5 relative">
                            <?php 
                                $posts = page('thinking')->children()->listed()->limit(5);
                                $i = 1;
                             ?>
                            <?php foreach ($posts as $post): ?>
                                <!-- SERVICE -->
                                <a href="<?php echo $post->url(); ?>" class="cis__section__services__service js-observe fade-and-slide-animation section-link" data-uri="<?php echo page('servicios')->uri(); ?>" data-title="<?php echo page('servicios')->title(); ?>" data-image-link="<?php echo $i++; ?>">
                                    <ul class="cis__section__services__service__list">
                                        <li class="cis__section__services__service__left cis__section__services__service__list__item">
                                            <p class="cis__section__services__service--text"><?php echo $post->title(); ?></p>
                                            <p class="cis__section__services__service__list__item--small-text cis__section__services__service--text">
                                                <?php 

                                                    $post_type = $post->posttype();
                                                    if ($post_type == 'news') {
                                                        $post_type = 'Insight';
                                                    } else if($post_type=='post'){
                                                        $post_type = 'Post';
                                                    }
                                                    echo $post_type;
                                                ?> 
                                                <?php if($post->author()->isNotEmpty() && $post_author = $post->author()->toUser()): ?>: Por <?php echo $post_author->firstName(); ?> <?php echo $post_author->lastName(); ?>. <?php echo strftime('%B %e, %Y', strtotime($post->date())); ?><?php endif ?>
                                            </p>
                                        </li>
                                        <li class="cis__section__services__service__right">
                                            <div class="cis__arrow-cta--compressed">
                                                <span class="cis__arrow-cta__arrow">
                                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="19.81px"height="15.41px" viewBox="0 0 19.81 15.41" style="overflow:visible;enable-background:new 0 0 19.81 15.41;"xml:space="preserve"><path class="cis__arrow__st0" d="M9.96,2.19l3.01,3.01l1.66,0.8H0v3.33h14.55L13,10.1l-3.06,3.05l2.11,2.26l7.76-7.74 L12.15,0L9.96,2.19z"/> </svg>
                                                </span>
                                                <span class="cis__arrow-cta__ellipse">
                                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="80px"height="80px" viewBox="0 0 80 80" style="overflow:visible;enable-background:new 0 0 80 80;" xml:space="preserve"> <defs> </defs><circle class="cis__arrow-cta__ellipse__st0" cx="40" cy="40" r="40"/> <circle class="cis__arrow-cta__ellipse__st1" cx="40" cy="40" r="39"/> </svg>
                                                </span>
                                            </div>
                                        </li>
                                    </ul>
                                    <span class="cis__section__services__service__line"></span>
                                </a>
                                <!-- SERVICE -->
                            <?php endforeach ?>
                        </div>
                        <div class="is-col col-9_5 offset-2_5 --padding-80">
                            <!-- SECTION LINK -->
                            <a href="<?php echo page('thinking')->url(); ?>" class="js-observe fade-and-slide-animation cis__arrow-cta cis__arrow-cta--red section-link" data-uri="<?php echo page('thinking')->uri(); ?>" data-title="<?php echo page('thinking')->title(); ?>" data-text="Regresar a Thinking">
                                <span class="cis__arrow-cta__deco-bar"></span>
                                <span class="cis__arrow-cta__text">Regresar a Thinking</span>
                                <span class="cis__arrow-cta__arrow">
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="19.81px"height="15.41px" viewBox="0 0 19.81 15.41" style="overflow:visible;enable-background:new 0 0 19.81 15.41;"xml:space="preserve"><path class="cis__arrow__st0" d="M9.96,2.19l3.01,3.01l1.66,0.8H0v3.33h14.55L13,10.1l-3.06,3.05l2.11,2.26l7.76-7.74 L12.15,0L9.96,2.19z"/> </svg>
                                </span>
                                <span class="cis__arrow-cta__ellipse">
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="80px"height="80px" viewBox="0 0 80 80" style="overflow:visible;enable-background:new 0 0 80 80;" xml:space="preserve"> <defs> </defs><circle class="cis__arrow-cta__ellipse__st0" cx="40" cy="40" r="40"/> <circle class="cis__arrow-cta__ellipse__st1" cx="40" cy="40" r="39"/> </svg>
                                </span>
                            </a>
                            <!-- SECTION LINK -->
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </article>
    

<?php endif ?>

<?php if(!array_key_exists('HTTP_X_KIRBY_FETCH', $_SERVER)): ?>

    <?php snippet('footer')?>
    <?php snippet('footer.code')?>

<?php endif ?>