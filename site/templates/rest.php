<?php

	if(array_key_exists('HTTP_X_KIRBY_FETCH', $_SERVER)){
		setlocale(LC_TIME, 'es_MX.utf8');
		header('Content-type: application/json; charset=utf-8');

		$param = param('target');

		if(param('privacy')) {
			Cookie::forever('privacy', 'dismissed');
		}
		$json = array();
		if (isset($target) && $target) {
			$json = array(
				'target'     => $target,
				//'social'        => $target->social(),
			);
		}
		echo json_encode($json);
	}
?>