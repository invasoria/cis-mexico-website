<?php if(!array_key_exists('HTTP_X_KIRBY_FETCH', $_SERVER)): ?>

    <?php snippet('default.header'); ?>
    <?php snippet('default.content'); ?>

    <!-- CONTENT LOADING CONTAINER -->
    <main class="main fetch-container" id="fetch-container" data-id="main" data-root="<?php echo $site->url(); ?>" data-title="<?php echo $site->title(); ?>"></main>
    <!-- CONTENT LOADING CONTAINER -->

<?php endif ?>


<?php if(array_key_exists('HTTP_X_KIRBY_FETCH', $_SERVER)): ?>

    <article class="article" id="new-section" data-id="<?php echo $page->autoid(); ?>" data-template="<?php echo $page->intendedTemplate(); ?>" data-url="<?php echo $page->url(); ?>" data-uri="<?php echo htmlspecialchars($page->uri()); ?>" data-title="<?php echo htmlspecialchars($page->title()); ?>">

        <section class="--section">
            <div class="container">

                <!-- BLOCKS -->
                <?php snippet('blocks', ['page' => $page->blocks()->blocks()]) ?>
                <!-- BLOCKS -->

                

            </div>
        </section>


        <!-- MORE ARTICLES -->
        <section class="article__functions --section grid-margin --section-bg--light overflow-hidden">
            <div class="is-row is-flex column-padding">
                <div class="is-col col-11 offset-_5">
                    <!-- LAYOUT ROW -->
                    <div class="is-row is-flex">
                        <!-- COLUMN -->
                        <div class="is-col col-2_5">
                            <h3 class="js-observe reveal-text-animation --color-underline--yellow --color-hightlight--blue"><?php echo t('more-services'); ?></h3>
                        </div>
                        <div class="is-col col-9_5 relative">
                            <?php 
                                $posts = $page->parent()->children()->listed()->limit(5);
                                $i = 1;
                             ?>
                            <?php foreach ($posts as $post): ?>
                                <?php if($page->autoid() == $post->autoid()): ?>
                                <?php else: ?>
                                    <!-- SERVICE -->
                                    <a href="<?php echo $post->url(); ?>" class="cis__section__services__service js-observe fade-and-slide-animation section-link" data-uri="<?php echo page('servicios')->uri(); ?>" data-title="<?php echo page('servicios')->title(); ?>" data-image-link="<?php echo $i++; ?>">
                                        <ul class="cis__section__services__service__list">
                                            <li class="cis__section__services__service__left cis__section__services__service__list__item">
                                                <p class="cis__section__services__service--text"><?php echo $post->title(); ?></p>
                                                <p class="cis__section__services__service__list__item--small-text cis__section__services__service--text">
                                                    <?php echo $post->subtitle(); ?>
                                                </p>
                                            </li>
                                            <li class="cis__section__services__service__right">
                                                <div class="cis__arrow-cta--compressed">
                                                    <span class="cis__arrow-cta__arrow">
                                                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="19.81px"height="15.41px" viewBox="0 0 19.81 15.41" style="overflow:visible;enable-background:new 0 0 19.81 15.41;"xml:space="preserve"><path class="cis__arrow__st0" d="M9.96,2.19l3.01,3.01l1.66,0.8H0v3.33h14.55L13,10.1l-3.06,3.05l2.11,2.26l7.76-7.74 L12.15,0L9.96,2.19z"/> </svg>
                                                    </span>
                                                    <span class="cis__arrow-cta__ellipse">
                                                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="80px"height="80px" viewBox="0 0 80 80" style="overflow:visible;enable-background:new 0 0 80 80;" xml:space="preserve"> <defs> </defs><circle class="cis__arrow-cta__ellipse__st0" cx="40" cy="40" r="40"/> <circle class="cis__arrow-cta__ellipse__st1" cx="40" cy="40" r="39"/> </svg>
                                                    </span>
                                                </div>
                                            </li>
                                        </ul>
                                        <span class="cis__section__services__service__line"></span>
                                    </a>
                                    <!-- SERVICE -->
                                <?php endif ?>
                            <?php endforeach ?>
                        </div>
                        <div class="is-col col-9_5 offset-2_5 --padding-80">
                            <!-- SECTION LINK -->
                            <a href="<?php echo $page->parent()->url(); ?>" class="cis__arrow-cta cis__arrow-cta--red section-link" data-uri="<?php echo $page->parent()->uri(); ?>" data-title="<?php echo $page->parent()->title(); ?>" data-text="Regresar a <?php echo $page->parent()->title(); ?>">
                                <span class="cis__arrow-cta__deco-bar"></span>
                                <span class="cis__arrow-cta__text">Regresar a <?php echo $page->parent()->title(); ?></span>
                                <span class="cis__arrow-cta__arrow">
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="19.81px"height="15.41px" viewBox="0 0 19.81 15.41" style="overflow:visible;enable-background:new 0 0 19.81 15.41;"xml:space="preserve"><path class="cis__arrow__st0" d="M9.96,2.19l3.01,3.01l1.66,0.8H0v3.33h14.55L13,10.1l-3.06,3.05l2.11,2.26l7.76-7.74 L12.15,0L9.96,2.19z"/> </svg>
                                </span>
                                <span class="cis__arrow-cta__ellipse">
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="80px"height="80px" viewBox="0 0 80 80" style="overflow:visible;enable-background:new 0 0 80 80;" xml:space="preserve"> <defs> </defs><circle class="cis__arrow-cta__ellipse__st0" cx="40" cy="40" r="40"/> <circle class="cis__arrow-cta__ellipse__st1" cx="40" cy="40" r="39"/> </svg>
                                </span>
                            </a>
                            <!-- SECTION LINK -->
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </article>
    

<?php endif ?>

<?php if(!array_key_exists('HTTP_X_KIRBY_FETCH', $_SERVER)): ?>

    <?php snippet('footer')?>
    <?php snippet('footer.code')?>

<?php endif ?>