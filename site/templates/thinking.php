<?php if(!array_key_exists('HTTP_X_KIRBY_FETCH', $_SERVER)): ?>

    <?php snippet('default.header'); ?>
    <?php snippet('default.content'); ?>

    <!-- CONTENT LOADING CONTAINER -->
    <main class="main fetch-container" id="fetch-container" data-id="main" data-root="<?php echo $site->url(); ?>" data-title="<?php echo $site->title(); ?>"></main>
    <!-- CONTENT LOADING CONTAINER -->

<?php endif ?>


<?php if(array_key_exists('HTTP_X_KIRBY_FETCH', $_SERVER)): ?>

    <div class="page-builder" id="new-section" data-id="<?php echo $page->autoid(); ?>" data-template="<?php echo $page->intendedTemplate(); ?>" data-url="<?php echo $page->url(); ?>" data-uri="<?php echo htmlspecialchars($page->uri()); ?>" data-title="<?php echo htmlspecialchars($page->title()); ?>">
        
        <?php 
            $articles_count = $page->children()->listed()->count();
            $articles = $page->children()->listed()->sortBy('date','asc')->flip()->paginate(9);
         ?>
        <?php
            $param = param('page');
            if(!$param): 
        ?>
        <section class="--section grid-margin --section-bg--light overflow-hidden">
            <div class="is-row is-flex column-padding">
                <div class="is-col col-11 offset-_5">
                    <!-- LAYOUT ROW -->
                    <div class="is-row is-flex --padding-top-320 --padding-bottom-0 --padding-mobile-top-160 --padding-mobile-bottom-80">
                        <!-- COLUMN -->
                        <div class="is-col col-12">
                            <div class="is-row is-row--responsive is-flex">
                                <div class="is-col col-8 offset-2 md-col-8 md-offset-2 xs-col-12 xs-offset-0 --color-underline--coral --color-highlight--coral content__modular-block-padding">
                                    <h1 class="content__jumbo__h1 content__center content__no-top-margin content__no-bottom-margin js-observe reveal-block-animation"><?php echo $page->title(); ?></h1>
                                </div>
                                <div class="is-col col-4 offset-2 md-col-4 md-offset-2 xs-col-12 xs-offset-0 --color-underline--coral --color-highlight--coral content__modular-block-padding">
                                    <h5 class="content__h5 content__left content__no-top-margin content__no-bottom-margin content__text-uppercase js-observe fade-animation">INSIGHTS, CURIOSIDADES &amp; STORIES</h5>
                                </div>
                                <div class="is-col col-1_5 offset-1 md-col-1_5 md-offset-1 xs-col-12 xs-offset-0 --color-underline--coral --color-highlight--coral content__modular-block-padding">
                                    <h5 class="content__h5 content__left content__no-top-margin content__no-bottom-margin content__text-uppercase js-observe fade-animation"><?php echo $articles_count; ?> POSTS</h5>
                                </div>
                                <div class="is-col col-3 offset-0 md-col-3 md-offset-0 xs-col-12 xs-offset-0 --color-underline--coral --color-highlight--coral content__modular-block-padding">
                                    <?php setlocale(LC_TIME, 'es_MX.utf8'); ?>
                                    <h5 class="content__h5 content__left content__no-top-margin content__no-bottom-margin content__text-uppercase js-observe fade-animation"><?php echo strftime('%B', strtotime(date('d-m-Y'))); ?> 2021</h5>
                                </div>
                            </div>
                        </div>
                        <!-- COLUMN -->
                    </div>
                    <!-- LAYOUT ROW -->
                </div>
            </div>
        </section>
        <?php endif ?>

        <section class="--section grid-margin --section-bg--light overflow-hidden">    
            <div class="is-row is-flex --padding-top-80 --padding-bottom-0 --padding-mobile-top-40 --padding-mobile-bottom-0 column-padding">
                <div class="is-col col-11 offset-_5">

                    <!-- LAYOUT ROW -->
                    <div class="cis__articles is-row is-row--special-gap is-flex --padding-top-80  --padding-bottom-80 --padding-mobile-top-40 --padding-mobile-bottom-40">
                        <!-- <div class="is-col col-6 red-bg">...</div> -->
                        <?php foreach ($articles as $article): ?>
                        <!-- ARTICLE COLUMN -->
                        <?php if($article->posttype()->isNotEmpty() && $article->posttype() == 'news'): ?>
                            <?php snippet('modules/card.news', ['article' => $article, 'special_grid' => true ]); ?>
                        <?php elseif($article->posttype()->isNotEmpty() && $article->posttype() == 'page'): ?>
                            <?php snippet('modules/card.page', ['article' => $article, 'special_grid' => true ]); ?>
                        <?php elseif($article->posttype()->isNotEmpty() && $article->posttype() == 'post'): ?>
                            <?php snippet('modules/card.post', ['article' => $article, 'special_grid' => true ]); ?>
                        <?php endif ?>
                        <!-- ARTICLE COLUMN -->
                        <?php endforeach ?>
                    </div>
                  <!-- LAYOUT ROW -->

                </div>
            </div>

            <?php $pagination = $articles->pagination() ?>
            <!-- PAGINATION -->
            <div class="cis__articles__pagination">
                <div class="is-row is-flex">
                    <div class="is-col col-11 offset-_5">
                        <nav class="container">
                            <ul class="is-row is-row--responsive is-flex column-padding cis__articles__pagination__list">
                                <?php if ($pagination->hasPrevPage()): ?>
                                    <li class="cis__articles__pagination__list__item">
                                        <h2>
                                            <a href="<?= $pagination->prevPageURL() ?>" class="section-link" data-uri="" data-title=""><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="16" viewBox="0 0 16 16"><path d="M16 7H3.83l5.59-5.59L8 0 0 8l8 8 1.41-1.41L3.83 9H16z" fill="currentColor"></path></svg> Más nuevos</a>
                                        </h2>
                                    </li>
                                <?php else: ?>
                                    <li class="cis__articles__pagination__list__item cis__articles__pagination__list__item--inactive">
                                        <h2>Inicio de Lista</h2>
                                    </li>
                                <?php endif ?>

                                <li class="cis__articles__pagination__list__item cis__articles__pagination__list__item--range">
                                    <ul class="cis__articles__pagination__list cis__articles__pagination__list--center">
                                        <?php foreach ($pagination->range(10) as $r): ?>
                                        <li class="cis__articles__pagination__list__item<?= $pagination->page() === $r ? ' cis__articles__pagination__list__item--active' : '' ?>">
                                            <h2>
                                                <a href="<?= $pagination->pageURL($r) ?>" class="section-link" data-uri="" data-title=""<?= $pagination->page() === $r ? ' aria-current="page"' : '' ?>><?php echo $r ?></a>
                                            </h2>
                                        </li>
                                        <?php endforeach ?>
                                    </ul>
                                </li>

                                <?php if ($pagination->hasNextPage()): ?>
                                    <li class="cis__articles__pagination__list__item">
                                        <h2>
                                            <a href="<?= $pagination->nextPageURL() ?>" class="section-link" data-uri="" data-title="">Anteriores <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="16" viewBox="0 0 16 16"><path d="M8 0L6.59 1.41 12.17 7H0v2h12.17l-5.58 5.59L8 16l8-8z" fill="currentColor"></path></svg></a>
                                        </h2>
                                    </li>
                                <?php else: ?>
                                    <li class="cis__articles__pagination__list__item cis__articles__pagination__list__item--inactive">
                                        <h2>Fin de lista</h2>
                                    </li>
                                <?php endif ?>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
            <!-- PAGINATION -->
        </section>

            
        <!-- SERVICES BLOCK -->
        <?php snippet('modules/cta.block', ['source' => $site]); ?>
        <!-- SERVICES BLOCK -->

    </div>
    

<?php endif ?>

<?php if(!array_key_exists('HTTP_X_KIRBY_FETCH', $_SERVER)): ?>

    <?php snippet('footer')?>
    <?php snippet('footer.code')?>

<?php endif ?>